#!/bin/bash

echo "ulimit =" $(ulimit -n)
sysctl net.core.somaxconn
sysctl net.ipv4.tcp_max_syn_backlog
sysctl vm.max_map_count
sysctl net.ipv4.tcp_tw_reuse
sysctl net.ipv4.ip_local_port_range
taskset -c 0-15,32-47 weighttp -n 5000000 -c 5000 -t 32 http://127.0.0.1:8080/index.html

