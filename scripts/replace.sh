# use this script to edit output.txt produced by microbenchmarks to filter out noisy configurations
path=output.txt
target=0
echo "config_$target --> config_x"
sed -i "s/config_$target(/config_x(/g" "${path}"
sed -i "s/config_$target\//config_x\//g" "${path}"

start=$(($target+1))
end=17
for configNumber in $(seq $start $end); do
  newConfigNumber=$(($configNumber-1))
  echo "$configNumber --> $newConfigNumber"
  sed -i "s/config_$configNumber(/config_$newConfigNumber(/g" "${path}"
  sed -i "s/config_$configNumber\//config_$newConfigNumber\//g" "${path}"
done
