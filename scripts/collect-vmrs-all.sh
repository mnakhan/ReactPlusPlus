#!/bin/bash

# A configuration file (config.ini) must be present in the same directory where this script is located.
# Core assignments are read from that file.

exePath=../tests/dist/release/fork-join-spawn
frameworkName=react++

#launch params
nIterations=20
nThreads=32
nActors=1000000
frequency=120    # sampling frequency in Hz
coreConfig='0-31'

# constants
rootDir=vmrs
outputFile='output.txt'
subscript=collect-vmrs-single.sh

function main() {  
  if [ ! -f "${exePath}" ]; then
    echo "could not locate executable ${exePath}"
    exit
  fi
  
  outputDir="${rootDir}/${frameworkName}"
  exeName=$(basename ${exePath})
  mkdir -p ${outputDir}
  outputPath="${outputDir}/${outputFile}"
  printf '' > ${outputPath}

  for i in $(seq 0 $(($nIterations-1))); do
    echo "mem-test/run=$i" >> ${outputPath}
    ./${subscript} ${exeName} ${frequency} | tee -a ${outputPath} &
    taskset -c ${coreConfig} ./${exePath} ${nThreads} ${nActors}
    #sleep 1
    pgrep ${subscript} | xargs kill -SIGKILL > /dev/null 2>&1
  done
  echo "output written to ${outputPath}"
}

main "$@"
