#!/bin/bash

SERVER_EXE_PATH='../dist/release/zmqs'
SERVER_EXE=$(basename ${SERVER_EXE_PATH})

CLIENT_EXE_PATH='../../ZMQC/dist/release/zmqc'

IP_ADDRESS='127.0.0.1'
PORT='8080'
SERVER_INSTANCES=2
 
OUTPUT_ROOT='zmq-collector'
OUTPUT_FILE='output.txt'

#MODE=DEBUG
PKG_MODE=AUTO
EXPORT_DIR="$HOME/tmp"

function main() {
  if [ "$MODE" = "DEBUG" ]; then
    N_SAMPLES=2
    DURATION=5
    SERVER_MIN_CORE=2
    SERVER_MAX_CORE=4
    SERVER_CPU_OFFSET=0
    CLIENT_CORES='4-7'
    N_CLIENTS=32  
  else
    N_SAMPLES=20
    DURATION=30
    SERVER_MIN_CORE=2
    SERVER_MAX_CORE=16
    SERVER_CPU_OFFSET=16
    CLIENT_CORES='0-15,32-47'
    N_CLIENTS=1024
  fi

  setNConfig
  
  for messageSize in 1024 8192 65536; do
  #for messageSize in 1024 8192; do
    executeBenchmark $messageSize 0
    executeBenchmark $messageSize 1
  done
  
  if [ "$PKG_MODE" == AUTO ]; then
    archive="${OUTPUT_ROOT}.zip"
    rm -f "${archive}"
    zip -r "${archive}" "${OUTPUT_ROOT}"
    mv "${archive}" "${EXPORT_DIR}"
    exit
  fi
}

function setNConfig() {
  i=0
  for corecount in 1 $(seq $SERVER_MIN_CORE 2 $SERVER_MAX_CORE); do
    i=$(($i+1))
  done
  N_CONFIGS=$i
}

# launchServer <number of I/O threads> <message size> <use actors>
function launchServer() {
  launchCMD="taskset -c ${SERVER_CPU_LIST} ${SERVER_EXE_PATH} ${PORT} ${SERVER_INSTANCES} $1 $2 $3"
  echo "${launchCMD}"
  ${launchCMD} &
  sleep 1
}

# launchServer <message size> <use actors>
function executeBenchmark() {
  messageSize=$1
  useActors=$2
  
  benchmarkName=$messageSize
  
  if [ "$useActors" == "0" ]; then
    frameworkName='zmq-native'
  else
    frameworkName='zmq-actors'
  fi
  
  configNumber=0
  benchmarkCMD="taskset -c $CLIENT_CORES ${CLIENT_EXE_PATH} ${IP_ADDRESS} ${PORT} ${SERVER_INSTANCES} ${N_CLIENTS} ${messageSize} ${DURATION}"

  outputDirectory="$OUTPUT_ROOT/$messageSize/$frameworkName"
  mkdir -p $outputDirectory
  output="$outputDirectory/$OUTPUT_FILE"
  echo -e "running benchmark with message size = $benchmarkName\n------------------------------------------"
  echo -e "output file --> $output\n"
  echo "nConfigs=$N_CONFIGS" > $output
  
  for corecount in 1 $(seq $SERVER_MIN_CORE 2 $SERVER_MAX_CORE); do
    echo "config_$configNumber(nServerCores=$corecount,nClientSamples=$N_SAMPLES)" | tee -a "$output"
    # generate server cpu list and launch server here
    generateCoreConfig ${SERVER_CPU_OFFSET} ${corecount}
      
    for (( sample=0; sample<$N_SAMPLES; sample++ )); do
      launchServer ${corecount} ${messageSize} ${useActors}
      echo "config_$configNumber/sample_$sample:" | tee -a "$output"
      echo "${benchmarkCMD}"
      ${benchmarkCMD} | tee -a "$output"
      nuke.sh ${SERVER_EXE}
      sleep 1
    done
    configNumber=$(($configNumber+1))
  done
  echo ''
}

function generateCoreConfig() {
  offset=$1
  corecount=$2
  begin=$offset
  end=$(($offset+$corecount-1))
  if [ "$begin" = "$end" ]; then
    SERVER_CPU_LIST=$begin
  else
    SERVER_CPU_LIST="$begin-$end"
  fi
}

main "$@"
