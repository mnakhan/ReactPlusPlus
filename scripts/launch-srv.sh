#!/bin/bash

# usage
# sudo ./launch-srv.sh '0-3' ../conf/webserver.ini
# sudo ./launch-srv.sh '16-31' ../conf/webserver.kosi32.ini

EXE=../tests/dist/release/rppws
CORES=$1
CONFIG_PATH=$2

if [ -z "$CORES" ] || [ -z "$CONFIG_PATH" ]; then
  echo "Usage: $0 <CPU mask> <configuration file>" 
  exit
fi

echo "ulimit =" $(ulimit -n)
sysctl net.core.somaxconn
sysctl net.ipv4.tcp_max_syn_backlog
sysctl vm.max_map_count
sysctl net.ipv4.tcp_tw_reuse
sysctl net.ipv4.ip_local_port_range
sudo taskset -c ${CORES} ${EXE} ${CONFIG_PATH}
