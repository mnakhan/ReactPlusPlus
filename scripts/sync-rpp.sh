#!/bin/bash

PSRC=~/Documents/Links/EclipseProjects/ReactPlusPlus
PDST=~/Documents/projects/webserver-kosi-tests/ReactPlusPlus

rm -rf $PDST/core/include $PDST/core/src $PDST/tests/include $PDST/tests/src
mkdir -p $PDST/core/include $PDST/core/src $PDST/tests/include $PDST/tests/src

cp $PSRC/include/*.hpp $PDST/core/include
cp $PSRC/src/*.cpp $PDST/core/src

extFiles=(atom driver examples matrix message_types profiling stage test_suite_base unit_tests user_statistics)
for i in ${extFiles[@]}; do
  target=$PDST/core/include/$i.hpp
  [ -f $target ] && mv $target $PDST/tests/include
  target=$PDST/core/src/$i.cpp
  [ -f $target ] && mv $target $PDST/tests/src
done

webserverFiles=(cluster_attribute_set garage picohttpparser self_pipe server server_io_policy session signaler tcp_listener time_stamp)
for i in ${webserverFiles[@]}; do
  target=$PDST/core/include/$i.hpp
  [ -f $target ] && mv $target $PDST/tests/include
  target=$PDST/core/src/$i.cpp
  [ -f $target ] && mv $target $PDST/tests/src
done

benchmarks=(rppws big counting-actor dining-philosophers fork-join-spawn fork-join-throughput latency lpexp prodcon quicksort quicksort2 thread-ring)
for i in ${benchmarks[@]}; do
  target=$PDST/core/src/$i.cpp
  echo "moving benchmark --> $target"
  [ -f $target ] && mv $target $PDST/tests/src
done

echo -e "Index: Library\n----------------"
cd $PDST/core/include
echo $(ls | tr '\n' ' ')
echo
cd $PDST/core/src
echo $(ls | sed 's/cpp$/o/' | tr '\n' ' ')

echo -e "\nIndex: Tests\n----------------"
cd $PDST/tests/include
echo $(ls | tr '\n' ' ')
echo
cd $PDST/tests/src
echo $(ls | sed 's/cpp$/o/' | tr '\n' ' ')



cp $PDST/benchmarks/* $PDST/tests/src
