#!/bin/bash

MODE=AUTO
EXPORT_DIR="$HOME/tmp"
framework=react++
# relative to script location, which is ActorFramework/scripts
binPath=../tests/dist/release
datadir='./collector'
statsOutput="${datadir}/stats-${framework}.txt"
mkdir -p "${datadir}"
touch "${statsOutput}"
echo "framework under test: $framework" > $statsOutput
echo "total running time for each benchmark" >> $statsOutput

for benchmark in 'prodcon' 'latency' 'counting-actor' 'fork-join-throughput' 'fork-join-spawn' 'big' 'thread-ring' 'quicksort' 'quicksort2' 'lpexp'; do
#for benchmark in 'quicksort2' 'lpexp'; do
  echo "benchmark=$benchmark" >> $statsOutput
  start=$(date +%s.%N)
  ./collect-single.sh $benchmark $framework "${binPath}/${benchmark}"
  end=$(date +%s.%N)
  echo $(python -c "print(${end} - ${start})") seconds >> $statsOutput
done

if [ "$MODE" == AUTO ]; then
  archive="${datadir}.zip"
  rm -f "${archive}"
  zip -r "${archive}" "${datadir}"
  mv "${archive}" "${EXPORT_DIR}"
  exit
fi
