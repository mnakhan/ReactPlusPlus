#!/bin/bash

GENSCRIPT=generate-single-config.sh

for i in {1..4}; do
  ./$GENSCRIPT kosi32 $i
done

for i in {1..8}; do
  ./$GENSCRIPT kosa64 $i
done

for i in {1..2}; do
  ./$GENSCRIPT local $i
done
