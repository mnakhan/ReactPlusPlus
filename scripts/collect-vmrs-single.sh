#!/bin/bash

# Run as ./memory.sh <name of executable>
function main() {
  frequency=$2
  sleepInterval=$(python -c "print(1.0 / ${frequency})")
  # if process does not start within max intervals, exit this script because the process is likely to have started and exited already because of incorrect cmd args
  maxIntervals=$((${frequency}*5))
  isFirst=true
  counter=0
  while :
  do
    pid=$(pgrep "${1}")
    if [ -z $pid ]; then
      if [ "$isFirst" = true ]; then
        counter=$(($counter+1))     
        if [ $counter -gt $maxIntervals ]; then
          echo "Process "${1}" is taking too long to start. Exiting memory profiler."
          exit
        fi
        sleep ${sleepInterval}s
        continue
      fi
      exit
    fi
    
    isFirst=false
    stat=$(cat /proc/$pid/statm 2> /dev/null)
    size=$(echo $stat | cut -d ' ' -f1)
    residentSet=$(echo $stat | cut -d ' ' -f2)
    if [ -z $size ] || [ -z $residentSet ] || [ $size -eq 0 ] || [ $residentSet -eq 0 ]; then
      exit
    fi
    timestamp=$(date +"%Y-%m-%d %H:%M:%S.%3N")
    echo "${timestamp}/VIRTUAL_MEM=${size}x4KB,RESIDENT_SET=${residentSet}x4KB"
    sleep ${sleepInterval}s
  done
}

main "$@"
