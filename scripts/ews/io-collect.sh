#!/bin/bash

EXE_PATH='../dist/release/ews'
EXE=$(basename ${EXE_PATH})

IP_ADDRESS='127.0.0.1'
PORT='8080'

OUTPUT_ROOT='io-collector'
OUTPUT_FILE='output.txt'
FRAMEWORK='ews'

#SERVER=kosi32
SERVER=kosa64

PKG_MODE=AUTO
EXPORT_DIR="$HOME/tmp"

SERVER_MIN_CORE=2
SERVER_MAX_CORE=4
SERVER_CPU_OFFSET=0
CLIENT_CORES='4-7'

WRK_DURATION=5
N_CLUSTERS=2
N_SAMPLES=1

function main() {

  if [ "$SERVER" = "kosi32" ]; then
    SERVER_MIN_CORE=4
    SERVER_MAX_CORE=16
    SERVER_CPU_OFFSET=16
    CLIENT_CORES='0-15,32-47'
  elif [ "$SERVER" = "kosa64" ]; then  
    SERVER_MIN_CORE=4
    SERVER_MAX_CORE=32
    SERVER_CPU_OFFSET=0
    CLIENT_CORES='32-63'
  fi

  if [ "$SERVER" = "kosi32" ] || [ "$SERVER" = "kosa64" ]; then
    WRK_DURATION=60
    N_CLUSTERS=4
    N_SAMPLES=20
  fi

  setNConfig

  for benchmark in 'wrk' 'weighhttp'; do
    executeBenchmark $benchmark
  done
  
  if [ "$PKG_MODE" == AUTO ]; then
    archive="${OUTPUT_ROOT}.zip"
    rm -f "${archive}"
    zip -r "${archive}" "${OUTPUT_ROOT}"
    mv "${archive}" "${EXPORT_DIR}"
    exit
  fi
}

function launchServer() {
  workerPerCluster=$1
  LAUNCH_CMD="taskset -c ${SERVER_CPU_LIST} ${EXE_PATH} ${IP_ADDRESS} ${PORT} ${N_CLUSTERS} ${workerPerCluster}"
  echo "${LAUNCH_CMD}"
  sudo ${LAUNCH_CMD} &
  sleep 3
}

function executeBenchmark() {
  benchmarkName=$1

  if [ "$benchmarkName" = "wrk" ]; then
    if [ "$SERVER" = "kosi32" ] || [ "$SERVER" = "kosa64" ]; then
      BENCHMARK_CMD="taskset -c $CLIENT_CORES wrk --timeout 1s --latency -d${WRK_DURATION}s -t 32 -c 15000 http://${IP_ADDRESS}:${PORT}/index.html" 
    else
      BENCHMARK_CMD="taskset -c $CLIENT_CORES wrk --timeout 1s --latency -d${WRK_DURATION}s -t 4 -c 400 http://${IP_ADDRESS}:${PORT}/index.html"
    fi
  elif [ "$benchmarkName" = "weighhttp" ]; then
    if [ "$SERVER" = "kosi32" ] || [ "$SERVER" = "kosa64" ]; then
      BENCHMARK_CMD="taskset -c $CLIENT_CORES weighttp -n 5000000 -c 5000 -t 32 http://${IP_ADDRESS}:${PORT}/index.html"
    else
      BENCHMARK_CMD="taskset -c $CLIENT_CORES weighttp -n 1000 -c 400 -t 4 http://${IP_ADDRESS}:${PORT}/index.html"
    fi
  else
    echo "could not run unknown benchmark $benchmarkName"
    exit
  fi

  OUTPUT_DIR="$OUTPUT_ROOT/$benchmarkName/$FRAMEWORK"
  mkdir -p $OUTPUT_DIR
  OUTPUT="$OUTPUT_DIR/$OUTPUT_FILE"
  echo -e "running benchmark $benchmarkName\n---------------------------"
  echo -e "output file --> $OUTPUT\n"  

  echo "nConfigs=$N_CONFIGS" > "$OUTPUT"
  configNumber=0
  workerPerCluster=1
  
  for corecount in $(seq $SERVER_MIN_CORE $N_CLUSTERS $SERVER_MAX_CORE); do
    echo "config_$configNumber(nServerCores=$corecount,nClientSamples=$N_SAMPLES)" | tee -a "$OUTPUT"   
    generateCoreConfig ${SERVER_CPU_OFFSET} ${corecount}
    launchServer ${workerPerCluster}
     
    for (( sample=0; sample<$N_SAMPLES; sample++ )); do
      echo "config_$configNumber/sample_$sample:" | tee -a "$OUTPUT"
      echo "${BENCHMARK_CMD}"
      sudo ${BENCHMARK_CMD} | tee -a "$OUTPUT"
      #${BENCHMARK_CMD} | tee -a "$OUTPUT"
      sleep 3
    done
    
    configNumber=$(($configNumber+1))
    workerPerCluster=$(($workerPerCluster+1))
    nuke.sh ${EXE} 
  done
}


function setNConfig() {
  i=0
  for corecount in $(seq $SERVER_MIN_CORE $N_CLUSTERS $SERVER_MAX_CORE); do
    i=$(($i+1))
  done
  N_CONFIGS=$i
}

function generateCoreConfig() {
  offset=$1
  corecount=$2
  begin=$offset
  end=$(($offset+$corecount-1))
  if [ "$begin" = "$end" ]; then
    SERVER_CPU_LIST=$begin
  else
    SERVER_CPU_LIST="$begin-$end"
  fi
}

main "$@"
