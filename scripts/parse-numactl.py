'''
Parse output from 'numactl -H' command on Linux and generates configuration for React++.
'''

import os
import sys
import numpy as np
from sys import exit
import getopt

class Range:
  def __init__(self):
    self.start, self.end, self.expected, self.isCounting  = 0, 0, 0, False

  def initialize(self, value):
    self.start, self.end, self.isCounting = value, 0, True
    self.expected = self.start + 1

  def advance(self, value):
    if not self.isCounting: return False

    if value == self.expected:
      self.expected += 1
      return True
    else:
      self.end = self.expected - 1
      self.isCounting = False
      return False

  def __str__(self):
    if self.start != self.end: return '{}-{}'.format(self.start, self.end)
    else: return str(self.start)

class Configuration:
  def __init__(self):
    self.numberOfNodes, self.cpuAssignments, self.distanceMatrix = 0, {}, None

  def validate(self):
    for r in range(0, self.numberOfNodes):
      for c in range(0, self.numberOfNodes):
        if self.distanceMatrix[r, c] != self.distanceMatrix[c, r]: exit('Distance from node {} to node {} is not equal to distance from node {} to node {}.\n{} --> {} = {}\n{} --> {} = {}'.format(r, c, c, r, r, c, self.distanceMatrix[r, c], c, r, self.distanceMatrix[c, r]))

  def generate(self):
    result = '[NUMA]\nNodeCount={}\n'.format(self.numberOfNodes)
    for i in range(0, self.numberOfNodes): result += 'Node{}={}\n'.format(i, self.cpuAssignments[i])
    result += '\n[NUMA Distances]\n'
    for r in range(0, self.numberOfNodes):
      for c in range(0, self.numberOfNodes):
        result += '{},{}={}'.format(r, c, self.distanceMatrix[r, c])
        if r < self.numberOfNodes - 1 or c < self.numberOfNodes - 1: result += '\n'
    return result

  @staticmethod
  def parseCPUAssignment(input):
    tokens = input.strip().split(' ')
    tokens = list(filter(lambda x: len(x) > 0, tokens))
    tokens = list(map(lambda x: int(x), tokens))
    tokens.sort()

    result = ''
    n = len(tokens)
    rng = Range()
    for i in range(0, n):
      value = tokens[i]
      if not rng.isCounting: rng.initialize(value)
      else:
        ret = rng.advance(value)
        if not ret:
          result += str(rng) + ','
          rng.initialize(value)

    if rng.isCounting:
      rng.advance(-1)
      result += str(rng)

    return result

def main():
  argc = len(sys.argv)
  if argc < 2: exit("Usage: {} <input file>".format(os.path.basename(sys.argv[0])))
  path = sys.argv[1]
  if not os.path.exists(path): exit('could not file input file --> {}'.format(path))
  config = Configuration()

  with open(path, 'r') as fileHandle:
    lineNumber = 1
    for line in fileHandle:
      line = line.strip()
      if line.startswith('available'):
        idx1 = line.find(':')
        if idx1 == -1: exit('bad input in line {} --> {}'.format(lineNumber, line))
        idx2 = line.find('node')
        if idx1 == -1: exit('bad input in line {} --> {}'.format(lineNumber, line))
        value = line[idx1+1:idx2].strip()
        try:
          config.numberOfNodes = int(value)
          config.distanceMatrix = np.zeros((config.numberOfNodes, config.numberOfNodes), dtype=int)
        except: exit('could not parse value {} in line {} --> {}'.format(value, lineNumber, line))
      elif line.startswith('node'):
        idx = line.find('cpu')
        if idx == -1:
          lineNumber += 1
          continue
        try:
          value = line[4:idx].strip()
          nodeNumber = int(value)
          value = line[idx+5:].strip()
          config.cpuAssignments[nodeNumber] = Configuration.parseCPUAssignment(value)
        except: exit('could not parse value {} in line {} --> {}'.format(value, lineNumber, line))
      else:
        idx = line.find(':')
        if idx == -1:
          lineNumber += 1
          continue
        prefix = line[0:idx]
        if not prefix.isnumeric():
          lineNumber += 1
          continue
        nodeNumber = int(prefix)
        distances = line[idx+1:].strip().split(' ')
        distances = list(filter(lambda x: len(x) > 0, distances))
        if len(distances) != config.numberOfNodes: exit('too few/too many values in line {} --> {}. Expecting exactly {} distances because there are {} nodes.'.format(lineNumber, line, config.numberOfNodes, config.numberOfNodes))
        for i in range(0, config.numberOfNodes): config.distanceMatrix[nodeNumber, i] = distances[i]
      lineNumber += 1

  config.validate()
  print(config.generate())
if __name__ == "__main__":
  main()