#!/bin/bash

# Usage: ./collect <benchmark> <framework> <path-to-executable>, where
# 'benchmark' is the name of the benchmark.
# 'framework' is the name of the framework under experiment.
# 'path-to-executable' is the path to the executable.
# Collected data is written to ~/OUTPUT_DIR/benchmark/framework/output.txt.

# NOTE:
# If the output files fail to capture any data from perf, use the launcher with sudo.
# sudo ./collect-all.sh

# Number of times an executable is run for each CPU configuration.
N_SAMPLES=20

# Run the benchmark with each CPU configuration between MIN_CORE and MAX_CORE with step 2.
MIN_CORE=2
MAX_CORE=32

OUTPUT_DIR='collector'
OUTPUT_FILE='output.txt'
#MODE=DEBUG
MODE=RELEASE
CONFIG_PATH="../conf/config.ini"

CURRENT_BENCHMARK=
FRAMEWORK=
EXE_PATH=
BENCHMARK_CMD=
N_CONFIGS=
counters=task-clock,context-switches,cpu-migrations,page-faults,cycles,stalled-cycles-frontend,stalled-cycles-backend,instructions,branches,branch-misses
filters='task-clock\|context-switches\|cpu-migrations\|page-faults\|cycles\|stalled-cycles-frontend\|stalled-cycles-backend\|instructions\|branches\|branch-misses\|seconds time elapsed'

# Set parameters for each benchmark here (DEBUG).
function setParameters_debug(){
  corecount=$1
  if [ "$CURRENT_BENCHMARK" == 'prodcon' ]; then
    nMessagesPerProducer=1
    nProducers=1
    nConsumers=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nMessagesPerProducer $nProducers $nConsumers"
  elif [ "$CURRENT_BENCHMARK" == 'latency' ]; then
    nIntialValue=1
    senderAffinity='1.0'
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nIntialValue $senderAffinity"
  elif [ "$CURRENT_BENCHMARK" == 'counting-actor' ]; then
    nMessages=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nMessages"
  elif [ "$CURRENT_BENCHMARK" == 'fork-join-throughput' ]; then
    nActors=1
    nMessagesPerActor=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors $nMessagesPerActor"
  elif [ "$CURRENT_BENCHMARK" == 'fork-join-spawn' ]; then
    nActors=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors"
  elif [ "$CURRENT_BENCHMARK" == 'big' ]; then
    nActors=1
    nMessages=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors $nMessages"
  elif [ "$CURRENT_BENCHMARK" == 'thread-ring' ]; then
    nActors=1
    initialValue=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors $initialValue"
  elif [ "$CURRENT_BENCHMARK" == 'quicksort' ]; then
    lstSize=1
    threshold=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $lstSize $threshold" 
  elif [ "$CURRENT_BENCHMARK" == 'quicksort2' ]; then
    arrSize=1
    depth=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $arrSize $depth"
  elif [ "$CURRENT_BENCHMARK" == 'lpexp' ]; then
    dimension=1
    maxTreeDepth=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $dimension $maxTreeDepth"
  else
    exit 1
  fi
}

# Set parameters for each benchmark here (RELEASE).
function setParameters_release(){
  corecount=$1
  if [ "$CURRENT_BENCHMARK" == 'prodcon' ]; then
    nMessagesPerProducer=1000000
    nProducers=32
    nConsumers=1
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nMessagesPerProducer $nProducers $nConsumers"
  elif [ "$CURRENT_BENCHMARK" == 'latency' ]; then
    nIntialValue=10000000
    senderAffinity='1.0'
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nIntialValue $senderAffinity"
  elif [ "$CURRENT_BENCHMARK" == 'counting-actor' ]; then
    nMessages=10000000
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nMessages"
  elif [ "$CURRENT_BENCHMARK" == 'fork-join-throughput' ]; then
    nActors=1000
    nMessagesPerActor=10000
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors $nMessagesPerActor"
  elif [ "$CURRENT_BENCHMARK" == 'fork-join-spawn' ]; then
    nActors=1000000
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors"
  elif [ "$CURRENT_BENCHMARK" == 'big' ]; then
    nActors=500
    nMessages=50000
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors $nMessages"
  elif [ "$CURRENT_BENCHMARK" == 'thread-ring' ]; then
    nActors=1000
    initialValue=10000000
    BENCHMARK_CMD="./${EXE_PATH} $corecount $nActors $initialValue"
  elif [ "$CURRENT_BENCHMARK" == 'quicksort' ]; then
    lstSize=10000000
    threshold=100000
    BENCHMARK_CMD="./${EXE_PATH} $corecount $lstSize $threshold" 
  elif [ "$CURRENT_BENCHMARK" == 'quicksort2' ]; then
    arrSize=500000000
    depth=5
    BENCHMARK_CMD="./${EXE_PATH} $corecount $arrSize $depth"
  elif [ "$CURRENT_BENCHMARK" == 'lpexp' ]; then
    dimension=12
    maxTreeDepth=8
    BENCHMARK_CMD="./${EXE_PATH} $corecount $dimension $maxTreeDepth"    
  else
    exit 1
  fi
}

function main() {
  if [ $# -lt 3 ]; then
    usage $0
    exit 1
  fi
  
  selectBenchmark $1
  FRAMEWORK=$2
  EXE_PATH=$3
  
  if [ ! -f "${EXE_PATH}" ]; then
    echo "Target executable ${EXE_PATH} not found."
    exit 1
  fi
  
  OUTPUT_DIR="${OUTPUT_DIR}/${CURRENT_BENCHMARK}/${FRAMEWORK}"
  mkdir -p "${OUTPUT_DIR}"
  if [ ! -d "${OUTPUT_DIR}" ]; then
    echo "Error creating output dir ${OUTPUT_DIR}."
    exit 1
  fi
  
  OUTPUT_FILE="${OUTPUT_DIR}/${OUTPUT_FILE}"
  rm -f "${OUTPUT_FILE}"
  touch "${OUTPUT_FILE}"
  if [ ! -f "${OUTPUT_FILE}" ]; then
    echo "Error creating output file ${OUTPUT_FILE}."
    exit 1
  fi
  setNConfig
  executeBenchmark
}

function executeBenchmark(){
  echo "nConfigs=$N_CONFIGS" >> "${OUTPUT_FILE}"
  configNumber=0
  for corecount in 1 $(seq $MIN_CORE 2 $MAX_CORE); do
    echo "config_$configNumber(nCores=$corecount,nSamples=$N_SAMPLES)" | tee -a "${OUTPUT_FILE}"
    for (( sample=0; sample<$N_SAMPLES; sample++ )); do
      echo "config_$configNumber/sample_$sample:" | tee -a "${OUTPUT_FILE}"
      cpulist=0-$(($corecount-1))
      if [ "$MODE" == DEBUG ]; then
        setParameters_debug $corecount
      elif [ "$MODE" == RELEASE ]; then
        setParameters_release $corecount
      else
        echo "FATAL: Unknown configuration."
        exit 1
      fi
      sed -i "s/Cluster0=.*/Cluster0=${cpulist}/g" ${CONFIG_PATH}
      echo "${BENCHMARK_CMD}"
      (perf stat -e $counters taskset -c $cpulist ${BENCHMARK_CMD}) 2>&1 | grep "$filters" >> "${OUTPUT_FILE}"
    done
    configNumber=$(($configNumber+1))
  done
}

function selectBenchmark() {
  CURRENT_BENCHMARK=$1
  for i in 'prodcon' 'latency' 'counting-actor' 'fork-join-throughput' 'fork-join-spawn' 'big' 'thread-ring' 'quicksort' 'quicksort2' 'lpexp'; do
    if [ "$i" == "$1" ]; then
      CURRENT_BENCHMARK=$1
      return
    fi
  done
  
  echo "Unknown benchmark: $1"
  exit 1
}

usage() {
  echo "$0 <benchmark> <framework> <path to executable>"
}

function setNConfig() {
  i=0
  for corecount in 1 $(seq $MIN_CORE 2 $MAX_CORE); do
    i=$(($i+1))
  done
  N_CONFIGS=$i
}

main "$@"
