import io_common as com

def main():
  maxCores=[16, 32]
  for i in maxCores:
    Global.initialize(i)
    useLatex = False
    wrk(useLatex)
    weighhttp(useLatex)

class Global:
  frameworksLabels = {
    'rppws-dp': 'React++ (distributed)',
    'rppws-cp': 'React++ (centralized)',
    'ulib': 'ULIB',
    'ews': 'Event-driven Prototype'
  }

  frameworkColors = {
    'rppws-dp': 'red',
    'rppws-cp': 'violet',
    'ulib': 'blue',
    'ews': 'green'
  }

  prefix = ''

  @staticmethod
  def initialize(maxCores):
    if maxCores == 16:
      com.Benchmark.root = 'io-collector.kosi32'
    elif maxCores == 32:
      com.Benchmark.root = 'io-collector.kosa64'
    else: exit('FATAL: Max cores must be 16 or 32. Given value was {}.'.format(maxCores))

    Global.suffix = str(maxCores)
    com.Benchmark.fileName = 'output.txt'
    com.Settings.traceEnabled = False

def plotThroughput(name, title, tag, useLatex):
  benchmark = com.Benchmark(name, title, Global.frameworksLabels, Global.frameworkColors)
  xLabel = 'Core Count'
  yLabel = 'Requests/sec x 1000'
  factor = 1.0 / 1000
  if useLatex: benchmark.toLatex(xLabel, yLabel, tag, '5.0', factor)
  else: benchmark.plot(Global.suffix, xLabel, yLabel, tag, factor)

def wrk(useLatex):
  name = 'wrk'
  title = 'Plaintext Throughput (wrk)'
  tag = com.Tag.WRK
  plotThroughput(name, title, tag, useLatex)

def weighhttp(useLatex):
  name = 'weighhttp'
  title = 'Plaintext Throughput (weighhttp)'
  tag = com.Tag.WEIGHHTTP
  plotThroughput(name, title, tag, useLatex)

if __name__ == "__main__":
  main()