import sys
import cpu_common as com

def main():
  Global.initialize()
  useLatex = False
  plotAll(useLatex)

class Global:
  frameworksLabels = {
    'react++': 'React++',
    'caf': 'CAF',
    'proto.actor': 'Proto.Actor',
    'akka': 'Akka'
  }

  frameworkColors = {
    'react++': 'red',
    'caf': 'blue',
    'proto.actor': 'green',
    'akka': 'purple'
  }

  @staticmethod
  def initialize():
    com.Benchmark.root = 'cpu-collector'
    com.Benchmark.fileName = 'output.txt'
    com.Entry.totalEntries = 12
    com.Settings.traceEnabled = False

def plotCPUUtilization(name, title, useLatex):
  benchmark = com.Benchmark(name, title, Global.frameworksLabels, Global.frameworkColors)
  entry = com.Entry('CPUs utilized', False, 0, 'cpu')
  xLabel = 'Core Count'
  yLabel = 'CPU Utilization [%]'
  if useLatex:
    yLabel = 'CPU Utilization [\%]'
    benchmark.toLatex(xLabel, yLabel, entry, '5.0')
  else: benchmark.plot(xLabel, yLabel, com.Entry('CPUs utilized', False, 0, 'cpu'))

def plotElapsedTime(name, title, useLatex):
  benchmark = com.Benchmark(name, title, Global.frameworksLabels, Global.frameworkColors)
  entry =  com.Entry('seconds time elapsed', True, 11, 'clock')
  xLabel = 'Core Count'
  yLabel = 'Elapsed Time [s]'
  if useLatex: benchmark.toLatex(xLabel, yLabel, entry, '5.0')
  else: benchmark.plot(xLabel, yLabel, entry)

def latexForElapsedTime(name, title, useLatex):
  com.Benchmark(name, title, Global.frameworksLabels, Global.frameworkColors).toLatex('Core Count', 'Elapsed Time [s]',
                                                                                   com.Entry('seconds time elapsed', True, 11, 'clock'), '5.1')
def plotCyclesPerTransaction(name, title, transactionCount, useLatex):
  benchmark = com.Benchmark(name, title, Global.frameworksLabels, Global.frameworkColors)
  entry =  com.Entry('cycles', True, 4, 'cycles')
  xLabel = 'Core Count'
  yLabel = 'CPU Cycles (x 1000) per Message'
  factor = 1.0 / (transactionCount * 1000)
  if useLatex: benchmark.toLatex(xLabel, yLabel, entry, '5.0', factor)
  else: benchmark.plot(xLabel, yLabel, entry, factor)

def plotAll(useLatex):
  prodcon(useLatex)
  latency(useLatex)
  countingActor(useLatex)
  forkJoin_throughput(useLatex)
  forkJoin_spawn(useLatex)
  big(useLatex)
  threadRing(useLatex)
  quicksort(useLatex)
  quicksort2(useLatex)
  lpexp(useLatex)

def prodcon(useLatex):
  name = 'prodcon'
  if useLatex: title = '\'Producer-Consumer\' benchmark with 32 producers, each producer sending 1M messages to single consumer.'
  else: title = 'Producer-Consumer (32:1) Benchmark with 1M/Producer'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)
  #plotCyclesPerTransaction(name, title, 32000000, useLatex)

def latency(useLatex):
  name = 'latency'
  if useLatex: title = '\'Ping-Pong\' benchmark with message count = 10M.'
  else: title = 'Pingpong Benchmark (10M)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)
  #plotCyclesPerTransaction(name, title, 10000000, useLatex)

def countingActor(useLatex):
  name = 'counting-actor'
  if useLatex: title = '\'Counting-Actor\' benchmark with message count = 10M.'
  else: title = title = 'Counting Actor Benchmark (10M)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)
  #plotCyclesPerTransaction(name, title, 10000000, useLatex)

def forkJoin_throughput(useLatex):
  name = 'fork-join-throughput'
  if useLatex: title = '\'Fork-Join: Throughput\' benchmark with 1K actors and 10K messages/actor per actor.'
  else: title = 'Fork-Join: Throughput (1K Actors, 10K Messages/Actor)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)
  #plotCyclesPerTransaction(name, title, 10000000, useLatex)

def forkJoin_spawn(useLatex):
  name = 'fork-join-spawn'
  if useLatex: title = '\'Fork-Join: Spawn\' benchmark with 1M actors.'
  else: title = 'Fork-Join: Spawn (1M)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)

  benchmark = com.Benchmark(name, title, Global.frameworksLabels, Global.frameworkColors)
  xLabel = 'Core Count'
  yLabel = 'CPU Cycles (x 1000) per Spawn'
  entry = com.Entry('cycles', True, 4, 'cycles')
  nSpawns = 1000000
  factor = 1.0 / (nSpawns * 1000)
  #if useLatex: benchmark.toLatex(xLabel, yLabel, entry, '5.0', factor)
  #else: benchmark.plot(xLabel, yLabel, entry, factor)

def big(useLatex):
  name = 'big'
  if useLatex: title = '\'Big\' benchmark with 500 actors and 50K pings from each.'
  else: title = 'Big (500 Actors, 50K Pings from Each)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)
  #plotCyclesPerTransaction(name, title, 50000000, useLatex)

def threadRing(useLatex):
  name = 'thread-ring'
  if useLatex: title = '\'Thread Ring\' benchmark with 1K actors and initial value = 10M.'
  else: title = 'Thread Ring (1K Actors Starting at 10M)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)
  #plotCyclesPerTransaction(name, title, 10000000, useLatex)

def quicksort(useLatex):
  name = 'quicksort'
  if useLatex: title = 'List-based \'Parallel Quicksort\' benchmark with 10M integers. Items-per-actor threshold is set to 100K.'
  else: title = 'Parallel Quicksort (10M Integers, Items-per-Actor Threshold = 100K)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)

def quicksort2(useLatex):
  name = 'quicksort2'
  if useLatex: title = 'Array-based \'Parallel Quicksort\' benchmark with 500M integers. Maximum tree depth is set to 5.'
  else: title = 'Parallel Quicksort (500M Integers, Maximum Tree Depth = 5)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)

def lpexp(useLatex):
  name = 'lpexp'
  if useLatex: title = '\'Parallel Laplace Expansion\' benchmark with a 12x12 matrix. Minimum submatrix size is set to 8x8.'
  else: title = 'Parallel Laplace Expansion (12x12/8x8)'
  plotElapsedTime(name, title, useLatex)
  plotCPUUtilization(name, title, useLatex)

if __name__ == "__main__":
  main()