import os
import sys
import numpy as np
from sys import exit
import matplotlib.pyplot as plt
import matplotlib._color_data as mcd

def main():
  prefix = 'vmrs'
  dataPath = 'output.txt'
  outputPath = 'space-req-boxplot.png'
  frameworks = {
    'akka': 'Akka',
    'proto.actor': 'Proto.Actor',
    'caf': 'CAF',
    'react++': 'React++'
  }
  
  frameworkColors = { 
    'akka': 'magenta',
    'proto.actor': 'green',
    'caf': 'blue',    
    'react++': 'crimson'
  }

  lstData = []
  labels = []
  for name in frameworks:
    print('name = {}, label = {}'.format(name, frameworks[name]))
    dataSet = DataSet()
    path = '{}\\{}\\output.txt'.format(prefix, name)
    #print(path)
    labels.append(frameworks[name] + ' (RS)')
    if name != 'akka': labels.append(frameworks[name] + ' (VM)')
    dataSet.load(path)
    lstData.append(dataSet.residentSetSamples)
    if name != 'akka': lstData.append(dataSet.virtualMemorySamples)

  blue_square = dict(markerfacecolor='b', marker='s')
  fig1, ax1 = plt.subplots()
  ax1.set_title('Memory Profile for Fork-Join-Spawn with 5M Actors')
  ax1.set_xlabel('Space (MB)')
  #ax1.set_ylabel('Frameworks')
  ax1.set_yticklabels(labels, rotation=45, fontsize=8)
  bplot = ax1.boxplot(lstData, vert=False, whis=1.5, patch_artist=True, notch=0, showfliers=True, sym='k+')
  
  colors = []
  for name in frameworkColors:
    colors.append(mcd.XKCD_COLORS['xkcd:' + frameworkColors[name]])
    if name != 'akka': colors.append(mcd.CSS4_COLORS[frameworkColors[name]])
  #plt.setp(bplot['boxes'], color='blue')
  for patch, color in zip(bplot['boxes'], colors):
    patch.set_color(color)
    patch.set_facecolor(color)

  for patch in bplot['medians']: patch.set_color('black')
 
  colorIdx = 0
  alternate = False
  for patch in bplot['whiskers']:
    patch.set_color(colors[colorIdx])
    if alternate: colorIdx += 1
    alternate = not alternate
    
  colorIdx = 0
  alternate = False    
  for patch in bplot['caps']:
    patch.set_color(colors[colorIdx])
    if alternate: colorIdx += 1
    alternate = not alternate
    
  plt.savefig(outputPath)
  plt.show()
  
  
class DataSet:
  PageSize = 4 * 1024       # page size = 4KB
  Divider = 1024 * 1024     # reported in MB
  def __init__(self):
    self.virtualMemorySamples = np.array([], dtype=float)
    self.residentSetSamples = np.array([], dtype=float)

  def appendToVMS(self, sample):
    self.virtualMemorySamples = np.append(self.virtualMemorySamples, sample)

  def appendToRSS(self, sample):
    self.residentSetSamples = np.append(self.residentSetSamples, sample)

  def __str__(self):
    return 'VM = {}, RS = {}'.format(self.virtualMemorySamples, self.residentSetSamples)

  def load(self, path):
    assert os.path.exists(path), 'could not file input file --> {}'.format(path)
    keys = ['VIRTUAL_MEM', 'RESIDENT_SET']
    print('loading dataset from {}'.format(path))
    with open(path, 'r') as fileHandle:
      lineNumber = 1
      for line in fileHandle:
        line = line.strip()
        if line.find('mem-test') >= 0:
          lineNumber += 1
          continue # skip header
        for key in keys:
          idx1 = line.find(key)
          assert idx1 >= 0, 'cannot find key {} in line {} --> {}'.format(key, lineNumber, line)
          idx2 = line.find('x4KB', idx1)
          value = ''
          assert idx2 >= 0, 'cannot find key {} in line {} --> {}'.format(key, lineNumber, line)
          value = line[idx1 + len(key) + 1:idx2]
          try:
            value = (float(value) * DataSet.PageSize) / DataSet.Divider
            #print('key = {}, value = {}'.format(key, value))
            if key == 'VIRTUAL_MEM': self.appendToVMS(value)
            else: self.appendToRSS(value)
          except: exit('FATAL: could not parse value {} for key {} in line {} --> {}'.format(value, key, lineNumber, line))

        lineNumber += 1
if __name__ == "__main__":
  main()