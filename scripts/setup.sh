#!/bin/bash

cpupower frequency-set -g performance
sysctl -w net.core.somaxconn=1048576
sysctl -w net.ipv4.tcp_max_syn_backlog=262144
sysctl -w vm.max_map_count=2097152
sysctl -w net.ipv4.tcp_tw_reuse=1
sysctl -w net.ipv4.ip_local_port_range="1024 65535"

