import io_common as com
import math

def main():
  Global.initialize()
  useLatex = False
  requestThroughput(useLatex)
  dataThroughput(useLatex)

class Global:
  messageSizes = [1024, 8192, 65536]
  frameworkKeys = ['zmq-native', 'zmq-actors']
  frameworkLabels = {
    '1024-zmq-native': 'ZMQ (native), 1K',
    '1024-zmq-actors': 'ZMQ (React++), 1K',
    '8192-zmq-native': 'ZMQ (native), 8K',
    '8192-zmq-actors': 'ZMQ (React++), 8K',
    '65536-zmq-native': 'ZMQ (native), 64K',
    '65536-zmq-actors': 'ZMQ (React++), 64K'
  }

  frameworkColors = {
    '1024-zmq-native': 'green',
    '1024-zmq-actors': 'lime',
    '8192-zmq-native': 'red',
    '8192-zmq-actors': 'orange',
    '65536-zmq-native': 'blue',
    '65536-zmq-actors': 'violet'
  }

  @staticmethod
  def initialize():
    com.Benchmark.root = 'zmq-collector'
    com.Benchmark.fileName = 'output.txt'
    com.Settings.traceEnabled = False

def requestThroughput(useLatex):
  title = 'Request Throughput'
  benchmark = com.ZMQBenchmark(title, Global.messageSizes, Global.frameworkKeys, Global.frameworkLabels, Global.frameworkColors)
  xLabel, yLabel = 'Core Count', 'Request/sec (x 1000)'
  factor = 1 / 1000

  if useLatex: benchmark.toLatex(xLabel, yLabel, com.Tag.ZMQ_REQ, '5.0', factor)
  else: benchmark.plot(xLabel, yLabel, com.Tag.ZMQ_REQ, factor)

def dataThroughput(useLatex):
  title = 'Data Throughput'
  benchmark = com.ZMQBenchmark(title, Global.messageSizes, Global.frameworkKeys, Global.frameworkLabels, Global.frameworkColors)
  xLabel, yLabel = 'Core Count', 'Gbps'
  factor = 8 / math.pow(10, 9)      # convert to Gbits/sec

  if useLatex: benchmark.toLatex(xLabel, yLabel, com.Tag.ZMQ_DAT, '5.0', factor)
  else: benchmark.plot(xLabel, yLabel, com.Tag.ZMQ_DAT, factor)

if __name__ == "__main__":
  main()