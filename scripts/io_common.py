import numpy as np
from sys import exit
import matplotlib.pyplot as plt
import re
import math

class Tag:
  WRK = 0
  WEIGHHTTP = 1
  ZMQ_REQ = 2
  ZMQ_DAT = 3


class Converter:
  INT = int
  FLOAT = float
  NONE = None

class Delimiter:
  COLON = ':'
  ASSIGN = '='
  NONE = None

class Settings:
  traceEnabled = False

class TypeDescriptor:
  def __init__(self, key, kvp, delim, converter):
    self.key = key
    if kvp:       # simple key-value pair
      self.converter = converter
      self.expression = '^\s*{}\s*{}\s*(.*)\s*$'.format(key, delim)
    else:         # nested expression
      converter = Converter.NONE
      self.expression = '^\s*{}_(.*)\s*\((.*)\)\s*$'.format(key)

class ConfigurationHeader:
  def __init__(self, nServerCores = 0, nClientSamples = 0):
    self.nServerCores = nServerCores
    self.nClientSamples = nClientSamples

  def __str__(self):
    return 'server cores = {}, client samples = {}'.format(self.nServerCores, self.nClientSamples)


class SampleSet:
  def __init__(self):
    self.__samples = {}

  @staticmethod
  def configToNCores(nConfigs):
    return 4 * (nConfigs + 1)

  def getSortedKeys(self):
    keys = []
    for key in sorted(self.__samples): keys.append(key)
    return keys

  def get(self, nServerCores):
    return self.__samples[nServerCores]

  def set(self, nServerCores, rps):
    if nServerCores not in self.__samples: self.__samples[nServerCores] = []
    self.__samples[nServerCores].append(rps)

  def __str__(self):
    return '{}'.format(self.__samples)

  def report(self, x_values_out, y_values_out, y_errors_out):
    nConfigs = self.getNConfigs()
    keys = self.getSortedKeys()
    for config in range(nConfigs):
      nServerCores = keys[config]
      x_values_out[config] = nServerCores
      y_values_out[config] = np.average(self.__samples[nServerCores])
      y_errors_out[config] = np.std(self.__samples[nServerCores])

  def getNConfigs(self):
    return len(self.__samples)

  def getSamples(self):
    return self.__samples

class GraphDataSet:
  def __init__(self, frameworkLabel, frameworkColor, sampleSet):
    self.frameworkLabel = frameworkLabel
    self.frameworkColor = frameworkColor
    self.nConfigs = sampleSet.getNConfigs()
    self.x_values, self.y_values, self.y_errors = np.empty(self.nConfigs), np.empty(self.nConfigs), np.empty(self.nConfigs)
    sampleSet.report(self.x_values, self.y_values, self.y_errors)

  def __str__(self):
    result = '{\n\tx, y, error\n'
    for i in range(self.nConfigs): result += '\t{}, {}, {}\n'.format(round(self.x_values[i], 2), round(self.y_values[i], 2), round(self.y_errors[i], 2))
    result += '};\n\\addlegendentry{' + self.frameworkLabel + '}\n'
    return result

  def toString(self):
    return 'x_values = {}, y_values = {}, y_errors = {}'.format(self.x_values, self.y_values, self.y_errors)

class Parser:
  @staticmethod
  def assertHeader(configNumber, sampleNumber, line):
    expected = 'config_{}/sample_{}:'.format(configNumber, sampleNumber)
    if line.strip() != expected: exit('FATAL: wrong configuration header --> {}, expecting {}'.format(line, expected))

  @staticmethod
  def parseValue(line, typeDescriptor):
    regex = re.compile(typeDescriptor.expression, re.I)
    result = regex.search(line)
    if result == None: exit('FATAL: Could not parse this line --> {}'.format(line))
    groups = result.groups()
    if len(groups) != 1: exit('FATAL: Could not parse this line --> {}'.format(line))
    value = result.group(1)
    targetType = typeDescriptor.converter
    if targetType == Converter.INT or targetType == Converter.FLOAT:
      try: return targetType(value.translate({ord(','): None}))
      except ValueError: exit('FATAL: Bad value for key "{}" in this line --> {}'.format(typeDescriptor.key, line))
    return value.strip()

  @staticmethod
  def parseHeader(line, expectedConfigNumber, keys, converters, config, assignOperator):
    key = 'config'
    typeDescriptor = TypeDescriptor(key, False, Delimiter.NONE, Converter.NONE)
    regex = re.compile(typeDescriptor.expression, re.I)
    result = regex.search(line)
    if result == None: exit('FATAL: Could not parse this line --> {}'.format(line))
    groups = result.groups()
    if len(groups) != 2: exit('FATAL: Could not parse this line --> {}'.format(line))

    # match configuration number
    configNumber = result.group(1)
    try: configNumber = int(configNumber.translate({ord(','): None}))
    except ValueError: exit('FATAL: Bad value for key "{}" in this line --> {}'.format(typeDescriptor.key, line))
    if configNumber != expectedConfigNumber: exit('FATAL: expecting config_{} in this header --> {}, '.format(expectedConfigNumber, line))

    value = result.group(2)
    tokens = value.split(',')
    nKeys = len(keys)
    if len(tokens) != nKeys: exit('FATAL: Could not parse this line --> {}'.format(line))

    for idx in range(nKeys):
      key = keys[idx]
      typeDescriptor = TypeDescriptor(key, True, Delimiter.ASSIGN, converters[idx])
      value = Parser.parseValue(tokens[idx], typeDescriptor)
      # print('{} --> {}'.format(key, value))
      assignOperator(config, idx, value)

  @staticmethod
  def seekLine(fileHandle, lineOffset = 0):
    if lineOffset < 0: return ''
    for i in range(lineOffset): fileHandle.readline().strip()
    return fileHandle.readline().strip()

  @staticmethod
  def filter_wrk_rps(inputPath):
    sampleSet = SampleSet()
    keys = ['nServerCores', 'nClientSamples']
    fieldName = 'requests/sec'
    fieldOffset = 12  # location of 'request/sec' starting from configuration header
    with open(inputPath, 'r') as fileHandle:
      line = Parser.seekLine(fileHandle)
      nConfigs = Parser.parseValue(line, TypeDescriptor('nConfigs', True, Delimiter.ASSIGN, Converter.INT))
      if Settings.traceEnabled: print('nConfigs = {}'.format(nConfigs))
      for configNumber in range(nConfigs):
        line = Parser.seekLine(fileHandle)
        config = ConfigurationHeader()
        Parser.parseHeader(line, configNumber, keys, [Converter.INT, Converter.INT], config, assignValue)
        if Settings.traceEnabled: print('config {}: {} = {}, {} = {}'.format(configNumber, keys[0], config.nServerCores, keys[1], config.nClientSamples))
        for sampleNumber in range(config.nClientSamples):
          line = Parser.seekLine(fileHandle)
          Parser.assertHeader(configNumber, sampleNumber, line)

          while True:
            line = Parser.seekLine(fileHandle)
            expression = r'^Requests/sec:(.*)$'
            regex = re.compile(expression, re.I)
            result = regex.search(line)
            if result != None:
              groups = result.groups()
              nResults = len(groups)
              if nResults != 1: exit('FATAL: Could not parse this line --> {}'.format(line))
              rps = result.group(1)
              break

          try: rps = float(rps.translate({ord(','): None}))
          except ValueError: exit('FATAL: Bad value req/s in this line --> {}'.format(line))

          if Settings.traceEnabled: print('\tsample {}: {} rps'.format(sampleNumber, rps))
          sampleSet.set(config.nServerCores, rps)
          Parser.seekLine(fileHandle)
    return sampleSet

  @staticmethod
  def filter_weighttp_rps(inputPath):
    sampleSet = SampleSet()
    keys = ['nServerCores', 'nClientSamples']
    fieldName = 'req/s'
    with open(inputPath, 'r') as fileHandle:
      line = Parser.seekLine(fileHandle)
      config = ConfigurationHeader()
      nConfigs = Parser.parseValue(line, TypeDescriptor('nConfigs', True, Delimiter.ASSIGN, Converter.INT))
      if Settings.traceEnabled: print('nConfigs = {}'.format(nConfigs))
      for configNumber in range(nConfigs):
        line = Parser.seekLine(fileHandle)
        config = ConfigurationHeader()
        Parser.parseHeader(line, configNumber, keys, [Converter.INT, Converter.INT], config, assignValue)
        if Settings.traceEnabled: print('config {}: {} = {}, {} = {}'.format(configNumber, keys[0], config.nServerCores, keys[1], config.nClientSamples))
        for sampleNumber in range(config.nClientSamples):
          line = Parser.seekLine(fileHandle)
          Parser.assertHeader(configNumber, sampleNumber, line)
          line = Parser.seekLine(fileHandle, 0)
          rps = 0
          while True:
            line = Parser.seekLine(fileHandle)
            expression = r'^finished in (.*), (.*) req/s(.*)$'
            regex = re.compile(expression, re.I)
            result = regex.search(line)
            if result != None:
              groups = result.groups()
              nResults = len(groups)
              if nResults != 3: exit('FATAL: Could not parse this line --> {}'.format(line))
              rps = result.group(2)
              break

          try: rps = int(rps.translate({ord(','): None}))
          except ValueError: exit('FATAL: Bad value req/s in this line --> {}'.format(line))
          if Settings.traceEnabled: print('\tsample {}: {} rps'.format(sampleNumber, rps))
          sampleSet.set(config.nServerCores, rps)
          line = Parser.seekLine(fileHandle, 2)
    return sampleSet

  @staticmethod
  def filter_zmq_rps(inputPath):
    return Parser.filter_wrk_rps(inputPath)

  @staticmethod
  def filter_zmq_tps(inputPath):
    sampleSet = SampleSet()
    keys = ['nServerCores', 'nClientSamples']
    with open(inputPath, 'r') as fileHandle:
      line = Parser.seekLine(fileHandle)
      nConfigs = Parser.parseValue(line, TypeDescriptor('nConfigs', True, Delimiter.ASSIGN, Converter.INT))
      if Settings.traceEnabled: print('nConfigs = {}'.format(nConfigs))
      for configNumber in range(nConfigs):
        line = Parser.seekLine(fileHandle)
        config = ConfigurationHeader()
        Parser.parseHeader(line, configNumber, keys, [Converter.INT, Converter.INT], config, assignValue)
        if Settings.traceEnabled: print('config {}: {} = {}, {} = {}'.format(configNumber, keys[0], config.nServerCores, keys[1], config.nClientSamples))
        for sampleNumber in range(config.nClientSamples):
          line = Parser.seekLine(fileHandle)
          Parser.assertHeader(configNumber, sampleNumber, line)

          while True:
            line = Parser.seekLine(fileHandle)
            expression = r'^Transfer/sec:(.*)$'
            regex = re.compile(expression, re.I)
            result = regex.search(line)
            if result != None:
              groups = result.groups()
              nResults = len(groups)
              if nResults != 1: exit('FATAL: Could not parse this line --> {}'.format(line))
              tps = result.group(1)
              break

          tps = convertToBytes(tps)

          if Settings.traceEnabled: print('\tsample {}: {}'.format(sampleNumber, tps))
          sampleSet.set(config.nServerCores, tps)
    return sampleSet


def convertToBytes(inputString):
  suffixes = ['byte', 'bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB']
  multipliers = [1, 1, math.pow(2, 10), math.pow(2, 20), math.pow(2, 30), math.pow(2, 40), math.pow(2, 50), math.pow(2, 60)]

  multiplierIndex, value = 0, 0
  for suffix in suffixes:
    idx = inputString.find(suffix)
    if idx != -1:
      value = inputString[0:idx]
      break
    multiplierIndex += 1

  if multiplierIndex >= len(multipliers):
    print('could not parse this string --> {}'.format(inputString))
    return 0

  try: value = float(value.translate({ord(','): None}))
  except ValueError:
    print('could not parse this string --> {}'.format(inputString))
    return 0

  return value * multipliers[multiplierIndex]

def assignValue(config, index, value):
  if index == 0: config.nServerCores = value
  elif index == 1: config.nClientSamples = value

class Viewport:
  def __init__(self, xLabel, yLabel, xMax, yMax):
    self.xLabel = xLabel
    self.yLabel = yLabel
    self.xMax = int(xMax) + 1
    self.yMax = yMax * 1.5
    self.ticks = np.array([], dtype=int)
    for i in range(self.xMax):
      if i > 0 and i % 2 == 0: self.ticks = np.append(self.ticks, i)

class Benchmark:
  root, fileName, imageDirectory = '', '', 'images'

  @staticmethod
  def getMax(lst):
    result = lst[0]
    for i in range(lst.size):
      if result < lst[i]: result = lst[i]

    return result

  def __init__(self, name, title, frameworkLabels, frameworkColors):
    self.name = name
    self.title = title
    self.frameworkLabels = frameworkLabels
    self.frameworkColors = frameworkColors

  def plot_h(self, path, viewPort, datasets):
    figure, axis = plt.subplots()
    plt.title(self.title)
    plt.xlabel(viewPort.xLabel)
    plt.ylabel(viewPort.yLabel)
    # plt.grid(True)
    plt.axis(np.array([0, viewPort.xMax, 0, viewPort.yMax]))
    plt.xticks(viewPort.ticks)

    for dataset in datasets:
      plt.errorbar(dataset.x_values, dataset.y_values, yerr=dataset.y_errors, uplims=True, lolims=True, ls='-', lw=0.5, label=dataset.frameworkLabel, color=dataset.frameworkColor)

    legend = axis.legend(loc='upper left', fontsize='10')

    path = '{}/{}'.format(Benchmark.imageDirectory, path)
    print('creating {}'.format(path))
    plt.savefig(path)
    #plt.show()
    plt.close()

  def plot(self, suffix, xLabel, yLabel, tag, factor = 1.0):
    xMax = 0
    yMax = 0
    graphDataSets = []
    for frameworkKey in self.frameworkLabels:
      inputPath = Benchmark.root + '/' + self.name + '/' + frameworkKey + '/' + Benchmark.fileName
      if tag == Tag.WRK: sampleSet = Parser.filter_wrk_rps(inputPath)
      elif tag == Tag.WEIGHHTTP: sampleSet = Parser.filter_weighttp_rps(inputPath)
      elif tag == Tag.ZMQ_REQ: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
      elif tag == Tag.ZMQ_DAT: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
      else: exit('FATAL: Unknown benchmark tag in Benchmark.plot() --> {}'.format(tag))
      graphDataSet = GraphDataSet(self.frameworkLabels[frameworkKey], self.frameworkColors[frameworkKey], sampleSet)
      print(graphDataSet.toString())
      nConfigs = graphDataSet.x_values.size
      for i in range(nConfigs):
        graphDataSet.y_values[i] *= factor
        graphDataSet.y_errors[i] *= factor
      xMax_ = Benchmark.getMax(graphDataSet.x_values)
      yMax_ = Benchmark.getMax(graphDataSet.y_values)
      if xMax < xMax_: xMax = xMax_
      if yMax < yMax_: yMax = yMax_
      graphDataSets.append(graphDataSet)
    viewPort = Viewport(xLabel, yLabel, xMax, yMax)
    self.plot_h('{}-{}.png'.format(self.name, suffix), viewPort, graphDataSets)

  def toLatex(self, xLabel, yLabel, tag, figureNumber, factor=1.0):
    xMax, yMax = 0, 0
    graphDataSets = []
    for frameworkKey in self.frameworkLabels:
      inputPath = Benchmark.root + '/' + self.name + '/' + frameworkKey + '/' + Benchmark.fileName
      if tag == Tag.WRK: sampleSet = Parser.filter_wrk_rps(inputPath)
      elif tag == Tag.WEIGHHTTP: sampleSet = Parser.filter_weighttp_rps(inputPath)
      elif tag == Tag.ZMQ_REQ: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
      elif tag == Tag.ZMQ_DAT: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
      else: exit('FATAL: Unknown benchmark tag in Benchmark.plot() --> {}'.format(tag))

      graphDataSet = GraphDataSet(self.frameworkLabels[frameworkKey], self.frameworkColors[frameworkKey], sampleSet)
      nConfigs = graphDataSet.x_values.size
      for i in range(nConfigs):
        graphDataSet.y_values[i] *= factor
        graphDataSet.y_errors[i] *= factor
      xMax_ = Benchmark.getMax(graphDataSet.x_values)
      yMax_ = Benchmark.getMax(graphDataSet.y_values)
      if xMax < xMax_: xMax = xMax_
      if yMax < yMax_: yMax = yMax_
      graphDataSets.append(graphDataSet)

    lastXTick = int(xMax)
    quotient = yMax / 10
    lastYTick = math.ceil(quotient) * 10
    yMax = lastYTick * 1.15
    yTickLength = math.ceil(lastYTick / 10)

    height = 9
    width = 14
    nSamples = 4096

    result = '%----------------------------------------------------------------------------------------\n\n'
    result += '\\begin{figure}\n\\centering\n\\begin{tikzpicture}\n\\begin{axis}\n[\n'
    result += '\theight={}cm, width={}cm, ymin=0, ymax={},\n'.format(height, width, round(yMax, 2))
    result += '\txtick={' + '0,2,...,{}'.format(lastXTick) + '},\n'
    result += '\tytick={' + '0,{},...,{}'.format(yTickLength, lastYTick) + '},\n'
    result += '\txtick align=inside, xtick pos=bottom,\n'
    result += '\tytick align=inside, ytick pos=left,\n'
    result += '\txlabel={' + xLabel + '},\n'
    result += '\tylabel={' + yLabel + '},\n'
    result += '\tlegend pos=north west,\n\tlegend style={draw=none},\n\tlegend cell align={left},\n\t/pgf/number format/.cd,\n\t1000 sep={}\n'
    #result += '\tsamples={}\n'.format(nSamples)
    result += ']\n'

    for graphDataSet in graphDataSets:
      result += '\\addplot+\n[\n'
      result += '\t{}, mark options='.format(graphDataSet.frameworkColor) + '{' + graphDataSet.frameworkColor + ', scale=1.0},\n'
      result += '\terror bars/.cd,\n\ty fixed,\n\ty dir=both,\n\ty explicit\n'
      result += ']\n'
      result += 'table [x=x, y=y,y error=error, col sep=comma]\n'
      result += str(graphDataSet) + '\n'


    result += '\\end{axis}\n\\end{tikzpicture}\n\\setlength{\\belowcaptionskip}{-10pt}\n'
    result += '\\caption{' + self.title + '}\n'
    result += '\\label{fig:' + figureNumber + '}\n\\end{figure}\n'

    print(result)

class ZMQBenchmark:
  root, fileName, imageDirectory = '', '', 'images'

  @staticmethod
  def getMax(lst):
    result = lst[0]
    for i in range(lst.size):
      if result < lst[i]: result = lst[i]

    return result

  def __init__(self, title, messageSizes, frameworkKeys, frameworkLabels, frameworkColors):
    self.title = title
    self.messageSizes = messageSizes
    self.frameworkKeys = frameworkKeys
    self.frameworkLabels = frameworkLabels
    self.frameworkColors = frameworkColors

  def plot_h(self, path, viewPort, datasets):
    figure, axis = plt.subplots()
    plt.title(self.title)
    plt.xlabel(viewPort.xLabel)
    plt.ylabel(viewPort.yLabel)
    # plt.grid(True)
    plt.axis(np.array([0, viewPort.xMax, 0, viewPort.yMax]))
    plt.xticks(viewPort.ticks)

    for dataset in datasets:
      plt.errorbar(dataset.x_values, dataset.y_values, yerr=dataset.y_errors, uplims=True, lolims=True, ls='-', lw=0.5, label=dataset.frameworkLabel, color=dataset.frameworkColor)

    legend = axis.legend(loc='upper left', fontsize='10')
    path = '{}/{}'.format(Benchmark.imageDirectory, path)
    print('creating {}'.format(path))
    plt.savefig(path)
    #plt.show()
    plt.close()

  def plot(self, xLabel, yLabel, tag, factor = 1.0):
    xMax = 0
    yMax = 0
    graphDataSets = []

    for messageSize in self.messageSizes:
      for frameworkKey in self.frameworkKeys:
        inputPath = Benchmark.root + '/' + str(messageSize) + '/' + frameworkKey + '/' + Benchmark.fileName
        print(inputPath)

        if tag == Tag.WRK: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
        elif tag == Tag.WEIGHHTTP: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
        elif tag == Tag.ZMQ_REQ: sampleSet = Parser.filter_zmq_rps(inputPath)
        elif tag == Tag.ZMQ_DAT: sampleSet = Parser.filter_zmq_tps(inputPath)
        else: exit('FATAL: Unknown benchmark tag in ZMQBenchmark.plot() --> {}'.format(tag))

        print(sampleSet)

        prefixedKey = str(messageSize) + '-' + frameworkKey
        graphDataSet = GraphDataSet(self.frameworkLabels[prefixedKey], self.frameworkColors[prefixedKey], sampleSet)

        nConfigs = graphDataSet.x_values.size
        for i in range(nConfigs):
          graphDataSet.y_values[i] *= factor
          graphDataSet.y_errors[i] *= factor
        xMax_ = Benchmark.getMax(graphDataSet.x_values)
        yMax_ = Benchmark.getMax(graphDataSet.y_values)
        if xMax < xMax_: xMax = xMax_
        if yMax < yMax_: yMax = yMax_
        graphDataSets.append(graphDataSet)

    viewPort = Viewport(xLabel, yLabel, xMax, yMax)

    suffix = 'unknown'
    if tag == Tag.ZMQ_REQ: suffix = 'req'
    elif tag == Tag.ZMQ_DAT: suffix = 'dat'

    self.plot_h('{}-{}.png'.format('zmq', suffix), viewPort, graphDataSets)

  def toLatex(self, xLabel, yLabel, tag, figureNumber, factor = 1.0):
    xMax = 0
    yMax = 0
    graphDataSets = []

    for messageSize in self.messageSizes:
      for frameworkKey in self.frameworkKeys:
        inputPath = Benchmark.root + '/' + str(messageSize) + '/' + frameworkKey + '/' + Benchmark.fileName

        if tag == Tag.WRK: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
        elif tag == Tag.WEIGHHTTP: exit('FATAL: This benchmark is implemented in a different class. --> {}'.format(tag))
        elif tag == Tag.ZMQ_REQ: sampleSet = Parser.filter_zmq_rps(inputPath)
        elif tag == Tag.ZMQ_DAT: sampleSet = Parser.filter_zmq_tps(inputPath)
        else: exit('FATAL: Unknown benchmark tag in ZMQBenchmark.plot() --> {}'.format(tag))


        prefixedKey = str(messageSize) + '-' + frameworkKey
        graphDataSet = GraphDataSet(self.frameworkLabels[prefixedKey], self.frameworkColors[prefixedKey], sampleSet)

        nConfigs = graphDataSet.x_values.size
        for i in range(nConfigs):
          graphDataSet.y_values[i] *= factor
          graphDataSet.y_errors[i] *= factor
        xMax_ = Benchmark.getMax(graphDataSet.x_values)
        yMax_ = Benchmark.getMax(graphDataSet.y_values)
        if xMax < xMax_: xMax = xMax_
        if yMax < yMax_: yMax = yMax_
        graphDataSets.append(graphDataSet)

    lastXTick = int(xMax)
    quotient = yMax / 10
    lastYTick = math.ceil(quotient) * 10
    yMax = lastYTick * 1.15
    yTickLength = math.ceil(lastYTick / 10)

    height = 9
    width = 14
    nSamples = 4096

    result = '%----------------------------------------------------------------------------------------\n\n'
    result += '\\begin{figure}\n\\centering\n\\begin{tikzpicture}\n\\begin{axis}\n[\n'
    result += '\theight={}cm, width={}cm, ymin=0, ymax={},\n'.format(height, width, round(yMax, 2))
    result += '\txtick={' + '0,2,...,{}'.format(lastXTick) + '},\n'
    result += '\tytick={' + '0,{},...,{}'.format(yTickLength, lastYTick) + '},\n'
    result += '\txtick align=inside, xtick pos=bottom,\n'
    result += '\tytick align=inside, ytick pos=left,\n'
    result += '\txlabel={' + xLabel + '},\n'
    result += '\tylabel={' + yLabel + '},\n'
    result += '\tlegend pos=north west,\n\tlegend style={draw=none},\n\tlegend cell align={left},\n\t/pgf/number format/.cd,\n\t1000 sep={}\n'
    #result += '\tsamples={}\n'.format(nSamples)
    result += ']\n'

    for graphDataSet in graphDataSets:
      result += '\\addplot+\n[\n'
      result += '\t{}, mark options='.format(graphDataSet.frameworkColor) + '{' + graphDataSet.frameworkColor + ', scale=1.0},\n'
      result += '\terror bars/.cd,\n\ty fixed,\n\ty dir=both,\n\ty explicit\n'
      result += ']\n'
      result += 'table [x=x, y=y,y error=error, col sep=comma]\n'
      result += str(graphDataSet) + '\n'

    result += '\\end{axis}\n\\end{tikzpicture}\n\\setlength{\\belowcaptionskip}{-10pt}\n'
    result += '\\caption{' + self.title + '}\n'
    result += '\\label{fig:' + figureNumber + '}\n\\end{figure}\n'

    print(result)