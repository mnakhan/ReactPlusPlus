import os
import sys
import numpy as np
from sys import exit
import matplotlib.pyplot as plt
import math

class Tag:
  Key = 0
  Comment = 1


class Entry:
  totalEntries = 0

  def __init__(self, name, isKey, lineOffset, suffix):
    self.name = name
    self.isKey = isKey
    self.lineOffset = lineOffset
    self.suffix = suffix


class Settings:
  traceEnabled = False


class SampleSet:
  def __init__(self, keyOrComment, nConfigs):
    self.__keyOrComment = keyOrComment
    self.__samples = {}

  @staticmethod
  def configToNCores(config):
    nCores = 0
    if config == 0:
      nCores = 1
    else:
      nCores = config * 2
    return nCores

  def getSortedKeys(self):
    keys = []
    for key in sorted(self.__samples): keys.append(key)
    return keys

  def get(self, nCores):
    return self.__samples[nCores]

  def set(self, nCores, value):
    if nCores not in self.__samples: self.__samples[nCores] = []
    self.__samples[nCores].append(value)

  def __str__(self):
    return 'sample set for "{}" = {}'.format(self.__keyOrComment, self.__samples)

  def report(self, x_values_out, y_values_out, y_errors_out):
    nConfigs = self.getNConfigs()
    keys = self.getSortedKeys()
    for config in range(nConfigs):
      nCores = keys[config]
      x_values_out[config] = nCores
      y_values_out[config] = np.average(self.__samples[nCores])
      y_errors_out[config] = np.std(self.__samples[nCores])

  def getNConfigs(self):
    return len(self.__samples)

  def getSamples(self):
    return self.__samples


class GraphDataSet:
  def __init__(self, frameworkLabel, frameworkColor, sampleSet):
    self.frameworkLabel = frameworkLabel
    self.frameworkColor = frameworkColor
    self.nConfigs = sampleSet.getNConfigs()
    self.x_values, self.y_values, self.y_errors = np.empty(self.nConfigs), np.empty(self.nConfigs), np.empty(self.nConfigs)
    sampleSet.report(self.x_values, self.y_values, self.y_errors)

  def __str__(self):
    result = '{\n\tx, y, error\n'
    for i in range(self.nConfigs): result += '\t{}, {}, {}\n'.format(round(self.x_values[i], 2), round(self.y_values[i], 2), round(self.y_errors[i], 2))
    result += '};\n\\addlegendentry{' + self.frameworkLabel + '}\n'
    return result

  def toString(self):
    return 'x_values = {}, y_values = {}, y_errors = {}'.format(self.x_values, self.y_values, self.y_errors)

class Parser:
  @staticmethod
  def filterByComment(inputPath, comment, lineOffset, totalEntries):
    if Settings.traceEnabled: print('Filtering samples by comment "{}", relative offset is {} within each record'.format(comment, lineOffset))
    return Parser.__filter(inputPath, comment, Tag.Comment, lineOffset, totalEntries)

  @staticmethod
  def filterByKey(inputPath, key, lineOffset, totalEntries):
    if Settings.traceEnabled: print('Filtering samples by key "{}", relative offset is {} within each record'.format(key, lineOffset))
    return Parser.__filter(inputPath, key, Tag.Key, lineOffset, totalEntries)

  '''
    Private members.
  '''

  @staticmethod
  def __assertHeader(configNumber, sampleNumber, line):
    expected = 'config_{}/sample_{}:'.format(configNumber, sampleNumber)
    if line.strip() != expected: exit('FATAL: Assertion failure for configuration header --> {}, expecting {}'.format(line, expected))

  @staticmethod
  def __filter(inputPath, keyOrComment, tag, lineOffset, totalEntries):
    sampleSet = None
    if not os.path.exists(inputPath): exit('FATAL: Could not open input file {}'.format(inputPath))
    with open(inputPath, 'r') as fileHandle:
      line = Parser.__seekLine(fileHandle)
      nConfigs = Parser.__parseValue('nConfigs', line)
      if Settings.traceEnabled: print('nConfigs = {}'.format(nConfigs))
      sampleSet = SampleSet(keyOrComment, nConfigs)
      for configNumber in range(nConfigs):
        line = Parser.__seekLine(fileHandle)
        nCores, nSamples = Parser.__parseHeader(configNumber, line)
        if Settings.traceEnabled: print('nCores = {}, nSamples = {}'.format(nCores, nSamples))

        for sampleNumber in range(nSamples):
          line = Parser.__seekLine(fileHandle)
          Parser.__assertHeader(configNumber, sampleNumber, line)

          line = Parser.__seekLine(fileHandle, lineOffset)
          value = None
          if tag == Tag.Key:
            value = Parser.__parseValueByKey(keyOrComment, line)
          elif tag == Tag.Comment:
            value = Parser.__parseValueByComment(keyOrComment, line)
          else:
            exit('FATAL: Invalid tag {} supplied to collectSample'.format(tag))

          if Settings.traceEnabled: print('"{}" --> {}'.format(keyOrComment, value))
          sampleSet.set(nCores, value)

          Parser.__seekLine(fileHandle, totalEntries - lineOffset - 2)

    return sampleSet

  @staticmethod
  def __parseHeader(configNumber, line):
    idx1 = line.find('(')
    idx2 = line.find(')')
    if idx1 == -1 or idx2 == -1 or idx1 >= idx2: exit('FATAL: Could not parse this line --> {}'.format(line))
    configHeader = line[0:idx1].strip()
    expectedHeader = 'config_{}'.format((configNumber))
    if configHeader != expectedHeader: exit('FATAL: Malformed configuration header in this line --> {}, expecting {}'.format(line, expectedHeader))
    configData = line[idx1 + 1:idx2].strip()
    tokens = configData.split(',')
    if len(tokens) != 2: exit('FATAL: Could not parse this line --> {}'.format(line))
    nCores = Parser.__parseValue('nCores', tokens[0])
    nSamples = Parser.__parseValue('nSamples', tokens[1])

    return nCores, nSamples

  @staticmethod
  def __parseValue(key, line):
    key = key.strip()
    tokens = line.split('=')
    if len(tokens) != 2: exit('FATAL: Could not parse this line --> {}'.format(line))
    if tokens[0].strip() != key: exit('FATAL: Could not find key "{}" in this line --> {}'.format(key, line))
    try:
      return int(tokens[1].translate({ord(','): None}))
    except ValueError:
      exit('FATAL: Bad value for key "{}" in this line --> {}'.format(key, line))

  @staticmethod
  def __parseValueByComment(comment, line):
    comment = comment.strip()
    idx1 = line.find('#')
    idx2 = line.find(comment)
    if idx1 == -1 or idx2 == -1 or idx1 >= idx2: exit('FATAL: Could not find comment "{}" in this line --> {}'.format(comment, line))
    value = line[idx1 + 1:idx2]
    try:
      value = float(value.translate({ord(','): None}))
      if comment == 'CPUs utilized': value *= 100.0
      return value
    except ValueError:
      exit('FATAL: Bad value for comment "{}" in this line --> {}'.format(comment, line))

  @staticmethod
  def __parseValueByKey(key, line):
    key = key.strip()
    idx = line.find(key)
    if idx == -1: exit('FATAL: Could not find key "{} in this line --> {}'.format(key, line))
    value = line[0:idx]
    try:
      return float(value.translate({ord(','): None}))
    except ValueError:
      exit('FATAL: Bad value for key "{}" in this line --> {}'.format(key, line))

  @staticmethod
  def __seekLine(fileHandle, lineOffset=0):
    if lineOffset < 0: return ''
    for i in range(lineOffset): fileHandle.readline()
    return fileHandle.readline()


class Viewport:
  def __init__(self, xLabel, yLabel, xMax, yMax):
    self.xLabel = xLabel
    self.yLabel = yLabel
    self.xMax = int(xMax) + 1
    self.yMax = yMax * 1.5
    self.ticks = np.array([], dtype=int)
    for i in range(self.xMax):
      if i > 0 and i % 2 == 0: self.ticks = np.append(self.ticks, i)


class Benchmark:
  root, fileName, imageDirectory = '', '', 'images'

  @staticmethod
  def getMax(lst):
    result = lst[0]
    for i in range(lst.size):
      if result < lst[i]: result = lst[i]

    return result

  def __init__(self, name, title, frameworkLabels, frameworkColors):
    self.name = name
    self.title = title
    self.frameworkLabels = frameworkLabels
    self.frameworkColors = frameworkColors

  def __plot_h(self, path, viewPort, datasets):
    figure, axis = plt.subplots()
    plt.title(self.title)
    plt.xlabel(viewPort.xLabel)
    plt.ylabel(viewPort.yLabel)
    # plt.grid(True)
    plt.axis(np.array([0, viewPort.xMax, 0, viewPort.yMax]))
    plt.xticks(viewPort.ticks)

    for dataset in datasets:
      plt.errorbar(dataset.x_values, dataset.y_values, yerr=dataset.y_errors, uplims=True, lolims=True, ls='-', lw=0.5, label=dataset.frameworkLabel,
                   color=dataset.frameworkColor)

    legend = axis.legend(loc='upper left', fontsize='10')
    path = '{}/{}'.format(Benchmark.imageDirectory, path)
    print('creating {}'.format(path))
    plt.savefig(path)
    # plt.show()
    plt.close()

  def plot(self, xLabel, yLabel, entry, factor=1.0):
    xMax = 0
    yMax = 0
    graphDataSets = []
    for frameworkKey in self.frameworkLabels:
      inputPath = Benchmark.root + '/' + self.name + '/' + frameworkKey + '/' + Benchmark.fileName
      if entry.isKey: sampleSet = Parser.filterByKey(inputPath, entry.name, entry.lineOffset, Entry.totalEntries)
      else: sampleSet = Parser.filterByComment(inputPath, entry.name, entry.lineOffset, Entry.totalEntries)

      graphDataSet = GraphDataSet(self.frameworkLabels[frameworkKey], self.frameworkColors[frameworkKey], sampleSet)
      if Settings.traceEnabled: print(graphDataSet.toString())

      nConfigs = graphDataSet.x_values.size
      for i in range(nConfigs):
        graphDataSet.y_values[i] *= factor
        graphDataSet.y_errors[i] *= factor
      xMax_ = Benchmark.getMax(graphDataSet.x_values)
      yMax_ = Benchmark.getMax(graphDataSet.y_values)
      if xMax < xMax_: xMax = xMax_
      if yMax < yMax_: yMax = yMax_
      graphDataSets.append(graphDataSet)
    viewPort = Viewport(xLabel, yLabel, xMax, yMax)
    self.__plot_h('{}-{}.png'.format(self.name, entry.suffix), viewPort, graphDataSets)

  def toLatex(self, xLabel, yLabel, entry, figureNumber, factor=1.0):
    xMax, yMax = 0, 0
    graphDataSets = []
    for frameworkKey in self.frameworkLabels:
      inputPath = Benchmark.root + '/' + self.name + '/' + frameworkKey + '/' + Benchmark.fileName
      if entry.isKey: sampleSet = Parser.filterByKey(inputPath, entry.name, entry.lineOffset, Entry.totalEntries)
      else: sampleSet = Parser.filterByComment(inputPath, entry.name, entry.lineOffset, Entry.totalEntries)

      graphDataSet = GraphDataSet(self.frameworkLabels[frameworkKey], self.frameworkColors[frameworkKey], sampleSet)

      nConfigs = graphDataSet.x_values.size
      for i in range(nConfigs):
        graphDataSet.y_values[i] *= factor
        graphDataSet.y_errors[i] *= factor
      xMax_ = Benchmark.getMax(graphDataSet.x_values)
      yMax_ = Benchmark.getMax(graphDataSet.y_values)
      if xMax < xMax_: xMax = xMax_
      if yMax < yMax_: yMax = yMax_
      graphDataSets.append(graphDataSet)

    lastXTick = int(xMax)
    quotient = yMax / 10
    lastYTick = math.ceil(quotient) * 10
    yMax = lastYTick * 1.15
    yTickLength = math.ceil(lastYTick / 10)

    height = 9
    width = 14
    nSamples = 4096

    result = '%----------------------------------------------------------------------------------------\n\n'
    result += '\\begin{figure}\n\\centering\n\\begin{tikzpicture}\n\\begin{axis}\n[\n'
    result += '\theight={}cm, width={}cm, ymin=0, ymax={},\n'.format(height, width, round(yMax, 2))
    result += '\txtick={' + '0,2,...,{}'.format(lastXTick) + '},\n'
    result += '\tytick={' + '0,{},...,{}'.format(yTickLength, lastYTick) + '},\n'
    result += '\txtick align=inside, xtick pos=bottom,\n'
    result += '\tytick align=inside, ytick pos=left,\n'
    result += '\txlabel={' + xLabel + '},\n'
    result += '\tylabel={' + yLabel + '},\n'
    result += '\tlegend pos=north west,\n\tlegend style={draw=none},\n\tlegend cell align={left},\n\t/pgf/number format/.cd,\n\t1000 sep={}\n'
    #result += '\tsamples={}\n'.format(nSamples)
    result += ']\n'

    for graphDataSet in graphDataSets:
      result += '\\addplot+\n[\n'
      result += '\t{}, mark options='.format(graphDataSet.frameworkColor) + '{' + graphDataSet.frameworkColor + ', scale=1.0},\n'
      result += '\terror bars/.cd,\n\ty fixed,\n\ty dir=both,\n\ty explicit\n'
      result += ']\n'
      result += 'table [x=x, y=y,y error=error, col sep=comma]\n'
      result += str(graphDataSet) + '\n'


    result += '\\end{axis}\n\\end{tikzpicture}\n\\setlength{\\belowcaptionskip}{-10pt}\n'
    result += '\\caption{' + self.title + '}\n'
    result += '\\label{fig:' + figureNumber + '}\n\\end{figure}\n'

    print(result)
