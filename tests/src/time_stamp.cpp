/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "time_stamp.hpp"

#ifdef LINUX

string webserver::time_stamp_t::getTime()
{
  static const char *weekdays[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
  static const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

  time_t t = time(nullptr);
  stringstream result;
  tm timeData;
  gmtime_r(&t, &timeData);
  result << weekdays[timeData.tm_wday] << ", " << timeData.tm_mday << ' ' << months[timeData.tm_mon] << ' ' << timeData.tm_year + 1900 << ' ';
  if (timeData.tm_hour < 10) result << '0';
  result << timeData.tm_hour << ':';
  if (timeData.tm_min < 10) result << '0';
  result << timeData.tm_min << ':';
  if (timeData.tm_sec < 10) result << '0';
  result << timeData.tm_sec << " GMT";
  return result.str();
}

webserver::time_stamp_t::time_stamp_t()
: currentTime(make_shared<string>(getTime())), isRunning(false)
{
}

webserver::time_stamp_t::~time_stamp_t()
{
  quit();
}

void webserver::time_stamp_t::initialize()
{
  isRunning = true;
  updater = thread([this]
  {
    while (true)
    {
      time_t counter = time(nullptr);
      tm timeData;
      memset(&timeData, 0, sizeof (tm));
      gmtime_r(&counter, &timeData);
      shared_ptr<string> newTime(make_shared<string>(getTime()));
      currentTime.swap(newTime);
      sleep_for(seconds(1));

      if (!isRunning) break;
    }
  });
}

string webserver::time_stamp_t::now() const
{
  return *currentTime;
}

void webserver::time_stamp_t::quit()
{
  isRunning = false;
  if (updater.joinable()) updater.join();
}

#endif // LINUX
