/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "tcp_listener.hpp"
#include "generic.hpp"
using namespace io;

#ifdef LINUX
printer_t& webserver::tcp_listener_t::tscout = GLOBAL(printer_t);

ostream& webserver::operator<<(ostream& out, sockaddr_in const& sa)
{
  string ip = "0.0.0.0";
  ushort port = 0;
  parseAddress(sa, ip, port);
  return out << ip << ':' << port;
}

bool webserver::parseAddress(sockaddr_in const& sa, string& ip, ushort& port)
{
  if (sa.sin_family != AF_INET) return false;

  char buffer[NI_MAXHOST] = {0};
  if (!inet_ntop(AF_INET, &sa.sin_addr, buffer, NI_MAXHOST)) return false;
  ip = buffer;
  port = ntohs(sa.sin_port);
  return true;
}

webserver::tcp_listener_t::tcp_listener_t(int backlog_, bool multihomed_)
: port(0), status(false), descriptor(-1), backlog(backlog_), multihomed(multihomed_)
{
}

webserver::tcp_listener_t::~tcp_listener_t()
{
}

SOCKET webserver::tcp_listener_t::accept() const
{
  sockaddr_in sa;
  memset(&sa, 0, sizeof (sockaddr_in));
  socklen_t sa_length = sizeof (sockaddr_in);
  descriptor_t cfd = ::accept(descriptor, (sockaddr*) & sa, &sa_length);
  return cfd;
}

SOCKET webserver::tcp_listener_t::accept(string& remoteIP, ushort& remotePort) const
{
  sockaddr_in sa;
  memset(&sa, 0, sizeof (sockaddr_in));
  socklen_t sa_length = sizeof (sockaddr_in);
  descriptor_t cfd = ::accept(descriptor, (sockaddr*) & sa, &sa_length);

  if (cfd == INVALID_SOCKET) return cfd;

  if (!parseAddress(sa, remoteIP, remotePort))
  {
    scout << "cannot parse client address" << endl;
    shutdown(cfd, SHUT_RDWR);
    ::close(cfd);
    return INVALID_SOCKET;
  }

  return cfd;
}

void webserver::tcp_listener_t::close()
{
  ::close(descriptor);
  status = false;
}

void webserver::tcp_listener_t::open(string const& ip_, ushort port_, int backlog_)
{
  backlog = backlog_;
  close();
  rpp_assert(signal(SIGPIPE, SIG_IGN) != SIG_ERR, "Error while opening server socket: SIGPIPE cannot be ignored.", tscout);

  descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  rpp_assert(descriptor != INVALID_SOCKET, "Descriptor for server socket was invalid.", tscout);

  linger l = {1, 0};
  int on = 1, qlen = 5;

  rpp_assert(setsockopt(descriptor, SOL_SOCKET, SO_LINGER, &l, sizeof (linger)) == 0, "Error while setting socket option: SOL_SOCKET = SO_LINGER", tscout);
  rpp_assert(setsockopt(descriptor, SOL_SOCKET, SO_REUSEADDR, &on, sizeof (int)) == 0, "Error while setting socket option: SOL_SOCKET = SO_REUSEADDR", tscout);
  rpp_assert(setsockopt(descriptor, SOL_SOCKET, SO_REUSEPORT, &on, sizeof (int)) == 0, "Error while setting socket option: SOL_SOCKET = SO_REUSEPORT", tscout);
  rpp_assert(setsockopt(descriptor, IPPROTO_TCP, TCP_FASTOPEN, &qlen, sizeof (int)) == 0, "Error while setting socket option: IPPROTO_TCP = TCP_FASTOPEN", tscout);
  rpp_assert(setsockopt(descriptor, IPPROTO_TCP, TCP_NODELAY, &on, sizeof (int)) == 0, "Error while setting socket option: IPPROTO_TCP = TCP_NODELAY", tscout);
  rpp_assert(setsockopt(descriptor, IPPROTO_TCP, TCP_DEFER_ACCEPT, &on, sizeof (int)) == 0, "Error while setting socket option: IPPROTO_TCP = TCP_DEFER_ACCEPT", tscout);

  sockaddr_in sa;
  memset(&sa, 0, sizeof (sockaddr_in));
  sa.sin_family = AF_INET;
  sa.sin_port = htons(port_);
  if (multihomed) sa.sin_addr.s_addr = htonl(INADDR_ANY);
  else rpp_assert(inet_pton(AF_INET, ip_.c_str(), &sa.sin_addr) > 0, "Error converting physical address to network address.", tscout);

  rpp_assert(bind(descriptor, (sockaddr*) & sa, sizeof (sockaddr_in)) != -1, "Could not bind server socket.", tscout);
  rpp_assert(listen(descriptor, backlog) != -1, "Could not start listening at server socket.", tscout);

  int flags = fcntl(descriptor, F_GETFL, 0);
  rpp_assert(fcntl(descriptor, F_SETFL, flags | O_NONBLOCK) != -1, "Could not run server socket in non-blocking mode.", tscout);

  if (parseAddress(sa, ip, port)) status = true;
  else ::close(descriptor);
}

descriptor_t webserver::tcp_listener_t::getDescriptor() const
{
  return descriptor;
}

string webserver::tcp_listener_t::getIP() const
{
  return ip;
}

ushort webserver::tcp_listener_t::getPort() const
{
  return port;
}

bool webserver::tcp_listener_t::isRunning() const
{
  return status;
}

ostream& webserver::operator<<(ostream& out, tcp_listener_t const& listener)
{
  return out << listener.ip << ':' << listener.port;
}
#endif // LINUX
