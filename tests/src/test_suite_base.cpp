/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "test_suite_base.hpp"

generic_ext::printer_t& ext::test_suite_base::tscout = GLOBAL(printer_t);
rpp::system_t& ext::test_suite_base::sys = GLOBAL(rpp::system_t);
ext::user_statistics_t& ext::test_suite_base::gc = GLOBAL(user_statistics_t);

void ext::test_suite_base::capture()
{
  watch.restart();
  isCapturing = true;
}

ostringstream& ext::test_suite_base::description()
{
  return oss_desc;
}

void ext::test_suite_base::initialize()
{
  [[maybe_unused]] uint32_t nThreads = MAX_HW_THREADS;
  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
}

void ext::test_suite_base::printFooter(bool isEnd)
{
  double elapsed = 0.0;
  if (isCapturing)
  {
    watch.pause();
    elapsed = watch.getElapsed();
    scout << "Elapsed time = " << elapsed << " seconds." << endl;
  }
  if (isEnd) scout << "-----------------------------END TEST\n" << endl;
  isCapturing = false;
}

void ext::test_suite_base::printHeader()
{
  scout << "BEGIN TEST-------------------" << endl;
  if (!oss_desc.tellp()) return;
  scout << oss_desc.str() << endl;
  oss_desc.str("");
}

ext::test_suite_base::test_suite_base()
{
}


ext::test_suite_base::~test_suite_base()
{
  sys.shutdown();
}
