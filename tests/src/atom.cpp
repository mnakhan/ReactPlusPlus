/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "atom.hpp"

namespace ext
{
  DECLARE_ATOM_TYPE(Trigger) atom::trigger;
  DECLARE_ATOM_TYPE(Ping) atom::ping;
  DECLARE_ATOM_TYPE(Pong) atom::pong;
  DECLARE_ATOM_TYPE(AllPingsSent) atom::allPingsSent;
  DECLARE_ATOM_TYPE(Delete) atom::terminate;
  DECLARE_ATOM_TYPE(Increment) atom::increment;
  DECLARE_ATOM_TYPE(QueryCounter) atom::queryCounter;
  DECLARE_ATOM_TYPE(Raise) atom::raise;
  DECLARE_ATOM_TYPE(Rearm) atom::rearm;
  DECLARE_ATOM_TYPE(Pulse) atom::pulse;
  DECLARE_ATOM_TYPE(ImmutableMessage) atom::constMsg;
}
