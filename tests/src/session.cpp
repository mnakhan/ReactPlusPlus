/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "picohttpparser.hpp"
#include "session.hpp"
#include "message_types.hpp"
#include "atom.hpp"
using namespace ext;

#ifdef LINUX
string webserver::session_t::header;
const char *webserver::session_t::contents = "";
size_t webserver::session_t::header_length = 0;
size_t webserver::session_t::contents_length = 0;

string webserver::session_t::_404_header;
const char *webserver::session_t::_404_contents = "";
size_t webserver::session_t::_404_header_length = 0;
size_t webserver::session_t::_404_contents_length = 0;

string webserver::session_t::_405_header;
const char *webserver::session_t::_405_contents = "";
size_t webserver::session_t::_405_header_length = 0;
size_t webserver::session_t::_405_contents_length = 0;

ext::user_statistics_t& webserver::session_t::stats = GLOBAL(user_statistics_t);
webserver::time_stamp_t& webserver::session_t::timeStamp = GLOBAL(webserver::time_stamp_t);

webserver::RequestStatus webserver::session_t::parse()
{
  char buffer[MAX_BUFFER_LENGTH] = {0};
  char *parser_entry = buffer, *recv_entry = buffer;
  size_t recv_remaining = ARRAY_SIZE(buffer), parser_window_size = 0, last_parser_window_size = 0;
  bool done = false, isPartialRequest = false;

  const char *method, *path;
  int minor_version;
  phr_header headers[32];
  size_t method_len, path_len, num_headers;
  num_headers = ARRAY_SIZE(headers);
  memset(headers, 0, sizeof (headers));

  if (pr.length > 0)
  {
    memcpy(buffer, pr.buffer, pr.length);
    parser_entry = buffer;
    parser_window_size = pr.length;
    recv_entry = buffer + pr.length;
    recv_remaining -= pr.length;
    pr.reset();
  }

  while (!done)
  {
    if (recv_remaining == 0) return RequestStatus::RS_BUFFER_OVERFLOW;

    ssize_t bytesRead;
    while ((bytesRead = ::recv(fd, recv_entry, recv_remaining, MSG_DONTWAIT)) == -1 && errno == EINTR);
    if (bytesRead == -1)
    {
      int e = errno;
      switch (e)
      {
        case EWOULDBLOCK:
          if (!isPartialRequest) return RequestStatus::RS_NO_DATA;
          pr.reset();
          memcpy(pr.buffer, parser_entry, parser_window_size);
          pr.length = parser_window_size;
          return RequestStatus::RS_PARTIAL_REQUEST;
        case EBADF:
        case ECONNRESET:
        default:
          return RequestStatus::RS_CONN_SEVERED;
      }
    }
    if (bytesRead == 0) return RequestStatus::RS_CONN_SEVERED;
    recv_entry += bytesRead;
    recv_remaining -= bytesRead;
    last_parser_window_size = parser_window_size;
    parser_window_size += bytesRead;
    bool requestsPending = true;
    while (requestsPending)
    {
      int pret = phr_parse_request(parser_entry, parser_window_size, &method, &method_len, &path, &path_len, &minor_version, headers, &num_headers, last_parser_window_size);

      isPartialRequest = false;
      switch (pret)
      {
        case -1:
          return RequestStatus::RS_PARSE_ERROR;
        case -2:
          isPartialRequest = true;
          break;
        default:
        {
          if (strncmp(method, "GET", method_len) == 0)
          {
            if (strncmp(path, DEFAULT_REQ_PATH, path_len) == 0)
            {
              if (!reply<true>(fd, 200)) return RequestStatus::RS_CONN_SEVERED;
            }
            else
            {
#ifdef DEBUG
              char requestPath[MAX_REQ_PATH_LEN];
              strncpy(requestPath, path, path_len);
              requestPath[path_len] = 0;
              scout << "not found --> " << requestPath << endl;
#endif // DEBUG
              if (!reply<true>(fd, 404)) return RequestStatus::RS_CONN_SEVERED;
            }
          }
          else
          {
#ifdef DEBUG
            char methodUsed[64];
            strncpy(methodUsed, method, method_len);
            methodUsed[method_len] = 0;
            scout << "method not supported --> " << methodUsed << endl;
#endif // DEBUG
            if (!reply<true>(fd, 405)) return RequestStatus::RS_CONN_SEVERED;
          }
        }

#ifdef USE_EDGE_TRIGGER
          ctx << atom::trigger; // mimick level-triggered epoll by sending a notification to self
#endif // USE_EDGE_TRIGGER
          return RequestStatus::RS_NO_DATA;

          num_headers = ARRAY_SIZE(headers);
          memset(headers, 0, sizeof (headers));
          break;
      }

      if (isPartialRequest) break;
      if ((int) parser_window_size > pret)
      {
        parser_entry += pret;
        last_parser_window_size = 0;
        parser_window_size -= pret;
      }
      else requestsPending = false;
    }
  }
  return RequestStatus::RS_CONN_SEVERED;
}

bool webserver::session_t::immediateContext(descriptor_t clientDescriptor)
{
  char buffer[4096];
  ssize_t bytesRead;
  while ((bytesRead = ::recv(clientDescriptor, buffer, ARRAY_SIZE(buffer), MSG_DONTWAIT)) == -1 && errno == EINTR);
  if (bytesRead == 0)
  {
    ::close(clientDescriptor);
    return false;
  }

  if (bytesRead == -1)
  {
    if (errno != EWOULDBLOCK)
    {
      ::close(clientDescriptor);
      return false;
    }
    return true;
  }

  const char *method, *path;
  size_t method_len, path_len, num_headers;
  int minor_version;
  phr_header headers[32];
  num_headers = ARRAY_SIZE(headers);

  int ret = phr_parse_request(buffer, bytesRead, &method, &method_len, &path, &path_len, &minor_version, headers, &num_headers, 0);
  if (bytesRead != ret)
  {
    ::close(clientDescriptor);
    return false;
  }

  if (strncmp(method, "GET", method_len) == 0)
  {
    if (strncmp(path, DEFAULT_REQ_PATH, path_len) == 0) reply<false>(clientDescriptor, 200);
    else
    {
#ifdef DEBUG
      char requestPath[MAX_REQ_PATH_LEN];
      strncpy(requestPath, path, path_len);
      requestPath[path_len] = 0;
      scout << "not found --> " << requestPath << endl;
#endif // DEBUG
      reply<false>(clientDescriptor, 404);
    }
  }
  else
  {
#ifdef DEBUG
    char methodUsed[64];
    strncpy(methodUsed, method, method_len);
    methodUsed[method_len] = 0;
    scout << "method not supported --> " << methodUsed << endl;
#endif // DEBUG
    reply<false>(clientDescriptor, 405);
  }

  for (size_t i = 0; i < num_headers; i++)
  {
    if (strncmp(headers[i].name, "Connection", headers[i].name_len) == 0
            && strncmp(headers[i].value, "close", headers[i].value_len) == 0)
    {
      close(clientDescriptor);
      return false;
    }
  }
  return true;
}

webserver::session_t::session_t(context_t& ctx_)
: behavior_t(ctx_), fd(-1), yieldNext(true)
{
  pr.reset();
}

webserver::session_t::~session_t()
{
#ifdef TRACE_DTOR
  scout << "~session_t: actor ID = " << ctx.getID() << ", descriptor = " << fd << endl;
#endif // TRACE_DTOR
}

void webserver::session_t::initialize(descriptor_t fd_)
{
  fd = fd_;
}

void webserver::session_t::receive(message_t *message, actor_id_t const& senderID)
{
  assert(fd != -1);
  if (!atom_t<True>::type<Trigger>(message))
  {
    if (atom_t<True>::type<Park>(message)) closeConnection();
    else if (atom_t<True>::type<Delete>(message))
    {
      closeConnection();
      ctx.quit();
    }
    return;
  }

  if (yieldNext)
  {
    yieldNext = false;
    YIELD();
  }

  RequestStatus status = parse();
  bool done = false;
  switch (status)
  {
    case RequestStatus::RS_NO_DATA:
      break;
    case RequestStatus::RS_PARSE_ERROR:
      scout << "parse error" << endl;
      done = true;
      break;
    case RequestStatus::RS_BUFFER_OVERFLOW:
      scout << "buffer overflow" << endl;
      done = true;
      break;
    case RequestStatus::RS_PARTIAL_REQUEST:
      scout << "partial request" << endl;
      break;
    case RequestStatus::RS_SEND_ERROR:
      scout << "send error" << endl;
      done = true;
      break;
    case RequestStatus::RS_CONN_SEVERED:
      //      scout << "connection severed" << endl;
      done = true;
      break;
    default:
      break;
  }

  if (done) closeConnection();
}

void webserver::session_t::closeConnection()
{
  yieldNext = true;
  ctx.park();
  ::close(fd);
}

void webserver::session_t::reset()
{
  {
    contents = (const char*) "\r\n\r\nHello, World!\r\n";
    contents_length = strlen(contents);

    header = "HTTP/1.1 200 OK\r\n";
    header += "Content-Length: " + toString(strlen(contents) - 4); // subtract length of 2 CRLF
    header += "\r\nContent-Type: text/plain; charset=UTF-8\r\nServer: 127.0.0.1\r\nDate: ";
    header_length = header.size();
  }

  {
    _404_contents = (const char*) "\r\n\r\nPage not found.\r\n";
    _404_contents_length = strlen(_404_contents);

    _404_header = "HTTP/1.1 404 Not Found\r\n";
    _404_header += "Content-Length: " + toString(strlen(_404_contents) - 4);
    _404_header += "\r\nContent-Type: text/plain; charset=UTF-8\r\nServer: 127.0.0.1\r\nDate: ";
    _404_header_length = _404_header.size();
  }

  {
    _405_contents = (const char*) "\r\n\r\nMethod not allowed.\r\n";
    _405_contents_length = strlen(_405_contents);

    _405_header = "HTTP/1.1 405 Method Not Allowed\r\n";
    _405_header += "Content-Length: " + toString(strlen(_405_contents) - 4);
    _405_header += "\r\nContent-Type: text/plain; charset=UTF-8\r\nServer: 127.0.0.1\r\nDate: ";
    _405_header_length = _405_header.size();
  }
}
#endif // LINUX
