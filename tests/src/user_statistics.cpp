/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "user_statistics.hpp"

void ext::user_statistics_t::reset()
{
  dtor_call_count = 0;
  isShuttingDown = false;
  n_laplace_actors = 0;
  n_laplace_messages = 0;
  n_qs_actors = 0;
  n_qs_messages = 0;
  n_immediate = 0;
  n_deffered = 0;
}
