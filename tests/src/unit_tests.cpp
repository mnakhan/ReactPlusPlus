/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "actor.hpp"
#include "atom.hpp"
#include "examples.hpp"
#include "policy.hpp"
#include "profiling.hpp"
#include "stage.hpp"
#include "unit_tests.hpp"
#include "tag_registry.hpp"

using namespace rpp;
using namespace io;

int ext::main([[maybe_unused]] int argc, [[maybe_unused]] char **argv)
{
  [[maybe_unused]] rpp::system_t& sys = test_suite_base::sys;
  [[maybe_unused]] printer_t& tscout = test_suite_base::tscout;
  [[maybe_unused]] random_t& rnd = THR_LOCAL(random_t);

  test_suite_base::initialize();
  test_suite suite;
  demo examples;

#ifdef TEST_SHUTDOWN_SEQ
#ifdef DEBUG
  suite.shutdown(3);
#else
  suite.shutdown(50);
#endif // DEBUG
#endif // TEST_SHUTDOWN_SEQ

#ifdef TEST_MIGRATION
  suite.migration(5);
#endif // TEST_MIGRATION

  [[maybe_unused]] auto loop = [&]
  {
    examples._1();
    examples._2();
    examples._3();
    suite.behavior();
    suite.typeMatching();
    suite.cluster();
    suite.synchronization(10);
    suite.promiseAndFuture();
    suite.timer();
    suite.polling();
    suite.signedStream();
    suite.messageFiltering();
    suite.flowControl();
    suite.broadcast();
    suite.forkedTrail();
#ifdef DEBUG
    suite.producerConsumer(5, 2, 1);
    suite.latency(5);
    suite.messageToSelf(5);
    suite.countingActor(5);
    suite.forkJoin_throughput(2, 2);
    suite.forkJoin_spawn(5);
    suite.big(2, 2);
    suite.partitionedBig(2, 2);
    suite.threadRing(2, 2);
    suite.quickSort(8, 1);
    suite.quickSort2(8, 1);
    suite.computeWorkload(4, 4);
    suite.laplaceExpansion(4, 1);
    suite.actorPool(10);
    suite.synchronousReply(3, 3);
    suite.diningPhilosophers(5, 10, 2);
    suite.messageTrail(1, 4);
#elif defined (USE_NATIVE_ACTORS) && defined(IA64) // RELEASE, NATIVE
    suite.producerConsumer(1_M, 20, 1);
    suite.latency(10_M);
    suite.messageToSelf(10_M);
    suite.countingActor(20_M);
    suite.forkJoin_throughput(1_K, 10_K);
    suite.forkJoin_spawn(1_M);
    suite.big(500, 50_K);
    suite.partitionedBig(500, 50_K);
    suite.threadRing(1_K, 10_M);
    suite.quickSort(20_M, 500_K);
    suite.quickSort2(1_G, 8);
    suite.computeWorkload(8, 1000);
    suite.laplaceExpansion(11, 8);
    suite.workStealing(steal_frequency_t::NORMAL, 1_K, 1_K);
    suite.actorPool(1_M);
    suite.synchronousReply(100_K, 1_M);
    suite.diningPhilosophers(100, 20_M, 100_K);
    suite.priorityAssignment(5, 30_M);
    suite.messageTrail(1_K, 10);
#elif !defined (USE_NATIVE_ACTORS) && defined(IA64) // RELEASE, DETACHED
    suite.producerConsumer(1_M, 20, 1);
    suite.latency(1_M);
    suite.messageToSelf(10_M);
    suite.countingActor(20_M);
    suite.forkJoin_throughput(1_K, 10_K);
    suite.forkJoin_spawn(10_K);
    suite.big(500, 50_K);
    suite.partitionedBig(500, 50_K);
    suite.threadRing(500, 1_M);
    suite.quickSort(20_M, 500_K);
    suite.computeWorkload(8, 1000);
    suite.laplaceExpansion(11, 8);
    suite.synchronousReply(100_K, 1_M);
    suite.messageTrail(1_K, 10);
#else
#endif // DEBUG
  };

  runTest(loop, 1);

  return 0;
}

void ext::test_suite::workStealing_s(WorkLoad type, uint32_t nActors, uint32_t nIterations)
{
  sys.clearStats();
  uint32_t nExpected = 2 * nIterations;
  using ContextType = typed_context_t<Consumer>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  vector<actor_id_t> ids = sys.spawnGroup<ContextType>(props, nActors);
  sys.initializeEach<Consumer>(ids, nExpected);
  actor_t<>::enableContexts<ContextType, Consumer>(ids);

  auto idle = [&]
  {
    for (uint32_t i = 0; i < nIterations; i++)
    {
      for_each(ids.cbegin(), ids.cend(), [](actor_id_t const& id)
      {
        actor_t<>::map(id) << atom::trigger;
      });
    }
  };

  auto busy = [&]
  {
    for_each(ids.cbegin(), ids.cend(), [nIterations](actor_id_t const& id)
    {
      actor_t<> actor = actor_t<>::map(id);
      for (uint32_t i = 0; i < nIterations; i++) actor << atom::trigger;
    });
  };

  thread sender1, sender2;
  switch (type)
  {
  case WorkLoad::IDLE:
    scout << "Workload 1: Idle Actors" << endl;
    sender1 = thread(idle);
    sender2 = thread(idle);
    break;
  case WorkLoad::BUSY:
    scout << "Workload 2: Busy Actors" << endl;
    sender1 = thread(busy);
    sender2 = thread(busy);
    break;
  case WorkLoad::MIXED:
    scout << "Workload 3: Mixed Actors" << endl;
    sender1 = thread(idle);
    sender2 = thread(busy);
    break;
  default:
    break;
  }

  joinAll(sender1, sender2);
  sys.waitForAll();
  scout << sys.report() << endl;
}

ext::test_suite::test_suite()
{
}

ext::test_suite::~test_suite()
{
}

void ext::test_suite::actorPool(uint32_t nActors)
{
  description() << "Test actor pooling and parking.";
  printHeader();
  capture();
  gc.reset();
  scout << "Spawning " << nActors << " actors, which park after the first message." << endl;

  actor_t<domain_t> *actors = new actor_t<domain_t>[nActors];
  using ContextType = typed_context_t<Simple>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  for (uint32_t i = 0; i < nActors; i++)
  {
    actors[i] = actor_t<domain_t>::spawn<ContextType>(props);
    actors[i].defaultBehavior()
      .on<int>([&]
    {
      return [&](context_t *ctx, int message, ...)
      {
#ifdef DEBUG
        scout << "received int: " << message << endl;
#endif // DEBUG
        ctx->park(); // clean the mailbox and go to blocked state
      };
    });
    actors[i].enableContext<ContextType>().withBehavior<Simple>();
  }

  scout << "Sending each 2 messages..." << endl;
  for (uint32_t i = 0; i < nActors; i++)
  {
    actors[i] | 100;
    actors[i] | 200; // will be ignored because actor parks after the first message
  }
  printFooter(false);

  scout << "Cleaning up " << nActors << " actors..." << endl;
  capture();
  delete[] actors;
  initialize();
  scout << "DTOR count = " << gc.dtor_call_count << endl;

  printFooter();
}

void ext::test_suite::behavior()
{
  description() << "Test context switch within the same actor.";
  printHeader();
  capture();

  using ContextType = TestActor<A, B, C>;
  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());

  actor_t<domain_t> actor = actor_t<domain_t>::spawn<ContextType>(props, 41);
  actor.initialize<ContextType>(42);
  actor.defaultBehavior()
    .on<Trigger>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "default::trigger" << endl;
      ctx->as<ContextType>().x++;
      ContextType::become<A>(ctx);
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });

  actor.enableContext<ContextType>().withDefault();
  for (int i = 0; i < 5; i++) actor << atom::trigger;
  actor | Delete{};
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::big(uint32_t nActors, uint32_t nMessages)
{
  description() << suffix(nActors) << " actors each send " << suffix(nMessages) << " pings to random recipients and replies to incoming pings with a pong. Total message count = " << suffix(nActors * nMessages * 2) << '.';
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nActors), toString(nMessages) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::big(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::broadcast()
{
  description() << "Test actor selection based on tags and message broadcast.";
  printHeader();
  capture();

  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());

  uint32_t nActors = 2; // 2 as native, 2 as detached
  vector<actor_t<native>> nativeActors(nActors);
  vector<actor_t<detached>> detachedActors(nActors);
  for (uint32_t i = 0; i < nActors; i++)
  {
    nativeActors[i] = actor_t<native>::spawn<>(props);
    nativeActors[i].defaultBehavior()
      .on<string>([&]
    {
      return [&](context_t *ctx, string const& s, actor_id_t const& senderID)
      {
        scout << "actor " << ctx->getID() << " received message: " << s << endl;
      };
    })
      .on<Delete>([&]
    {
      return [&](context_t *ctx, ...)
      {
        ctx->quit();
      };
    });

    if (i == 0)
    {
      sys.bind(nativeActors[i].getID(), "x");
      sys.bind(nativeActors[i].getID(), "y");
    }
    else
    {
      sys.bind(nativeActors[i].getID(), "a");
      sys.bind(nativeActors[i].getID(), "b");
    }

    nativeActors[i].enableContext().withDefault();

    detachedActors[i] = actor_t<detached>::spawn<>(props);
    detachedActors[i].defaultBehavior()
      .on<string>([&]
    {
      return [&](context_t *ctx, string const& s, actor_id_t const& senderID)
      {
        scout << "actor " << ctx->getID() << " received message: " << s << endl;
      };
    })
      .on<Delete>([&]
    {
      return [&](context_t *ctx, ...)
      {
        ctx->quit();
      };
    });

    if (i == 0)
    {
      sys.bind(detachedActors[i].getID(), "x");
      sys.bind(detachedActors[i].getID(), "y");
    }
    else
    {
      sys.bind(detachedActors[i].getID(), "a");
      sys.bind(detachedActors[i].getID(), "b");
    }

    detachedActors[i].enableContext().withDefault();
  }

  auto result =
    tag_registry_t::select<pair<actor_id_t, tag_t>>::from<native, detached>::where(
      [](actor_id_t const& id)
  {
    return true;
  },
      [](tag_t const& tag)
  {
    return true;
  });

  // display current map
  scout << "current map between IDs and tags:" << endl;
  ostringstream out;
  auto it = result.cbegin();
  while (it != result.cend())
  {
    out << "ID = " << it->first << " <--> tag = " << it->second << endl;
    it++;
  }
  scout << out.str() << endl;

  // broadcast to those tagged with x
  sys.broadcast(string("broadcast for actors tagged with 'x'"), tag_registry_t::select<actor_id_t>::from<native, detached>::where(
    [](actor_id_t const& id)
  {
    return true;
  },
    [](tag_t const& tag)
  {
    return tag.compare("x") == 0;
  }));

  // broadcast to those tagged with y
  sys.broadcast(string("broadcast for actors tagged with 'y'"), tag_registry_t::select<actor_id_t>::from<native, detached>::where(
    [](actor_id_t const& id)
  {
    return true;
  },
    [](tag_t const& tag)
  {
    return tag.compare("y") == 0;
  }));

  // broadcast to those tagged with a
  sys.broadcast(string("broadcast for actors tagged with 'a'"), tag_registry_t::select<actor_id_t>::from<native, detached>::where(
    [](actor_id_t const& id)
  {
    return true;
  },
    [](tag_t const& tag)
  {
    return tag.compare("a") == 0;
  }));

  // broadcast to those tagged with b
  sys.broadcast(string("broadcast for actors tagged with 'b'"), tag_registry_t::select<actor_id_t>::from<native, detached>::where(
    [](actor_id_t const& id)
  {
    return true;
  },
    [](tag_t const& tag)
  {
    return tag.compare("b") == 0;
  }));

  // broadcast delete message
  sys.broadcast<true>(Delete{}, tag_registry_t::select<actor_id_t>::from<native, detached>::where(
    [](actor_id_t const& id)
  {
    return true;
  },
    [](tag_t const& tag)
  {
    return true;
  }));
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::cluster()
{
  description() << "Spawn actors on different clusters. Actor IDs are printed in decimal-dotted notation (i.e. X.Y refers to actor #Y on cluster #X). No cluster prefix indicates a detached actor.";
  printHeader();
  capture();
  sys.restart(nullptr, 2, 4, 1); // 3 clusters, first has 2 workers, second has 4, third has 1

  location_t locations[3];
  for (uint32_t i = 0; i < ARRAY_SIZE(locations); i++) locations[i] = location_t::map(false, i);
  using ContextType = typed_context_t<Simple2>;

  // spawn 2 actors on the first cluster
  uint32_t nActors = 2;
  {
    vector<actor_t < domain_t>> actors = actor_t<domain_t>::spawnGroup<ContextType>(
      props_t::with<mailbox_type_t::DV>()
      .at(locations[0]), nActors);

    actor_t<domain_t>::initializeEach<Simple2>(actors, "init message");
    actor_t<domain_t>::enableContexts<ContextType, Simple2>(actors);
    actor_t<domain_t>::broadcast(42, actors);
    actor_t<domain_t>::broadcast(string("42"), actors);
    actor_t<domain_t>::broadcast(Delete{}, actors);
    sys.waitForAll();
  }
  scout << "--------------------------------" << endl;

  // spawn a single actor on the second cluster
  {
    actor_t<domain_t> actor = actor_t<domain_t>::spawn<ContextType>(
      props_t::with<mailbox_type_t::DV>()
      .at(locations[1]));

    actor.initialize<Simple2>("init message");
    actor.enableContext<ContextType>().withBehavior<Simple2>();

    actor
      << atom::trigger
      | 42
      | string("42")
      | Delete
    {
    };
    sys.waitForAll();
  }
  scout << "--------------------------------" << endl;

  {
    vector<actor_id_t> ids = sys.spawnGroup<ContextType>(
      props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
      .at(locations[2]), nActors);
#else
      .at(location_t::DetachedCluster), nActors);
#endif // USE_NATIVE_ACTORS

    sys.initializeEach<Simple2>(ids, "init message");
    actor_t<domain_t>::enableContexts<ContextType, Simple2>(ids);
    sys.broadcast(42, ids);
    sys.broadcast(string("42"), ids);
    sys.broadcast(Delete{}, ids);
    sys.waitForAll();
  }
  printFooter();

  initialize();
}

void ext::test_suite::computeWorkload(uint32_t matrixDimension, uint32_t nTasks)
{
  description() << "Run a compute benchmark involving matrix inversion and finding determinant.";
  printHeader();
  capture();
  using ContextType = typed_context_t<Collector>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
  actor_t<domain_t> collector = actor_t<domain_t>::spawn<ContextType>(props);
  collector.initialize<Collector>("data/output.txt", matrixDimension, nTasks);
  collector.enableContext<ContextType>().withBehavior<Collector>();
  collector << atom::trigger;
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::countingActor(uint32_t nMessages)
{
  description() << "A producer sends " << suffix(nMessages) << " increment messages to a counting actor, after which the cumulated value is queried and returned.";
  printHeader();
  capture();

  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nMessages) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::countingActor(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::diningPhilosophers(uint32_t nPhilosophers, uint32_t requiredThroughput, uint32_t windowSize)
{
  description() << suffix(nPhilosophers) << " dining philosophers who are required to eat " << suffix(requiredThroughput) << " times (cumulative) without starvation under the supervision of an arbiter. Window size is the number of fork allocation requests each philosopher is allowed to send at once and currently set to " << suffix(windowSize) << '.';
  printHeader();
  capture();

  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nPhilosophers), toString(requiredThroughput), toString(windowSize) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::diningPhilosophers(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::flowControl()
{
  description() << "Using a clock generator to enforce flow control on multiple channels.";
  printHeader();
  capture();

  {
    actor_system_t sys;
    auto& timer = sys().getTimer();

    uint32_t period = 500;

    auto props = props_t::with<mailbox_type_t::DV>()
      .at(location_t::map());

    using BehaviorType = TestChannel;
    using ContextType = typed_context_t<BehaviorType>;

    actor_t<domain_t> actor = actor_t<domain_t>::spawn<ContextType>(props);

    vector<channel_t> channels(2);
    for (uint32_t i = 0; i < channels.size(); i++)
    {
      channel_t& channel = channels[i] = channel_t::open<>(props);        // location in props will be ignored
      channel.defaultBehavior().on<Delete>([&]
      {
        return [&](context_t *ctx, ...)
        {
          ctx->quit();
        };
      });

      if (i == 0) channel.defaultBehavior().on<Trigger>([&]
      {
        return [&](context_t *ctx, ...)
        {
          scout << "received atom through channel 1" << endl;
        };
      });
      else channel.defaultBehavior().on<Trigger>([&]
      {
        return [&](context_t *ctx, ...)
        {
          scout << "received atom through channel 2" << endl;
        };
      });

      channel.enableContext().withDefault();
    }

    actor.initialize<BehaviorType>(channels[0], channels[1]);
    actor.enableContext<ContextType>().withBehavior<BehaviorType>();

    // push enough messages into each channel
    for (uint32_t j = 0; j < 1_K; j++) channel_t::broadcast<true>(Trigger{}, channels);
    auto token = timer.registerHandler<true>(period, new periodic_task_t<>([this, actor]
    {
      actor << atom::pulse;
    }));

    sleep_for(seconds(3));
    timer.cancel(token);
    actor << atom::terminate;
  }

  printFooter();
}

void ext::test_suite::forkJoin_spawn(uint32_t nActors)
{
  description() << "Spawn " << suffix(nActors) << " actors and send a message to each immediately after spawning.";
  printHeader();
  capture();

  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nActors) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::forkJoin_spawn(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::forkJoin_throughput(uint32_t nActors, uint32_t nMessagesPerActor)
{
  description() << "Spawn " << suffix(nActors) << " actors and send " << suffix(nMessagesPerActor) << " messages to each in a round-robin fashion. Total number of messages sent = " << suffix(nActors * nMessagesPerActor) << '.';
  printHeader();
  capture();

  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nActors), toString(nMessagesPerActor) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::forkJoin_throughput(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::forkedTrail()
{
  description() << "Forks a message trail down a binary tree of actors.";
  printHeader();
  capture();
  /*
    |-->d--|
    |      |
    |-->b--|      |
    |      |      |
    |      |-->e--|
    a--|             |-->h
    |      |-->f--|
    |      |      |
    |-->c--|      |
    |      |
    |-->g--|

    Create 4 distinct message trail from a to h that rewind back to a via exceptions, as following:
    a --> b --> d --> h --> ... a
    a --> b --> e --> h --> ... a
    a --> c --> f --> h --> ... a
    a --> c --> g --> h --> ... a
  */

  uint32_t nActors = 8;
  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());
  using BehaviorType = ForkedTrail;
  using ContextType = typed_context_t<BehaviorType>;

  char start = 'a';
  vector<actor_t<domain_t>> actors(nActors);
  for (uint32_t i = 0; i < nActors; i++)
  {
    auto& actor = actors[i] = actor_t<domain_t>::spawn<ContextType>(props);
    actor.initialize<BehaviorType>([&start, this, &actor]
    {
      auto name = toString(start++);
      sys.bind(actor.getID(), name);
      return name;
    }());
    actor.enableContext<ContextType>().withBehavior<BehaviorType>();
  }
  actors[0] || [&] { return message_t::beginTrail<string>(""); };
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::laplaceExpansion(uint32_t dimension, uint32_t maxTreeDepth)
{
  description() << "Compute the determinant of a " << dimension << 'x' << dimension << " matrix in parallel with a maximum tree depth of " << maxTreeDepth << '.';
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(dimension), toString(maxTreeDepth) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });
  profiling::benchmarks::laplaceExpansion(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::latency(uint32_t initialValue)
{
  description() << "An actor takes initial value " << suffix(initialValue) << ", decrements it and sends it to a partner. The partner does the same and returns the token. When it reaches 0, both actors terminate.";
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(initialValue), toString(1.0) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });
  profiling::benchmarks::pingPong(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::messageFiltering()
{
  description() << "Test message filtering based on predicates.";
  printHeader();
  capture();
  using ContextType = typed_context_t<Simple2>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> actor = actor_t<domain_t>::spawn<ContextType>(props);

  actor.defaultBehavior()
    .on<int>([&]
  {
    return [&](context_t *ctx, int x, ...)
    {
      scout << "received int: " << x << endl;
    };
  }, [](int x)
  {
    return x % 2 == 0;
  }) // only receive even integers
    .on<string>([&]
  {
    return [&](context_t *ctx, string const& x, ...)
    {
      scout << "received string: " << x << endl;
    };
  }, [](string const& x)
  {
    return x.find('a') == 0;
  }) // only receive strings that start with 'a'
    .any([&]
  {
    return [&](context_t *ctx, message_t *message, ...) // default handler, deals with all other types
    {
      vector<float> lst;
      message->extract(lst);
      scout << "received list: " << lst << endl;
    };
  }, [](const message_t *message)
  {
    return atom_t<>::type<vector<float>>(message); // only accept vector<float>
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, Delete, ...)
    {
      ctx->quit();
    };
  });

  actor.enableContext<ContextType>().withDefault();

  for (int i = 0; i < 5; i++) actor | i;
  actor
    | string("abcd")
    | string("does not start with an 'a'") // will be rejected
    | vector<int>({ 8, 7, 6 }) // will be rejected
    | vector<float>({ 10.0f, 11.0f, 12.0f })
    | Delete{};

  sys.waitForAll();
  printFooter();
}

void ext::test_suite::messageToSelf(uint32_t nMessages)
{
  description() << "An actor sends " << suffix(nMessages * 2) << " messages to itself, yielding every other time.";
  printHeader();
  capture();

  using ThisContext = typed_context_t<Spinner>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> actor = actor_t<domain_t>::spawn<ThisContext>(props);
  actor.initialize<Spinner>(nMessages);
  actor.enableContext<ThisContext>().withBehavior<Spinner>();
  actor << atom::trigger;
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::messageTrail(uint32_t expected, uint32_t chainLength)
{
  description() << "Creates a message trail that captures all sender IDs and message contents along its path. If an exception is thrown by any actor on the path, the message can travel back along the trail giving each sender an opportunity to fix the message and forward it to the exception source. In this example, the expected value is " << expected << " and actors are arranged in a chain of length = " << chainLength << ". Exception is raised by last actor in the chain and handled by the first.";
  printHeader();
  capture();

  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());

  using BehaviorType = TestException;
  using ContextType = typed_context_t<TestException>;

  vector<actor_t<domain_t>> actors(chainLength);
  for (uint32_t i = 0; i < chainLength; i++) actors[i] = actor_t<domain_t>::spawn<ContextType>(props);
  for (uint32_t i = 0; i < chainLength; i++)
  {
    actors[i].initialize<BehaviorType>(expected, i == 0, i == chainLength - 1, actors[(i + 1) % chainLength]);
    actors[i].enableContext<ContextType>().withBehavior<BehaviorType>();
  }

  actors[0] || [&] { return message_t::beginTrail(0); };

  sys.waitForAll();
  printFooter();
}

void ext::test_suite::migration(uint32_t nIterations)
{
  description() << "Migrates an actor back and forth between two clusters " << nIterations << " times on user prompt.";
  printHeader();
  capture();

  rpp_assert(sys.getClusterCount() >= 2, "At least two clusters need to present for this experiment.", GLOBAL(printer_t));

  scout << ThisThread::getPlatformString(true) << endl;
  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map(false, CLUSTER<0>{}));

  actor_t<> actor = actor_t<>::spawn<>(props);
  actor.defaultBehavior()
    .on<Trigger>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "Runtime info queried from within the actor:\n" << ThisThread::getPlatformString() << endl;
#ifndef DEBUG
      for (uint32_t i = 0; i < (uint32_t)100_M; i++) actor << atom::increment;
#endif // DEBUG
    };
  })
    .on<Increment>([&]
  {
    return [&](context_t *ctx, ...)
    {
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });

  actor.enableContext().withDefault();
  bool migrate = true;
  for (uint32_t i = 0; i < nIterations; i++)
  {
    scout << "Press any key..." << endl;
    RM_LF();

    if (migrate)
    {
      scout << "Migrating the actor from cluster 0 to cluster 1." << endl;
      actor.migrate(CLUSTER<1>{});
    }
    else
    {
      scout << "Return the actor from cluster 1 to cluster 0." << endl;
      actor.returnToOrigin();
    }

    migrate = !migrate;
    actor << atom::trigger;
  }

  scout << "Press any key to terminate the actor." << endl;
  RM_LF();
  actor.migrate(CLUSTER<1>{}); // verify that a migrated actor can be killed on a remote cluster
  actor << atom::terminate;

  sys.waitForAll();
  printFooter();
}

void ext::test_suite::partitionedBig(uint32_t nActors, uint32_t nMessages)
{
  description() << suffix(nActors) << " actors each send " << suffix(nMessages) << " pings to random recipients from the same cluster and replies to incoming pings with a pong. Total message count = " << suffix(nActors * nMessages * 2) << '.';
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nActors), toString(nMessages) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::partitionedBig(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::polling()
{
  description() << "Poll a channel to process messages by an external thread.";
  printHeader();
  capture();

  auto props = props_t::with<mailbox_type_t::DV>();
  channel_t channel = channel_t::open<>(props);
  channel.defaultBehavior()
    .on<Trigger>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "received atom" << endl;
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });
  channel.enableContext().withDefault();

  for (int i = 0; i < 5; i++) channel << atom::trigger;
  channel | Delete{};

  thread t = thread([channel]
  {
    while (channel.poll()) channel.process();
    scout << "done" << endl;
  });

  joinAll(t);
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::priorityAssignment(uint32_t multiplier, uint32_t windowSize)
{
  // If window size is too small, effects of priority assignment are not visible. Adjust as necessary when running in release mode.
  uint32_t dispatchSize = 1;
  description() << "Test assignment of priorities to actors by adjusting the dispatch size. Each actor increments its own counter. The second actor has a dispatch size " << multiplier << " times higher than the first, which should be reflected by ratio of counter values. Since each actor does an equal amount of work as specified by the window size, the final ratio approaches 1.";
  printHeader();
  capture();

  sys.restart(nullptr, 1); // single-threaded execution simplifies this experiment
  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map())
    .withDispatchSize(dispatchSize);

  uint32_t nActors = 2;
  vector<actor_t < domain_t >> actors(nActors);

  // spawn first actor
  actors[0] = actor_t<domain_t>::spawn<>(props);

  // spawn the second actor with higher priority
  props.withDispatchSize(dispatchSize * multiplier);
  actors[1] = actor_t<domain_t>::spawn<>(props);

  vector<atomic < uint >> counters(nActors);
  for (uint32_t i = 0; i < nActors; i++)
  {
    actors[i].defaultBehavior()
      .on<uint32_t>([&]
    {
      return [&](context_t *ctx, uint32_t actorIndex, ...)
      {
        // actors normally should not write to captured variables this way (violation of actor model)
        // these are statistical counters, so aren't considered part of the actors' internal states

        counters[actorIndex].fetch_add(1); // increment respective counter
      };
    })
      .on <Delete>([&]
    {
      return [&](context_t *ctx, ...)
      {
        ctx->quit();
      };
    });
    actors[i].enableContext<>().withDefault();
  }

  atomic<bool> startAll = false, observerDone = false;

  // stream an equal number of messages to both actors from separate threads
  auto stream = [&](uint32_t actorIndex)
  {
    while (!startAll);
    for (uint32_t i = 0; i < windowSize; i++)
    {
      actors[actorIndex] | actorIndex;
    }
  };

  // print out counter values and their ratios at regular intervals
  auto observe = [&]
  {
    while (!startAll);
    while (!observerDone)
    {
      uint32_t x1 = counters[0].load(), x2 = counters[1].load();
      scout << "x1 = " << x1 << ", x2 = " << x2;
      if (x1 == 0) scout << endl;
      else scout << ", x2 / x1 = " << x2 / (float)x1 << endl;
      sleep_for(seconds(1));
    }
  };

  vector<thread> streamers(nActors);
  uint32_t index = 0;
  for_each(streamers.begin(), streamers.end(), [&](auto& streamer)
  {
    streamer = thread(stream, index++);
  });

  auto observer = thread(observe);
  startAll = true;

  for_each(streamers.begin(), streamers.end(), [&](auto& streamer)
  {
    streamer.join();
  }); // streamers are done

  actor_t<domain_t>::broadcast(Delete{}, actors);
  sys.waitForAll(); // actors are done

  observerDone = true;
  observer.join(); // observer is done

  // final report
  uint32_t x1 = counters[0].load(), x2 = counters[1].load();
  scout << "x1 = " << x1 << ", x2 = " << x2;
  if (x1 == 0) scout << endl;
  else scout << ", x2 / x1 = " << x2 / (float)x1 << endl;
  printFooter();

  initialize(); // restore default system configuration
}

void ext::test_suite::producerConsumer(uint32_t nMessagesPerProducer, uint32_t nProducers, uint32_t nConsumers)
{
  description() << nProducers << " producer(s) each send " << suffix(nMessagesPerProducer) << " messages to each of " << nConsumers << " consumer(s). Total messages = " << suffix(nMessagesPerProducer * nProducers * nConsumers) << '.';
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nMessagesPerProducer), toString(nProducers), toString(nConsumers) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });
  profiling::benchmarks::producerConsumer(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::promiseAndFuture()
{
  description() << "Block on the future linked to a promise made by an actor.";
  printHeader();
  capture();


  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());
  using PromiseType = Promise<double, Trigger, string, string>;
  uint32_t nActors = 2;
  vector<actor_t<domain_t>> actors(nActors);
  vector<PromiseType> promises(nActors);
  vector<future_t<double>> futures(nActors);

  // using default behavior
  actors[0] = actor_t<domain_t>::spawn<>(props);
  auto& actor1 = actors[0];
  actor1.defaultBehavior()
    .on<synchronize_t>([&]
  {
    return [&](context_t *ctx, synchronize_t const& sync, ...)
    {
      sync();
    };
  })
    .on <PromiseType>([&]
  {
    return [&](context_t *ctx, PromiseType p, ...)
    {
      scout << "actor " << ctx->getID() << " received a promise, payload = " << toString(p.data) << endl;
      double first, second;
      if (parse<double>(get<0>(p.data), first) && parse<double>(get<1>(p.data), second))
        p.deliver(first + second);
    };
  })
    .on <Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "terminating actor " << ctx->getID() << endl;
      ctx->quit();
    };
  });

  actor1.enableContext().withDefault();

  // using a named behavior
  using ContextType = typed_context_t<Simple2>;
  actors[1] = actor_t<domain_t>::spawn<ContextType>(props);
  auto& actor2 = actors[1];
  actor2.enableContext<ContextType>().withBehavior<Simple2>();

  {
    barrier_t scope(-1, actor1, actor2);
    // trigger a parse error
    for (uint32_t i = 0; i < nActors; i++)
    {
      auto& promise = promises[i];
      promise.pack(string("25.0"), string("junk"));
      futures[i] = actors[i] + promise;
    }

    // alternative syntax
    // future_n = sys.make(promise_n, actor_n.getID());

    for (uint32_t i = 0; i < nActors; i++)
    {
      double ret = futures[i].block(-1.0);
      scout << "actor " << actors[i].getID() << " returned value " << ret << " to global context" << endl;
    }

    // parse, add, reply
    for (uint32_t i = 0; i < nActors; i++)
    {
      auto& promise = promises[i];
      promise.pack(string("25.0"), string("10.0"));
      futures[i] = actors[i] + promise;
    }

    for (uint32_t i = 0; i < nActors; i++)
    {
      double ret = futures[i].block(-1.0);
      scout << "actor " << actors[i].getID() << " returned value " << ret << " to global context" << endl;
    }
  }

  actor_t<domain_t>::broadcast<true>(Delete{}, actors);
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::quickSort(uint32_t size, uint32_t threshold)
{
  description() << "Parallel quicksort applied to a list of " << suffix(size) << " integer elements with a threshold value of " << suffix(threshold) << ". The threshold indicates the maximum size of a partition that is sequentially sorted by an actor.";
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(size), toString(threshold) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::quickSort(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::quickSort2(uint32_t size, uint32_t depth)
{
  description() << "Parallel quicksort applied to an array of " << suffix(size) << " integer elements with a maximum tree depth of " << suffix(depth) << '.';
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(size), toString(depth) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::quickSort2(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::sandbox()
{

}

void ext::test_suite::shutdown(uint32_t nIterations)
{
  description() << "Restart the system asynchronously from multiple threads with different cluster configurations. Number of iterations = " << nIterations << '.';
  printHeader();
  capture();

  auto sr = [nIterations](int index)
  {
    for (uint32_t i = 0; i < nIterations; i++)
    {
      switch (index)
      {
      case 1:
        sys.restart(nullptr, 4, 4); // 2 clusters each with 4 workers
        break;
      case 2:
        sys.restart(nullptr, 2, 4, 2); // 3 clusters with 2, 4 and 2 workers respectively
        break;
      }
      uint32_t j = i + 1;
#ifdef DEBUG
      scout << "restarter " << index << ", iterations = " << j << endl;
#else
      if (j > 0 && j % 10 == 0) scout << "restarter " << index << ", iterations = " << j << endl;
#endif // DEBUG
    }
  };

  thread t1 = thread(sr, 1), t2 = thread(sr, 2);
  joinAll(t1, t2);

  // restore original configuration
  initialize();
  printFooter();
}

void ext::test_suite::signedStream()
{
  description() << "Sign a message stream with a sender ID.";
  printHeader();
  capture();

  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());
  actor_t<domain_t> actor = actor_t<domain_t>::spawn<>(props);
  actor.sign(actor_id_t(0, 71)); // sign with a sender ID
  actor.defaultBehavior()
    .on<Trigger>([&]
  {
    return [&](context_t *ctx, Trigger, actor_id_t const& senderID)
    {
      scout << "received trigger from " << senderID << endl;
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });
  actor.enableContext().withDefault();

  actor << atom::trigger;
  actor | Trigger{};
  actor | Delete{};

  sys.waitForAll();
  printFooter();
}

void ext::test_suite::synchronization(uint32_t nMessages)
{
  description() << "Send a batch of messages and wait until actor is done processing them.";
  printHeader();
  capture();

  using ContextType = typed_context_t<Simple2>;
  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());

  actor_t<domain_t> a1 = actor_t<domain_t>::spawn<ContextType>(props);
  a1.enableContext<ContextType>().withBehavior<Simple2>();

  actor_t<domain_t> a2 = actor_t<domain_t>::spawn<ContextType>(props);
  a2.enableContext<ContextType>().withBehavior<Simple2>();

  {
    barrier_t scope(3000, a1, a2);
    for (uint32_t i = 0; i < nMessages; i++)
    {
      a1 | i;                           // stream 0 ... N - 1 to first actor
      a2 | (i + nMessages);             // stream N ... 2N - 1 to second actor
    }
  }

  // Won't go past this point until the above messages have been processed by both actors.
  scout << "sending delete message" << endl;
  a1 | Delete{};
  a2 | Delete{};
  sys.waitForAll();
  printFooter();
}

void ext::test_suite::synchronousReply(uint32_t nRequests, uint32_t nStrayMessages)
{
  description() << "Test synchronous request/reply.";
  printHeader();
  capture();

  using ServerContext = typed_context_t<Server>;
  using ClientContext = typed_context_t<Client>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> server = actor_t<domain_t>::spawn<ServerContext>(props),
    client = actor_t<domain_t>::spawn<ClientContext>(props);

  client.initialize<Client>(server.getID(), nRequests);
  server.enableContext<ServerContext>().withBehavior<Server>();
  client.enableContext<ClientContext>().withBehavior<Client>();

  policy_t::enable<dispatch_size::single>();
  client << atom::trigger;
  for (uint32_t i = 0; i < nStrayMessages; i++) client | string("stray message");
  client << atom::terminate;
  sys.waitForAll();
  policy_t::disable<dispatch_size::single>();
  printFooter();
}

void ext::test_suite::threadRing(uint32_t nActors, uint32_t initialValue)
{
  description() << "A ring of " << suffix(nActors) << " actors decrements and pass around a token with initial value " << suffix(initialValue) << " until it reaches 0.";
  printHeader();
  capture();
  vector<string> lstArgs = { "internal", toString(MAX_HW_THREADS), toString(nActors), toString(initialValue) };
  const char *argv[MAX_ARG_LENGTH];
  int argc = 0;
  for_each(lstArgs.cbegin(), lstArgs.cend(), [&argv, &argc](string const& arg)
  {
    argv[argc++] = arg.c_str();
  });

  profiling::benchmarks::threadRing(argc, const_cast<char**> (argv));
  printFooter();
}

void ext::test_suite::timer()
{
  description() << "Timer test with periodic and oneshot registration.";
  printHeader();
  capture();
  rpp::timer_t& timer = sys.getTimer();

  /*
   * Timer thread sends periodic notification once per period.
   * On receiving it, actor requests oneshot notification after an interval. Therefore, this oneshot event is implicitly periodic.
   * The periodic event is canceled after a while.
   */

  uint32_t period = 500, interval = 2000; // in milliseconds

  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());
  actor_t<domain_t> actor = actor_t<domain_t>::spawn<>(props);
  actor.defaultBehavior()
    .on <PeriodicNotification>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "received periodic atom" << endl;
    };
  })
    .on <OneShotNotification>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "received oneshot atom" << endl;
    };
  })
    .on <Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });
  actor.enableContext().withDefault();

  auto token = timer.registerHandler<true>(period, new periodic_task_t<>([this, actor]
  {
    actor | PeriodicNotification{};
  }));


  timer.registerHandler<false>(interval, new one_shot_task_t([this, actor]
  {
    actor | OneShotNotification{};
  }));

  sleep_for(seconds(3));
  timer.cancel(token);
  actor | Delete{};

  sys.waitForAll();
  printFooter();
}

void ext::test_suite::typeMatching()
{
  description() << "Tests type matching.";
  printHeader();
  capture();

  auto props = props_t::with<mailbox_type_t::DV>()
    .at(location_t::map());

  actor_t<domain_t> actor = actor_t<domain_t>::spawn<>(props);
  actor.defaultBehavior()
    .on <Trigger>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "received atom" << endl;
    };
  })
    .on<int>([&]
  {
    return [&](context_t *ctx, int message, ...)
    {
      scout << "received int: " << message << endl;
      *ctx | Trigger{};
    };
  })
    .on<string>([&]
  {
    return [&](context_t *ctx, string const& message, ...)
    {
      scout << "received string: " << message << endl;
    };
  })
    .on<CustomMessage>([&]
  {
    return [&](context_t *ctx, CustomMessage const& message, ...)
    {
      scout << "received custom message: " << message.i << ' ' << message.s << endl;
      // switch behavior
      ctx->defaultBehavior().on<short>([&]
      {
        return [&](context_t *current, short message, ...)
        {
          scout << "received short: " << message << endl;
        };
      });
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });

  CustomMessage cmsg;
  cmsg.i = 42;
  cmsg.s = "fourty-two";

  actor.enableContext<>().withDefault();
  string original = "another string";
  (actor << atom::trigger)
    | 42
    | string("fourty-two")
    | cmsg
    | (short)42
    | move(original)
    | Delete{};

  rpp_assert(original.length() == 0, "MOVE operation failed.", tscout);
  sys.waitForAll();

  printFooter();
}

void ext::test_suite::workStealing(rpp::steal_frequency_t frequency, uint32_t nActors, uint32_t nIterations)
{
  description() << "Measure work-stealing frequency with different workloads." << endl
    << "Total actors = " << nActors << ", number of iterations = " << nIterations << ", sender threads = 2, total number of messages = nActors x nIterations x 2 = " << suffix(nActors * nIterations * 2) << endl << "Stealing frequncy set to " << frequency << '.';
  printHeader();
  capture();
  policy_t::enable<dispatch_size::single>()
    .enableWith<work_stealing>(frequency);

  workStealing_s(WorkLoad::IDLE, nActors, nIterations);
  workStealing_s(WorkLoad::BUSY, nActors, nIterations);
  workStealing_s(WorkLoad::MIXED, nActors, nIterations);

  // restore default
  policy_t::reset<dispatch_size::single>()
    .reset<work_stealing>();

  printFooter();
}
