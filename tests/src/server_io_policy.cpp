/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "atom.hpp"
#include "policy.hpp"
#include "self_pipe.hpp"
#include "server_io_policy.hpp"
#include "session.hpp"
#include "signaler.hpp"
#include "tcp_listener.hpp"

using namespace generic_ext;
using namespace rpp;
using namespace ext;
using namespace io;

#ifdef LINUX
webserver::garage_t& webserver::server_io_policy_t::garage = GLOBAL(webserver::garage_t);
rpp::system_t& webserver::server_io_policy_t::sys = GLOBAL(rpp::system_t);
random_t& webserver::server_io_policy_t::rnd = THR_LOCAL(random_t);
system_statistics_t& webserver::server_io_policy_t::stats = GLOBAL(system_statistics_t);

#ifdef TRACE_IO_EVENT

#endif // TRACE_IO_EVENT

webserver::server_io_policy_t::server_io_policy_t()
{
  serverIP = environment::ipAddress;
  listeningPort = environment::portNumber;
  nClusters = environment::clusterCount;
  L1Size = environment::workerCountPerCluster[0];
  rpp_assert(nClusters > 0 && L1Size > 0, "Attempted to initialize server I/O policy with incorrect parameters.", GLOBAL(printer_t));

  for (cluster_id_t clusterID = 1; clusterID < nClusters; clusterID++)
  {
    worker_id_t nWorkerInCluster = environment::workerCountPerCluster[clusterID];
    rpp_assert(L1Size == nWorkerInCluster, "Each cluster must have an equal number of workers for this policy.", GLOBAL(printer_t));
  }

  cas = new cluster_attribute_set_t(nClusters);

#ifndef USE_EDGE_TRIGGER
  spin = 512; // max spin when level trigger is used
#endif // USE_EDGE_TRIGGER

  pinSignaler = environment::pinSignaler;
  pinSession = environment::pinSession;
  maxActorsPerCluster = environment::maxStandbyActorsPerCluster;
  backlog = environment::backlog;
  enableDirectSignaling = environment::enableDirectSignaling;
  enableRestlessSpin = environment::enableRestlessSpin;
  restlessSpin = environment::restlessSpinCount;
  enableImmediateContext = environment::enableImmediateContext;
  enableDistributedPolling = environment::enableDistributedPolling;
  enableStatsReporter = environment::enableStatsReporter;

  garage.initialize(nClusters, L1Size, maxActorsPerCluster);
  listeners = new tcp_listener_t[nClusters];
  if (enableDistributedPolling) peerStates = nullptr;
  else peerStates = new l1_peer_states[nClusters];

  self_pipe_t& pipe = GLOBAL(self_pipe_t);
  selfPipe = pipe.getReadEnd();

  default_io_policy_t::initialize();

  webserver::time_stamp_t& timeStamp = GLOBAL(webserver::time_stamp_t);
  timeStamp.initialize();
}

webserver::server_io_policy_t::~server_io_policy_t()
{
  delete cas;
  cas = nullptr;
  for (cluster_id_t i = 0; i < nClusters; i++) listeners[i].close();
  delete [] listeners;
  listeners = nullptr;

  delete [] peerStates;
  peerStates = nullptr;

  webserver::time_stamp_t& timeStamp = GLOBAL(webserver::time_stamp_t);
  timeStamp.quit();
}

void webserver::server_io_policy_t::churn(cluster_id_t clusterID)
{
  tcp_listener_t& listener = listeners[clusterID];
  while (true)
  {
    descriptor_t connDescriptor = listener.accept();
    if (connDescriptor == -1) break;

    if (enableImmediateContext)
    {
      if (!respond(connDescriptor)) continue;
    }

    location_t location = location_t::map(pinSession, clusterID);

    actor_id_t garageID(clusterID, connDescriptor);
    actor_t<>& session = garage.select<io_actor_type_t::session>(garageID);
    if (!session) // if not there, create this session
    {
      location.workerID = connDescriptor % L1Size;
      auto props = props_t::with<mailbox_type_t::DV>()
              .at(location);
      using ContextType = typed_context_t < session_t >;
      session = actor_t<>::spawn<ContextType> (props);
      session.initialize<session_t>(connDescriptor);
      session.enableContext<ContextType>().withBehavior<session_t>();
    }
    else location.workerID = session.getWorkerID();

    if (!enableDistributedPolling) location.workerID = 0; // use the first
    watchDescriptor<true>(location, connDescriptor); // location can also be used to identify an L1 poller
    session << atom::trigger;
  }
}

bool webserver::server_io_policy_t::postLaunch(cluster_id_t clusterID)
{
  if (enableDistributedPolling)
  {
    for (worker_id_t w = 0; w < L1Size; w++)
    {
      auto props = props_t::with<mailbox_type_t::DV>()
              .at(location_t::map(pinSignaler, clusterID, w));
      using ContextType = typed_context_t < signaler_t >;

      // create signaler
      actor_id_t garageID(clusterID, w);
      auto& signaler
              = garage.select<io_actor_type_t::signaler>(garageID)
              = actor_t<>::spawn<ContextType> (props);

      signaler.initialize<signaler_t>(this);
      signaler.enableContext<ContextType>().withBehavior<signaler_t>();
    }
  }
  else
  {
    auto props = props_t::with<mailbox_type_t::DV>()
            .at(location_t::map(pinSignaler, clusterID));

    using L0 = l0_signaler_t;
    using L1 = l1_signaler_t;
    using C0 = typed_context_t < L0 >;
    using C1 = typed_context_t < L1 >;

    // spawn L0 signaler
    auto& l0Signaler
            = garage.select<io_actor_type_t::signaler>(actor_id_t{clusterID, L1Size})
    = actor_t<>::spawn<C0> (props);

    // spawn L1 signalers
    peerStates[clusterID].peerHandles = vector <actor_t<>>(L1Size);
    peerStates[clusterID].readyStates = vector < atomic<bool>>(L1Size);
    for (worker_id_t w = 0; w < L1Size; w++)
    {
      auto props2 = props_t::with<mailbox_type_t::DV>()
              .at(location_t::map(true, clusterID, w)); // must pin these, otherwise chuarning throughput goes down

      peerStates[clusterID].peerHandles[w]
              = garage.select<io_actor_type_t::signaler>(actor_id_t{clusterID, w})
      = actor_t<>::spawn<C1> (props2);

      peerStates[clusterID].readyStates[w].store(false);
    }

    // enable actors
    l0Signaler.initialize<L0>(this);
    l0Signaler.enableContext<C0>().withBehavior<L0>();

    for (worker_id_t w = 0; w < L1Size; w++)
    {
      auto& l1Signaler = selectL1Signaler(clusterID, w);
      l1Signaler.initialize<L1>(this, w);
      l1Signaler.enableContext<C1>().withBehavior<L1>();
    }
  }

  return default_io_policy_t::postLaunch(clusterID);
}

bool webserver::server_io_policy_t::preLaunch(cluster_id_t clusterID)
{
  bool ret = default_io_policy_t::preLaunch(clusterID);

  tcp_listener_t& listener = listeners[clusterID];
  listener.open(serverIP, listeningPort, backlog);
  rpp_assert(listener.isRunning(), "FATAL: Could not start listener. You may not have permission or there's another service listening at this port.", GLOBAL(printer_t));
  descriptor_t acceptorFD = listener.getDescriptor();
  default_io_policy_t::watchOnLevel<1, true>(clusterID, acceptorFD);

  watchDescriptor<true>(location_t::map(true, clusterID, 0), selfPipe);

#ifdef TRACE_IO_EVENT
  scout << "TRACE: started listener " << acceptorFD << " on cluster " << clusterID << " with backlog = " << backlog << endl;
#endif // TRACE_IO_EVENT

  descriptor_t L0Handle = getL0Handle(clusterID);
  vector<descriptor_t> noBlockHandles = getL1Handles(clusterID);

  cas->append(clusterID, L0Handle, noBlockHandles, acceptorFD);

  if (cas->isDone())
  {
#ifdef DEBUG
    scout << '\n' << cas->toString();
    if (enableStatsReporter) scout << "\n  Press ENTER to display scheduler stats any time.";
    scout << endl;
#else
    if (enableStatsReporter) scout << "Press ENTER to display scheduler stats any time." << endl;
#endif // DEBUG
  }

  if (enableRestlessSpin) policy_t::enableWith<work_stealing>(steal_frequency_t::RESTLESS, 1.0);
  return ret;
}

cluster_id_t webserver::server_io_policy_t::getClusterCount() const
{
  return nClusters;
}

worker_id_t webserver::server_io_policy_t::getWorkerCount(cluster_id_t clusterID) const
{
  return L1Size;
}

bool webserver::server_io_policy_t::onBlock(cluster_id_t clusterID, worker_id_t workerID)
{
  if (enableRestlessSpin) policy_t::enableWith<work_stealing>(steal_frequency_t::RESTLESS, 1.0);
  if (enableDistributedPolling) return default_io_policy_t::onBlock(clusterID, workerID);
  return true;
}

bool webserver::server_io_policy_t::onEvent(cluster_id_t clusterID, worker_id_t workerID, io_event_t const& event)
{
  epoll_event& e = *event.details;
  tcp_listener_t& listener = listeners[clusterID];
  switch (event.poller_type)
  {
    case poller_type_t::MASTER:
    {
      if (e.events & EPOLLIN)
      {
        worker_id_t workerID = (worker_id_t) e.data.u64;
        if (enableDirectSignaling) sys.raise(clusterID, workerID);
        if (enableDistributedPolling)
        {
          // notify the signaler to execute L1 polling
          actor_id_t garageID(clusterID, workerID);
          garage.select<io_actor_type_t::signaler>(garageID)
                  << atom::trigger;
        }
        else selectL0Signaler(clusterID) << atom::trigger;
      }
      else if (e.events & (EPOLLHUP | EPOLLERR))
        scout << "WARNING: master poller from cluster " << clusterID << " reported an error" << endl;
    }
      break;
    case poller_type_t::SLAVE:
    {
      descriptor_t descriptor = e.data.fd;
      if (descriptor == listener.getDescriptor()) churn(clusterID);
      else if (descriptor == selfPipe) garage.terminate(clusterID);
      else if (e.events & (EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLHUP))
      {
        // raise last-level event-handlers, which are session actors
        actor_id_t garageID(clusterID, descriptor);
        garage.select<io_actor_type_t::session>(garageID)
                << atom::trigger;
      }
    }

      break;
    default:
      assert(false);
      break;
  }
  return true;
}

bool webserver::server_io_policy_t::onIdle(cluster_id_t clusterID, worker_id_t workerID)
{
  bool retval = true;
  if (enableDistributedPolling) retval = default_io_policy_t::onIdle(clusterID, workerID);
  if (!enableRestlessSpin) return retval;
  if (rnd.nextInt<uint32_t>(0U, restlessSpin) == 0U) policy_t::enableWith<work_stealing>(steal_frequency_t::HIGH, 1.0);
  return retval;
}

// immediate context
// bypass epoll registration for weighhttp benchmark

bool webserver::server_io_policy_t::respond(descriptor_t clientDescriptor)
{
  return session_t::immediateContext(clientDescriptor);
}

actor_t<>& webserver::server_io_policy_t::selectL0Signaler(cluster_id_t clusterID)
{
  actor_id_t garageID(clusterID, L1Size);
  return garage.select<io_actor_type_t::signaler>(garageID);
}

actor_t<>& webserver::server_io_policy_t::selectL1Signaler(cluster_id_t clusterID, worker_id_t workerID)
{
  actor_id_t garageID(clusterID, workerID);
  return garage.select<io_actor_type_t::signaler>(garageID);
}

void webserver::server_io_policy_t::set_l1_alarm(cluster_id_t clusterID, worker_id_t workerID)
{
  assert(peerStates);
  peerStates[clusterID].readyStates[workerID].store(false);
}

void webserver::server_io_policy_t::raise_l1_alarm(cluster_id_t clusterID)
{
  assert(peerStates);
  for (worker_id_t w = 0; w < L1Size; w++)
  {
    bool expected = false;
    if (peerStates[clusterID].readyStates[w].compare_exchange_strong(expected, true, memory_order_acq_rel, memory_order_relaxed))
      peerStates[clusterID].peerHandles[w] << atom::raise;
  }
}

#endif // LINUX
