/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "garage.hpp"
#include "atom.hpp"
using namespace ext;

webserver::garage_t::garage_t()
: nClusters(0), L1Size(0), maxStandby(0), signalers(nullptr), sessions(nullptr), initialized(false)
{
}

webserver::garage_t::~garage_t()
{
#ifdef TRACE_DTOR
  scout << "~garage_t" << endl;
#endif // TRACE_DTOR

  for (cluster_id_t i = 0; i < nClusters; i++)
  {
    delete[] signalers[i];
    delete[] sessions[i];
  }

  delete[] signalers;
  delete[] sessions;
}

void webserver::garage_t::initialize(cluster_id_t nClusters_, worker_id_t L1Size_, local_id_t maxStandby_)
{
  nClusters = nClusters_;
  L1Size = L1Size_;
  maxStandby = maxStandby_;

  signalers = new actor_t<>*[nClusters];
  sessions = new actor_t<>*[nClusters];
  for (cluster_id_t i = 0; i < nClusters; i++)
  {
    signalers[i] = new actor_t<>[L1Size + 1]; // +1 for l0 signaler when centralized polling is active
    sessions[i] = new actor_t<>[maxStandby];
  }
  initialized = true;
}

void webserver::garage_t::terminate(cluster_id_t clusterID)
{
  for (worker_id_t w = 0; w < L1Size + 1; w++)
  {
    if (signalers[clusterID][w])
      signalers[clusterID][w] << atom::terminate;
  }

  for (local_id_t lid = 0; lid < maxStandby; lid++)
  {
    if (sessions[clusterID][lid])
      sessions[clusterID][lid] << atom::terminate;
  }
}
