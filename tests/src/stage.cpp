/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "actor.hpp"
#include "stage.hpp"
#include "typed_context.hpp"

void ext::A::receive(message_t *message, actor_id_t const& senderID)
{
  if (atom_t<>::type<Delete>(message))
  {
    ctx.quit();
    return;
  }

  if (!atom_t<>::type<Trigger>(message)) return;
  else scout << "A::receive" << endl;
  x++;
  as<ContextType>().x++;

  ContextType::become<B>(this);
}

void ext::B::receive(message_t *message, actor_id_t const& senderID)
{
  if (atom_t<>::type<Delete>(message))
  {
    ctx.quit();
    return;
  }

  if (!atom_t<>::type<Trigger>(message)) return;
  else scout << "B::receive" << endl;
  x++;
  as<ContextType>().x++;

  ContextType::become<C>(this);
}

void ext::C::receive(message_t *message, actor_id_t const& senderID)
{
  if (atom_t<>::type<Delete>(message))
  {
    ctx.quit();
    return;
  }

  if (!atom_t<>::type<Trigger>(message)) return;
  else scout << "C::receive" << endl;
  x++;
  as<ContextType>().x++;

  ctx.defaultBehavior()
    .on<Trigger>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "default 2::trigger" << endl;
      ctx->as<ContextType>().x++;
      ContextType::become<A>(ctx);
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });

  ContextType::becomeDefault(this);
}

ext::Producer::Producer(context_t& ctx_)
  : behavior_t(ctx_), nMessages(0)
{
}

ext::Producer::~Producer()
{
#ifdef TRACE_DTOR
  scout << "~Producer" << endl;
#endif // TRACE_DTOR
}

void ext::Producer::initialize(uint32_t nMessages_, vector<actor_id_t> const& consumerIDs_)
{
  nMessages = nMessages_;
  consumerIDs = consumerIDs_;
}

void ext::Producer::receive(message_t *message, actor_id_t const& senderID)
{
  message->on<Trigger>([this](...)
  {
    for_each(consumerIDs.cbegin(), consumerIDs.cend(), [&, this](actor_id_t const& id)
    {
      actor_t<domain_t> actor = actor_t<domain_t>::map(id);
      for (uint32_t i = 0; i < nMessages; i++) actor << atom::trigger;
    });
    ctx.quit();
  });
}

ext::Consumer::Consumer(context_t& ctx_)
  : behavior_t(ctx_), received(0), expected(0)
{
}

ext::Consumer::~Consumer()
{
#ifdef TRACE_DTOR
  scout << "~Consumer" << endl;
#endif // TRACE_DTOR
}

void ext::Consumer::initialize(uint32_t expected_)
{
  expected = expected_;
}

void ext::Consumer::receive(message_t *message, actor_id_t const& senderID)
{
  message->on<Trigger>([this](...)
  {
#ifdef DEBUG
    scout << "trigger" << endl;
#endif // DEBUG
    received++;
    if (received == expected) ctx.quit();
  });
}

void ext::Spinner::_atom()
{
  ctx | 0U; // first message to itself
}

void ext::Spinner::_uint32_t(uint32_t message)
{
  nReceived++;
#ifdef DEBUG
  scout << message << endl;
#endif // DEBUG
  if (nReceived == nExpected) ctx.quit();
  else ctx | (message + 1);
}

ext::Spinner::Spinner(context_t& ctx_)
  : behavior_t(ctx_), yield(true), nReceived(0), nExpected(0)
{
}

ext::Spinner::~Spinner()
{
#ifdef TRACE_DTOR
  scout << "~Spinner" << endl;
#endif // TRACE_DTOR
}

void ext::Spinner::initialize(uint32_t nExpected_)
{
  nExpected = nExpected_;
}

void ext::Spinner::receive(message_t *message, actor_id_t const& senderID)
{
  if ((yield = !yield))
  {
#ifdef DEBUG
    scout << "yielding" << endl;
#endif // DEBUG
    YIELD();
  }

  message->forwardIf<uint32_t>(this, &Spinner::_uint32_t)
    || message->forwardAtomIf<Trigger>(this, &Spinner::_atom);
}

void ext::PingPong::_uint32_t(uint32_t token)
{
#ifdef DEBUG
  scout << token << endl;
#endif // DEBUG
  if (token > 0) partner | (token - 1);
  else
  {
#ifdef DEBUG
    scout << "done" << endl;
#endif // DEBUG
    partner | Delete{};
    partner.reset();
    ctx.quit();
  }
}

void ext::PingPong::_Delete(Delete const&)
{
  ctx.quit();
}

ext::PingPong::PingPong(context_t& ctx_)
  : behavior_t(ctx_)
{
}

ext::PingPong::~PingPong()
{
#ifdef TRACE_DTOR
  scout << "~PingPong" << endl;
#endif // TRACE_DTOR
}

void ext::PingPong::initialize(actor_t<domain_t> const& partner_)
{
  partner = partner_;
}

void ext::PingPong::receive(message_t *message, actor_id_t const& senderID)
{
  message->forwardIf<uint32_t>(this, &PingPong::_uint32_t)
    || message->forwardIf<Delete>(this, &PingPong::_Delete);
}

ext::Counter::Counter(context_t& ctx_)
  : behavior_t(ctx_), currentValue(0)
{
}

ext::Counter::~Counter()
{
#ifdef TRACE_DTOR
  scout << "~Counter" << endl;
#endif // TRACE_DTOR
}

void ext::Counter::receive(message_t *message, actor_id_t const& senderID)
{
  if (atom_t<True>::type<Increment>(message))
  {
#ifdef DEBUG
    scout << "increment" << endl;
#endif // DEBUG
    currentValue++;
  }
  else if (atom_t<True>::type<QueryCounter>(message))
  {
    actor_t<domain_t> producer = actor_t<domain_t>::map(senderID);
    producer | currentValue;
    ctx.quit();
  }
}

random_t& ext::Big::rnd = THR_LOCAL(random_t);

void ext::Big::cleanup()
{
  // release references
  size_t nActors = recipients.size();
  for (uint32_t i = 0; i < nActors; i++) recipients[i].reset();
  sink.reset();
  ctx.quit();
}

bool ext::Big::mayExit() const
{
  return flags.termReceived && flags.allPongsReceived;
}

ext::Big::Big(context_t& ctx_)
  : behavior_t(ctx_), maxMessages(0), totalPongsReceived(0)
{
  flags = {};
}

ext::Big::~Big()
{
#ifdef TRACE_DTOR
  scout << "~Big" << endl;
#endif // TRACE_DTOR
}

void ext::Big::initialize(uint32_t maxMessages_, vector<actor_t<domain_t>> const& recipients_, actor_t<domain_t> const& sink_, uint32_t selfIndex_)
{
  maxMessages = maxMessages_;
  recipients = recipients_;
  sink = sink_;
  selfIndex = selfIndex_;
  selfID = ctx.getID();
  for (auto& r : recipients) r.sign(selfID);
  sink.sign(selfID);
}

void ext::Big::receive(message_t *message, actor_id_t const& senderID)
{
#ifdef BYPASS_HTABLE_FOR_BIG
  if (message->extract(pingWithIndex))
  {
    recipients[pingWithIndex.senderIndex] << atom::pong;
#ifdef DEBUG
    scout << senderID << " <--pong-- " << selfID << endl;
#endif // DEBUG 
  }
#else
  if (atom_t<True>::type<Ping>(message))
  {
    actor_t<domain_t> sender = actor_t<domain_t>::map(senderID);
    sender << atom::pong;
#ifdef DEBUG
    scout << senderID << " <--pong-- " << selfID << endl;
#endif // DEBUG 
  }
#endif // BYPASS_HTABLE_FOR_BIG
  else if (atom_t<True>::type<Pong>(message))
  {
    totalPongsReceived++;
    if (totalPongsReceived == maxMessages) flags.allPongsReceived = 1;
    if (mayExit()) cleanup();
  }
  else if (atom_t<True>::type<Trigger>(message))
  {
    size_t nActors = recipients.size();
    for (uint32_t i = 0; i < maxMessages; i++)
    {
      size_t actorIndex = rnd.nextInt<size_t>(0, nActors - 1);
#ifdef BYPASS_HTABLE_FOR_BIG
      recipients[actorIndex] | PingWithIndex{ selfIndex };
#else
      recipients[actorIndex] << atom::ping;
#endif // BYPASS_HTABLE_FOR_BIG
#ifdef DEBUG
      scout << selfID << " --ping--> " << recipients[actorIndex].getID() << endl;
#endif
    }
    sink << atom::allPingsSent;
  }
  else if (atom_t<True>::type<Delete>(message))
  {
#ifdef DEBUG
    scout << "actor " << selfID << " received delete message" << endl;
#endif // DEBUG
    flags.termReceived = 1;
    if (mayExit()) cleanup();
  }
}

ext::Sink::Sink(context_t& ctx_)
  : behavior_t(ctx_), nReceived(0), nActors(0)
{
}

ext::Sink::~Sink()
{
#ifdef TRACE_DTOR
  scout << "~Sink" << endl;
#endif // TRACE_DTOR
}

void ext::Sink::initialize(vector<actor_t<domain_t>> const& recipients_)
{
  recipients = recipients_;
  nActors = recipients.size();
  selfID = ctx.getID();
  for (auto& r : recipients) r.sign(selfID);
}

void ext::Sink::receive(message_t *message, actor_id_t const& senderID)
{
  if (!atom_t<True>::type<AllPingsSent>(message)) return;
  nReceived++;
  if (nReceived < nActors) return;
  for (uint32_t i = 0; i < nActors; i++) recipients[i] << atom::terminate;
  ctx.quit();
}

ext::ThreadRingNode::ThreadRingNode(context_t& ctx)
  : behavior_t(ctx)
{
}

ext::ThreadRingNode::~ThreadRingNode()
{
#ifdef TRACE_DTOR
  scout << "~ThreadRingNode" << endl;
#endif // TRACE_DTOR
}

void ext::ThreadRingNode::initialize(actor_t<domain_t> const& next_)
{
  next = next_;
}

void ext::ThreadRingNode::receive(message_t *message, actor_id_t const& senderID)
{
  uint32_t token;
  if (message->extract(token))
  {
#ifdef DEBUG
    scout << "actor " << ctx.getID() << " received " << token << endl;
#endif // DEBUG

    if (token > 0) next | (token - 1);
    else next << atom::terminate;
  }
  else if (atom_t<True>::type<Delete>(message))
  {
    next << atom::terminate;
    next.reset();
    ctx.quit();
  }
}

void ext::Collector::checkProgress()
{
  if (expectedInverses > 0 || expectedDeterminants > 0) return;
  ofstream out(outputPath);
  if (!out)
  {
    scout << "Error writing to output file " << outputPath << endl;
    ctx.quit();
    return;
  }

  scout << "Writing to output file " << outputPath << endl;
  for (uint32_t i = 0; i < numberOfTasks; i++)
  {
    out << "original\n"
      << matrices[i] << endl
      << "inverse\n"
      << inverses[i]
      << "\ndeterminant\n"
      << determinants[i]
      << "\n--------------------------------------\n\n";
  }
  out.close();
  ctx.quit();
}

void ext::Collector::collectDeterminant(pair<uint32_t, double> const& reply)
{
  determinants[reply.first] = reply.second;
  if (expectedDeterminants) expectedDeterminants--;
  checkProgress();
}

void ext::Collector::collectInverse(pair<uint32_t, mat_d_t> const& reply)
{
  inverses[reply.first] = reply.second;
  if (expectedInverses) expectedInverses--;
  checkProgress();
}

void ext::Collector::spawnWorkers()
{
  scout << "Generating " << numberOfTasks << ' ' << matrixDimension << 'x' << matrixDimension << " random matrices..." << endl;
  for (uint32_t i = 0; i < numberOfTasks; i++) matrices[i] = mat_d_t::random(matrixDimension, matrixDimension, -1.0, 1.0);

  scout << "Spawning compute actors..." << endl;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_id_t selfID = ctx.getID();
  for (uint32_t i = 0; i < numberOfTasks; i++)
  {
    actor_t<domain_t> inverse = actor_t<domain_t>::spawn<Compute < Inverse >>(props, selfID, i),
      determinant = actor_t<domain_t>::spawn<Compute < Determinant >>(props, selfID, i);

    inverse.defaultBehavior()
      .on<mat_d_t>([&]
    {
      return [&](context_t *ctx, mat_d_t const& matrix, ...)
      {
        using ContextType = Compute<Inverse>;
        auto& typedContext = ctx->as<ContextType>();

        actor_t<domain_t> collector = actor_t<domain_t>::map(typedContext.generatorID);
        mat_d_t result = matrix;
        try // not all matrices are invertible
        {
          result.inverse();
        }
        catch (logic_error const& e)
        {
          scout << e.what() << endl;
        }
        collector | pair<uint32_t, mat_d_t>(typedContext.matrixIndex, result); // send back the result
        ctx->quit();
      };
    });

    determinant.defaultBehavior()
      .on<mat_d_t>([&]
    {
      return [&](context_t *ctx, mat_d_t const& matrix, ...)
      {
        using ContextType = Compute<Determinant>;
        auto& typedContext = ctx->as<ContextType>();

        actor_t<domain_t> collector = actor_t<domain_t>::map(typedContext.generatorID);
        double determinant = 0.0;
        try // only square matrices have determinants
        {
          determinant = matrix.getDeterminant();
        }
        catch (logic_error const& e)
        {
          scout << e.what() << endl;
        }
        collector | pair<uint32_t, double>(typedContext.matrixIndex, determinant);
        ctx->quit();
      };
    });

    inverse.enableContext<Compute<Inverse>>().withDefault();
    determinant.enableContext<Compute<Determinant>>().withDefault();

    inverse | matrices[i];
    determinant | matrices[i];
  }
}

ext::Collector::Collector(context_t& ctx_)
  : behavior_t(ctx_)
{
}

ext::Collector::~Collector()
{
#ifdef TRACE_DTOR
  scout << "~Collector" << endl;
#endif // TRACE_DTOR
}

void ext::Collector::receive(message_t *message, actor_id_t const& senderID)
{
  message->forwardAtomIf<Trigger>(this, &Collector::spawnWorkers)
    || message->forwardIf<pair<uint32_t, mat_d_t >>(this, &Collector::collectInverse)
    || message->forwardIf<pair<uint32_t, double>>(this, &Collector::collectDeterminant);
}

void ext::Collector::initialize(string const& outputPath_, uint32_t matrixDimension_, uint32_t numberOfTasks_)
{
  outputPath = outputPath_;
  matrixDimension = matrixDimension_;
  numberOfTasks = expectedInverses = expectedDeterminants = numberOfTasks_;
  matrices.resize(numberOfTasks);
  inverses.resize(numberOfTasks);
  determinants.resize(numberOfTasks);
}

ext::user_statistics_t& ext::Compute2::gc = GLOBAL(user_statistics_t);

void ext::Compute2::replyToParent() const
{
  actor_t<domain_t> parent = actor_t<domain_t>::map(parentID);
  parent | ReplyType(rowIndexOfMinor, columnIndexOfMinor, determinant);
  ctx | Delete{};
  gc.n_laplace_messages.fetch_add(2);
}

uint32_t ext::Compute2::maxTreeDepth = 1;

ext::Compute2::Compute2(context_t& ctx_)
  : behavior_t(ctx_), determinant(0.0), nReplies(0)
{
  gc.n_laplace_actors++;
}

ext::Compute2::~Compute2()
{
#ifdef TRACE_DTOR
  scout << "~Compute2" << endl;
#endif // TRACE_DTOR
}

void ext::Compute2::initialize(actor_id_t parentID_, size_t rowIndexOfMinor_, size_t columnIndexOfMinor_)
{
  parentID = parentID_;
  rowIndexOfMinor = rowIndexOfMinor_;
  columnIndexOfMinor = columnIndexOfMinor_;
}

void ext::Compute2::receive(message_t *message, actor_id_t const& senderID)
{
  message->on<mat_d_t>([this](mat_d_t matrix_)
  {
    matrix = move(matrix_);
    size_t rc = matrix.getRowCount(), cc = matrix.getColumnCount();
    assert(rc == cc && rc > 0);
    if (rc <= maxTreeDepth)
    {
      determinant = matrix.getDeterminant();
      replyToParent();
      return;
    }

    size_t i = 0;
    for (size_t j = 0; j < cc; j++)
    {
      mat_d_t subMatrix = matrix.getSubmatrix(i, j);
      auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
        .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
        .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
      actor_t<domain_t> child = actor_t<domain_t>::spawn<typed_context_t<Compute2>>(props);
      child.initialize<Compute2>(ctx.getID(), i, j);
      child.enableContext<typed_context_t<Compute2>>().withBehavior<Compute2>();
      child | subMatrix;
      gc.n_laplace_messages++;
    }
  })
    .on<ReplyType>([this](ReplyType const& reply)
  {
    nReplies++;
    size_t r = get<0>(reply), c = get<1>(reply);
    double detSubMatrix = get<2>(reply);
    double cofactor = matrix[r][c] * detSubMatrix;
    if ((r + c) % 2 != 0) cofactor *= -1.0;
    determinant += cofactor;
    if (nReplies == matrix.getColumnCount()) replyToParent();
  })
    .on<Delete>([this](Delete)
  {
    ctx.quit();
  });
}

ext::Simple::Simple(context_t& ctx_)
  : behavior_t(ctx_)
{
}

ext::Simple::~Simple()
{
#ifdef TRACE_DTOR
  scout << "~Simple" << endl;
#endif // TRACE_DTOR
  user_statistics_t& gc = GLOBAL(user_statistics_t);
  gc.dtor_call_count++;
}

void ext::Simple2::_trigger()
{
  scout << "Actor " << ctx.getID() << " received trigger" << endl;
}

void ext::Simple2::_int(int x)
{
  scout << "Actor " << ctx.getID() << " received int: " << x << endl;
}

void ext::Simple2::_uint(uint32_t x)
{
  scout << "Actor " << ctx.getID() << " received unsigned int: " << x << endl;
}

void ext::Simple2::_string(string const& x)
{
  scout << "Actor " << ctx.getID() << " received string: " << x << endl;
}

void ext::Simple2::_delete()
{
  scout << "terminating actor " << ctx.getID() << endl;
  ctx.quit();
}

void ext::Simple2::_Delete(Delete const&)
{
  _delete();
}

ext::Simple2::Simple2(context_t& ctx_)
  : behavior_t(ctx_)
{
}

ext::Simple2::~Simple2()
{
#ifdef TRACE_DTOR
  scout << "~Simple2: " << ctx.getID() << endl;
#endif // TRACE_DTOR
  user_statistics_t& gc = GLOBAL(user_statistics_t);
  gc.dtor_call_count++;
}

void ext::Simple2::initialize(string const& initMessage)
{
  scout << "Initializing " << ctx.getID() << " with \"" << initMessage << "\"" << endl;
}

void ext::Simple2::receive(message_t *message, actor_id_t const& senderID)
{
  message->forwardAtomIf<Trigger>(this, &Simple2::_trigger)
    || message->forwardIf<int>(this, &Simple2::_int)
    || message->forwardIf<uint32_t>(this, &Simple2::_uint)
    || message->forwardIf<string>(this, &Simple2::_string)
    || message->forwardAtomIf<Delete>(this, &Simple2::_delete)
    || message->forwardIf<Delete>(this, &Simple2::_Delete)
    || [&]
  {
    Promise<double, Trigger, string, string> p;
    string promise_input1, promise_input2;
    if (p.unpack(message, promise_input1, promise_input2))
    {
      scout << "actor " << ctx.getID() << " received a promise, payload = {" << promise_input1 << ", " << promise_input2 << '}' << endl;
      double first, second;
      if (parse<double>(promise_input1, first) && parse<double>(promise_input2, second))
        p.deliver(first + second);
    }
    else synchronize(message);
    return true;
  }();
}

ext::Server::Server(context_t& ctx_)
  : behavior_t(ctx_)
{
}

ext::Server::~Server()
{
#ifdef TRACE_DTOR
  scout << "~Server" << endl;
#endif // TRACE_DTOR
}

void ext::Server::receive(message_t *message, actor_id_t const& senderID)
{
  if (atom_t<>::type<Delete>(message))
  {
    ctx.quit();
    return;
  }

  string request;
  if (!message->isSynchronous() || !message->extract(request)) return;
#ifdef DEBUG
  scout << "server <-- " << request << endl;
  sleep_for(milliseconds(500)); // emulate a delay on the server-side
#endif // DEBUG

  actor_t<domain_t> client = actor_t<domain_t>::map(senderID);
  client.reply(toUppercase(request), message, ctx.getID());
}

void ext::Client::echo()
{
  if (nCompleted < nRequests) waitForReply<string>("echo " + toString(nCompleted), serverID);
  else actor_t<domain_t>::map(serverID) << atom::terminate;
}

ext::Client::Client(context_t& ctx_)
  : behavior_t(ctx_), nRequests(0), nCompleted(0)
{
}

ext::Client::~Client()
{
#ifdef TRACE_DTOR
  scout << "~Client" << endl;
#endif // TRACE_DTOR
}

void ext::Client::initialize(actor_id_t const& serverID_, uint32_t nRequests_)
{
  serverID = serverID_;
  nRequests = nRequests_;
  nCompleted = 0;
}

void ext::Client::receive(message_t *message, actor_id_t const& senderID)
{
  message->on<Trigger>([this](...)
  {
    echo();
  }).on<string>([this, senderID](string const& reply)
  {
#ifdef DEBUG
    scout << "client <-- " << reply << endl;
#endif // DEBUG
    if (senderID != serverID) return;
    nCompleted++;
    echo();
  }).on<Delete>([this](...)
  {
    ctx.quit();
  });
}

ext::Arbiter::Arbiter()
  :nPhilosophers(0), states(nullptr)
{
}

ext::Arbiter::~Arbiter()
{
#ifdef TRACE_DTOR
  scout << "~Arbiter" << endl;
#endif // TRACE_DTOR
  delete[] states;
}

void ext::Arbiter::initialize(vector<actor_t<domain_t>> const& philosophers_)
{
  philosophers = philosophers_;
  nPhilosophers = (uint32_t)philosophers.size();
  states = new PhilosopherState[nPhilosophers];
  for (uint32_t i = 0; i < nPhilosophers; i++) states[i] = PhilosopherState::Thinking;
}

string ext::Arbiter::toString() const
{
  ostringstream out;
  for (uint32_t i = 0; i < nPhilosophers; i++)
  {
    if (states[i] == PhilosopherState::Eating) out << 'E';
    else if (states[i] == PhilosopherState::Thinking)out << 'T';
    else assert(false);
    if (i < nPhilosophers - 1) out << ' ';
  }
  return out.str();
}

bool ext::Arbiter::acquire(uint32_t philosopherLocation)
{
  if (leftOf(philosopherLocation) == PhilosopherState::Eating
    || rightOf(philosopherLocation) == PhilosopherState::Eating || states[philosopherLocation] != PhilosopherState::Thinking)
  {
#ifdef DEBUG
    scout << "DENIED: acquire from philosopher at = " << philosopherLocation << ", table status = " << toString() << endl;
#endif // DEBUG
    return false;
  }
  states[philosopherLocation] = PhilosopherState::Eating;
#ifdef DEBUG
  scout << "philosopher at " << philosopherLocation << " resumed eating" << ", table status = " << toString() << endl;
#endif // DEBUG
  return true;
}

bool ext::Arbiter::release(uint32_t philosopherLocation)
{
  if (states[philosopherLocation] != PhilosopherState::Eating) return false;
  states[philosopherLocation] = PhilosopherState::Thinking;
#ifdef DEBUG
  scout << "philosopher at " << philosopherLocation << " resumed thinking" << ", table status = " << toString() << endl;
#endif // DEBUG
  return true;
}

ext::PhilosopherState ext::Arbiter::leftOf(uint32_t philosopherLocation) const
{
  return states[(philosopherLocation + nPhilosophers - 1) % nPhilosophers];
}

ext::PhilosopherState ext::Arbiter::rightOf(uint32_t philosopherLocation) const
{
  return states[(philosopherLocation + 1) % nPhilosophers];
}

ext::BeginCycle::BeginCycle(context_t& ctx_)
  :behavior_t(ctx_), context(nullptr)
{
}

ext::BeginCycle::~BeginCycle()
{
#ifdef TRACE_DTOR
  scout << "~BeginCycle" << endl;
#endif // TRACE_DTOR
}

void ext::BeginCycle::initialize()
{
  context = typedContext<ContextType>();
  ContextType::become<BeginCycle>(&ctx);
}

void ext::BeginCycle::receive(message_t *message, actor_id_t const& senderID)
{
  message->on<Trigger>([this](...)
  {
    actor_t<domain_t>::broadcast(ArbiterReady{}, context->philosophers);
    ContextType::become<Allocate>(&ctx);
  });
}

ext::Allocate::Allocate(context_t& ctx_)
  :behavior_t(ctx_), context(nullptr),
  numRequestsExpected(0), numRequestsProcessed(0),
  currentThroughput(0), requiredThroughput(0), failedAttempts(0)
{
}

ext::Allocate::~Allocate()
{
#ifdef TRACE_DTOR
  scout << "~Allocate" << endl;
#endif // TRACE_DTOR
}

void ext::Allocate::initialize(uint32_t requiredThroughput_, uint32_t windowSize)
{
  context = typedContext<ContextType>();
  numRequestsExpected = context->nPhilosophers * windowSize;
  requiredThroughput = requiredThroughput_;
  currentThroughput = failedAttempts = 0;
}

void ext::Allocate::receive(message_t *message, actor_id_t const& senderID)
{
  if (message->extract<Acquire>(acq))
  {
    if (!context->acquire(acq.philosopherLocation))
    {
      failedAttempts++;
      ctx | acq;
    }
    return;
  }


  if (!message->extract<Release>(rel)) return;

  if (!context->release(rel.philosopherLocation))
  {
    ctx | rel;
    return;
  }

  numRequestsProcessed++;
  if (numRequestsProcessed < numRequestsExpected) return;

  currentThroughput += numRequestsProcessed;
  numRequestsProcessed = 0;

  if (currentThroughput < requiredThroughput)
  {
    ctx << atom::trigger;
    ContextType::become<BeginCycle>(&ctx);
    return;
  }

  actor_t<domain_t>::broadcast(Delete{}, context->philosophers);
  scout << "arbiter exiting, total requests processed = " << requiredThroughput << ", number of retries = " << failedAttempts << " (" << failedAttempts * 100.0 / (double)requiredThroughput << "%)" << endl;

  ctx.quit();
}

ext::TestChannel::TestChannel(context_t& ctx_)
  :behavior_t(ctx_)
{
}

ext::TestChannel::~TestChannel()
{
  ch1.reset();
  ch2.reset();
}

void ext::TestChannel::initialize(channel_t const& ch1_, channel_t const& ch2_)
{
  ch1 = ch1_;
  ch2 = ch2_;
}

void ext::TestChannel::receive(message_t *message, actor_id_t const& senderID)
{
  message->on<Pulse>([this](...)
  {
    channel_t::processEach(1, ch1);     // remove 1 message per trigger
    channel_t::processEach(2, ch2);
  }).on<Delete>([this](...)
  {
    channel_t::drainEach(ch1, ch2);
    ctx.quit();
  });
}

ext::TestException::TestException(context_t& ctx_)
  :behavior_t(ctx_)
{
}

ext::TestException::~TestException()
{
}

void ext::TestException::initialize(int32_t expected_, bool isFirst_, bool isLast_, actor_t<domain_t> const& next_)
{
  expected = expected_;
  isFirst = isFirst_;
  isLast = isLast_;
  next = next_;
  next.sign(ctx.getID());
}

void ext::TestException::receive(message_t *message, actor_id_t const& senderID)
{
  if (!message->mayThrow())
  {
    if(atom_t<>::type<Delete>(message))
    {
      if (!isLast) next << atom::terminate;
      next.reset();
      ctx.quit();
    }
    return;
  }

#ifdef DEBUG
  scout << message->toString() << endl;
#endif // DEBUG

  int x = 0;
  message->peek(x);

  if (!isLast)
  {
    if (!message->isRaised()) next || [&] { return message->link<int>(); };
    else
    {
      if (!isFirst) throw message;
      ctx.resumeTrailWith<int>(message, x + 1);
    }
    return;
  }

  if (x != expected)
  {
#ifdef DEBUG
    scout << "final actor received unexpected message: " << x << endl;
#endif // DEBUG
    throw message;
  }

  scout << "final actor received expected message: " << x << endl;
  next << atom::terminate;
}

ext::ForkedTrail::ForkedTrail(context_t& ctx_)
  :behavior_t(ctx_), isRoot(false), isSink(false), nReceived(0)
{
}

void ext::ForkedTrail::initialize(string const& name_)
{
  name = name_;
  switch (name[0])
  {
  case 'a':
    receiverTags = { "b", "c" };
    isRoot = true;
    break;
  case 'b':
    receiverTags = { "d", "e" };
    break;
  case 'c':
    receiverTags = { "f", "g" };
    break;
  case 'd':
  case 'e':
  case 'f':
  case 'g':
    receiverTags = { "h" };
    break;
  case 'h':
    isSink = true;
    break;
  }
}

void ext::ForkedTrail::receive(message_t *message, actor_id_t const& senderID)
{
  message->on<Delete>([this](...)
  {
    ctx.quit();
  }).any([this, message]
  {
    assert(message->mayThrow());
    if (isSink) throw message;
    if (!message->isRaised())
    {
      string contents;
      message->peek(contents);

      auto receiverIDs = tag_registry_t::select<actor_id_t>::from<native, detached>::where(ALL_IDS,
        [this](tag_t const& tag)
      {
        for (size_t i = 0; i < receiverTags.size(); i++)
          if (tag.compare(receiverTags[i]) == 0)
            return true;
        return false;
      });

      for_each(receiverIDs.cbegin(), receiverIDs.cend(), [&](auto const& id)
      {
        auto receiver = actor_t<domain_t>::map(id);
        receiver.sign(ctx.getID());
        receiver || [&] { return message->fork<string>(contents + name); };
      });
    }
    else
    {
      if (!isRoot) throw message;
      scout << message->toString() << "\n" << endl;
      nReceived++;
      if (nReceived == 4) sys.broadcast<true>(Delete{}, tag_registry_t::selectAll<actor_id_t>());
    }
  });


}
