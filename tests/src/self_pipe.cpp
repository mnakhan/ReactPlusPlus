/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "self_pipe.hpp"

#ifdef LINUX
webserver::self_pipe_t::self_pipe_t()
{
  int ret = pipe(endpoints);
  assert(ret != -1);
  int flags = fcntl(endpoints[0], F_GETFL);
  flags |= O_NONBLOCK;
  ret = fcntl(endpoints[0], F_SETFL, flags);
  assert(ret != -1);
  flags = fcntl(endpoints[1], F_GETFL);
  flags |= O_NONBLOCK;
  ret = fcntl(endpoints[1], F_SETFL, flags);
  assert(ret != -1);
}

webserver::self_pipe_t::~self_pipe_t()
{
  close(endpoints[0]);
  close(endpoints[1]);
}

descriptor_t webserver::self_pipe_t::getReadEnd() const
{
  return endpoints[0];
}

descriptor_t webserver::self_pipe_t::getWriteEnd() const
{
  return endpoints[1];
}
#endif // LINUX
