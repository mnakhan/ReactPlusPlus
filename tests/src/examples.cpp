/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "actor.hpp"
#include "atom.hpp"
#include "examples.hpp"
#include "message_types.hpp"
#include "system.hpp"
#include "typed_context.hpp"

using namespace ext;
using namespace io;

void ext::demo::_1()
{
  description() << "Example 1";
  printHeader();
  capture();

  struct Switch { int i; string s; };
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
  actor_t<domain_t> actor = actor_t<domain_t>::spawn<>(props);  // find a scheduling location automatically
  actor.defaultBehavior()   // actor behavior, which is a set of message handlers
    .on<Trigger>([&]
  {
    // context_t usually holds actor state, here we leave it unused for simplicity
    // the ellipsis ... expands to message body (in this case an atom of a known type so it is not required) and sender ID
    return [&](context_t *ctx, ...)
    {
      scout << "received Trigger" << endl;
    };
  })
    .on<int>([&]
  {
    return [&](context_t *ctx, int message, ...)
    {
      scout << "received int: " << message << endl;
    };
  })
    .on<Switch>([&]
  {
    return [&](context_t *ctx, Switch const& message, ...)
    {
      scout << "received Switch: " << message.i << ' ' << message.s << endl;
      //// switch behavior
      ctx->defaultBehavior().on<float>([&]
      {
        return [&](context_t *current, float message, ...)
        {
          scout << "received float: " << message << endl;
        };
      })
        .off<int>()       // no longer accept int
        .on<Trigger>([&]  // repurpose handler for Trigger
      {
        return [&](context_t *ctx, ...)
        {
          scout << "modified receiver for Trigger" << endl;
        };
      });
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "terminating actor" << endl;
      ctx->quit();
    };
  });

  actor.enableContext().withDefault();
  ((actor << atom::trigger)
    | 42
    | Switch { 42, "fourty-two" }
    | 42.0f
    | 100)     // discarded because handler is disabled by previous message
    << atom::trigger
    | Delete{};
  sys.waitForAll();
  printFooter();
}

void ext::demo::_2()
{
  description() << "Example 2";
  printHeader();
  capture();

  using ContextType = typed_context_t<ClassBasedBehavior>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
  actor_t<domain_t> actor = actor_t<domain_t>::spawn<ContextType>(props);

  actor.defaultBehavior()

    // conditional receive for integers with filter defined next
    .on<int>([&] {
    return [&](context_t *ctx, int x, ...)  // unused parameters can be replaced by ...
    {
      scout << "received int: " << x << endl;
    };
  }, [](int x) // filter to receive only even integers
  {
    return x % 2 == 0;
  })

    // filter to  receive only strings that start with "xyz"
    .on<string>([&] {
    return [&](context_t *ctx, string const& x, ...)
    {
      scout << "received string: " << x << endl;
    };
  }, [](string const& x)
  {
    return x.find("xyz") == 0;
  })

    // termination
    .on<Delete>([&] {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  })

    // catch-all
    .any([&] {
    return [&](context_t *ctx, message_t *message, ...) // default handler, deals with all other types
    {
      scout << "unexpected message for this behavior, switching to X" << endl;
      ContextType::become<ClassBasedBehavior>(ctx);
      ctx->yield();
    };
  }, [&](const message_t *message) { return true; });

  actor.enableContext<ContextType>().withDefault();

  for (int i = 0; i < 4; i++) actor | i;  // only the even integers will be received
  actor
    | string("xyzabcd")
    | string("yzabcdx") // rejected
    | vector<int>({ 8, 7, 6 }) // causes a behavior switch from default to X
    | 42   // causes a behavior switch from X to default
    | Delete{};

  sys.waitForAll();
  printFooter();
}

void ext::demo::_3()
{
  description() << "Example 3";
  printHeader();
  capture();

  using ContextType = MyActor<X, Y, Z>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
  actor_t<domain_t> actor = actor_t<domain_t>::spawn<ContextType>(props, 0, "initial context");
  actor.initialize<X>(0, "updated X");
  actor.initialize<Y>(0, "updated Y");
  actor.initialize<Z>(0, "updated Z");
  actor.initialize<ContextType>(0, "updated context");

  actor.defaultBehavior()
    .on<Trigger>([&]
  {
    return [&](context_t *ctx, ...)
    {
      scout << "trigger received, switching behavior from default to X" << endl;
      ContextType::become<X>(ctx);
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });

  actor.enableContext<ContextType>().withDefault();

  actor << atom::trigger;
  actor | 'y' | 'z' | 'x' | 'x' | Delete{};

  sys.waitForAll();
  printFooter();
}

void ext::ClassBasedBehavior::_Delete(Delete const&)
{
  ctx.quit();
}

ext::ClassBasedBehavior::ClassBasedBehavior(context_t& ctx_)
  :behavior_t(ctx_)
{
}

ext::ClassBasedBehavior::~ClassBasedBehavior()
{
#ifdef TRACE_DTOR
  scout << "~ClassBasedBehavior" << endl;
#endif // TRACE_DTOR
}

void ext::ClassBasedBehavior::receive(message_t *message, actor_id_t const& senderID)
{
#ifdef USE_NAMED_HANDLERS   // use named handlers

  message->forwardIf<vector<int>>(this, &ClassBasedBehavior::_vector<int>)
    || message->forwardIf<Delete>(this, &ClassBasedBehavior::_Delete)
    || [&]
  {
    scout << "unexpected message for this behavior, switching to default" << endl;
    ContextType::becomeDefault(this);
    ctx.yield();
    return true;
}();

#elif defined(USE_ANON_HANDLERS) // use anonymous handlers
  message->on<vector<int>>([this](vector<int> const& lst)
  {
    scout << "received lst: " << lst << endl;
  })
    .on<Delete>([this](Delete)
  {
    scout << "here too" << endl;
    ctx.quit();
  })
    .any([this, message]()
  {
    scout << "unexpected message for this behavior, switching to default" << endl;
    ContextType::becomeDefault(this);
    ctx.yield();
  });

#elif defined(USE_INPLACE_HANDLING) // use in-place handling
  vector<int> lst;
  if (message->extract(lst)) scout << "received lst: " << lst << endl;
  else if(atom_t<>::type<Delete>(message)) ctx.quit();
  else
  {
    scout << "unexpected message for this behavior, switching to default" << endl;
    ContextType::becomeDefault(this);
    ctx.yield();
  }

#else
#error no mechanism specified
#endif // USE_NAMED_HANDLERS
}

ext::X::X(context_t& ctx_)
  :behavior_t(ctx_), xCounter(0), xBehaviorInfo("initial X")
{
  scout << "In behavior (X) CTOR: x counter = " << xCounter << ", behavior metadata = " << xBehaviorInfo << endl;
}

ext::X::~X()
{
  scout << "In behavior (X) DTOR: x counter = " << xCounter << ", behavior metadata = " << xBehaviorInfo << endl;
#ifdef TRACE_DTOR
  scout << "~X" << endl;
#endif // TRACE_DTOR
}

void ext::X::initialize(int xCounter_, string const& xBehaviorInfo_)
{
  xCounter = xCounter_;
  xBehaviorInfo = xBehaviorInfo_;
  scout << "In behavior (X) initializer: x counter = " << xCounter << ", behavior metadata = " << xBehaviorInfo << endl;
}

void ext::X::receive(message_t *message, actor_id_t const& senderID)
{
  scout << "X::receive activated" << endl;
  if (atom_t<>::type<Delete>(message))
  {
    ctx.quit();
    return;
  }

  char c;
  if (!message->extract(c)) return;

  switch (c)
  {
  case 'x':
    xCounter++;
    as<ContextType>().totalMessages++;
    scout << "incremented x counter (behavior-specific) to " << xCounter << " and total message count (context-specific) to " << as<ContextType>().totalMessages << endl;
    return;
  case 'y':
    scout << "received 'y' while behavior X is active, switching behavior to Y with message re-dispatch" << endl;
    ContextType::become<Y>(this);
    YIELD();
  case 'z':
    scout << "received 'z' while behavior X is active, switching behavior to Z with message re-dispatch" << endl;
    ContextType::become<Z>(this);
    YIELD();
  default:
    return;
  }
}

ext::Y::Y(context_t& ctx_)
  :behavior_t(ctx_), yCounter(0), yBehaviorInfo("initial Y")
{
  scout << "In behavior (Y) CTOR: y counter = " << yCounter << ", behavior metadata = " << yBehaviorInfo << endl;
}

ext::Y::~Y()
{
  scout << "In behavior (Y) DTOR: y counter = " << yCounter << ", behavior metadata = " << yBehaviorInfo << endl;
#ifdef TRACE_DTOR
  scout << "~Y" << endl;
#endif // TRACE_DTOR
}

void ext::Y::initialize(int yCounter_, string const& yBehaviorInfo_)
{
  yCounter = yCounter_;
  yBehaviorInfo = yBehaviorInfo_;
  scout << "In behavior (Y) initializer: y counter = " << yCounter << ", behavior metadata = " << yBehaviorInfo << endl;
}

void ext::Y::receive(message_t *message, actor_id_t const& senderID)
{
  scout << "Y::receive activated" << endl;
  if (atom_t<>::type<Delete>(message))
  {
    ctx.quit();
    return;
  }

  char c;
  if (!message->extract(c)) return;

  switch (c)
  {
  case 'x':
    scout << "received 'x' while behavior Y is active, switching behavior to X with message re-dispatch" << endl;
    ContextType::become<X>(this);
    YIELD();
  case 'y':
    yCounter++;
    as<ContextType>().totalMessages++;
    scout << "incremented y counter (behavior-specific) to " << yCounter << " and total message count (context-specific) to " << as<ContextType>().totalMessages << endl;
    return;
  case 'z':
    scout << "received 'z' while behavior Y is active, switching behavior to Z with message re-dispatch" << endl;
    ContextType::become<Z>(this);
    YIELD();
  default:
    return;
  }
}

ext::Z::Z(context_t& ctx_)
  :behavior_t(ctx_), zCounter(0), zBehaviorInfo("initial Z")
{
  scout << "In behavior (Z) CTOR: z counter = " << zCounter << ", behavior metadata = " << zBehaviorInfo << endl;
}

ext::Z::~Z()
{
  scout << "In behavior (Z) DTOR: z counter = " << zCounter << ", behavior metadata = " << zBehaviorInfo << endl;
#ifdef TRACE_DTOR
  scout << "~Z" << endl;
#endif // TRACE_DTOR
}

void ext::Z::initialize(int zCounter_, string const& zBehaviorInfo_)
{
  zCounter = zCounter_;
  zBehaviorInfo = zBehaviorInfo_;
  scout << "In behavior (Z) initializer: z counter = " << zCounter << ", behavior metadata = " << zBehaviorInfo << endl;
}

void ext::Z::receive(message_t *message, actor_id_t const& senderID)
{
  scout << "Z::receive activated" << endl;
  if (atom_t<>::type<Delete>(message))
  {
    ctx.quit();
    return;
  }

  char c;
  if (!message->extract(c)) return;

  switch (c)
  {
  case 'x':
    scout << "received 'x' while behavior Z is active, switching behavior to X with message re-dispatch" << endl;
    ContextType::become<X>(this);
    YIELD();
  case 'y':
    scout << "received 'y' while behavior Z is active, switching behavior to Y with message re-dispatch" << endl;
    ContextType::become<Y>(this);
    YIELD();
  case 'z':
    zCounter++;
    as<ContextType>().totalMessages++;
    scout << "incremented z counter (behavior-specific) to " << zCounter << " and total message count (context-specific) to " << as<ContextType>().totalMessages << endl;
    return;
  default:
    return;
  }
}
