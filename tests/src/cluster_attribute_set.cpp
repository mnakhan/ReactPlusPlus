/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "cluster_attribute_set.hpp"
#include "generic.hpp"

webserver::cluster_attribute_set_t::cluster_attribute_set_t(cluster_id_t expectedSize_)
: expectedSize(expectedSize_), currentSize(0)
{
}

void webserver::cluster_attribute_set_t::append(cluster_id_t clusterID, descriptor_t L0_descriptor, vector<descriptor_t> const& L1_descriptors, descriptor_t listener_descriptor)
{
  lock_guard<mutex> bolt(mx_clusterAttributes);
  if (currentSize >= expectedSize) return;

  clusterAttributes[clusterID]["L0"] = generic_ext::toString(L0_descriptor);
  clusterAttributes[clusterID]["L1"] = generic_ext::toString(L1_descriptors);
  clusterAttributes[clusterID]["Acceptor"] = generic_ext::toString(listener_descriptor);
  currentSize++;
}

bool webserver::cluster_attribute_set_t::isDone()
{
  lock_guard<mutex> bolt(mx_clusterAttributes);
  return currentSize >= expectedSize;
}

string webserver::cluster_attribute_set_t::toString()
{
  ostringstream out;
  lock_guard<mutex> bolt(mx_clusterAttributes);
  if (currentSize < expectedSize) return out.str();


  out << "Summary of descriptors\n----------------------" << endl;
  for (cluster_id_t clusterID = 0; clusterID < currentSize; clusterID++)
  {
    out << "Cluster: " << clusterID << endl;
    out << "  L0 --> " << clusterAttributes[clusterID]["L0"] << endl;
    out << "  L1 --> " << clusterAttributes[clusterID]["L1"] << endl;
    out << "  Acceptor --> " << clusterAttributes[clusterID]["Acceptor"] << endl;
    if (clusterID < currentSize - 1) out << endl;
  }

  return out.str();
}
