/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "event_generator.hpp"
#include "server.hpp"
#include "session.hpp"
#include "server_io_policy.hpp"
#include "user_statistics.hpp"
#include "policy.hpp"
#include "self_pipe.hpp"

using namespace io;
using namespace ext;
#ifdef LINUX

rpp::system_t& webserver::server_t::sys = GLOBAL(rpp::system_t);

void webserver::server_t::run(string const& configurationPath)
{
  webserver::session_t::reset();
  auto& stats = GLOBAL(user_statistics_t);
  stats.reset();
  attachHandler<SIGINT>();

  policy_t::enable<dispatch_size::single>();
  environment::configPath = configurationPath;

#ifdef DEBUG
  environment::reload(true);
#else
  environment::reload(false);
#endif // DEBUG

  io_policy_t *policy = new server_io_policy_t;
  sys.restartWithConfiguration(policy);

  thread interactive;

  auto reporter = [&] (bool isExiting)
  {
    scout << sys.report() << "\nStandby actors\n--------------\n";
    auto m = sys.getActorCount();
    for_each(m.cbegin(), m.cend(), [this](auto& kvp)
    {
      scout << "cluster " << kvp.first << " --> " << kvp.second << '\n';
    });
    scout << "\nTotal replies sent\n------------------\nimmediate: " << stats.n_immediate.load() << "\ndeferred: " << stats.n_deffered.load();
    if (isExiting) scout << endl;
    else scout << "\n\n  Press ENTER to refresh." << endl;
  };


  if (environment::enableStatsReporter)
  {
    interactive = thread([&]
    {
      while (true)
      {
        RM_LF();
        reporter(false);
      }
    });
    interactive.detach();
  }

  sys.waitForAll();
  reporter(true);
  sys.shutdown();
  delete policy;
}

int webserver::main(int argc, char **argv)
{
  server_t server;
  if (argc < 2)server.run("");
  else server.run(argv[1]);

  return 0;
}
#endif // LINUX
