/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "signaler.hpp"
#include "server_io_policy.hpp"
#include "atom.hpp"
using namespace ext;

#ifdef LINUX

webserver::signaler_t::signaler_t(context_t& ctx_)
: behavior_t(ctx_)
{
}

webserver::signaler_t::~signaler_t()
{
#ifdef TRACE_DTOR
  scout << "~signaler_t: actor ID = " << ctx.getID() << endl;
#endif // TRACE_DTOR
}

void webserver::signaler_t::initialize(io_policy_t *policy_)
{
  policy = policy_;
}

void webserver::signaler_t::receive(message_t *message, actor_id_t const& senderID)
{
  if (atom_t<True>::type<Trigger>(message))
  {
    server_io_policy_t *serverPolicy = dynamic_cast<server_io_policy_t *> (policy);
    cluster_id_t clusterID = ThisThread::getClusterID();
    worker_id_t workerID = ThisThread::getWorkerID();

    if (!serverPolicy || !serverPolicy->onIdle(clusterID, workerID)) return;
    serverPolicy->churn(clusterID);
    YIELD();
  }
  else if (atom_t<True>::type<Delete>(message)) ctx.quit();
}

webserver::l0_signaler_t::l0_signaler_t(context_t& ctx_)
: behavior_t(ctx_),
clusterID(DEFAULT_CLUSTER_ID),
policy(nullptr),
l0Generator(nullptr),
L1Size(0)
{
}

webserver::l0_signaler_t::~l0_signaler_t()
{
#ifdef TRACE_DTOR
  scout << "~l0_signaler_t: actor ID = " << ctx.getID() << endl;
#endif // TRACE_DTOR
}

void webserver::l0_signaler_t::initialize(io::io_policy_t *policy_)
{
  policy = policy_;
  server_io_policy_t *extPolicy = (server_io_policy_t*) policy;

  L1Size = extPolicy->getL1Size();
  cluster_id_t selfClusterID = ctx.getID().clusterID;
  l0Generator = extPolicy->getL0Generator(selfClusterID);
  l1Signalers = vector<rpp::actor_t < domain_t >> (L1Size);
  for (worker_id_t i = 0; i < L1Size; i++) l1Signalers[i] = extPolicy->selectL1Signaler(selfClusterID, i);
}

void webserver::l0_signaler_t::receive(message_t *message, actor_id_t const& senderID)
{
  if (atom_t<True>::type<Trigger>(message))
  {
    for (worker_id_t i = 0; i < L1Size; i++)
      l1Signalers[i] << atom::raise;
  }
  else if (atom_t<True>::type<Rearm>(message))
  {
    _L0_ENGINE_ *engine = (_L0_ENGINE_ *) l0Generator;
    engine->rearm(0);
  }
  else if (atom_t<True>::type<Delete>(message))
  {
    for (worker_id_t i = 0; i < L1Size; i++) l1Signalers[i].reset();
    ctx.quit();
    return;
  }
}

generic_ext::random_t& webserver::l1_signaler_t::rnd = THR_LOCAL(random_t);
atomic<uint64_t> webserver::l1_signaler_t::round = 0;

webserver::l1_signaler_t::l1_signaler_t(context_t& ctx_)
: behavior_t(ctx_),
clusterID(DEFAULT_CLUSTER_ID),
policy(nullptr), l1Generator(nullptr),
burstLength(1), capacity(5),
spinCount(0), maxSpin(8192),
lastSnapshot(UINT64_MAX),
isSnapshotSet(false)
{
}

webserver::l1_signaler_t::~l1_signaler_t()
{
#ifdef TRACE_DTOR
  scout << "~l1_signaler_t: actor ID = " << ctx.getID() << endl;
#endif // TRACE_DTOR
}

void webserver::l1_signaler_t::initialize(io::io_policy_t *policy_, worker_id_t peerIndex_)
{
  policy = policy_;
  peerIndex = peerIndex_;
  server_io_policy_t *extPolicy = (server_io_policy_t*) policy;
  actor_id_t selfID = ctx.getID();
  clusterID = selfID.clusterID;
  l0Signaler = extPolicy->selectL0Signaler(clusterID);
  l0Signaler.sign(selfID);
  l1Generator = extPolicy->getL1Generator(clusterID, 0);
  lastSnapshot = getSnapshot();
  isSnapshotSet = true;
  savedEventCounter = policy->eventCounter;
}

void webserver::l1_signaler_t::receive(message_t *message, actor_id_t const& senderID)
{
  bool isRaise = false, isTerm = false;
  if (atom_t<True>::type<Raise>(message)) isRaise = true;
  else if (atom_t<True>::type<Delete>(message)) isTerm = true;
  assert(isRaise || isTerm);
  if (isTerm)
  {
    l0Signaler.reset();
    ctx.quit();
    return;
  }

  _L1_ENGINE_ *engine = (_L1_ENGINE_ *) l1Generator;
  poll_result_t result;
  engine->spin<true>(result, burstLength);
  server_io_policy_t *extPolicy = (server_io_policy_t*) policy;

  if (result == poll_result_t::PR_SUCCESS)
  {
    spinCount = 0;
    if (burstLength < capacity) burstLength++;
    advanceRound();
    ((webserver::server_io_policy_t*)policy)->churn(ctx.getID().clusterID);
    extPolicy->raise_l1_alarm(clusterID);
    YIELD();
  }

  burstLength = 1;

  if (result == poll_result_t::PR_TRY_AGAIN) YIELD();

  if (result == poll_result_t::PR_FAILURE)
  {
    uint64_t currentSnapshot = getSnapshot();
    if (lastSnapshot != currentSnapshot)
    {
      spinCount = 0;
      lastSnapshot = currentSnapshot;
      YIELD();
    }

    uint64_t previousEventCounter = savedEventCounter;
    savedEventCounter = policy->eventCounter;
    if (previousEventCounter != savedEventCounter)
    {
      spinCount = 0;
      YIELD();
    }

    if (spinCount < maxSpin)
    {
      spinCount++;
      YIELD();
    }

    extPolicy->set_l1_alarm(clusterID, peerIndex);
    l0Signaler << atom::rearm;
    return;
  }
}

uint64_t webserver::l1_signaler_t::getSnapshot()
{
  return round;
}

void webserver::l1_signaler_t::advanceRound()
{
  round++;
}

#endif // LINUX
