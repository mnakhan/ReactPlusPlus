/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "actor.hpp"
#include "profiling.hpp"
#include "policy.hpp"
#include "stage.hpp"
#include "user_statistics.hpp"

using namespace rpp;
using namespace ext;

void profiling::benchmarks::usage_s()
{
  scout << endl;
  exit(1);
}

void profiling::benchmarks::big(int argc, char **argv)
{
  uint32_t nThreads, nActors, nMessages;
  if (argc < 4
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], nActors)
    || !parse<uint32_t>(argv[3], nMessages))
    usage(argv[0], "number of threads", "number of actors", "number of messages");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("number of actors", nActors, LR(1U, UINT32_MAX));
  validate("number of messages", nMessages, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  // aliases for actor contexts
  using Context1 = typed_context_t<Big>;
  using Context2 = typed_context_t<Sink>;

  policy_t::disable<schedule_at_sender>();
  vector<actor_t<domain_t>> actors(nActors);
  for (uint32_t i = 0; i < nActors; i++)
  {
    auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
      .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
      .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
    actors[i] = actor_t<domain_t>::spawn<Context1>(props);
  }

  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
  actor_t<domain_t> sink = actor_t<domain_t>::spawn<Context2>(props);
  for (uint32_t i = 0; i < nActors; i++) actors[i].initialize<Big>(nMessages, actors, sink, i);
  sink.initialize<Sink>(actors);

  for (uint32_t i = 0; i < nActors; i++) actors[i].enableContext<Context1>().withBehavior<Big>();
  sink.enableContext<Context2>().withBehavior<Sink>();

  for (uint32_t i = 0; i < nActors; i++) actors[i] << atom::trigger;
  sys.waitForAll();
}

void profiling::benchmarks::partitionedBig(int argc, char ** argv)
{
  uint32_t nActors, nMessages;
  if (argc < 3
    || !parse<uint32_t>(argv[2], nActors)
    || !parse<uint32_t>(argv[3], nMessages))
    usage(argv[0], "number of actors", "number of messages");

  validate("number of actors", nActors, LR(1U, UINT32_MAX));
  validate("number of messages", nMessages, LR(1U, UINT32_MAX));

  // number of clusters and number of threads assigned to each cluster are read from the configuration file

  rpp::system_t& sys = GLOBAL(rpp::system_t);
  sys.restartWithConfiguration(nullptr);

  uint32_t nClusters = sys.getClusterCount();
  if (!nClusters)  // fix configuration file
  {
    scout << "The configuration file must specify cluster count and thread assignment to run this experiment." << endl;
    return;
  }

  // aliases for actor contexts
  using Context1 = typed_context_t<Big>;
  using Context2 = typed_context_t<Sink>;

  uint32_t nActorsPerCluster = nActors / nClusters;
  uint32_t nExtraActors = nActors - nActorsPerCluster * nClusters;   // spawn these on the first cluster

  vector<actor_t<domain_t>> spawners(nClusters);
  vector<vector<actor_t<domain_t>>> actors(nClusters);
  //policy_t::enableWith<schedule_at_sender::probability>(0.9);

  for (cluster_id_t clusterID = 0; clusterID < nClusters; clusterID++)
  {
    /**
      Each cluster has its own spawner, pinned to the first core from that cluster. Therefore, memory for all actors spawned by a specific spawner comes from the same NUMA node. The NUMA node is home to the CPU assigned to the spawner. This relies on the first-touch policy.

      To run a partitioned benchmark, edit the configuration file so that each cluster takes an entire but different NUMA node. Since the benchmark ensures that all messages are between actors from the same cluster, there is no inter-cluster communication (and therefore no messages across NUMA boundaries) and the results should scale as expected.

      To run a striped benchmark, edit the configuration file so that each cluster has cores from different NUMA nodes. This is useful to validate hierarchical work-stealing within the same cluster, where cores from the same NUMA nodes are prioritized before those from remote nodes.
    **/

    spawners[clusterID] = actor_t<domain_t>::spawn<>(props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
      .at(location_t::map(true, clusterID, CPU<0>{})));
#else
      .at(location_t::DetachedCluster));    // NOTE: for detached actors, this benchmark is no different than the original benchmark
#endif // USE_NATIVE_ACTORS


    uint32_t spawnCount = clusterID != 0 ? nActorsPerCluster : nActorsPerCluster + nExtraActors;
    actors[clusterID] = vector<actor_t<domain_t>>(spawnCount);

    spawners[clusterID].defaultBehavior()
      .on<Trigger>([&, spawnCount]
    {
      return[&actors, nMessages, clusterID, spawnCount](context_t *ctx, ...)
      {
        auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
          .at(location_t::map(false, clusterID));
#else
          .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

        for (uint32_t i = 0; i < spawnCount; i++) actors[clusterID][i] = actor_t<domain_t>::spawn<Context1>(props);
        actor_t<domain_t> sink = actor_t<domain_t>::spawn<Context2>(props);

        for (uint32_t i = 0; i < spawnCount; i++) actors[clusterID][i].initialize<Big>(nMessages, actors[clusterID], sink, i);
        sink.initialize<Sink>(actors[clusterID]);

        for (uint32_t i = 0; i < spawnCount; i++)  actors[clusterID][i].enableContext<Context1>().withBehavior<Big>();
        sink.enableContext<Context2>().withBehavior<Sink>();

        for (uint32_t i = 0; i < spawnCount; i++) actors[clusterID][i] << atom::trigger;
        ctx->quit();
      };
    });
    spawners[clusterID].enableContext().withDefault();
  }

  actor_t<domain_t>::broadcast(Trigger{}, spawners);
  sys.waitForAll();
  //policy_t::disable<schedule_at_sender>();
}

void profiling::benchmarks::countingActor(int argc, char **argv)
{
  uint32_t nThreads, nMessages;
  if (argc < 3
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], nMessages))
    usage(argv[0], "number of threads", "number of messages");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("number of messages", nMessages, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  using CContext = typed_context_t<Counter>;

  auto props1 = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map(true, CLUSTER<0>{}, CPU<0>{}));
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  auto props2 = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map(true, CLUSTER<0>{}, CPU<1>{}));
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> producer = actor_t<domain_t>::spawn<>(props1),
    counter = actor_t<domain_t>::spawn<CContext>(props2);
  counter.sign(producer.getID());
  producer.defaultBehavior()
    .on<Trigger>([&]
  {
    return[&](context_t *ctx, ...)
    {
      for (uint32_t i = 0; i < nMessages; i++) counter << atom::increment;
      counter << atom::queryCounter;
    };
  })
    .on<uint32_t>([&]
  {
    return[&](context_t *ctx, uint32_t currentValue, ...)
    {
#ifdef DEBUG
      scout << "current value = " << currentValue << endl;
#endif // DEBUG
      ctx->quit();
    };
  });

  producer.enableContext().withDefault();
  counter.enableContext<CContext>().withBehavior<Counter>();

  producer << atom::trigger;
  sys.waitForAll();
}

void profiling::benchmarks::diningPhilosophers(int argc, char ** argv)
{
  uint32_t nThreads, nPhilosophers, requiredThroughput, windowSize;
  if (argc < 5
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], nPhilosophers)
    || !parse<uint32_t>(argv[3], requiredThroughput)
    || !parse<uint32_t>(argv[4], windowSize))
    usage(argv[0], "number of threads", "number of philosophers", "required throughput", "window size");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("number of philosophers", nPhilosophers, LR(1U, UINT32_MAX));
  validate("required throughput", requiredThroughput, LR(1U, UINT32_MAX));
  validate("window size", windowSize, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
#ifdef DEBUG
  random_t& rnd = THR_LOCAL(random_t);
#endif // DEBUG
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> arbiter = actor_t<domain_t>::spawn<Arbiter>(props);
  vector<actor_t<domain_t>> philosophers(nPhilosophers);

  for (uint32_t i = 0; i < nPhilosophers; i++)
  {
    philosophers[i] = actor_t<domain_t>::spawn<>(props);
    uint32_t philosopherLocation = i;
    philosophers[i].defaultBehavior()
      .on<ArbiterReady>([&, philosopherLocation]
    {
      return [&, philosopherLocation](context_t *ctx, ...)
      {
        for (uint32_t j = 0; j < windowSize; j++)
        {
          arbiter | Acquire{ philosopherLocation };
#ifdef DEBUG
          sleep_for(microseconds(rnd.nextInt(0, 10)));
#endif // DEBUG
          arbiter | Release{ philosopherLocation };
        }
      };
    })
      .on<Delete>([&, philosopherLocation]
    {
      return [&, philosopherLocation](context_t *ctx, ...)
      {
#ifdef DEBUG
        scout << "philosopher at " << philosopherLocation << " has left the table" << endl;
#endif // DEBUG
        ctx->quit();
      };
    });

    philosophers[i].enableContext().withDefault();
  }

  arbiter.initialize<Arbiter>(philosophers);  // context initialization

                                              // behavior initialization
  arbiter.initialize<BeginCycle>();
  arbiter.initialize<Allocate>(requiredThroughput, windowSize);

  // enable arbiter and set initial behavior to begin cycle
  arbiter.enableContext<Arbiter>().withBehavior<BeginCycle>();

  arbiter << atom::trigger;
  sys.waitForAll();
}

void profiling::benchmarks::forkJoin_spawn(int argc, char **argv)
{
  uint32_t nThreads, nActors;
  if (argc < 3
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], nActors))
    usage(argv[0], "number of threads", "number of actors");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("number of actors", nActors, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  using CContext = typed_context_t<Consumer>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  uint32_t degreeOfConcurrency = MIN(nActors, nThreads);
  vector<actor_t<domain_t>> spawners(degreeOfConcurrency);
  uint32_t actorsPerSpawner = nActors / degreeOfConcurrency;
  for (uint32_t i = 0; i < degreeOfConcurrency; i++)
  {
    spawners[i] = actor_t<domain_t>::spawn<>(props);
    uint32_t n;
    if (i < degreeOfConcurrency - 1) n = actorsPerSpawner;
    else n = nActors - (degreeOfConcurrency - 1) * actorsPerSpawner;
    spawners[i].defaultBehavior()
      .on<Trigger>([&, n]
    {
      return[&, n](context_t *ctx, ...)
      {
        for (uint32_t a = 0; a < n; a++)
        {
          actor_t<domain_t> actor = actor_t<domain_t>::spawn<CContext>(props);
          actor.initialize<Consumer>(1);
          actor.enableContext<typed_context_t<Consumer>>().withBehavior<Consumer>();
          actor << atom::trigger;
        }
        ctx->quit();
      };
    });
    spawners[i].enableContext().withDefault();
  }

  actor_t<domain_t>::broadcast(Trigger{}, spawners);
  sys.waitForAll();
}

void profiling::benchmarks::forkJoin_throughput(int argc, char **argv)
{
  uint32_t nThreads, nActors, nMessagesPerActor;
  if (argc < 4
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], nActors)
    || !parse<uint32_t>(argv[3], nMessagesPerActor))
    usage(argv[0], "number of threads", "number of actors", "number of messages per actor");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("number of actors", nActors, LR(1U, UINT32_MAX));
  validate("number of messages per actor", nMessagesPerActor, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  vector<actor_t<domain_t>> actors(nActors);
  using CContext = typed_context_t<Consumer>;

  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> spawner = actor_t<domain_t>::spawn<>(props);

  spawner.defaultBehavior()
    .on<Trigger>([&]
  {
    return [&](context_t *ctx, ...)
    {
      for (uint32_t i = 0; i < nActors; i++) actors[i] = actor_t<domain_t>::spawn<CContext>(props);
      actor_t<domain_t>::initializeEach<Consumer>(actors, nMessagesPerActor);
      actor_t<domain_t>::enableContexts<CContext, Consumer>(actors);
      for (uint32_t m = 0; m < nMessagesPerActor; m++)
        for (uint32_t a = 0; a < nActors; a++)
          actors[a] << atom::trigger;
    };
  })
    .on<Delete>([&]
  {
    return [&](context_t *ctx, ...)
    {
      ctx->quit();
    };
  });

  spawner.enableContext().withDefault();
  policy_t::enable<schedule_at_sender>();
  spawner << atom::trigger << atom::terminate;
  sys.waitForAll();
  policy_t::disable<schedule_at_sender>();
}

void profiling::benchmarks::laplaceExpansion(int argc, char **argv)
{
  uint32_t nThreads, dimension, maxTreeDepth;
  if (argc < 4
    || !parse<uint32_t>(argv[1], nThreads) || nThreads == 0
    || !parse<uint32_t>(argv[2], dimension) || dimension == 0
    || !parse<uint32_t>(argv[3], maxTreeDepth) || maxTreeDepth == 0)
    usage(argv[0], "number of threads", "matrix dimension", "max tree depth");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("matrix dimension", dimension, LR(1U, 1024U));
  validate("max tree depth", maxTreeDepth, LR(1U, 32U));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  Compute2::maxTreeDepth = maxTreeDepth;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  auto& gc = GLOBAL(user_statistics_t);
  gc.reset();

  /**
  * Total number of actors = 1 + n + n(n - 1) + n(n - 1)(n - 2) + ... + P_i + ... + P, where
  * i'th term, P_i = n(n - 1)(n - 2)...(n - i + 1)
  * last term, P = n(n - 1)(n - 2)...(m + 1)
  * m is the recursion depth
  **/

  actor_t<domain_t> actor = actor_t<domain_t>::spawn<>(props);
  actor.defaultBehavior()
    .on<mat_d_t>([&]
  {
    return [&](context_t *ctx, mat_d_t&& m, ...)
    {
      auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
        .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
        .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
      actor_t<domain_t> child = actor_t<domain_t>::spawn<typed_context_t<Compute2>>(props);
      child.initialize<Compute2>(actor.getID(), 0, 0);
      child.enableContext<typed_context_t<Compute2>>().withBehavior<Compute2>();
      child | move(m);
    };
  })
    .on<tuple<size_t, size_t, double>>([&]
  {
    return [&](context_t *ctx, tuple<size_t, size_t, double> const& result, ...)
    {
      scout << "Determinant (w/ actors): " << get<2>(result) << endl;
      ctx->quit();
    };
  });
  actor.enableContext().withDefault();

  mat_d_t m = mat_d_t::random(dimension, dimension, -1.0, 1.0);
#ifdef DEBUG
  scout << "Generated matrix:\n" << m << endl;
  scout << "Determinant (w/o actors): " << m.getDeterminant() << endl;
#endif // DEBUG
  actor | move(m);
  sys.waitForAll();
  scout << "Total actors spawned = " << gc.n_laplace_actors << "\nTotal messages = " << gc.n_laplace_messages << endl;
}

void profiling::benchmarks::pingPong(int argc, char ** argv)
{
  uint32_t nThreads, initialValue;
  double senderAffinity;

  if (argc < 4
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], initialValue)
    || !parse<double>(argv[3], senderAffinity))
    usage(argv[0], "number of threads", "initial value", "sender affinity");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("initial value", initialValue, LR(1U, UINT32_MAX));
  validate("sender affinity", senderAffinity, LR(0.0, 1.0));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  using Latency = typed_context_t<PingPong>;
  auto props = props_t::with<mailbox_type_t::DV>()
    .withSenderAffinity(senderAffinity)
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> first = actor_t<domain_t>::spawn<Latency>(props),
    second = actor_t<domain_t>::spawn<Latency>(props);
  first.initialize<PingPong>(second);
  second.initialize<PingPong>(first);

  first.enableContext<Latency>().withBehavior<PingPong>();
  second.enableContext<Latency>().withBehavior<PingPong>();

  first | initialValue;
  sys.waitForAll();
}

void profiling::benchmarks::producerConsumer(int argc, char **argv)
{
  uint32_t nThreads, nMessagesPerProducer, nProducers, nConsumers;
  if (argc < 5
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], nMessagesPerProducer)
    || !parse<uint32_t>(argv[3], nProducers)
    || !parse<uint32_t>(argv[4], nConsumers))
    usage(argv[0], "number of threads", "messages per producers", "number of producers", "number of consumers");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("messages per producers", nMessagesPerProducer, LR(1U, UINT32_MAX));
  validate("number of producers", nProducers, LR(1U, UINT32_MAX));
  validate("number of consumers", nConsumers, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  using C_Context = typed_context_t<Consumer>;
  using P_Context = typed_context_t<Producer>;

  vector<actor_id_t> consumerIDs(nConsumers), producerIDs(nProducers);
  for (uint32_t i = 0; i < nConsumers; i++)
  {
    auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
      .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
      .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
    consumerIDs[i] = sys.spawn<C_Context>(props);
  }

  for (uint32_t i = 0; i < nProducers; i++)
  {
    auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
      .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
      .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
    producerIDs[i] = sys.spawn<P_Context>(props);
  }

  sys.initializeEach<Consumer>(consumerIDs, nMessagesPerProducer * nProducers);
  sys.initializeEach<Producer>(producerIDs, nMessagesPerProducer, consumerIDs);

  for (size_t i = 0; i < consumerIDs.size(); i++) actor_t<domain_t>::map(consumerIDs[i]).enableContext<C_Context>().withBehavior<Consumer>();
  for (size_t i = 0; i < producerIDs.size(); i++) actor_t<domain_t>::map(producerIDs[i]).enableContext<P_Context>().withBehavior<Producer>();

  sys.broadcast<true>(Trigger{}, producerIDs);
  sys.waitForAll();
}

void profiling::benchmarks::quickSort(int argc, char **argv)
{
  uint32_t nThreads, size, threshold;
  if (argc < 4
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], size)
    || !parse<uint32_t>(argv[3], threshold))
    usage(argv[0], "number of threads", "list size", "concurrent threshold");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("list size", size, LR(1U, UINT32_MAX));
  validate("concurrent threshold", threshold, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  random_t& rnd = THR_LOCAL(random_t);
  list<int> lst(size);
  auto it = lst.begin();
  while (it != lst.end())
  {
#ifdef DEBUG
    *it = rnd.nextInt(-5, 5);
#else
    *it = rnd.nextInt();
#endif // DEBUG
    it++;
  }
  Sorter<int>::threshold = threshold;

  using IntSorter = Sorter<int>;
  using Context1 = typed_context_t<IntSorter>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  actor_t<domain_t> actor = actor_t<domain_t>::spawn<>(props);
  actor.defaultBehavior()
    .on<list<int>>([&]
  {
    return [&](context_t *ctx, list<int>&& lst, ...)
    {
      actor_t<domain_t> child = actor_t<domain_t>::spawn<Context1>(props);
      child.initialize<IntSorter>(actor.getID(), true);
      child.enableContext<Context1>().withBehavior<IntSorter>();
      child | move(lst);
    };
  })
    .on<pair<bool, list<int>>>([&]
  {
    return [&](context_t *ctx, pair<bool, list<int>> const& reply, ...)
    {
#ifdef DEBUG
      scout << "Sorted list: " << get<1>(reply) << endl;
#endif // DEBUG
      ctx->quit();
    };
  });

#ifdef DEBUG
  scout << "Original list: " << lst << endl;
#endif // DEBUG

  actor.enableContext().withDefault();
  actor | move(lst);

  sys.waitForAll();
}

void profiling::benchmarks::quickSort2(int argc, char **argv)
{
  uint32_t nThreads, size, depth;
  if (argc < 4
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], size)
    || !parse<uint32_t>(argv[3], depth))
    usage(argv[0], "number of threads", "array size", "tree depth");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("array size", size, LR(1U, UINT32_MAX));
  validate("tree depth", depth, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  auto& counters = GLOBAL(user_statistics_t);
  counters.reset();

  using DataType = int;
  DataType *data = generic_ext::parallel_fill<DataType>(size, nThreads);

  using BehaviorType = Sorter2<DataType>;
  using ContextType = typed_context_t<BehaviorType>;
#ifdef DEBUG
  scout << "Original: ";
  PRINT_ARRAY2(data, size, scout);
  scout << endl;
#endif // DEBUG

  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  policy_t::enable<dispatch_size::single>();
  actor_t<domain_t> root = actor_t<domain_t>::spawn<ContextType>(props);
  root.initialize<BehaviorType>(data, 0, size - 1, depth);
  root.enableContext<ContextType>().withBehavior<BehaviorType>();
  root << atom::trigger;
  counters.n_qs_messages++;
  sys.waitForAll();
#ifdef DEBUG
  scout << "Sorted: ";
  PRINT_ARRAY2(data, size, scout);
  scout << "\nVerified: ";
  auto s = generic_ext::isSorted(data, size) ? "Yes" : "No";
  scout << s << endl;
#endif // DEBUG

  scout << "Total actors spawned = " << counters.n_qs_actors << ", total messages sent = " << counters.n_qs_messages << endl;
  policy_t::reset<dispatch_size>();
  delete[] data;
}

void profiling::benchmarks::shutdown()
{
  auto& sys = GLOBAL(rpp::system_t);
  sys.restart(nullptr, 1, 1);
  sys.shutdown();
}

void profiling::benchmarks::threadRing(int argc, char **argv)
{
  uint32_t nThreads, nActors, initialValue;
  if (argc < 4
    || !parse<uint32_t>(argv[1], nThreads)
    || !parse<uint32_t>(argv[2], nActors)
    || !parse<uint32_t>(argv[3], initialValue))
    usage(argv[0], "number of threads", "number of actors", "initial value of token");

  validate("number of threads", nThreads, LR(1U, 128U));
  validate("number of actors", nActors, LR(1U, UINT32_MAX));
  validate("initial value of token", initialValue, LR(1U, UINT32_MAX));

  auto& sys = GLOBAL(rpp::system_t);
  INIT_SYSTEM;
  using Context1 = typed_context_t<ThreadRingNode>;
  auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
    .at(location_t::map());
#else
    .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

  vector<actor_t<domain_t>> actors(nActors);
  for (uint32_t i = 0; i < nActors; i++) actors[i] = actor_t<domain_t>::spawn<Context1>(props);
  for (uint32_t i = 0; i < nActors; i++)
  {
    uint32_t nextIndex = (i + 1) % nActors;
    actors[i].initialize<ThreadRingNode>(actors[nextIndex]);
  }

  actor_t<domain_t>::enableContexts<Context1, ThreadRingNode>(actors);
  policy_t::enable<schedule_at_sender>();
  actors[0] | initialValue;
  sys.waitForAll();
  policy_t::disable<schedule_at_sender>();
}
