/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
using namespace generic_ext;

#ifdef LINUX
namespace webserver
{
  ostream& operator<<(ostream& out, sockaddr_in const& sa);
  bool parseAddress(sockaddr_in const& sa, string& ip, ushort& port);

  class tcp_listener_t
  {
  private:
    string ip;
    ushort port;
    bool status;
    SOCKET descriptor;
    int backlog;
    bool multihomed;
    static printer_t& tscout;
  public:
    tcp_listener_t(int backlog_ = 1024, bool multihomed_ = false);
    ~tcp_listener_t();
    SOCKET accept() const;
    SOCKET accept(string& remoteIP, ushort& remotePort) const;
    void close();
    void open(string const& ip_ = LOCALHOST, ushort port_ = LOCALPORT, int backlog_ = 1024);
    descriptor_t getDescriptor() const;
    string getIP() const;
    ushort getPort() const;
    bool isRunning() const;
    friend ostream& operator<<(ostream& out, const tcp_listener_t& listener);
  };

  ostream& operator<<(ostream& out, tcp_listener_t const& listener);
}
#endif // LINUX
