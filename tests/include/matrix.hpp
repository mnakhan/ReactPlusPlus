/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
using namespace generic_ext;
namespace ext
{
#define EPSILON 1.0e-08
  template<typename T>
  class matrix_t;

  template<typename T, typename = Constraint<is_arithmetic<T>>>
  class vector_t
  {
  private:
    size_t size;
    T *data;
    static random_t& rnd;
  public:
    vector_t();
    vector_t(initializer_list<T> const& data_);
    vector_t(size_t size_, T value = T());
    vector_t(vector_t<T> const& v);
    vector_t(vector_t<T>&& v);
    ~vector_t();

    void clear();
    bool equals(vector_t<T> const& v, T epsilon = EPSILON) const;
    T getMagnitude() const;
    size_t getSize() const;

    signature<vector_t<T>&, vector_t<T> const&> operator=, operator+=, operator-=;
    signature<vector_t<T>&, vector_t<T>&&> operator=;
    signature<vector_t<T>&, T> operator*=, operator/=;
    signature<T&, size_t> operator[];

    const_signature<vector_t<T>, vector_t<T> const&> operator+, operator-;
    const_signature<vector_t<T>, T> operator*, operator/;
    const_signature<T, vector_t<T> const&> operator*;
    const_signature<const T&, size_t> operator[];

    static signature<vector_t<T>, size_t, T, T> random, random_i;

    friend vector_t<T> operator*(T c, vector_t<T> const& v)
    {
      return v * c;
    }

    vector_t<T> partition(size_t at);
    void resize(size_t newSize);
    void satinize(T epsilon = EPSILON);
    void splice(vector_t<T> const& v);

    friend class matrix_t<T>;
  };

  template<typename T>
  class matrix_t
  {
  private:
    vector_t<T> *rowVectors;
    size_t rowCount, columnCount;

    size_t firstNonZeroColumnIndex(size_t rowIndex) const;
    size_t firstNonZeroRowIndex(size_t startFrom = 0) const;
    void fusedMultiplyAdd(size_t srcRowIndex, T c, size_t dstRowIndex);
    void produceLeadingOnes();
    void rearrangeRows();
    void swap(size_t rowIndex1, size_t rowIndex2);
    void fixUnequalRows();

  public:
    matrix_t();
    matrix_t(size_t rowCount_, size_t columnCount_, T value = 0.0);
    matrix_t(initializer_list<vector_t<T>> const& rowVectors_);
    matrix_t(matrix_t<T> const& m);
    matrix_t(matrix_t<T>&& m);
    ~matrix_t();

    void clear();
    bool equals(matrix_t<T> const& m, T epsilon = EPSILON) const;
    T getCofactor(size_t rowIndex, size_t columnIndex) const;
    size_t getColumnCount() const;
    T getDeterminant() const;
    T getMinor(size_t rowIndex, size_t columnIndex) const;
    size_t getRowCount() const;
    matrix_t<T> getSubmatrix(size_t rowIndex, size_t columnIndex) const;
    static matrix_t<T> identity(size_t rowCount);
    void inverse();
    bool isSquare() const;

    signature<matrix_t<T>&, matrix_t<T> const&> operator=, operator+=, operator-=, operator*=;
    signature<matrix_t<T>&, matrix_t<T>&&> operator=;
    signature<vector_t<T>&, size_t> operator[];

    const_signature<matrix_t<T>, matrix_t<T> const&> operator+, operator-, operator*;
    const_signature<matrix_t<T>, T> operator*;
    const_signature<const vector_t<T>&, size_t> operator[];

    static signature<matrix_t<T>, size_t, size_t, T, T> random, random_i;

    friend matrix_t<T> operator*(T c, matrix_t<T> const& m)
    {
      return m * c;
    }

    friend vector_t<T> operator*(matrix_t<T> const& m, vector_t<T> const& v)
    {
      if (m.columnCount != v.getSize()) throw length_error("Matrix-vector multiplication is not allowed when column count of the matrix does not match the size of the vector.");

      vector_t<T> result = vector_t<T>(m.getRowCount(), 0.0);
      for (size_t i = 0; i < result.getSize(); i++) result[i] = m.rowVectors[i] * v;

      return result;
    }

    static bool parseMatrix(const string& data, matrix_t<T>& m);
    matrix_t<T> partition(size_t columnIndex);

    void reduce(bool showSteps = false);
    void resize(size_t newRowCount, size_t newColumnCount);
    void splice(matrix_t<T> const& m);
    string toString(bool mathInputFormat = false) const;
    void transpose();

    friend class vector_t<T>;
  };

  template<typename T, typename _>
  random_t& vector_t<T, _>::rnd = THR_LOCAL(random_t);

  template<typename T, typename _>
  inline vector_t<T, _>::vector_t()
    : size(0), data(nullptr)
  {
  }

  template<typename T, typename _>
  inline vector_t<T, _>::vector_t(initializer_list<T> const& data_)
    : vector_t<T>()
  {
    size_t size_;
    if ((size_ = data_.size()) == 0) return;

    size = size_;
    data = new T[size];
    typename initializer_list<T>::const_iterator it = data_.begin(), e = data_.end();
    size_t i = 0;
    while (it != e) data[i++] = *it++;
  }

  template<typename T, typename _>
  inline vector_t<T, _>::vector_t(size_t size_, T value)
    : vector_t<T>()
  {
    if (size_ == 0) return;
    size = size_;
    data = new T[size];
    for (size_t i = 0; i < size; i++) data[i] = value;
  }

  template<typename T, typename _>
  inline vector_t<T, _>::vector_t(vector_t<T> const& v)
    : vector_t<T>()
  {
    if (v.size == 0) return;

    size = v.size;
    data = new T[size];
    for (size_t i = 0; i < size; i++) data[i] = v.data[i];
  }

  template<typename T, typename _>
  inline vector_t<T, _>::vector_t(vector_t<T>&& v)
  {
    size = v.size;
    data = v.data;
    v.size = 0;
    v.data = nullptr;
  }

  template<typename T, typename _>
  inline vector_t<T, _>::~vector_t()
  {
    clear();
  }

  template<typename T, typename _>
  inline void vector_t<T, _>::clear()
  {
    delete[] data;
    data = nullptr;
    size = 0;
  }

  template<typename T, typename _>
  inline bool vector_t<T, _>::equals(vector_t<T> const& v, T epsilon) const
  {
    if (this == &v) return true;
    if (size != v.size) return false;
    for (size_t i = 0; i < size; i++)
      if (fabs(data[i] - v.data[i]) > epsilon) return false;
    return true;
  }

  template<typename T, typename _>
  inline T vector_t<T, _>::getMagnitude() const
  {
    T result = 0.0;
    for (size_t i = 0; i < size; i++) result += data[i];
    return sqrt(result);
  }

  template<typename T, typename _>
  inline size_t vector_t<T, _>::getSize() const
  {
    return size;
  }

  template<typename T, typename _>
  inline vector_t<T>& vector_t<T, _>::operator=(vector_t<T> const& v)
  {
    if (this == &v) return *this;

    clear();
    if (v.size == 0) return *this;

    size = v.size;
    data = new T[size];
    for (size_t i = 0; i < size; i++) data[i] = v.data[i];

    return *this;
  }

  template<typename T, typename _>
  inline vector_t<T>& vector_t<T, _>::operator=(vector_t<T>&& v)
  {
    if (this == &v) return *this;
    clear();

    size = v.size;
    data = v.data;
    v.size = 0;
    v.data = nullptr;

    return *this;
  }

  template<typename T, typename _>
  inline vector_t<T> vector_t<T, _>::operator+(vector_t<T> const& v) const
  {
    if (size != v.size) throw length_error("Vector addition is not allowed when sizes of operands do not match.");

    vector_t<T> result = vector_t<T>(size);
    for (size_t i = 0; i < size; i++) result.data[i] = data[i] + v.data[i];
    return result;
  }

  template<typename T, typename _>
  inline vector_t<T>& vector_t<T, _>::operator+=(vector_t<T> const& v)
  {
    return *this = move(*this + v);
  }

  template<typename T, typename _>
  inline vector_t<T> vector_t<T, _>::operator-(vector_t<T> const& v) const
  {
    if (size != v.size) throw length_error("Vector subtraction is not allowed when sizes of operands do not match.");

    vector_t<T> result = vector_t<T>(size);
    for (size_t i = 0; i < size; i++) result.data[i] = data[i] - v.data[i];
    return result;
  }

  template<typename T, typename _>
  inline vector_t<T>& vector_t<T, _>::operator-=(vector_t<T> const& v)
  {
    return *this = move(*this - v);
  }

  template<typename T, typename _>
  inline vector_t<T> vector_t<T, _>::operator*(T c) const
  {
    vector_t<T> result = *this;
    for (size_t i = 0; i < size; i++) result.data[i] *= c;
    return result;
  }

  template<typename T, typename _>
  inline vector_t<T>& vector_t<T, _>::operator*=(T c)
  {
    return *this = *this * c;
  }

  template<typename T, typename _>
  inline T vector_t<T, _>::operator*(vector_t<T> const& v) const
  {
    if (size != v.size) throw length_error("Vector dot multiplication is not allowed when sizes of operands do not match.");

    T result = 0.0;
    for (size_t i = 0; i < size; i++) result += data[i] * v.data[i];

    return result;
  }

  template<typename T, typename _>
  inline vector_t<T> vector_t<T, _>::operator/(T c) const
  {
    return (*this) * (1.0f / c);
  }

  template<typename T, typename _>
  inline vector_t<T> & vector_t<T, _>::operator/=(T c)
  {
    return *this = *this / c;
  }

  template<typename T, typename _>
  inline T& vector_t<T, _>::operator[](size_t index)
  {
    if (size == 0 || index > size - 1) throw out_of_range("Attempting to access a non-existent element in a vector.");

    return data[index];
  }

  template<typename T, typename _>
  inline const T& vector_t<T, _>::operator[](size_t index) const
  {
    if (size == 0 || index > size - 1) throw length_error("Attempting to access a non-existent element in a vector.");

    return data[index];
  }

  template<typename T, typename _>
  inline vector_t<T> vector_t<T, _>::partition(size_t at)
  {
    if (size < at) return vector_t<T>();

    vector_t<T> left = vector_t<T>(at), right = vector_t<T>(size - at);

    for (size_t i = 0; i < left.size; i++) left.data[i] = data[i];
    for (size_t i = 0; i < right.size; i++) right.data[i] = data[at + i];

    *this = move(left);
    return right;
  }

  template<typename T, typename _>
  inline vector_t<T> vector_t<T, _>::random(size_t size, T min, T max)
  {
    static_assert(is_floating_point<T>::value, "For integral types, use the function \"random_i\" instead of \"random\".");
    vector_t<T> result = vector_t<T>(size);
    for (size_t i = 0; i < size; i++) result.data[i] = rnd.nextReal<T>(min, max);
    return result;
  }

  template<typename T, typename _>
  inline vector_t<T> vector_t<T, _>::random_i(size_t size, T min, T max)
  {
    static_assert(is_integral<T>::value, "For floating-point types, use the function \"random\" instead of \"random_i\".");
    vector_t<T> result = vector_t<T>(size);
    for (size_t i = 0; i < size; i++) result.data[i] = rnd.nextInt<T>(min, max);
    return result;
  }

  template<typename T, typename _>
  inline void vector_t<T, _>::resize(size_t newSize)
  {
    if (size == newSize) return;

    vector_t<T> newVector = vector_t<T>(newSize);
    size_t smallerSize = size <= newSize ? size : newSize;

    for (size_t i = 0; i < smallerSize; i++) newVector[i] = data[i];
    *this = move(newVector);
  }

  template<typename T, typename _>
  inline void vector_t<T, _>::satinize(T epsilon)
  {
    for (size_t i = 0; i < size; i++)
      if (data[i] <= epsilon) data[i] = T();
  }

  template<typename T, typename _>
  inline void vector_t<T, _>::splice(vector_t<T> const& v)
  {
    size_t oldSize = size;
    resize(size + v.size);
    for (size_t i = 0; i < v.size; i++) data[i + oldSize] = v.data[i];
  }

  template<typename T>
  inline size_t matrix_t<T>::firstNonZeroColumnIndex(size_t rowIndex) const
  {
    if (rowIndex >= rowCount) return columnCount; // "not found" is represented by columnCount
    for (size_t i = 0; i < columnCount; i++)
      if (fabs(rowVectors[rowIndex].data[i]) != 0.0) return i;

    return columnCount;
  }

  template<typename T>
  inline size_t matrix_t<T>::firstNonZeroRowIndex(size_t startFrom) const
  {
    for (size_t i = startFrom; i < rowCount; i++)
      if (firstNonZeroColumnIndex(i) < columnCount) return i;
    return columnCount;
  }

  template<typename T>
  inline void matrix_t<T>::fusedMultiplyAdd(size_t srcIndex, T c, size_t dstIndex)
  {
    if (srcIndex >= rowCount || dstIndex >= rowCount) return;
    rowVectors[dstIndex] += rowVectors[srcIndex] * c;
  }

  template<typename T>
  inline void matrix_t<T>::produceLeadingOnes()
  {
    for (size_t i = 0; i < rowCount; i++)
    {
      size_t x = firstNonZeroColumnIndex(i);
      if (x < columnCount) // non-zero value exists
        rowVectors[i] /= rowVectors[i].data[x];
    }
  }

  template<typename T>
  inline void matrix_t<T>::rearrangeRows()
  {
    for (size_t i = 0; i < rowCount - 1; i++)
    {
      for (size_t j = i + 1; j < rowCount; j++)
      {
        size_t x = firstNonZeroColumnIndex(i), y = firstNonZeroColumnIndex(j);
        if (x > y) swap(i, j);
      }
    }
  }

  template<typename T>
  inline void matrix_t<T>::swap(size_t rowIndex1, size_t rowIndex2)
  {
    if (rowIndex1 >= rowCount || rowIndex2 >= rowCount || rowIndex1 == rowIndex2) return;

    vector_t<T> t = move(rowVectors[rowIndex1]);
    rowVectors[rowIndex1] = move(rowVectors[rowIndex2]);
    rowVectors[rowIndex2] = move(t);
  }

  template<typename T>
  inline matrix_t<T>::matrix_t()
    : rowVectors(nullptr), rowCount(0), columnCount(0)
  {
  }

  template<typename T>
  inline matrix_t<T>::matrix_t(size_t rowCount_, size_t columnCount_, T value)
    : matrix_t<T>()
  {
    if (rowCount_ == 0 || columnCount_ == 0) return;
    rowCount = rowCount_;
    rowVectors = new vector_t<T>[rowCount];
    columnCount = columnCount_;
    for (size_t i = 0; i < rowCount; i++) rowVectors[i] = vector_t<T>(columnCount, value);
  }

  template<typename T>
  inline matrix_t<T>::matrix_t(initializer_list<vector_t<T>> const&rowVectors_)
    : matrix_t<T>()
  {
    size_t rowCount_;
    if ((rowCount_ = rowVectors_.size()) == 0) return;

    rowCount = rowCount_;
    rowVectors = new vector_t<T>[rowCount];
    typename initializer_list<vector_t < T>>::const_iterator it = rowVectors_.begin(), e = rowVectors_.end();

    size_t i = 0;
    while (it != e) rowVectors[i++] = *it++;

    fixUnequalRows();
  }

  template<typename T>
  inline matrix_t<T>::matrix_t(matrix_t<T> const& m)
    : matrix_t<T>()
  {
    if (m.rowCount == 0) return;

    rowCount = m.rowCount;
    columnCount = m.columnCount;
    rowVectors = new vector_t<T>[rowCount];
    for (size_t i = 0; i < rowCount; i++) rowVectors[i] = m.rowVectors[i];
  }

  template<typename T>
  inline matrix_t<T>::matrix_t(matrix_t<T>&& m)
  {
    rowCount = m.rowCount;
    columnCount = m.columnCount;
    rowVectors = m.rowVectors;
    m.rowCount = m.columnCount = 0;
    m.rowVectors = nullptr;
  }

  template<typename T>
  inline matrix_t<T>::~matrix_t()
  {
    clear();
  }

  template<typename T>
  inline void matrix_t<T>::clear()
  {
    delete[] rowVectors;
    rowVectors = nullptr;
    rowCount = columnCount = 0;
  }

  template<typename T>
  inline bool matrix_t<T>::equals(matrix_t<T> const& m, T epsilon) const
  {
    if (this == &m) return true;
    if (rowCount != m.rowCount) return false;
    for (size_t i = 0; i < rowCount; i++)
      if (!rowVectors[i].equals(m.rowVectors[i], epsilon)) return false;
    return true;
  }

  template<typename T>
  inline void matrix_t<T>::fixUnequalRows()
  {
    columnCount = 0;
    for (size_t i = 0; i < rowCount; i++) columnCount = columnCount < rowVectors[i].size ? rowVectors[i].size : columnCount;

    if (columnCount == 0)
    {
      clear();
      return;
    }

    for (size_t i = 0; i < rowCount; i++) rowVectors[i].resize(columnCount);
  }

  template<typename T>
  inline T matrix_t<T>::getCofactor(size_t rowIndex, size_t columnIndex) const
  {
    T result = rowVectors[rowIndex].data[columnIndex] * getMinor(rowIndex, columnIndex);
    return (rowIndex + columnIndex) % 2 == 0 ? result : -result;
  }

  template<typename T>
  inline size_t matrix_t<T>::getColumnCount() const
  {
    return columnCount;
  }

  template<typename T>
  inline T matrix_t<T>::getDeterminant() const
  {
    if (rowCount != columnCount || rowCount == 0) throw logic_error("Only square matrices have determinants.");
    if (rowCount == 1) return rowVectors[0].data[0];
    if (rowCount == 2) return rowVectors[0].data[0] * rowVectors[1].data[1] - rowVectors[0].data[1] * rowVectors[1].data[0];
    T determinant = 0.0;
    // Laplace expansion along first row
    for (size_t j = 0; j < columnCount; j++) determinant += getCofactor(0, j);
    return determinant;
  }

  template<typename T>
  inline T matrix_t<T>::getMinor(size_t rowIndex, size_t columnIndex) const
  {
    return getSubmatrix(rowIndex, columnIndex).getDeterminant();
  }

  template<typename T>
  inline size_t matrix_t<T>::getRowCount() const
  {
    return rowCount;
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::getSubmatrix(size_t rowIndex, size_t columnIndex) const
  {
    if (rowCount == 0 || rowIndex >= rowCount || columnCount == 0 || columnIndex >= columnCount) return matrix_t<T>();
    matrix_t<T> result(rowCount - 1, columnCount - 1);
    size_t p = 0, q;
    for (size_t i = 0; i < rowCount; i++)
    {
      if (i == rowIndex) continue;
      q = 0;
      for (size_t j = 0; j < columnCount; j++)
      {
        if (j == columnIndex) continue;
        result.rowVectors[p].data[q] = rowVectors[i].data[j];
        q++;
      }
      p++;
    }
    return result;
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::identity(size_t rowCount)
  {
    matrix_t<T> result = matrix_t<T>(rowCount, rowCount);
    for (size_t i = 0; i < rowCount; i++)
      for (size_t j = 0; j < rowCount; j++)
        if (i == j) result.rowVectors[i].data[j] = 1.0f;
    return result;
  }

  template<typename T>
  inline void matrix_t<T>::inverse()
  {
    if (!isSquare()) throw logic_error("Matrix is not invertible.");

    matrix_t<T> copy = *this; // save a copy in case we need to roll back the changes
    matrix_t<T> id = matrix_t<T>::identity(rowCount);
    splice(id); // form augmented matrix (M | I)
    reduce(); // reduce augmented matrix; if inverse exists it should be in this form: (I | M_inverse)
    matrix_t<T> right = partition(rowCount); // get the right partition of the augmented matrix; the current object (*this) now contains the left partition

    if (id.equals(*this)) *this = move(right); // if the current object (*this) is identity matrix, then inversion was successful; therefore assign the result to the current matrix
    else
    {
      *this = move(copy); // inversion failed, restore original matrix
      throw logic_error("Matrix is not invertible.");
    }
  }

  template<typename T>
  inline bool matrix_t<T>::isSquare() const
  {
    return rowCount == columnCount;
  }

  template<typename T>
  inline matrix_t<T>& matrix_t<T>::operator=(matrix_t<T> const& m)
  {
    if (this == &m) return *this;

    clear();
    if (m.rowCount == 0) return *this;

    rowCount = m.rowCount;
    columnCount = m.columnCount;
    rowVectors = new vector_t<T>[rowCount];
    for (size_t i = 0; i < rowCount; i++) rowVectors[i] = m.rowVectors[i];

    return *this;
  }

  template<typename T>
  inline matrix_t<T>& matrix_t<T>::operator=(matrix_t<T>&& m)
  {
    if (this == &m) return *this;

    clear();
    rowCount = m.rowCount;
    columnCount = m.columnCount;
    rowVectors = m.rowVectors;
    m.rowCount = m.columnCount = 0;
    m.rowVectors = nullptr;

    return *this;
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::operator+(matrix_t<T> const& m) const
  {
    if (rowCount != m.rowCount || columnCount != m.columnCount) throw length_error("Matrix addition is not allowed when sizes of operands do not match.");

    matrix_t<T> result = *this;
    for (size_t i = 0; i < rowCount; i++) result.rowVectors[i] += m.rowVectors[i];
    return result;
  }

  template<typename T>
  inline matrix_t<T>& matrix_t<T>::operator+=(matrix_t<T> const& m)
  {
    return *this = move(*this + m);
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::operator-(matrix_t<T> const& m) const
  {
    if (rowCount != m.rowCount || columnCount != m.columnCount) throw length_error("Matrix subtraction is not allowed when sizes of operands do not match.");

    matrix_t<T> result = *this;
    for (size_t i = 0; i < rowCount; i++) result.rowVectors[i] -= m.rowVectors[i];
    return result;
  }

  template<typename T>
  inline matrix_t<T>& matrix_t<T>::operator-=(matrix_t<T> const& m)
  {
    return *this = move(*this - m);
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::operator*(T c) const
  {
    matrix_t<T> result = *this;
    for (size_t i = 0; i < rowCount; i++) result.rowVectors[i] *= c;
    return result;
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::operator*(matrix_t<T> const& m) const
  {
    if (columnCount != m.rowCount) throw length_error("Matrix-matrix multiplication is not allowed when column count of the first matrix does not match row count of the second.");

    matrix_t<T> result = matrix_t<T>(rowCount, m.columnCount);
    matrix_t<T> t = m;
    t.transpose();
    for (size_t i = 0; i < rowCount; i++)
    {
      for (size_t j = 0; j < t.rowCount; j++)
        result.rowVectors[i].data[j] = rowVectors[i] * t.rowVectors[j];
    }

    return result;
  }

  template<typename T>
  inline matrix_t<T>& matrix_t<T>::operator*=(matrix_t<T> const& m)
  {
    return *this = move(*this * m);
  }

  template<typename T>
  inline vector_t<T>& matrix_t<T>::operator[](size_t rowIndex)
  {
    if (rowCount == 0 || rowIndex > rowCount - 1) throw out_of_range("Attempting to access a non-existent row in a matrix.");
    return rowVectors[rowIndex];
  }

  template<typename T>
  inline vector_t<T> const & matrix_t<T>::operator[](size_t rowIndex) const
  {
    if (rowCount == 0 || rowIndex > rowCount - 1) throw out_of_range("Attempting to access a non-existent row in a matrix.");

    return rowVectors[rowIndex];
  }

  template<typename T>
  inline bool matrix_t<T>::parseMatrix(string const& data, matrix_t<T>& m)
  {
    size_t rowCount, columnCount;
    vector<string> tokens = split(data, " ");
    if (!parse<size_t>(tokens[0], rowCount)) return false;
    if (!parse<size_t>(tokens[1], columnCount)) return false;
    if (rowCount * columnCount != tokens.size() - 2) return false;

    m = matrix_t(rowCount, columnCount);
    size_t k = 2;
    for (size_t i = 0; i < rowCount; i++)
      for (size_t j = 0; j < columnCount; j++)
        if (!parse<T>(tokens[k++], &m.rowVectors[i].data[j])) return false;
    return true;
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::partition(size_t columnIndex)
  {
    if (rowCount == 0 || columnCount == 0 || columnCount < columnIndex) return matrix_t<T>();

    fixUnequalRows();
    matrix_t<T> right = matrix_t<T>(rowCount, columnCount - columnIndex);
    columnCount = columnIndex;
    for (size_t i = 0; i < rowCount; i++) right.rowVectors[i] = rowVectors[i].partition(columnIndex);

    return right;
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::random(size_t rowCount, size_t columnCount, T min, T max)
  {
    matrix_t<T> result = matrix_t<T>(rowCount, columnCount);
    for (size_t i = 0; i < rowCount; i++) result.rowVectors[i] = vector_t<T>::random(columnCount, min, max);
    return result;
  }

  template<typename T>
  inline matrix_t<T> matrix_t<T>::random_i(size_t rowCount, size_t columnCount, T min, T max)
  {
    matrix_t<T> result = matrix_t<T>(rowCount, columnCount);
    for (size_t i = 0; i < rowCount; i++) result.rowVectors[i] = vector_t<T>::random_i(columnCount, min, max);
    return result;
  }

  template<typename T>
  inline void matrix_t<T>::reduce(bool showSteps)
  {
    fixUnequalRows();
    if (showSteps) cout << "Initial matrix:" << endl << *this << endl << endl;

    // Gauss-Jordan elimination
    size_t srcRowIndex = 0;
    for (size_t i = 0; i < columnCount; i++)
    {
      rearrangeRows();
      srcRowIndex = firstNonZeroRowIndex(srcRowIndex);
      size_t selectedColumn = firstNonZeroColumnIndex(srcRowIndex);
      if (selectedColumn >= columnCount) break;

      for (size_t j = 0; j < rowCount; j++)
      {
        if (j == srcRowIndex) continue;
        size_t dstRowIndex = j;

        T dstValue = rowVectors[dstRowIndex].data[selectedColumn];
        if (dstValue != 0.0)
        {
          T multiplier = -dstValue / rowVectors[srcRowIndex].data[selectedColumn];
          fusedMultiplyAdd(srcRowIndex, multiplier, dstRowIndex);
          rowVectors[dstRowIndex].data[selectedColumn] = T();
        }
      }

      if (showSteps) cout << "Step " << (i + 1) << ':' << endl << *this << endl << endl;
      srcRowIndex++;
    }

    produceLeadingOnes();
    if (showSteps) cout << "Final step:" << endl << *this << endl << endl;
  }

  template<typename T>
  inline void matrix_t<T>::resize(size_t newRowCount, size_t newColumnCount)
  {
    if (rowCount == newRowCount && columnCount == newColumnCount) return;

    matrix_t<T> newMatrix = matrix_t<T>(newRowCount, newColumnCount);
    size_t smallerRowCount = rowCount <= newRowCount ? rowCount : newRowCount;

    for (size_t i = 0; i < smallerRowCount; i++)
    {
      newMatrix.rowVectors[i] = rowVectors[i];
      if (columnCount != newColumnCount) newMatrix.rowVectors[i].resize(newColumnCount);
    }

    *this = move(newMatrix);
  }

  template<typename T>
  inline void matrix_t<T>::splice(matrix_t<T> const& m)
  {
    if (rowCount != m.rowCount) throw length_error("Splicing matrices is not allowed when they have unqual number of rows.");

    fixUnequalRows();
    for (size_t i = 0; i < rowCount; i++) rowVectors[i].splice(m.rowVectors[i]);
    columnCount += m.columnCount;
  }

  template<typename T>
  inline string matrix_t<T>::toString(bool mathInputFormat) const
  {
    stringstream ss;

    if (!mathInputFormat)
    {
      ss << *this;
      return ss.str();
    }

    ss << "matrix {";
    for (size_t i = 0; i < rowCount; i++)
    {
      ss << '{';
      for (size_t j = 0; j < columnCount; j++)
      {
        ss << rowVectors[i].data[j];
        if (j < columnCount - 1) ss << ", ";
      }
      ss << '}';
      if (i < rowCount - 1) ss << ", ";
    }
    ss << '}';
    return ss.str();
  }

  template<typename T>
  inline void matrix_t<T>::transpose()
  {
    matrix_t<T> result = matrix_t<T>(columnCount, rowCount);
    for (size_t i = 0; i < result.rowCount; i++)
      for (size_t j = 0; j < result.columnCount; j++)
        result.rowVectors[i].data[j] = rowVectors[j].data[i];

    *this = move(result);
  }

  using vec_s_t = vector_t<float>;
  using vec_d_t = vector_t<double>;
  using mat_s_t = matrix_t<float>;
  using mat_d_t = matrix_t<double>;
}

namespace generic_ext
{
  template<typename T>
  inline ostream& operator<<(ostream& out, ext::vector_t<T> const& v)
  {
    out << '[';
    size_t n = v.getSize();
    for (size_t i = 0; i < n; i++)
    {
      out << v[i];
      if (i < n - 1) out << ", ";
    }
    return out << ']';
  }

  template<typename T>
  inline ostream& operator<<(ostream& out, ext::matrix_t<T> const& m)
  {
    out << '[';
    size_t r = m.getRowCount(), c = m.getColumnCount();
    for (size_t i = 0; i < r; i++)
    {
      if (i > 0) out << ' ';
      out << '[';
      for (size_t j = 0; j < c; j++)
      {
        out << m[i][j];
        if (j < c - 1) out << ' ';
      }
      out << ']';
      if (i < r - 1) out << endl;
    }
    return out << ']';
  }
}
