/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "behavior.hpp"
#include "typed_context.hpp"
#include "time_stamp.hpp"
#include "user_statistics.hpp"
using namespace rpp;
using namespace ext;
#define MAX_REQ_PATH_LEN 128
#define DEFAULT_REQ_PATH "/index.html"
#define CRLF "\r\n"

#ifdef LINUX
namespace webserver
{

  enum class IOStatus
  {
    BUFFER_OVERFLOW, SOCKET_ERROR, CONNECTION_LOST, PARSE_SUCCESS, PARSE_ERROR, INCOMPLETE_REQUEST, WOULD_BLOCK, NO_SUCH_CONNECTION
  };

  struct PartialRequest
  {
    char buffer[MAX_BUFFER_LENGTH] = {0};
    size_t length = 0;

    void reset()
    {
      memset(buffer, 0, MAX_BUFFER_LENGTH);
      length = 0;
    }
  };

  enum class RequestStatus
  {
    RS_NO_DATA, RS_PARSE_ERROR, RS_BUFFER_OVERFLOW, RS_PARTIAL_REQUEST, RS_SEND_ERROR, RS_CONN_SEVERED
  };

  struct session_t : public behavior_t
  {
  private:
    static string header;
    static const char *contents;
    static size_t header_length, contents_length;

    static string _404_header;
    static const char *_404_contents;
    static size_t _404_header_length, _404_contents_length;

    static string _405_header;
    static const char *_405_contents;
    static size_t _405_header_length, _405_contents_length;

    static user_statistics_t& stats;
    static time_stamp_t &timeStamp;

    descriptor_t fd;
    bool yieldNext;
    PartialRequest pr;
    RequestStatus parse();
  public:
    session_t(context_t& ctx_);
    ~session_t();
    void closeConnection();
    void initialize(descriptor_t fd_);
    static void reset();
    void receive(message_t *message, actor_id_t const& senderID) override;
    template<bool IsDeferred>
    static bool reply(descriptor_t fd_, int statusCode);
    static bool immediateContext(descriptor_t clientDescriptor);
  };

  template<bool IsDeferred>
  inline bool webserver::session_t::reply(descriptor_t fd_, int statusCode)
  {
    string response;
    response.reserve(header_length + contents_length + 64);

    switch (statusCode)
    {
      case 200:
        response.append(header.c_str()).append(timeStamp.now()).append(contents);
        break;
      case 404:
        response.append(_404_header.c_str()).append(timeStamp.now()).append(_404_contents);
        break;
      case 405:
        response.append(_405_header.c_str()).append(timeStamp.now()).append(_405_contents);
        break;
      default:
        rpp_assert(false, "invalid status code", GLOBAL(printer_t));
        break;
    }

    size_t n = response.length();
    ssize_t ret = ::send(fd_, response.c_str(), n, MSG_NOSIGNAL);
    if (ret != (ssize_t) n) return false;

    if (IsDeferred) stats.n_deffered++;
    else stats.n_immediate++;
    return true;
  }
}
#endif // LINUX
