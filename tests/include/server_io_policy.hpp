/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "default_io_policy.hpp"
#include "garage.hpp"
#include "cluster_attribute_set.hpp"
#include "system_statistics.hpp"

#ifdef LINUX
namespace io
{
  template<poller_type_t PollerType>
  struct event_poller_t;
}

namespace webserver
{
  struct tcp_listener_t;

  struct l1_peer_states
  {
    vector<actor_t<>> peerHandles;
    vector<atomic<bool>> readyStates;
  };

  class server_io_policy_t : public default_io_policy_t
  {
  private:
    string serverIP;
    ushort listeningPort;

    bool pinSignaler, pinSession;
    local_id_t maxActorsPerCluster;
    uint32_t backlog;
    bool enableDirectSignaling, enableRestlessSpin;
    uint32_t restlessSpin;
    bool enableImmediateContext, enableDistributedPolling, enableStatsReporter;

    tcp_listener_t *listeners;
    cluster_attribute_set_t *cas;
    l1_peer_states *peerStates;

    static garage_t& garage;
    static rpp::system_t& sys;
    static random_t& rnd;

    static rpp::system_statistics_t& stats;
    descriptor_t selfPipe;

  public:
    server_io_policy_t();
    ~server_io_policy_t();
    void churn(cluster_id_t clusterID);
    cluster_id_t getClusterCount() const;
    worker_id_t getWorkerCount(cluster_id_t clusterID) const;
    bool onBlock(cluster_id_t clusterID, worker_id_t workerID) override;
    bool onEvent(cluster_id_t clusterID, worker_id_t workerID, io_event_t const& event) override;
    bool onIdle(cluster_id_t clusterID, worker_id_t workerID) override;
    bool postLaunch(cluster_id_t clusterID) override;
    bool preLaunch(cluster_id_t clusterID) override;
    static bool respond(descriptor_t clientDescriptor);
    actor_t<>& selectL0Signaler(cluster_id_t clusterID);
    actor_t<>& selectL1Signaler(cluster_id_t clusterID, worker_id_t workerID);
    template<bool Incoming>
    void watchDescriptor(location_t const& location, descriptor_t descriptor);

    void set_l1_alarm(cluster_id_t clusterID, worker_id_t workerID);
    void raise_l1_alarm(cluster_id_t clusterID);
  };

  template<bool Incoming>
  inline void server_io_policy_t::watchDescriptor(location_t const& location, descriptor_t descriptor)
  {
    watchWithNoBlock<Incoming>(location.clusterID, location.workerID, descriptor);

#ifdef TRACE_IO_EVENT
    scout << "watching " << descriptor << " with L1 poller " << location.workerID << " on cluster " << location.clusterID << endl;
#endif // TRACE_IO_EVENT
  }
}
#endif // LINUX
