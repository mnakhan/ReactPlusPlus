/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "test_suite_base.hpp"
namespace ext
{
  int main(int argc, char **argv);

  enum class WorkLoad
  {
    BUSY, IDLE, MIXED
  };

  class test_suite : public test_suite_base
  {
  private:
    void workStealing_s(WorkLoad type, uint32_t nActors, uint32_t nIterations);
  public:
    test_suite();
    ~test_suite();
    void actorPool(uint32_t nActors);
    void behavior();
    void big(uint32_t nActors, uint32_t nMessages);
    void broadcast();
    void cluster();
    void computeWorkload(uint32_t matrixDimension, uint32_t nTasks);
    void countingActor(uint32_t nMessages);
    void diningPhilosophers(uint32_t nPhilosophers, uint32_t requiredThroughput, uint32_t windowSize);
    void flowControl();
    void forkJoin_spawn(uint32_t nActors);
    void forkJoin_throughput(uint32_t nActors, uint32_t nMessagesPerActor);
    void forkedTrail();
    void laplaceExpansion(uint32_t dimension, uint32_t maxTreeDepth);
    void latency(uint32_t initialValue);
    void messageFiltering();
    void messageToSelf(uint32_t nMessages);
    void messageTrail(uint32_t expected, uint32_t chainLength);
    void migration(uint32_t nIterations);
    void partitionedBig(uint32_t nActors, uint32_t nMessages);
    void polling();
    void priorityAssignment(uint32_t multiplier, uint32_t windowSize);
    void producerConsumer(uint32_t nMessagesPerProducer, uint32_t nProducers, uint32_t nConsumers);
    void promiseAndFuture();
    void quickSort(uint32_t size, uint32_t threshold);
    void quickSort2(uint32_t size, uint32_t depth);
    void sandbox();
    void shutdown(uint32_t nIterations);
    void signedStream();
    void synchronization(uint32_t nMessages);
    void synchronousReply(uint32_t nRequests, uint32_t nStrayMessages);
    void threadRing(uint32_t nActors, uint32_t initialValue);
    void timer();
    void typeMatching();
    void workStealing(rpp::steal_frequency_t frequency, uint32_t nActors, uint32_t nIterations);
  };
}
