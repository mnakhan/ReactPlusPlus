/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "behavior.hpp"
#include "test_suite_base.hpp"
using namespace rpp;

//#define USE_NAMED_HANDLERS
#define USE_ANON_HANDLERS
//#define USE_INPLACE_HANDLING

namespace ext
{
  struct demo : public test_suite_base
  {
    void _1();
    void _2();
    void _3();
  };

  class ClassBasedBehavior : public behavior_t
  {
  private:
    using ContextType = typed_context_t<ClassBasedBehavior>;
    template<typename T>
    void _vector(vector<T> const& lst);
    void _Delete(Delete const&);

  public:
    ClassBasedBehavior(context_t& ctx_);
    ~ClassBasedBehavior();
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  template<typename T>
  inline void ClassBasedBehavior::_vector(vector<T> const & lst)
  {
    scout << "received lst: " << lst << endl;
  }

  class X;
  class Y;
  class Z;

  template<typename... BehaviorTypes>
  class MyActor : public typed_context_t<BehaviorTypes...>
  {
  private:
    int totalMessages;
    string contextInfo;

  public:
    MyActor(int totalMessages_, string const& contextInfo_);
    ~MyActor();
    void initialize(int totalMessages_, string const& contextInfo_);

    friend class X;
    friend class Y;
    friend class Z;
  };

  template<typename... BehaviorTypes>
  inline MyActor<BehaviorTypes...>::MyActor(int totalMessages_, string const& contextInfo_)
    :totalMessages(totalMessages_), contextInfo(contextInfo_)
  {
    scout << "In context CTOR: total message count = " << totalMessages << ", context metadata = " << contextInfo << endl;
  }

  template<typename... BehaviorTypes>
  inline MyActor<BehaviorTypes...>::~MyActor()
  {
    scout << "In context DTOR: total message count = " << totalMessages << ", context metadata = " << contextInfo << endl;
  }

  template<typename... BehaviorTypes>
  inline void MyActor<BehaviorTypes...>::initialize(int totalMessages_, string const& contextInfo_)
  {
    totalMessages = totalMessages_;
    contextInfo = contextInfo_;
    scout << "In context initializer: total message count = " << totalMessages << ", context metadata = " << contextInfo << endl;
  }

  class X : public behavior_t
  {
  private:
    using ContextType = MyActor<X, Y, Z>;
    int xCounter;
    string xBehaviorInfo;
  public:
    X(context_t& ctx_);
    ~X();
    void initialize(int xCounter_, string const& xBehaviorInfo_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Y : public behavior_t
  {
  private:
    using ContextType = MyActor<X, Y, Z>;
    int yCounter;
    string yBehaviorInfo;
  public:
    Y(context_t& ctx_);
    ~Y();
    void initialize(int yCounter_, string const& yBehaviorInfo_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Z : public behavior_t
  {
  private:
    using ContextType = MyActor<X, Y, Z>;
    int zCounter;
    string zBehaviorInfo;
  public:
    Z(context_t& ctx_);
    ~Z();
    void initialize(int zCounter_, string const& zBehaviorInfo_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };
};

