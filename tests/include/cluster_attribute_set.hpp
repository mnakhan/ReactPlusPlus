/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace webserver
{

  class cluster_attribute_set_t
  {
  private:
    cluster_id_t expectedSize, currentSize;
    unordered_map<cluster_id_t, unordered_map<string, string>> clusterAttributes;
    mutex mx_clusterAttributes;
  public:
    cluster_attribute_set_t(cluster_id_t expectedSize_);
    void append(cluster_id_t clusterID, descriptor_t L0_descriptor, vector<descriptor_t> const& L1_descriptors, descriptor_t listener_descriptor);
    bool isDone();
    string toString();
  };
}
