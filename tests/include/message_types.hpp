/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace ext
{
  struct Trigger {};

  struct CustomMessage
  {
    int i;
    string s;
  };

  struct Increment {};
  struct QueryCounter {};
  struct OneShotNotification {};
  struct PeriodicNotification {};
  struct Ping {};
  struct PingWithIndex
  {
    uint32_t senderIndex;
  };
  struct Pong {};
  struct AllPingsSent {};
  struct Acquire
  {
    uint32_t philosopherLocation;
  };

  struct Release
  {
    uint32_t philosopherLocation;
  };

  struct ArbiterReady {};
  struct Raise {};
  struct Rearm {};

  struct Pulse {};

  struct ImmutableMessage
  {
    string contents;
    ImmutableMessage() : contents("xyz") {}
  };
}
