/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "system.hpp"
using namespace rpp;

#ifdef LINUX
namespace webserver
{
  int main(int argc, char **argv);

  class server_t
  {
  private:
    static rpp::system_t& sys;
  public:
    server_t() = default;
    server_t(server_t const&) = delete;
    void run(string const& configurationPath);
  };
}
#endif // LINUX
