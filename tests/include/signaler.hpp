/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "behavior.hpp"
#include "actor.hpp"
using namespace rpp;

namespace io
{
  struct io_policy;
}

#ifdef LINUX
namespace webserver
{

  class signaler_t : public behavior_t
  {
  private:
    io::io_policy_t *policy;
  public:
    signaler_t(context_t& ctx_);
    ~signaler_t();
    void initialize(io::io_policy_t *policy_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class l0_signaler_t : public behavior_t
  {
  private:
    cluster_id_t clusterID;
    io::io_policy_t *policy;
    void *l0Generator;
    worker_id_t L1Size;
    vector<rpp::actor_t<domain_t>> l1Signalers;

  public:
    l0_signaler_t(context_t& ctx_);
    ~l0_signaler_t();
    void initialize(io::io_policy_t *policy_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class l1_signaler_t : public behavior_t
  {
  private:
    cluster_id_t clusterID;
    worker_id_t peerIndex;

    io::io_policy_t *policy;
    void *l1Generator;
    uint32_t burstLength, capacity;

    actor_t<> l0Signaler;
    uint32_t spinCount, maxSpin;
    uint64_t lastSnapshot;
    bool isSnapshotSet;

    uint64_t savedEventCounter;

    static generic_ext::random_t& rnd;
    static atomic<uint64_t> round;

  public:
    l1_signaler_t(context_t& ctx_);
    ~l1_signaler_t();
    void initialize(io::io_policy_t *policy_, worker_id_t peerIndex_);
    void receive(message_t *message, actor_id_t const& senderID) override;
    static uint64_t getSnapshot();
    static void advanceRound();

  };

}
#endif // LINUX
