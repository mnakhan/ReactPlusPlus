/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
#include "system_statistics.hpp"
using namespace rpp;
using namespace generic_ext;

#ifdef LINUX
namespace webserver
{
  template<int SignalNumber>
  void signalHandler(int);

  class self_pipe_t
  {
  private:
    descriptor_t endpoints[2];
  public:
    self_pipe_t();
    ~self_pipe_t();
    descriptor_t getReadEnd() const;
    descriptor_t getWriteEnd() const;
  };

  template<>
  inline void signalHandler<SIGINT>(int)
  {
    system_statistics_t& stats = GLOBAL(system_statistics_t);
    if (stats.isShuttingDown) exit(0);

    stats.isShuttingDown = true;
    self_pipe_t& pipe = GLOBAL(self_pipe_t);
    char c = 0;
    ssize_t ret = write(pipe.getWriteEnd(), &c, 1);
    assert(ret == 1);
  }

  template<int SignalNumber>
  inline void attachHandler()
  {
    struct sigaction a;
    sigemptyset(&a.sa_mask);
    a.sa_flags = 0;
    a.sa_handler = signalHandler<SignalNumber>;
    sigaction(SignalNumber, &a, nullptr);
  }
}
#endif // LINUX
