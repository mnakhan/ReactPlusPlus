/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "actor.hpp"
using namespace rpp;

namespace webserver
{

  enum class io_actor_type_t
  {
    signaler = 0, session = 1
  };

  class garage_t
  {
  private:
    cluster_id_t nClusters;
    worker_id_t L1Size;
    local_id_t maxStandby;
    actor_t<> **signalers, **sessions;
    bool initialized;
  public:
    garage_t();
    ~garage_t();
    template<io_actor_type_t ActorType>
    actor_t<>& select(actor_id_t garageID);
    void initialize(cluster_id_t nClusters_, worker_id_t L1Size_, local_id_t maxStandby_);
    void terminate(cluster_id_t clusterID);
  };

  template<>
  inline actor_t<>& garage_t::select<io_actor_type_t::signaler>(actor_id_t garageID)
  {
    return signalers[garageID.clusterID][garageID.localID];
  }

  template<>
  inline actor_t<>& garage_t::select<io_actor_type_t::session>(actor_id_t garageID)
  {
    return sessions[garageID.clusterID][garageID.localID];
  }
}
