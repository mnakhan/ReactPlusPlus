/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "generic.hpp"
using namespace io;
namespace profiling
{
  struct benchmarks
  {
  private:
    static void usage_s();
    template<typename First, typename... Rest>
    static void usage_s(First first, Rest... rest);
    template<typename... ArgTypes>
    static void usage(const char *programName, ArgTypes... args);
  public:
    static void big(int argc, char **argv);
    static void partitionedBig(int argc, char **argv);
    static void countingActor(int argc, char **argv);
    static void diningPhilosophers(int argc, char **argv);
    static void forkJoin_spawn(int argc, char **argv);
    static void forkJoin_throughput(int argc, char **argv);
    static void laplaceExpansion(int argc, char **argv);
    static void pingPong(int argc, char **argv);
    static void producerConsumer(int argc, char **argv);
    static void quickSort(int argc, char **argv);
    static void quickSort2(int argc, char **argv);
    static void shutdown();
    static void threadRing(int argc, char **argv);
  };

  template<typename First, typename... Rest>
  inline void benchmarks::usage_s(First first, Rest... rest)
  {
    scout << ' ' << '<' << first << '>';
    usage_s(rest...);
  }

  template<typename... ArgTypes>
  inline void benchmarks::usage(const char *programName, ArgTypes... args)
  {
    scout << "Usage: " << programName;
    usage_s(args...);
  }
};
