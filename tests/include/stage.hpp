/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "behavior.hpp"
#include "typed_context.hpp"
#include "matrix.hpp"
#include "atom.hpp"
#include "user_statistics.hpp"

using namespace rpp;

namespace ext
{
  template<typename... BehaviorTypes>
  struct TestActor : public typed_context_t<BehaviorTypes...>
  {
    int x = 0;
    TestActor(int x_)
      :x(x_)
    {
      scout << "TestActor::CTOR " << x << endl;
    }
    ~TestActor()
    {
      scout << "TestActor::x = " << x << endl;
#ifdef TRACE_DTOR
      scout << "~TestActor" << endl;
#endif // TRACE_DTOR
    }
    void initialize(int x_)
    {
      x = x_;
      scout << "TestActor::init " << x << endl;
    }
  };

  struct B;
  struct C;

  struct A : public behavior_t
  {
    using ContextType = TestActor<A, B, C>;
    int x = 0;
    A(context_t& ctx_) : behavior_t(ctx_) {}
    void initialize(int x_)
    {
      scout << "A::init " << x_ << endl;
    }

    ~A()
    {
      scout << "A::x = " << x << endl;
#ifdef TRACE_DTOR
      scout << "~A" << endl;
#endif // TRACE_DTOR
    }
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  struct B : public behavior_t
  {
    using ContextType = TestActor<A, B, C>;
    int x = 0;
    B(context_t& ctx_) : behavior_t(ctx_) {}
    ~B()
    {
      scout << "B::x = " << x << endl;
#ifdef TRACE_DTOR
      scout << "~B" << endl;
#endif // TRACE_DTOR
    }
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  struct C : public behavior_t
  {
    using ContextType = TestActor<A, B, C>;
    int x = 0;
    C(context_t& ctx_) : behavior_t(ctx_) {}
    ~C()
    {
      scout << "C::x = " << x << endl;
#ifdef TRACE_DTOR
      scout << "~C" << endl;
#endif // TRACE_DTOR
    }
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Producer : public behavior_t
  {
  private:
    uint32_t nMessages;
    vector<actor_id_t> consumerIDs;
  public:
    Producer(context_t& ctx_);
    ~Producer();
    void initialize(uint32_t nMessages_, vector<actor_id_t> const& consumerIDs_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Consumer : public behavior_t
  {
  private:
    uint32_t received, expected;
  public:
    Consumer(context_t& ctx_);
    ~Consumer();
    void initialize(uint32_t expected_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Spinner : public behavior_t
  {
  private:
    bool yield;
    uint32_t nReceived, nExpected;
    void _atom();
    void _uint32_t(uint32_t message);
  public:
    Spinner(context_t& ctx_);
    ~Spinner();
    void initialize(uint32_t nExpected_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class PingPong : public behavior_t
  {
  private:
    actor_t<domain_t> partner;
    void _uint32_t(uint32_t token);
    void _Delete(Delete const&);
  public:
    PingPong(context_t& ctx_);
    ~PingPong();
    void initialize(actor_t<domain_t> const& partner_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Counter : public behavior_t
  {
  private:
    uint32_t currentValue;
  public:
    Counter(context_t& ctx_);
    ~Counter();
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Big : public behavior_t
  {
  private:
    struct
    {
      unsigned termReceived : 1;
      unsigned allPongsReceived : 1;
    } flags;

    uint32_t maxMessages, totalPongsReceived;
    vector<actor_t<domain_t>> recipients;
    actor_t<domain_t> sink;
    actor_id_t selfID;
    uint32_t selfIndex;
    PingWithIndex pingWithIndex;
    static random_t& rnd;
    void cleanup();
    bool mayExit() const;
  public:
    Big(context_t& ctx_);
    ~Big();
    void initialize(uint32_t maxMessages_, vector<actor_t<domain_t>> const& recipients_, actor_t<domain_t> const& sink_, uint32_t selfIndex_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Sink : public behavior_t
  {
  private:
    vector<actor_t<domain_t>> recipients;
    uint32_t nReceived;
    size_t nActors;
    actor_id_t selfID;
  public:
    Sink(context_t& ctx_);
    ~Sink();
    void initialize(vector<actor_t<domain_t>> const& recipients_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class ThreadRingNode : public behavior_t
  {
  private:
    actor_t<domain_t> next;
  public:
    ThreadRingNode(context_t& ctx);
    ~ThreadRingNode();
    void initialize(actor_t<domain_t> const& next_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  template<typename T>
  struct Sorter : public behavior_t
  {
    typedef pair<bool, list<T>> ReplyType;
    actor_id_t parentID;
    bool isFirst;
    T pivotValue;
    size_t nReplies;
    list<T> result, firstPartition, secondPartition;
    static random_t& rnd;
    void replyToParent() const;
  public:
    static uint32_t threshold;
    Sorter(context_t& ctx_);
    ~Sorter();
    void initialize(actor_id_t const& parentID_, bool isFirst_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  template<typename T>
  uint32_t Sorter<T>::threshold = 1;
  template<typename T>
  random_t& Sorter<T>::rnd = THR_LOCAL(random_t);

  template<typename T>
  inline void Sorter<T>::replyToParent() const
  {
    actor_t<domain_t> parent = actor_t<domain_t>::map(parentID);
    parent | make_pair(isFirst, move(result));
    ctx.quit();
  }

  template<typename T>
  inline Sorter<T>::Sorter(context_t& ctx_)
    :behavior_t(ctx_), isFirst(false), pivotValue(T()), nReplies(0)
  {
  }

  template<typename T>
  inline void Sorter<T>::initialize(actor_id_t const& parentID_, bool isFirst_)
  {
    parentID = parentID_;
    isFirst = isFirst_;
  }

  template<typename T>
  inline Sorter<T>::~Sorter()
  {
#ifdef TRACE_DTOR
    scout << "~Sorter" << endl;
#endif // TRACE_DTOR
  }

  template<typename T>
  inline void Sorter<T>::receive(message_t *message, actor_id_t const& senderID)
  {
    ReplyType reply;
    if (message->extract(result))
    {
      actor_id_t selfID = ctx.getID();
      if (result.size() <= threshold)
      {
        result = quickSort(move(result));
        replyToParent();
        return;
      }

      size_t pivotIndex = rnd.nextInt<size_t>(0, result.size() - 1);
      typename list<T>::iterator it = result.begin();
      advance(it, pivotIndex);
      assert(it != result.end());
      pivotValue = *it;
      result.erase(it);
      typename list<T>::iterator pivot = partition(result.begin(), result.end(), [this](T const& x)
      {
        return x <= pivotValue;
      });

      firstPartition.splice(firstPartition.end(), result, result.begin(), pivot);
      secondPartition.splice(secondPartition.end(), result, result.begin(), result.end());


      using T_Sorter = Sorter<T>;
      using T_Context = typed_context_t<T_Sorter>;
      auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
        .at(location_t::map());
#else
        .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS

      actor_t<domain_t> child1 = actor_t<domain_t>::spawn<T_Context>(props),
        child2 = actor_t<domain_t>::spawn<T_Context>(props);

      child1.initialize<T_Sorter>(selfID, true);
      child2.initialize<T_Sorter>(selfID, false);

      child1.enableContext<T_Context>().template withBehavior<T_Sorter>();
      child2.enableContext<T_Context>().template withBehavior<T_Sorter>();

#ifdef DEBUG
      scout << "actor " << selfID << " split list into partitions " << firstPartition << " (sent to " << child1.getID() << ") and " << secondPartition << " (sent to " << child2.getID() << ")." << endl;
#endif // DEBUG

      child1 | move(firstPartition);
      child2 | move(secondPartition);
    }
    else if (message->extract<ReplyType>(reply))
    {
      bool isFirst = reply.first;
      if (isFirst) firstPartition = move(reply.second);
      else secondPartition = move(reply.second);
      nReplies++;
      if (nReplies < 2) return;

      result = move(firstPartition);
      result.push_back(pivotValue);
      result.splice(result.end(), secondPartition, secondPartition.begin(), secondPartition.end());
      replyToParent();
    }
  }

  template<typename OperationType>
  struct Compute : public typed_context_t<>
  {
    actor_id_t generatorID;
    uint32_t matrixIndex;
    Compute(actor_id_t const& generatorID_, uint32_t matrixIndex_);
    ~Compute();
  };

  template<typename OperationType>
  inline Compute<OperationType>::Compute(actor_id_t const& generatorID_, uint32_t matrixIndex_)
    : generatorID(generatorID_), matrixIndex(matrixIndex_)
  {
  }

  template<typename OperationType>
  inline Compute<OperationType>::~Compute()
  {
#ifdef TRACE_DTOR
    scout << "~Compute" << endl;
#endif // TRACE_DTOR
  }

  struct Inverse
  {
  };

  struct Determinant
  {
  };

  class Collector : public behavior_t
  {
  private:
    vector<mat_d_t> matrices, inverses;
    vector<double> determinants;
    string outputPath;
    uint32_t matrixDimension, numberOfTasks, expectedInverses, expectedDeterminants;

    void checkProgress();
    void collectDeterminant(pair<uint32_t, double> const& reply);
    void collectInverse(pair<uint32_t, mat_d_t> const& reply);
    void spawnWorkers();
  public:
    Collector(context_t& ctx_);
    ~Collector();
    void receive(message_t *message, actor_id_t const& senderID) override;
    void initialize(string const& outputPath_, uint32_t matrixDimension_, uint32_t numberOfTasks_);
  };

  class Compute2 : public behavior_t
  {
  private:
    typedef tuple<size_t, size_t, double> ReplyType;

    mat_d_t matrix;
    actor_id_t parentID;
    size_t rowIndexOfMinor, columnIndexOfMinor;

    double determinant;
    size_t nReplies;
    static user_statistics_t& gc;

    void replyToParent() const;
  public:
    static uint32_t maxTreeDepth;
    Compute2(context_t& ctx_);
    ~Compute2();
    void initialize(actor_id_t parentID_, size_t rowIndexOfMinor_, size_t columnIndexOfMinor_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  struct Simple : public behavior_t
  {
  public:
    Simple(context_t& ctx_);
    ~Simple();
  };

  class Simple2 : public behavior_t
  {
  private:
    void _trigger();
    void _int(int x);
    void _uint(uint32_t x);
    void _string(string const& x);
    void _delete();
    void _Delete(Delete const&);
  public:
    Simple2(context_t& ctx_);
    ~Simple2();
    void initialize(string const& initMessage);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  struct Server : public behavior_t
  {
    Server(context_t& ctx_);
    ~Server();
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Client : public behavior_t
  {
  private:
    actor_id_t serverID;
    uint32_t nRequests, nCompleted;
    void echo();
  public:
    Client(context_t& ctx_);
    ~Client();
    void initialize(actor_id_t const& serverID_, uint32_t nRequests_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };


  template<typename T>
  class Sorter2 : public behavior_t
  {
  private:
    static user_statistics_t& counters;
    T *data;
    uint32_t low, high, depth;
    static void quickSort(T *data, uint32_t low, uint32_t high, uint32_t depth);
  public:
    Sorter2(context_t& ctx_);
    ~Sorter2();
    void initialize(T *data_, uint32_t low_, uint32_t high_, uint32_t depth_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  template<typename T>
  ext::user_statistics_t& Sorter2<T>::counters = GLOBAL(user_statistics_t);

  template<typename T>
  inline Sorter2<T>::Sorter2(context_t& ctx_)
    :behavior_t(ctx_), data(nullptr), low(0), high(0), depth(0)
  {
    counters.n_qs_actors++;
  }

  template<typename T>
  inline Sorter2<T>::~Sorter2()
  {
#ifdef TRACE_DTOR
    scout << "~Sorter2" << endl;
#endif // TRACE_DTOR
  }

  template<typename T>
  inline void Sorter2<T>::initialize(T *data_, uint32_t low_, uint32_t high_, uint32_t depth_)
  {
    data = data_;
    low = low_;
    high = high_;
    depth = depth_;
  }

  template<typename T>
  inline void Sorter2<T>::receive(message_t *message, actor_id_t const& senderID)
  {
    if (!atom_t<>::type<Trigger>(message)) return;
    quickSort(data, low, high, depth);
    ctx.quit();
  }

  template<typename T>
  void Sorter2<T>::quickSort(T *data, uint32_t low, uint32_t high, uint32_t depth)
  {
    if (low >= high) return;
    uint32_t mid = low + (high - low) / 2;
    T pivot = data[mid];
    uint32_t left = low, right = high;
    do
    {
      while (data[left] < pivot) left++;
      while (pivot < data[right]) right--;
      if (left > right) continue;
      T tmp = move(data[left]);
      data[left] = move(data[right]);
      data[right] = move(tmp);
      left++;
      if (right > 0) right--;
      else break;
    } while (left <= right);


    if (depth > 0)
    {
      using BehaviorType = Sorter2<T>;
      using ContextType = typed_context_t<BehaviorType>;
      auto props = props_t::with<mailbox_type_t::DV>()
#ifdef USE_NATIVE_ACTORS
        .at(location_t::generate<RoundRobin>::on(CLUSTER<0>{}));
#else
      .at(location_t::DetachedCluster);
#endif // USE_NATIVE_ACTORS
      actor_t<domain_t> child = actor_t<domain_t>::spawn<ContextType>(props);
      child.initialize<BehaviorType>(data, low, right, depth - 1);
      child.enableContext<ContextType>().template withBehavior<BehaviorType>();
      child << atom::trigger;
      counters.n_qs_messages++;
      quickSort(data, left, high, depth - 1);
    }
    else
    {
      quickSort(data, low, right, 0);
      quickSort(data, left, high, 0);
    }
  }

  enum class PhilosopherState : int
  {
    Thinking = 0,
    Eating = 1
  };

  class BeginCycle;
  class Allocate;
  class Arbiter : public typed_context_t<BeginCycle, Allocate>
  {
  private:
    uint32_t nPhilosophers;
    PhilosopherState *states;
    vector<actor_t<domain_t>> philosophers;
  public:
    Arbiter();
    ~Arbiter();
    bool acquire(uint32_t philosopherLocation);
    void initialize(vector<actor_t<domain_t>> const& philosophers_);
    PhilosopherState leftOf(uint32_t philosopherLocation) const;
    bool release(uint32_t philosopherLocation);
    PhilosopherState rightOf(uint32_t philosopherLocation) const;
    string toString() const;

    friend class BeginCycle;
    friend class Allocate;
  };

  class BeginCycle : public behavior_t
  {
  private:
    using ContextType = Arbiter;
    ContextType *context;
  public:
    BeginCycle(context_t& ctx_);
    ~BeginCycle();
    void initialize();
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class Allocate : public behavior_t
  {
  private:
    using ContextType = Arbiter;
    ContextType *context;
    uint32_t numRequestsExpected, numRequestsProcessed, currentThroughput, requiredThroughput, failedAttempts;
    Acquire acq;
    Release rel;
  public:
    Allocate(context_t& ctx_);
    ~Allocate();
    void initialize(uint32_t requiredThroughput_, uint32_t windowSize);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class TestChannel : public behavior_t
  {
  private:
    channel_t ch1, ch2;
  public:
    TestChannel(context_t& ctx_);
    ~TestChannel();
    void initialize(channel_t const& ch1_, channel_t const& ch2_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class TestException : public behavior_t
  {
  private:
    int32_t expected;
    bool isFirst, isLast;
    actor_t<domain_t> next;
  public:
    TestException(context_t& ctx_);
    ~TestException();
    void initialize(int32_t expected_, bool isFirst_, bool isLast_, actor_t<domain_t> const& next_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };

  class ForkedTrail : public behavior_t
  {
    string name;
    vector<string> receiverTags;
    bool isRoot, isSink;
    uint32_t nReceived;
  public:
    ForkedTrail(context_t& ctx_);
    void initialize(string const& name_);
    void receive(message_t *message, actor_id_t const& senderID) override;
  };
}
