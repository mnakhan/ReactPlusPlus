/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "clock.hpp"
#include "system.hpp"
#include "user_statistics.hpp"

namespace ext
{
  class test_suite_base
  {
  protected:
    clock_hr_t watch;
    ostringstream oss_desc;
    bool isCapturing;

    void capture();
    ostringstream& description();
    void printFooter(bool isEnd = true);
    void printHeader();
  public:
    static rpp::system_t& sys;
    static printer_t& tscout;
    static user_statistics_t& gc;

    test_suite_base();
    virtual ~test_suite_base();

    static void initialize();
  };
}
