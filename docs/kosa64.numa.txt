available: 8 nodes (0-7)
node 0 cpus: 0 1 2 3 4 5 6 7
node 0 size: 64444 MB
node 0 free: 64268 MB
node 1 cpus: 8 9 10 11 12 13 14 15
node 1 size: 64510 MB
node 1 free: 64279 MB
node 2 cpus: 16 17 18 19 20 21 22 23
node 2 size: 64510 MB
node 2 free: 63938 MB
node 3 cpus: 24 25 26 27 28 29 30 31
node 3 size: 64510 MB
node 3 free: 64229 MB
node 4 cpus: 32 33 34 35 36 37 38 39
node 4 size: 64510 MB
node 4 free: 64297 MB
node 5 cpus: 40 41 42 43 44 45 46 47
node 5 size: 64510 MB
node 5 free: 64263 MB
node 6 cpus: 48 49 50 51 52 53 54 55
node 6 size: 64510 MB
node 6 free: 63921 MB
node 7 cpus: 56 57 58 59 60 61 62 63
node 7 size: 64472 MB
node 7 free: 64080 MB
node distances:
node   0   1   2   3   4   5   6   7 
  0:  10  16  16  22  16  22  16  22 
  1:  16  10  22  16  22  16  22  16 
  2:  16  22  10  16  16  22  16  22 
  3:  22  16  16  10  22  16  22  16 
  4:  16  22  16  22  10  16  16  22 
  5:  22  16  22  16  16  10  22  16 
  6:  16  22  16  22  16  22  10  16 
  7:  22  16  22  16  22  16  16  10
