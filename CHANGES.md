ActorFramework
---------------
* no busy-waiting in scheduler
* lock-free mailbox (2 variants) and scheduling (atomic fetch_add/fetch_sub)
* minimal references to the global hashtable
* dispatch table mapping type --> lambda (see functional actors) for pattern matching

Changelog (5.8.2019)
--------------------
* added support for cluster
* a new ready queue to reduce lock-contention
* synchronized send

Changelog (5.17.2019)
---------------------
* integrated detached actors
* new work-stealing parameters
* a new type of mailbox adapted from ZeroMQ
* generic poller (passed to the scheduler as a work-generator)

Changelog (6.11.2019)
---------------------
* polling support for inert actors
* timer with generic handlers for periodic and oneshot events
* added compute benchmark using pattern matching syntax
* yielding to scheduler
* static atoms with zero memory allocation
* a thread-safe stream (scout) that uses "<<" syntax, the original printer works like printf.
* message filtering based on typed predicates (predicate applies if the type matches)

Changelog (8.6.2019)
---------------------
* separation of behavior and context
* synchronous request/reply (userspace blocking support)
* atoms are allowed to carry senderID (relayed by message node)

Changelog (1.16.2020)
----------------------
* Workers proactively balance queues --> reduces unsuccessful thefts.
* Added barrier (enableContext<ContextType>().withBehavior<BehaviorType>()) to prevent potential data races caused by incorrect usage of initialize<T>.
* Mailbox type is allowed vary on a per-actor basis.
* Added per-actor dispatch size, which overrides global dispatch size.
* Spawn configuration is encapsulated in Props class.

Changelog (3.12.2020)
----------------------
* Added NUMA awareness and hierarchical work-stealing to scheduler.

Changelog (4.26.2020)
---------------------
Designed I/O subsystem and combined with AWSDP project.

Changelog (5.21.2020)
---------------------
Combined with AWSCP project. The webserver can switch between distributed and centralized polling using a configuration parameter.

Changelog (6.8.2020)
---------------------
Added I/O scripts for wrk and weighhttp benchmarks.

Changelog (6.29.2020)
---------------------
Added channel prototype, webserver cleanup and ZMQ benchmark scripts.

Changelog (7.13.2020)
---------------------
Updated channel and barrier definitions.
Added support for tags. Tags can be bound to actor IDs (many-to-many relations) and groups of actors can be selected using associated tags.

Changelog (7.18.2020)
---------------------
Added support for message trail.

Changelog (7.21.2020)
---------------------
Fixed quicksort2 anomaly (round-robin placement & parallel loader).
Switched to thread-local random number generators.
Added a configuration generator.
Added version info.

Changelog (8.8.2020)
---------------------
Added support for bounded mailbox.
