CONFIG = RELEASE

ifeq (${CONFIG},DEBUG)
SO_PATH = core/dist/debug/librpp.so
else ifeq (${CONFIG},RELEASE)
SO_PATH = core/dist/release/librpp.so
endif

.PHONY: help all run clean

help:
	@echo "Makefile Targets"
	@echo " * all: build actor library and tests"
	@echo " * run-tests: build and run unit tests"
	@echo " * run-webserver: build and run actor-based webserver"
	
all:
	cd core && ${MAKE} CONFIG=${CONFIG}
	cd tests && ${MAKE} CONFIG=${CONFIG}

run-tests:
	@if [ ! -f ${SO_PATH} ]; then \
		cd core && ${MAKE} clean && ${MAKE} CONFIG=${CONFIG}; \
	fi
	cd tests && ${MAKE} run-tests CONFIG=${CONFIG}

run-webserver:
	@if [ ! -f ${SO_PATH} ]; then \
		cd core && ${MAKE} clean && ${MAKE} CONFIG=${CONFIG}; \
	fi
	cd tests && ${MAKE} run-webserver CONFIG=${CONFIG}
	
clean:
	cd core && ${MAKE} clean
	cd tests && ${MAKE} clean
