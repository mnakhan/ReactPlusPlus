/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "message.hpp"
#include "receiver.hpp"

void rpp::type_erased_receiver_t::clear()
{
  if (isDisabled) return;
  for_each(dispatchTable.cbegin(), dispatchTable.cend(), [](pair<type_index, receiver_t*> const& p)
  {
    delete p.second;
  });
  dispatchTable.clear();
  anyHandler.isSet = false;
}

rpp::type_erased_receiver_t& rpp::type_erased_receiver_t::placeholder()
{
  thread_local static type_erased_receiver_t receiver(true);
  return receiver;
}

rpp::type_erased_receiver_t::type_erased_receiver_t(type_erased_receiver_t&& receiver)
{
  if (isDisabled) return;
  dispatchTable = move(receiver.dispatchTable);
  anyHandler = move(receiver.anyHandler);
}

rpp::type_erased_receiver_t::type_erased_receiver_t(bool isDisabled_)
  :isDisabled(isDisabled_)
{
}

rpp::type_erased_receiver_t::~type_erased_receiver_t()
{
  clear();
}

bool rpp::type_erased_receiver_t::match(context_t *ctx, message_t *message, actor_id_t const& senderID) const
{
  if (isDisabled) return false;
  unordered_map<type_index, receiver_t*>::const_iterator it = dispatchTable.find(message->tiContents);
  if (it != dispatchTable.end()) return it->second->match(ctx, message, senderID);
  if (anyHandler.isSet && anyHandler.predicatePtr(message))
  {
    anyHandler.handlerPtr(ctx, message, senderID);
    return true;
  }
  return false;
}

rpp::type_erased_receiver_t& rpp::type_erased_receiver_t::operator=(type_erased_receiver_t&& receiver)
{
  if (this == &receiver || isDisabled) return *this;
  clear();
  dispatchTable = move(receiver.dispatchTable);
  anyHandler = move(receiver.anyHandler);
  return *this;
}

rpp::type_erased_receiver_t& rpp::type_erased_receiver_t::reset()
{
  clear();
  return *this;
}
