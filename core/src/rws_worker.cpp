/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "rws_scheduler.hpp"
#include "rws_worker.hpp"
#include "platform.hpp"

namespace rpp
{
  generic_ext::random_t& rpp::rws_worker_t::rnd = THR_LOCAL(random_t);
  generic_ext::printer_t& rpp::rws_worker_t::tscout = GLOBAL(printer_t);
  uint64_t rpp::rws_worker_t::maxMessagesPerDispatch = UINT64_MAX;
  rpp::steal_frequency_t rpp::rws_worker_t::frequency = steal_frequency_t::NORMAL;
  double rpp::rws_worker_t::freqMultiplier = 1.0;
}

rpp::rws_worker_t::rws_worker_t()
  : clusterID(DEFAULT_CLUSTER_ID), workerID(DEFAULT_WORKER_ID),
    nodeID(DEFAULT_NODE_ID), cpuID(DEFAULT_CPU_ID),
    nInvocations(0), nThefts(0), nIdleSpins(0),
    scheduler(nullptr), isRunning(false), hasShutdown(false), workerActive(false),
    searchLevel(0), isNUMAAware(false)
{
}

rpp::rws_worker_t::~rws_worker_t()
{
  shutdown();
}

worker_id_t rpp::rws_worker_t::chooseVictim() const
{
  if (isNUMAAware)
  {
    size_t neighborhoodSize = neighborCPUs.size();
    if (neighborhoodSize == 1 && neighborCPUs[0] == workerID) return DEFAULT_WORKER_ID;

    size_t victimIndex = rnd.nextInt<size_t>(0, neighborhoodSize - 1);
    processor_id_t victimCPUID = neighborCPUs[victimIndex];

    // don't select thief as victim
    if (victimCPUID == cpuID)
    {
      victimIndex = (victimIndex + 1) % neighborhoodSize;
      victimCPUID = neighborCPUs[victimIndex];
      if(victimCPUID == cpuID) return DEFAULT_WORKER_ID;
    }
    return scheduler->platform->mapProcessorToWorker(victimCPUID);
  }

  // NUMA-agnostic victim selection
  worker_id_t victimID = rnd.nextInt<worker_id_t>(0, scheduler->nWorkers - 1);
  if (victimID == workerID) victimID = (victimID + 1) % scheduler->nWorkers;
  if (victimID == workerID) return DEFAULT_WORKER_ID;
  return victimID;
}

void rpp::rws_worker_t::clearStats()
{
  nInvocations = nThefts = nIdleSpins = 0;
  readyQueue.allocator.clearStats();
}

double rpp::rws_worker_t::getFrequencyMultiplier()
{
  return freqMultiplier;
}

uint64_t rpp::rws_worker_t::getIdleSpinCount() const
{
  return nIdleSpins;
}

uint64_t rpp::rws_worker_t::getInvocationCount() const
{
  return nInvocations;
}

uint64_t rpp::rws_worker_t::getReadyQueueLength() const
{
  return readyQueue.getSize();
}

rpp::steal_frequency_t rpp::rws_worker_t::getStealFrequency()
{
  return frequency;
}

uint64_t rpp::rws_worker_t::getTheftCount() const
{
  return nThefts;
}

void rpp::rws_worker_t::setStealFrequency(steal_frequency_t frequency_)
{
  frequency = frequency_;
}

void rpp::rws_worker_t::setFrequencyMultiplier(double freqMultiplier_)
{
  freqMultiplier = freqMultiplier_;
}

void rpp::rws_worker_t::initialize(cluster_id_t clusterID_, worker_id_t workerID_, rws_scheduler_t *scheduler_)
{
  clusterID = clusterID_;
  workerID = workerID_;
  nInvocations = nThefts = nIdleSpins = 0;
  scheduler = scheduler_;
  isRunning = true;
  workerActive = false;
  hasShutdown = false;

  neighborCPUs.clear();
  searchLevel = 0;
  isNUMAAware = scheduler->platform && scheduler->platform->hasValidConfiguration() && scheduler->platform->hasNUMAAwareness();
  
  if (isNUMAAware)
  {
    cpuID = scheduler->platform->mapWorkerToProcessor(clusterID, workerID);
    nodeID = scheduler->platform->mapProcessorToNode(cpuID);
  }
  else
  {
    cpuID = DEFAULT_CPU_ID;
    nodeID = DEFAULT_NODE_ID;
  }
}

void rpp::rws_worker_t::makeReady(shared_ptr<runnable_t> const& job)
{
  readyQueue.stage(job);
}

void rpp::rws_worker_t::resetDispatchPolicy()
{
  maxMessagesPerDispatch = UINT64_MAX;
}

void rpp::rws_worker_t::run()
{
  rpp_assert(!workerActive, "Reentry into worker subroutine while it is still active.", tscout);
  if (isNUMAAware)
  {
    [[maybe_unused]] bool success = false;
    success = platform_t::pinSelfToCPU(cpuID);
#ifdef TRACE_STARTUP
    if (success) tscout.printlnnws("TRACE: c", clusterID, "/w", workerID, " is pinned to CPU ", cpuID);
    else tscout.printlnnws("TRACE: could not pin c", clusterID, "/w", workerID, " to CPU ", cpuID);
#endif // TRACE_STARTUP
  }

  ThisThread::registerThread(nodeID, cpuID, clusterID, workerID);
  workerActive = true;
  auto executor = [this](shared_ptr<runnable_t> const& job)
  {
    uint64_t totalDispatched = job->dispatch(maxMessagesPerDispatch);
    nInvocations += totalDispatched;
    return job->mayReschedule(totalDispatched);
  };

  io_policy_t *policy = scheduler->policy;
  uint32_t maxSpin = policy ? policy->spin : 0U;
  rws_worker_t *workers = scheduler->workers;
  bool maxRadiusReached = false;

  while (isRunning)
  {
    /*
    Ready queue has two internal queues: OQ and LQ. OQ = Orchestration Queue (can be stolen), LQ = Local Queue (cannot be stolen).
    All jobs for this worker arrive at this worker's OQ. All jobs processed by this worker are done by traversing this worker's LQ.
    At the beginning an empty LQ swaps with an OQ. LQ traversal applies the executor function above to each runnable job and blocks job (unlinks node) only if the executor returns false. Ready jobs remain on LQ, which is equivalent to rescheduling.
    LQ swaps with OQ after each traversal to prevent starvation of jobs that arrived on OQ in the meanwhile, unless OQ is empty. If OQ is empty, LQ is traversed over and over again. If LQ is also empty, try to steal somebody else's OQ.
    A theft involves stealing an entire queue of runnable jobs from another worker by swapping that worker's OQ with this worker's LQ, after which these jobs are eventually returned to the thief's own OQ. If a stolen job blocks, return the container node to the allocator of the victim.
    */
    bool initiallyEmpty = readyQueue.isEmpty();
    bool swappedWithOQ = readyQueue.swap();
    if (readyQueue.isEmpty())
    {
      // execute policy function "maxSpin" times to pull in work
      if (policy)
      {
        for (uint32_t i = 0; i < maxSpin; i++) policy->onIdle(clusterID, workerID);
        nIdleSpins += maxSpin;
      }
      swappedWithOQ = readyQueue.swap();
    }

    if (!readyQueue.isEmpty())
    {
      if (!initiallyEmpty && swappedWithOQ)  // ready queue had items before a successful swap, raise a sleeping worker to become thief and steal OQ
      {
        for (worker_id_t i = 0; i < scheduler->nWorkers; i++)
        {
          if (i != workerID && workers[i].readyQueue.isSleeping) workers[i].readyQueue.signal();
          break;
        }
      }
      readyQueue.traverse(executor); // execute ready queue
    }
    else if (scheduler->nWorkers > 1) // policy did not pull anything, resort to stealing
    {
      if (isNUMAAware)
      {
        // increase search radius for victim selection
        maxRadiusReached = !scheduler->platform->increaseSearchRadius(nodeID, clusterID, searchLevel, neighborCPUs);
        if (maxRadiusReached)
        {
          searchLevel = 0;
          neighborCPUs.clear();
          scheduler->platform->increaseSearchRadius(nodeID, clusterID, searchLevel, neighborCPUs);
        }
#ifdef TRACE_THEFT
        tscout.printlnnws("TRACE: c", clusterID, "/w", workerID, " mapped to N", nodeID, "/P", cpuID, " is attempting to steal from neighborhood --> ", neighborCPUs, ", search level = ", searchLevel);
#endif // TRACE_THEFT
      }
      uint32_t nTheftAttempts = static_cast<uint32_t>(static_cast<uint32_t>(frequency) * freqMultiplier);
      uint64_t nJobs = 0;
      ready_queue_t::AllocatorType *victimsAllocator = nullptr;
      while (nTheftAttempts > 0)
      {
        worker_id_t victimID = chooseVictim();

        if (victimID == DEFAULT_WORKER_ID) break;

        assert(victimID != workerID && victimID < scheduler->nWorkers);

#ifdef TRACE_THEFT
        tscout.printlnnws("TRACE: c", clusterID, "/w", workerID, " mapped to N", nodeID, "/P", cpuID, " is attempting to steal from victim --> c", clusterID, "/w", victimID);
#endif // TRACE_THEFT
 
        // steal from victim's OQ
        victimsAllocator = readyQueue.steal(workers[victimID].readyQueue);
        nJobs = readyQueue.getSize();
        if (nJobs > 0 || !isRunning) break;
        if (frequency != steal_frequency_t::RESTLESS) nTheftAttempts--;
        else if (policy)
        {
          for (uint32_t i = 0; i < maxSpin; i++) policy->onIdle(clusterID, workerID);
          nIdleSpins += maxSpin;
        }
      }

      if (nJobs > 0) // jobs stolen from a peer, don't forget to return the nodes
      {
        rpp_assert(victimsAllocator, "Could not find victim's allocator after theft.", tscout);
        nThefts += nJobs;
        readyQueue.traverse(executor, victimsAllocator);
      }
      else // could not steal anything, rearm policy before blocking
      {
        readyQueue.wait(clusterID, workerID, scheduler->policy);
      }
    }
    else // single worker, don't bother stealing --> rearm and block
    {
      readyQueue.wait(clusterID, workerID, scheduler->policy);
    }
  }
  nInvocations = nThefts = nIdleSpins = 0;
  hasShutdown = true;
}

void rpp::rws_worker_t::setDispatchPolicy(uint64_t maxMessagesPerDispatch_)
{
  maxMessagesPerDispatch = maxMessagesPerDispatch_;
}

void rpp::rws_worker_t::shutdown()
{
  if (!isRunning) return;
  isRunning = false;
  while (!hasShutdown)
  {
    readyQueue.signal();
    sleep_for(nanoseconds(1));
  }

#ifdef TRACE_SHUTDOWN
  tscout.printlnnws("TRACE: c", clusterID, "/w", workerID, " has shut down");
#endif // TRACE_SHUTDOWN
}
