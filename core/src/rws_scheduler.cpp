/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "runnable.hpp"
#include "rws_scheduler.hpp"
#include "rws_worker.hpp"
#include "policy.hpp"

random_t& rpp::rws_scheduler_t::rnd = THR_LOCAL(random_t);
printer_t& rpp::rws_scheduler_t::tscout = GLOBAL(printer_t);

void rpp::rws_scheduler_t::joinAll() const
{
  for (worker_id_t i = 0; i < nWorkers; i++)
    if (pool[i].joinable()) pool[i].join();
}

void rpp::rws_scheduler_t::pollWorkerStatus() const
{
  while (true)
  {
    worker_id_t nActive = 0;
    for (worker_id_t i = 0; i < nWorkers; i++)
      if (workers[i].workerActive) nActive++;
    if (nActive == nWorkers) break;
  }
}

rpp::rws_scheduler_t::rws_scheduler_t()
  : clusterID(DEFAULT_CLUSTER_ID), nWorkers(0), workers(nullptr), pool(nullptr), nActivations(0), isRunning(false)
{
}

rpp::rws_scheduler_t::~rws_scheduler_t()
{
  shutdown();
}

void rpp::rws_scheduler_t::clearStats()
{
  for (worker_id_t i = 0; i < nWorkers; i++) workers[i].clearStats();
  nActivations = 0;
}

void rpp::rws_scheduler_t::enqueue(shared_ptr<runnable_t> const& job)
{
  rpp_assert(nWorkers > 0, "System was not initialized.", tscout);
  worker_id_t preferredWorker = job->workerID;

  bool pullToSender = job->scheduleAtSender(); // apply per-actor policy
  if (!pullToSender) // apply global policy
  {
    double g_senderAffinity = schedule_at_sender::probability::value();
    pullToSender = schedule_at_sender::isEnabled() && (g_senderAffinity == 1.0 || rnd.nextReal(0.0, 1.0) < g_senderAffinity);
  }

  if (pullToSender)
  {
    thread::id senderLocation = this_thread::get_id();
    auto it = workerLocations.find(senderLocation);
    if (it != workerLocations.end()) preferredWorker = it->second;
  }

  if (preferredWorker < nWorkers)
    workers[preferredWorker].makeReady(job);
  else
  {
    worker_id_t nextWorker = rnd.nextInt<worker_id_t>(0, nWorkers - 1);
    workers[nextWorker].makeReady(job);
  }
  nActivations++;
}

void rpp::rws_scheduler_t::raise(worker_id_t workerID) const
{
  rpp_assert(workerID < nWorkers, "Worker ID referenced a non-existent worker while raising a worker.", tscout);
  workers[workerID].readyQueue.signal();
}

string rpp::rws_scheduler_t::readStats() const
{
  vector<uint64_t> invocations(nWorkers), thefts(nWorkers), idleSpins(nWorkers), RQLengths(nWorkers), allocations(nWorkers), deallocations(nWorkers);
  vector<double> invocation_ratios(nWorkers), theft_ratios(nWorkers);
  uint64_t nodeSize = ready_queue_t::getNodeSize();
  for (worker_id_t i = 0; i < nWorkers; i++)
  {
    invocations[i] = workers[i].getInvocationCount();
    thefts[i] = workers[i].getTheftCount();
    idleSpins[i] = workers[i].getIdleSpinCount();
    RQLengths[i] = workers[i].getReadyQueueLength();
    pair<uint64_t, uint64_t> counters = workers[i].readyQueue.queryAllocations();
    allocations[i] = counters.first;
    deallocations[i] = counters.second;
  }

  uint64_t totalInvocations = (uint64_t)accumulate(invocations.cbegin(), invocations.cend(), 0ULL),
    totalThefts = (uint64_t)accumulate(thefts.cbegin(), thefts.cend(), 0ULL),
    totalIdleSpins = (uint64_t)accumulate(idleSpins.cbegin(), idleSpins.cend(), 0ULL),
    totalAllocated = (uint64_t)accumulate(allocations.cbegin(), allocations.cend(), 0ULL) * nodeSize,
    totalDeallocated = (uint64_t)accumulate(deallocations.cbegin(), deallocations.cend(), 0ULL) * nodeSize;

  for (worker_id_t i = 0; i < nWorkers; i++)
  {
    invocation_ratios[i] = totalInvocations == 0ULL ? 0.0 : invocations[i] / (double)totalInvocations * 100.0;
    theft_ratios[i] = totalThefts == 0ULL ? 0.0 : thefts[i] / (double)totalThefts * 100.0;
  }

  ostringstream tmp;
  tmp.precision(1);
  tmp << std::fixed;
  double theftRatio = totalInvocations == 0ULL ? 0.0 : totalThefts / (double)totalInvocations * 100.0;
  tmp << "Scheduler activations = " << nActivations << endl << "Worker invocations = " << totalInvocations << endl << "Total thefts = " << totalThefts << " (" << theftRatio << "% of total invocations)" << endl << "Total idle spins = " << totalIdleSpins << endl << "Total allocated = " << totalAllocated << " bytes" << endl << "Total deallocated = " << totalDeallocated << " bytes" << endl << endl;
  ostringstream oss;
  oss << "Cluster: " << clusterID << endl;
  oss << tmp.str();
  worker_id_t index = 0, nColumns = 4;
  while (true)
  {
    if (index > nWorkers - 1) break;

    oss << "\t\t";
    for (worker_id_t i = 0; i < nColumns; i++)
    {
      if (index + i > nWorkers - 1) break;
      oss.width(24);
      oss << std::left;
      string s = "Worker " + toString(index + i);
      oss << s;
    }

    oss << endl << "Invocations\t";
    for (worker_id_t i = 0; i < nColumns; i++)
    {
      if (index + i > nWorkers - 1) break;
      ostringstream tmp;
      tmp.precision(6);
      tmp << invocations[i + index] << " (";
      tmp.precision(1);
      tmp << std::fixed;
      tmp << invocation_ratios[i + index] << "%)";
      oss.width(24);
      oss << std::left;
      oss << tmp.str();
    }

    oss << endl << "Thefts\t\t";
    for (worker_id_t i = 0; i < nColumns; i++)
    {
      if (index + i > nWorkers - 1) break;
      ostringstream tmp;
      tmp.precision(6);
      tmp << thefts[i + index] << " (";
      tmp.precision(1);
      tmp << std::fixed;
      tmp << theft_ratios[i + index] << "%)";
      oss.width(24);
      oss << std::left;
      oss << tmp.str();
    }

    oss << endl << "Idle Spins\t";
    for (worker_id_t i = 0; i < nColumns; i++)
    {
      if (index + i > nWorkers - 1) break;
      ostringstream tmp;
      tmp.precision(6);
      tmp << idleSpins[i + index];
      oss.width(24);
      oss << std::left;
      oss << tmp.str();
    }

    oss << endl << "Ready\t\t";
    for (worker_id_t i = 0; i < nColumns; i++)
    {
      if (index + i > nWorkers - 1) break;
      ostringstream tmp;
      tmp.precision(6);
      tmp << RQLengths[i + index];
      oss.width(24);
      oss << std::left;
      oss << tmp.str();
    }

    oss << endl << "Allocations\t";
    for (worker_id_t i = 0; i < nColumns; i++)
    {
      if (index + i > nWorkers - 1) break;
      ostringstream tmp;
      tmp.precision(6);
      tmp << allocations[i + index];
      oss.width(24);
      oss << std::left;
      oss << tmp.str();
    }

    oss << endl << "Deallocations\t";
    for (worker_id_t i = 0; i < nColumns; i++)
    {
      if (index + i > nWorkers - 1) break;
      ostringstream tmp;
      tmp.precision(6);
      tmp << deallocations[i + index];
      oss.width(24);
      oss << std::left;
      oss << tmp.str();
    }

    oss << endl;
    if (index + nColumns < nWorkers - 1) oss << endl;
    index += nColumns;
  }

  return oss.str();
}

void rpp::rws_scheduler_t::run(cluster_id_t clusterID_, worker_id_t nWorkers_, io_policy_t *policy_, const platform_t* platform_)
{
  if (isRunning) return;
  isRunning = true;

  clusterID = clusterID_;
  nWorkers = nWorkers_;
  policy = policy_;
  platform = platform_;
  nActivations = 0;
  workers = new rws_worker_t[nWorkers];
  pool = new thread[nWorkers];

  for (worker_id_t i = 0; i < nWorkers; i++) workers[i].initialize(clusterID, i, this);
  for (worker_id_t i = 0; i < nWorkers; i++)
  {
    pool[i] = thread(&rws_worker_t::run, ref(workers[i]));
    workerLocations[pool[i].get_id()] = i;
  }

  pollWorkerStatus();
  schActive = true;
}

void rpp::rws_scheduler_t::shutdown()
{
  if (!isRunning) return;
  isRunning = false;
  for (worker_id_t i = 0; i < nWorkers; i++) workers[i].shutdown();
  joinAll();
  delete[] workers;
  workers = nullptr;
  delete[] pool;
  pool = nullptr;
  nActivations = 0;
}
