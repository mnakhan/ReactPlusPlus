/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "generic.hpp"
#include "tag_registry.hpp"

int generic_ext::compareIgnoreCase(string const& s1, string const& s2)
{
  return toLowercase(s1).compare(toLowercase(s2));
}

void generic_ext::joinAll()
{
}

void generic_ext::lockAll()
{
}

ostream& generic_ext::operator<<(ostream& out, string const& s)
{
  out << s.c_str();
  return out;
}

string generic_ext::padLeft(string const& s, int width, char c)
{
  ostringstream oss;
  oss.width(width);
  oss.fill(c);
  oss << s;
  return oss.str();
}

string generic_ext::padRight(string const& s, int width, char c)
{
  ostringstream oss;
  oss.width(width);
  oss.fill(c);
  oss << std::left << s;
  return oss.str();
}

string generic_ext::suffix2(uint64_t sizeInBytes)
{
  constexpr uint32_t divisorsSize = 6;
  array<pair<double, char>, divisorsSize> divisors = {
    pair<double, char>{1ULL << 60, 'E'},
    pair<double, char>{1ULL << 50, 'P'},
    pair<double, char>{1ULL << 40, 'T'},
    pair<double, char>{1ULL << 30, 'G'},
    pair<double, char>{1ULL << 20, 'M'},
    pair<double, char>{1ULL << 10, 'K'} };

  ostringstream result;
  result << fixed;
  result.precision(2);

  uint32_t i;
  for (i = 0; i < divisorsSize; i++)
    if (sizeInBytes >= divisors[i].first)
      break;

  if (i >= divisorsSize)
  {
    result << sizeInBytes << " byte";
    if (sizeInBytes > 1) result << 's';
  }
  else result << sizeInBytes / divisors[i].first << ' ' << divisors[i].second << 'B';

  return result.str();
}

vector<string> generic_ext::split(string const& s, string const& delim)
{
  vector<string> tokens;
  bool done = false;
  size_t m = 0, n;
  string token;
  while (!done)
  {
    n = s.find_first_of(delim, m);
    if (n == string::npos)
    {
      token = s.substr(m);
      done = true;
    }
    else
    {
      token = s.substr(m, n - m);
      m = n + 1;
    }

    token = trim(token);
    if (!token.empty()) tokens.push_back(token);
  }

  return tokens;
}

string generic_ext::toLowercase(string const& s)
{
  string r = s;
  for (size_t i = 0; i < r.length(); i++) r[i] = tolower(r[i]);
  return r;
}

string generic_ext::toUppercase(string const& s)
{
  string r = s;
  for (size_t i = 0; i < r.length(); i++) r[i] = toupper(r[i]);
  return r;
}

string generic_ext::trim(string const& s, string const& charsToTrim)
{
  string result = s;
  size_t n = result.find_first_not_of(charsToTrim);
  if (n == string::npos) return "";
  result = result.substr(n);
  n = result.find_last_not_of(charsToTrim);
  result = result.substr(0, n + 1);
  return result;
}

void generic_ext::unlockAll()
{
}

void generic_ext::wait()
{
  printer_t& tscout = GLOBAL(printer_t);
  tscout.println("Press ENTER to continue...");
  RM_LF();
}
