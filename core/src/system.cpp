/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "system.hpp"

generic_ext::printer_t& rpp::system_t::tscout = GLOBAL(printer_t);
generic_ext::random_t& rpp::system_t::rnd = THR_LOCAL(random_t);
const rpp::location_t rpp::location_t::DetachedCluster = location_t(DETACHED_CLUSTER_ID, DEFAULT_WORKER_ID);

generic_ext::rw_lock_t rpp::location_t::mx_generator;
vector<atomic<worker_id_t>> rpp::location_t::generator;

rpp::system_t& rpp::actor_system_t::sys = GLOBAL(rpp::system_t);

void rpp::location_t::resetGenerator(cluster_id_t nClusters)
{
  mx_generator.lock<rw_locking_mode_t::Write>();
  generator = vector<atomic<worker_id_t>>(nClusters);
  for (size_t i = 0; i < generator.size(); i++) generator[i].store(CPU<0>());
  mx_generator.unlock<rw_locking_mode_t::Write>();
}

void rpp::system_t::extract()
{
}

void rpp::system_t::joinAll()
{
  for (cluster_id_t i = 0; i < nClusters; i++)
    if (pool[i].joinable()) pool[i].join();
}

void rpp::system_t::loadPlatform()
{
  rpp_assert(!platform, "platform was not null before initialization", tscout);
  platform = new platform_t;
  rpp::configuration_t config;
  if (config.load(environment::configPath)) platform->reload(config);
}

bool rpp::system_t::rewind(message_t *message, actor_id_t const& receiverID, actor_id_t const& senderID)
{
  cluster_id_t clusterID = receiverID.clusterID;
  if (clusterID < nClusters)
  {
    shared_ptr<runnable_t> instance;
    if (clusters[clusterID].registry.get(receiverID.localID, instance))
    {
      if (!instance->isEnabled()) return false;
      if (instance->maySchedule(message, senderID)) instance->residence->scheduler.enqueue(instance);
      return true;
    }
    return false;
  }
  else if (clusterID == DETACHED_CLUSTER_ID)
  {
    shared_ptr<executor_t> executor;
    if (detachedCluster.registry.get(receiverID.localID, executor))
    {
      if (!executor->isEnabled()) return false;
      executor->send(message, senderID);
      return true;
    }
    return false;
  }
  else rpp_assert(false, "Actor ID referenced a non-existent cluster while sending a message.", tscout);
  return false;
}

rpp::system_t::system_t()
  : isRunning(false), nClusters(0), clusters(nullptr), pool(nullptr)
{
}

rpp::system_t::~system_t()
{
  shutdown();
}

void rpp::system_t::bind(actor_id_t const& id, tag_t const& tag)
{
  tags.bind(id, tag);
}

void rpp::system_t::clearStats() const
{
  for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].clearStats();
}

bool rpp::system_t::exists(actor_id_t const& actorID) const
{
  cluster_id_t clusterID = actorID.clusterID;
  if (clusterID < nClusters)
  {
    shared_ptr<runnable_t> instance;
    return clusters[clusterID].registry.get(actorID.localID, instance);
  }
  else if (clusterID == DETACHED_CLUSTER_ID)
  {
    shared_ptr<executor_t> executor;
    return detachedCluster.registry.get(actorID.localID, executor);
  }
  return false;
}

map<cluster_id_t, uint64_t> rpp::system_t::getActorCount() const
{
  map<cluster_id_t, uint64_t> result;
  for (cluster_id_t i = 0; i < nClusters; i++) result.insert(make_pair(clusters[i].clusterID, clusters[i].getActorCount()));
  return result;
}

cluster_id_t rpp::system_t::getClusterCount() const
{
  assert(platform);
  return platform->getClusterCount();
}

rpp::tag_registry_t& rpp::system_t::getTagRegistry()
{
  return tags;
}

rpp::timer_t& rpp::system_t::getTimer()
{
  return timer;
}

map<cluster_id_t, worker_id_t> rpp::system_t::getWorkerCount() const
{
  map<cluster_id_t, worker_id_t> result;
  for (cluster_id_t i = 0; i < nClusters; i++) result.insert(make_pair(clusters[i].clusterID, clusters[i].getWorkerCount()));
  return result;
}

bool rpp::system_t::migrate(actor_id_t const& actorID, cluster_id_t targetClusterID)
{
  cluster_id_t residentClusterID = actorID.clusterID;
  rpp_assert(residentClusterID != DETACHED_CLUSTER_ID, "Cannot migrate a detached actor.", tscout);
  rpp_assert(targetClusterID != DETACHED_CLUSTER_ID, "Cannot migrate to a detached cluster.", tscout);
  rpp_assert(residentClusterID < nClusters, "Resident cluster ID referenced a non-existent cluster.", tscout);
  rpp_assert(targetClusterID < nClusters, "Target cluster ID referenced a non-existent cluster.", tscout);

  shared_ptr<runnable_t> instance;
  if (clusters[residentClusterID].registry.get(actorID.localID, instance))
  {
    cluster_t<NATIVE> *newResidence = &clusters[targetClusterID];
    return instance->migrate(newResidence);
  }
  return false;
}

void rpp::system_t::raise(cluster_id_t clusterID, worker_id_t workerID)
{
  rpp_assert(clusterID < nClusters, "Cluster ID referenced a non-existent cluster while raising a worker.", tscout);
  clusters[clusterID].scheduler.raise(workerID);
}

string rpp::system_t::report() const
{
  lock_guard<mutex> bolt(mx_state);
  ostringstream result;
  for (cluster_id_t i = 0; i < nClusters; i++)
    result << clusters[i].report() << endl;
  return result.str();
}

void rpp::system_t::restartWithConfiguration(io_policy_t *policy, cluster_id_t nClusters_, worker_id_t nWorkersPerCluster)
{
  shutdown();

  lock_guard<mutex> bolt(mx_state);
  if (isRunning) return;
#ifdef TRACE_STARTUP
  tscout.println("TRACE: initializing actor system");
#endif // TRACE_STARTUP

  isRunning = true;
  loadPlatform();

  detachedCluster.run();
  nClusters = nClusters_;
  location_t::resetGenerator(nClusters);
  threadsPerCluster = vector<worker_id_t>(nClusters, nWorkersPerCluster);
  clusters = new cluster_t<NATIVE>[nClusters];
  pool = new thread[nClusters];

  if (policy)
  {
    rpp_assert(nClusters == policy->getClusterCount(), "Runtime and I/O subsystem do not agree on cluster count.", tscout);
    for (cluster_id_t i = 0; i < nClusters; i++)
      rpp_assert(threadsPerCluster[i] == policy->getWorkerCount(i), "Runtime and I/O subsystem do not agree on worker count per cluster.", tscout);
  }

  for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].initialize(i, threadsPerCluster[i], policy);
  for (cluster_id_t i = 0; i < nClusters; i++) pool[i] = thread(&cluster_t<NATIVE>::run, ref(clusters[i]));
  for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].pollSchedulerStatus();

  timer.initialize();
  timer_thread = thread(&timer_t::run, ref(timer));
}

void rpp::system_t::restartWithConfiguration(io_policy_t *policy)
{
  shutdown();

  lock_guard<mutex> bolt(mx_state);
  if (isRunning) return;

#ifdef TRACE_STARTUP
  tscout.println("TRACE: initializing actor system");
#endif // TRACE_STARTUP

  isRunning = true;
  loadPlatform();

  ThisThread::currentPlatform = platform;
  detachedCluster.run();

  bool foundConfig = platform->hasValidConfiguration();
  if (!foundConfig)
  {
    vector<string> paths = { DEFAULT_CONFIG_FILE, DEFAULT_CONFIG_FILE_ALT_1, DEFAULT_CONFIG_FILE_ALT_2 };
    for (size_t i = 0; i < paths.size() && !foundConfig; i++)
    {
      configuration_t config;
      if (!config.load(paths[i])) continue;
      platform->reload(config);
      foundConfig = platform->hasValidConfiguration();
    }
  }

  if (foundConfig) nClusters = platform->getClusterCount();
  else
  {
    tscout.println("missing configuration file, initializing system with single cluster");
    nClusters = 1;
  }
  location_t::resetGenerator(nClusters);
  clusters = new cluster_t<NATIVE>[nClusters];
  pool = new thread[nClusters];

  if (policy) rpp_assert(nClusters == policy->getClusterCount(), "Runtime and I/O subsystem do not agree on cluster count.", tscout);

  for (cluster_id_t i = 0; i < nClusters; i++)
  {
    worker_id_t nWorkers;
    if (platform->hasValidConfiguration()) nWorkers = platform->getWorkerCount(i);
    else nWorkers = MAX_HW_THREADS;
    if (policy) rpp_assert(nWorkers == policy->getWorkerCount(i), "Runtime and I/O subsystem do not agree on worker count per cluster.", tscout);
    clusters[i].initialize(i, nWorkers, policy, platform);
  }
  for (cluster_id_t i = 0; i < nClusters; i++) pool[i] = thread(&cluster_t<NATIVE>::run, ref(clusters[i]));
  for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].pollSchedulerStatus();

  timer.initialize();
  timer_thread = thread(&timer_t::run, ref(timer));
}

bool rpp::system_t::returnToOrigin(actor_id_t const& actorID)
{
  cluster_id_t residentClusterID = actorID.clusterID;
  rpp_assert(residentClusterID != DETACHED_CLUSTER_ID, "Cannot return a detached actor to its origin.", tscout);
  rpp_assert(residentClusterID < nClusters, "Resident cluster ID referenced a non-existent cluster.", tscout);

  shared_ptr<runnable_t> instance;
  if (clusters[residentClusterID].registry.get(actorID.localID, instance)) return instance->returnToOrigin();
  return false;
}

void rpp::system_t::shutdown()
{
  lock_guard<mutex> bolt(mx_state);
  if (!isRunning) return;
  isRunning = false;

  timer.shutdown();
  timer_thread.join();

  threadsPerCluster.clear();
  for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].shutdown();
  joinAll();
  delete[] clusters;
  delete[] pool;
  clusters = nullptr;
  pool = nullptr;
  detachedCluster.shutdown();
  delete platform;
  platform = nullptr;
#ifdef TRACE_SHUTDOWN
  tscout.println("TRACE: system has shutdown");
#endif // TRACE_SHUTDOWN
}

void rpp::system_t::unbind(actor_id_t const& id, tag_t const& tag)
{
  tags.unbind(id, tag);
}

void rpp::system_t::waitForAll() const
{
  for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].waitForAll();
  detachedCluster.waitForAll();
}

rpp::scheduler_type_t rpp::system_t::whichDomain(actor_id_t const& actorID) const
{
  scheduler_type_t result = scheduler_type_t::NONE;
  cluster_id_t clusterID = actorID.clusterID;
  if (clusterID < nClusters)
  {
    shared_ptr<runnable_t> instance;
    if (clusters[clusterID].registry.get(actorID.localID, instance)) result = scheduler_type_t::USERSPACE;
  }
  else if (clusterID == DETACHED_CLUSTER_ID)
  {
    shared_ptr<executor_t> executor;
    if (detachedCluster.registry.get(actorID.localID, executor))
    {
      if (executor->useInternalThread) result = scheduler_type_t::KERNEL;
      else result = scheduler_type_t::INERT;
    }
  }
  return result;
}

rpp::system_t& rpp::location_t::sys = GLOBAL(system_t);
generic_ext::printer_t& rpp::location_t::tscout = GLOBAL(printer_t);
generic_ext::random_t& rpp::location_t::rnd = THR_LOCAL(random_t);


cluster_id_t rpp::location_t::getClusterCount()
{
  return sys.nClusters;
}

worker_id_t rpp::location_t::getWorkerCount(cluster_id_t clusterID)
{
  return sys.clusters[clusterID].getWorkerCount();
}

rpp::location_t::location_t(cluster_id_t clusterID_, worker_id_t workerID_)
  : clusterID(clusterID_), workerID(workerID_)
{
}

bool rpp::location_t::exists() const
{
  return clusterID < sys.nClusters && workerID < sys.clusters[clusterID].getWorkerCount();
}

rpp::location_t rpp::location_t::map(bool pinToWorker, cluster_id_t clusterID, worker_id_t workerID)
{
  rpp_assert(sys.nClusters > 0, "System was not initialized.", tscout);
  rpp::location_t location(clusterID, workerID);
  // assign a random cluster if supplied value is invalid
  if (location.clusterID >= sys.nClusters) location.clusterID = rnd.nextInt<cluster_id_t>(0, sys.nClusters - 1);

  worker_id_t nWorkers = sys.clusters[location.clusterID].getWorkerCount(); // # of workers available on the selected cluster
  if (location.workerID >= nWorkers) // supplied value is invalid
  {
    // randomly assign a preferred worker (actor becomes pinned)
    if (pinToWorker) location.workerID = rnd.nextInt<worker_id_t>(0, nWorkers - 1);
    else location.workerID = DEFAULT_WORKER_ID; // special value to indicate a random assignment each time actor is scheduled
  }
  return location;
}

ostream& rpp::operator<<(ostream& out, location_t const& location)
{
  return out << '[' << location.clusterID << ", " << location.workerID << ']';
}

rpp::actor_system_t::actor_system_t()
{
}

rpp::actor_system_t::~actor_system_t()
{
  sys.waitForAll();
}

rpp::system_t& rpp::actor_system_t::operator()() const
{
  return sys;
}
