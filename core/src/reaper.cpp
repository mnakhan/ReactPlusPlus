/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "reaper.hpp"

rpp::reaper_t::reaper_t()
  :isRunning(false), hasExited(false), reaperActive(false)
{
}

rpp::reaper_t::~reaper_t()
{
}

void rpp::reaper_t::initialize()
{
  isRunning = true;
  reaperActive = hasExited = false;
}

void rpp::reaper_t::pollReaperStatus()
{
  while (!reaperActive);
}

void rpp::reaper_t::run()
{
  reaperActive = true;
  unique_lock<mutex> bolt(mx_tasks, defer_lock);
  while (true)
  {
    bolt.lock();
    if (tasks.empty())
    {
      if (!isRunning) break;
      cv_noTasks.wait(bolt, [this]
      {
        return !isRunning || !tasks.empty();
      });
    }

    if (!isRunning && tasks.empty()) break;

    packaged_task<void()> task = move(tasks.front());
    tasks.pop_front();
    bolt.unlock();

    task();
  }
  hasExited = true;
}

void rpp::reaper_t::shutdown()
{
  if (!isRunning) return;
  isRunning = false;
  while (!hasExited)
  {
    {
      lock_guard<mutex> bolt(mx_tasks);
      cv_noTasks.notify_one();
    }
    sleep_for(nanoseconds(1));
  }
}
