/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "printer.hpp"

generic_ext::stream_lock_t generic_ext::g_streamLock;
generic_ext::printer_t::printer_t(ostream& out_)
  :out(out_)
{
}

const generic_ext::printer_t& generic_ext::printer_t::print() const
{
  return *this;
}

generic_ext::printer_t const& generic_ext::printer_t::println() const
{
  lock_guard<recursive_mutex> bolt(g_streamLock.mx_out);
  out << endl;
  return *this;
}

generic_ext::printer_t const& generic_ext::printer_t::printnws() const
{
  return print();
}

generic_ext::printer_t const& generic_ext::printer_t::printlnnws() const
{
  return println();
}
