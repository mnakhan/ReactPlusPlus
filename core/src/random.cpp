/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "random.hpp"

generic_ext::random_t::random_t()
{
  engine.seed((uint32_t)time(nullptr));
}

string generic_ext::random_t::nextString(size_t length)
{
  if (length == 0) return "";
  string s;
  s.reserve(length);
  string::iterator itr = s.begin();
  char min, max;
  for (size_t i = 0; i < length; i++)
  {
    if (nextInt() % 2 == 0) min = '0', max = '9';
    else min = 'A', max = 'Z';
    s.insert(itr, (char)nextInt((short)min, (short)max));
    itr++;
  }
  return s;
}
