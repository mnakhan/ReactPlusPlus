/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "context.hpp"
#include "cluster.hpp"
#include "behavior.hpp"

generic_ext::printer_t& rpp::context_t::tscout = GLOBAL(printer_t);
generic_ext::random_t& rpp::context_t::rnd = THR_LOCAL(random_t);
uint64_t rpp::context_t::dispatch(uint64_t maxNumberOfMessages)
{
#ifdef DEBUG
  thread::id comparand;
  rpp_assert(executorID.compare_exchange_strong(comparand, this_thread::get_id(), memory_order_acq_rel, memory_order_relaxed), "FATAL: Worker re-entrancy detected.", tscout);
#endif // DEBUG

  uint64_t totalDispatched = 0, effectiveDispatchSize = MIN(privateDispatchSize, maxNumberOfMessages);
  while (true)
  {
    if (rpp_unlikely(isParking.load(memory_order_acquire)))
    {
      totalDispatched += drain();
      break;
    }
    message_t *message = nullptr;
    if (rpp_unlikely(isYielding))
    {
      generic_ext::swap(message, yieldedMessage);
      isYielding = false;
    }
    else mailbox->pop(&message, lastSenderID);

    if (rpp_unlikely(!message)) break;
    if (rpp_likely(!isWaitingForReply)) handle(message, lastSenderID);
    else
    {
      if (replyToken == message->replyToken)
      {
        replyToken = DEFAULT_REPLY_TOKEN;
        isWaitingForReply = false;
        handle(message, lastSenderID);
      }
      else skippedMessages.push_back({ message, lastSenderID });
    }
    if (rpp_unlikely(isYielding))
    {
#ifdef ENABLE_SYSTEM_STATS
      GLOBAL(system_statistics_t).total_messages_sent++;    // yield counts as another send
#endif // ENABLE_SYSTEM_STATS
      break;
    }
    if (rpp_likely(!isWaitingForReply)) resume();

    totalDispatched++;
    if (totalDispatched == effectiveDispatchSize) break;
  }

#ifdef DEBUG
  // Clear reentrancy flag.
  comparand = this_thread::get_id();
  rpp_assert(executorID.compare_exchange_strong(comparand, thread::id(), memory_order_acq_rel, memory_order_relaxed), "FATAL: Worker re-entrancy detected.", tscout);
#endif // DEBUG
  return totalDispatched;
}

uint64_t rpp::context_t::drain()
{
  rpp_assert(isParking.load(memory_order_acquire), "Cannot drain the mailbox without enabling the parking mode.", tscout);
  uint64_t totalRemoved = 0;
  message_t *message = nullptr;
  bool done = false;
  while (!done && mailbox->pop(&message, lastSenderID))
  {
    if (atom_t<True>::type<Barrier>(message)) done = true;
    if (!message->isSingleInstance) delete message;
    totalRemoved++;
  }
  isParking.store(false, memory_order_release);
  lastSenderID = actor_id_t();
  return totalRemoved;
}

bool rpp::context_t::maySchedule(message_t *message, actor_id_t const& senderID)
{
  bool success = mailboxSize.fetch_add(1, memory_order_acq_rel) == 0;
  mailbox->push(message, senderID);
#ifdef ENABLE_SYSTEM_STATS
  GLOBAL(system_statistics_t).total_messages_sent++;
#endif // ENABLE_SYSTEM_STATS
  return success;
}

bool rpp::context_t::mayReschedule(uint64_t lastDispatch)
{
  return mailboxSize.fetch_sub(lastDispatch, memory_order_acq_rel) != lastDispatch;
}

bool rpp::context_t::migrate(cluster_t<NATIVE> *newResidence)
{
  if (origin)   // native actor
  {
    if (residence != newResidence)
    {
      residence = newResidence;
      return true;
    }
    return false;
  }

  rpp_assert(false, "Migration not supported for detached actors.", tscout);
  return false;
}

bool rpp::context_t::returnToOrigin()
{
  if (origin)   // native actor
  {
    if (residence != origin)
    {
      residence = origin;
      return true;
    }
    return false;
  }

  rpp_assert(false, "Migration not supported for detached actors.", tscout);
  return false;
}

void rpp::context_t::handle(message_t *message, actor_id_t const& senderID)
{
#ifdef ENABLE_SYSTEM_STATS
  GLOBAL(system_statistics_t).total_messages_received++;
#endif // ENABLE_SYSTEM_STATS

  if (rpp_likely(!message->isTrail))
  {
    if (rpp_likely(currentBehavior != nullptr)) currentBehavior->receive(message, senderID);
    else receiver.match(this, message, senderID);
    if (rpp_unlikely(isYielding)) // save the yielded message so that it can be re-dispatched
    {
      yieldedMessage = message;
      return;
    }
    // cleanup (not applicable for statically allocated atoms)
    if (rpp_unlikely(!message->isSingleInstance)) delete message;
    return;
  }

  assert(!message->isSingleInstance);     // trails cannot be atoms
  if (!message->mayThrow())    // empty trail, nothing to do
  {
    delete message;
    return;
  }

  bool raiseException = false;
  isResumingTrail = false;
  try
  {
    if (currentBehavior) currentBehavior->receive(message, senderID);
    if (isYielding)
    {
      yieldedMessage = message;
      return;
    }
  }
  catch (message_t*)
  {
    raiseException = true;
  }

  if (isResumingTrail) return;    // ignore raise immediately followed by resume in the same context (semantic error)

  bool success = false;
  if (message->isRaised()) success = message->rewind(); // current context did not handle a previously raised exception
  else if (raiseException) success = message->raise(selfID).rewind();    // raise for the first time

  if (success) success = returnToSender(message, message->getFaultLocation());
  if (!success) delete message;
}

void rpp::context_t::resume()
{
  if (skippedMessages.empty()) return;
  auto entry = skippedMessages.front();
  skippedMessages.pop_front();
  mailboxSize.fetch_add(1, memory_order_acq_rel);
  mailbox->push(entry.first, entry.second);
}

bool rpp::context_t::returnToSender(message_t *message, actor_id_t const& senderID)
{
  if (senderID == selfID) // optimization
  {
    mailboxSize.fetch_add(1, memory_order_acq_rel);
    mailbox->push(message, selfID);
#ifdef ENABLE_SYSTEM_STATS
    GLOBAL(system_statistics_t).total_messages_sent++;
#endif // ENABLE_SYSTEM_STATS
    return true;
  }

  if (senderID.is<external>()) return false;
  return GLOBAL(system_t).rewind(message, senderID, selfID);
}

void rpp::context_t::awaitReply(uint64_t replyToken_)
{
  rpp_assert(!isWaitingForReply, "Cannot wait for a reply while waiting for the previous reply.", tscout);
  isWaitingForReply = true;
  replyToken = replyToken_;
}

rpp::context_t::context_t()
  : isParking(false),
  mailboxSize(0),
  yieldedMessage(nullptr),
  executorID(thread::id()),
  mailbox(nullptr),
  isYielding(false),
  isResumingTrail(false),
  ctxEnabled(false),
  isWaitingForReply(false),
  replyToken(DEFAULT_REPLY_TOKEN),
  currentBehavior(nullptr)
{
  // NOTE: mailbox is allocated in cluster_t::spawn
}

rpp::context_t::~context_t()
{
  mailbox->clear();
  for_each(skippedMessages.begin(), skippedMessages.end(), [this](pair<message_t*, actor_id_t> const& entry)
  {
    message_t *message = entry.first;
    if (!message->isSingleInstance) delete message;
  });
  skippedMessages.clear();

  auto it = behaviors.begin();
  while (it != behaviors.end())
  {
    delete it->second;
    it++;
  }
  behaviors.clear();
  delete mailbox;
}

void rpp::context_t::adoptDefault()
{
  currentBehavior = nullptr;
}

rpp::type_erased_receiver_t& rpp::context_t::defaultBehavior(bool reset)
{
  // only affects context before it is enabled
  if (reset) return receiver.reset();
  return receiver;
}

bool rpp::context_t::enable(bool assumingDefault)
{
  bool expected = false;
  if (ctxEnabled.compare_exchange_strong(expected, true, memory_order_acq_rel, memory_order_relaxed))
  {
    // one-shot (at initialization)
    // successfully enabled context, assume behavior
    if (assumingDefault) currentBehavior = nullptr;
    // otherwise class-based behavior is assumed in become() after sanity checks
    return true;
  }
  return false;
}

bool rpp::context_t::isEnabled() const
{
  return ctxEnabled.load(memory_order_acquire);
}

void rpp::context_t::initialize()
{
}

void rpp::context_t::park()
{
  DECLARE_ATOM_TYPE(Barrier) barrier;
  isParking.store(true, memory_order_release); // set parking flag
  *this << barrier; // set message barrier, any message arriving afterwards won't be removed in parking drain
}

void rpp::context_t::quit() const
{
  if (origin) origin->kill(selfID);
  else
  {
    rpp_assert(detachedOrigin, "This actor does not belong to either a native or the detached cluster.", tscout);
    detachedOrigin->kill(selfID);
  }
}

bool rpp::context_t::scheduleAtSender() const
{
  if (senderAffinity == 1.0) return true;
  return rnd.nextReal(0.0, 1.0) < senderAffinity;
}

void rpp::context_t::yield()
{
  isYielding = true;
}
