/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "locks.hpp"

generic_ext::spin_lock_t::spin_lock_t() {}

void generic_ext::spin_lock_t::lock()
{
  while (!try_lock());
}

void generic_ext::spin_lock_t::unlock()
{
  isLocked.clear(memory_order_release);
}

bool generic_ext::spin_lock_t::try_lock()
{
  return !isLocked.test_and_set(memory_order_acquire);
}

generic_ext::rw_lock_t::rw_lock_t()
  :nReaders(0), nReadersInQueue(0), nWritersInQueue(0), writeInProgress(false) {}
