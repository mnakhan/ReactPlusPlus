/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "system_statistics.hpp"

void rpp::system_statistics_t::reset()
{
  isShuttingDown = false;

#ifdef ENABLE_SYSTEM_STATS
  total_native_actors_spawned
    = total_native_actors_killed
    = total_detached_actors_spawned
    = total_detached_actors_killed
    = total_channels_opened
    = total_channels_closed

    = total_messages_sent
    = total_messages_received = 0ULL;
#endif // ENABLE_SYSTEM_STATS
}
