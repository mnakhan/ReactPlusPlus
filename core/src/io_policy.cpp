/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "io_policy.hpp"

io::io_policy_t::~io_policy_t()
{
}

io::io_event_t::io_event_t(poller_type_t poller_type_, void *details_)
  :poller_type(poller_type_)
{
#ifdef LINUX
  details = (epoll_event*)details_;
#else
  details = (generic_event_t*)details_;
#endif // LINUX
}

cluster_id_t io::io_policy_t::getClusterCount() const
{
  return 0;
}

worker_id_t io::io_policy_t::getWorkerCount(cluster_id_t clusterID) const
{
  return 0;
}
