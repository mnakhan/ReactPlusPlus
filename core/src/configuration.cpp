/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "configuration.hpp"
#include "generic.hpp"

rpp::configuration_t::configuration_t(string const& path_)
  :path(path_)
{
}

rpp::configuration_t::~configuration_t()
{
}

void rpp::configuration_t::clear(string const& section)
{
  string section_lc = generic_ext::toLowercase(section);
  if (section_lc.empty()) kvStore.clear();
  else kvStore[section_lc].clear();
}

bool rpp::configuration_t::load(string const& path_)
{
  if (!path_.empty()) path = path_;
  ifstream in(path);
  if (!in) return false;

  bool done = false;
  string section = "general";
  while (!done)
  {
    string line;
    getline(in, line);
    line = generic_ext::trim(line, "\r ");
    if (!line.empty())
    {
      if (line[0] == '#')
      {
        done = !in.good();
        continue;   // skip comment
      }
      if (line[0] == '[')
      {
        size_t end = line.find(']');
        if (end != string::npos) line = line.substr(1, end - 1);
        section = line;
      }
      else if (line.find('=') != string::npos)
      {
        vector<string> tokens = generic_ext::split(line, "=");
        string key = tokens[0], value;
        if (tokens.size() > 1)
        {
          value = generic_ext::toLowercase(tokens[1]);
          if (value.compare("true") == 0) value = "1";
          else if (value.compare("false") == 0) value = "0";
        }
        kvStore[generic_ext::toLowercase(section)][generic_ext::toLowercase(key)] = generic_ext::toLowercase(value);
      }
    }

    done = !in.good();
  }

  return !kvStore.empty();
}

map<string, string>& rpp::configuration_t::operator[](string const& key)
{
  return kvStore[key];
}

string rpp::configuration_t::toString() const
{
  ostringstream out;
  for_each(kvStore.cbegin(), kvStore.cend(), [&out](auto& kvp)
  {
    out << "Section: " << kvp.first << endl;
    auto& value = kvp.second;
    for_each(value.cbegin(), value.cend(), [&out](auto& kvp2)
    {
      out << "Key: " << kvp2.first << ", Value: " << kvp2.second << endl;
    });
  });
  return out.str();
}
