/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "policy.hpp"

bool& rpp::schedule_at_sender::isEnabled()
{
  static bool flag = false;
  return flag;
}

double& rpp::schedule_at_sender::probability::value()
{
  static double value_ = 0.0;
  return value_;
}

rpp::policy_t& rpp::policy_t::placeholder()
{
  static policy_t policy;
  return policy;
}
