/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "tag_registry.hpp"
#include "system.hpp"
using namespace generic_ext;

rpp::tag_registry_t& rpp::tag_registry_t::globalInstance()
{
  system_t& sys = GLOBAL(system_t);
  return sys.getTagRegistry();
}

void rpp::tag_registry_t::bind(actor_id_t const& id, tag_t const& tag)
{
  bolt.lock<rw_locking_mode_t::Write>();
  unbind(id, tag, false);
  id2tag.insert({ id, tag });
  tag2id.insert({ tag, id });
  bolt.unlock<rw_locking_mode_t::Write>();
}

ostream& rpp::operator<<(ostream& out, rpp::id_to_tag const& p)
{
  return out << p.first << " <--> " << p.second;
}

ostream& rpp::operator<<(ostream& out, rpp::tag_to_id const& p)
{
  return out << p.first << " <--> " << p.second;
}

void rpp::tag_registry_t::unbind(actor_id_t const& id, tag_t const& tag, bool acquire)
{
  if(acquire) bolt.lock<rw_locking_mode_t::Write>();
  {
    auto ret = id2tag.equal_range(id);
    for (auto it = ret.first; it != ret.second; it++)
    {
      if (tag.compare(it->second) == 0)
      {
        id2tag.erase(it);
        break;
      }
    }
  }

  {
    auto ret = tag2id.equal_range(tag);
    for (auto it = ret.first; it != ret.second; it++)
    {
      if (id == it->second)
      {
        tag2id.erase(it);
        break;
      }
    }
  }
  if (acquire) bolt.unlock<rw_locking_mode_t::Write>();
}
