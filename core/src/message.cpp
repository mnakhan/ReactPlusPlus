/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "message.hpp"
#include "barrier.hpp"

generic_ext::random_t& rpp::message_t::rnd = GLOBAL(generic_ext::random_t);
void rpp::message_t::generateToken()
{
  while (true)
  {
    replyToken = rnd.nextInt<uint64_t>();
    if (replyToken != DEFAULT_REPLY_TOKEN) break;
  }
}

rpp::message_t& rpp::message_t::emptyHandle()
{
  static message_t message(true);
  return message;
}

rpp::message_t::message_t(bool hasMatched_)
: hasMatched(hasMatched_), replyToken(DEFAULT_REPLY_TOKEN), isSingleInstance(false), isTrail(false), tiContents(type_index(typeid (nullptr)))
{
}

rpp::message_t::~message_t()
{
}

void rpp::message_t::discard()
{
  rpp_assert(false, "Operation not supported for this type.", GLOBAL(printer_t));
}

rpp::actor_id_t rpp::message_t::getFaultLocation() const
{
  rpp_assert(false, "Operation not supported for this type.", GLOBAL(printer_t));
  return actor_id_t();
}

bool rpp::message_t::isRaised() const
{
  rpp_assert(false, "Operation not supported for this type.", GLOBAL(printer_t));
  return false;
}

bool rpp::message_t::isSynchronous() const
{
  return replyToken != DEFAULT_REPLY_TOKEN;
}

bool rpp::message_t::mayThrow() const
{
  return false;
}

rpp::message_t& rpp::message_t::raise(actor_id_t const& source)
{
  rpp_assert(false, "Operation not supported for this type.", GLOBAL(printer_t));
  return *this;
}

bool rpp::message_t::rewind()
{
  rpp_assert(false, "Operation not supported for this type.", GLOBAL(printer_t));
  return false;
}

string rpp::message_t::toString() const
{
  return string();
}

ostream& rpp::operator<<(ostream& out, message_t const& message)
{
  return out << message.toString();
}

ostream& rpp::operator<<(ostream& out, Delete const& message)
{
  return out << "Delete";
}

ostream& rpp::operator<<(ostream& out, Park const& message)
{
  return out << "Park";
}

rpp::si_message_t::si_message_t()
: message_t()
{
}

rpp::trail_t::trail_details_t::trail_details_t()
{
  path = vector<actor_id_t>{};
  raised = false;
  source = actor_id_t();
  fault = path.crend();
  remaining = 0;
  faultIndex = -1;
}

rpp::trail_t::trail_t()
  :empty(true), trail_details(nullptr)
{
  isTrail = true;
}

rpp::trail_t::~trail_t()
{
  delete trail_details;
  trail_details = nullptr;
}

void rpp::trail_t::discard()
{
  delete trail_details;
  trail_details = nullptr;
  empty = true;
}

rpp::actor_id_t rpp::trail_t::getFaultLocation() const
{
  if (trail_details->fault != trail_details->path.crend()) return *(trail_details->fault);
  return actor_id_t();
}

bool rpp::trail_t::isRaised() const
{
  return trail_details && trail_details->raised;
}

void rpp::trail_t::link(actor_id_t const& senderID)
{
  trail_details->path.push_back(senderID);
}

bool rpp::trail_t::mayThrow() const
{
  return !empty;
}

rpp::message_t& rpp::trail_t::raise(actor_id_t const& source)
{
  rpp_assert(trail_details, "Cannot raise exception with an empty trail.", GLOBAL(printer_t));
  trail_details->raised = true;
  trail_details->source = source;
  trail_details->remaining = (uint32_t)trail_details->path.size();
  return *this;
}

bool rpp::trail_t::rewind()
{
  rpp_assert(trail_details && trail_details->raised, "Cannot rewind trail without raising an exception first.", GLOBAL(printer_t));
  if (!trail_details->remaining) return false;      // end of trail

  if (trail_details->remaining == (uint32_t)trail_details->path.size())
  {
    trail_details->fault = trail_details->path.crbegin();
    trail_details->faultIndex = static_cast<int>(trail_details->path.size()) - 1;
    trail_details->remaining--;
  }
  else
  {
    advance(trail_details->fault, 1);
    trail_details->faultIndex--;
    trail_details->remaining--;
  }

  return true;
}
