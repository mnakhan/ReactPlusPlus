/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "mailbox.hpp"
#include "message.hpp"

rpp::mailbox_t::~mailbox_t()
{
}

rpp::STD_DEQUE::STD_DEQUE()
{
}

rpp::STD_DEQUE::~STD_DEQUE()
{
  clear();
}

void rpp::STD_DEQUE::clear()
{
  lock_guard<mutex> bolt(mx_queue);
  for_each(queue.cbegin(), queue.cend(), [this](pair<message_t*, actor_id_t> const& entry)
  {
    message_t *message = entry.first;
    if (!message->isSingleInstance) delete message;
  });
  queue.clear();
}

bool rpp::STD_DEQUE::pop(message_t ** message, actor_id_t & senderID)
{
  lock_guard<mutex> bolt(mx_queue);
  if (queue.empty()) return false;

  pair<message_t*, actor_id_t> entry = queue.front();
  *message = entry.first;
  senderID = entry.second;
  queue.pop_front();
  return true;
}

void rpp::STD_DEQUE::push(message_t * message, actor_id_t const & senderID)
{
  lock_guard<mutex> bolt(mx_queue);
  queue.push_back({ message, senderID });
}

void rpp::CAF_MPSCQ::reverse(LFMB_Node **lst, LFMB_Node **result)
{
  LFMB_Node *p = *lst;
  while (p)
  {
    LFMB_Node *q = p;
    p = p->next;
    q->next = *result;
    *result = q;
  }
  *lst = nullptr;
}

rpp::CAF_MPSCQ::CAF_MPSCQ()
{
  top.store(nullptr, memory_order_release);
  cache = nullptr;
}

rpp::CAF_MPSCQ::~CAF_MPSCQ()
{
  clear();
}

void rpp::CAF_MPSCQ::clear()
{
  message_t *message;
  actor_id_t lastSenderID;
  while (pop(&message, lastSenderID))
    if (!message->isSingleInstance) delete message;
  delete top.load();
  delete cache;
  top.store(nullptr, memory_order_release);
  cache = nullptr;
}

bool rpp::CAF_MPSCQ::pop(message_t **message, actor_id_t& senderID)
{
  if (!cache)
  {
    LFMB_Node *tmp = nullptr;
    tmp = top.exchange(tmp, memory_order_acq_rel);
    reverse(&tmp, &cache);
  }

  if (cache)
  {
    LFMB_Node *p = cache;
    cache = cache->next;
    *message = p->message;
    senderID = p->senderID;
    delete p;
    return true;
  }

  return false;
}

void rpp::CAF_MPSCQ::push(message_t *message, actor_id_t const& senderID)
{
  LFMB_Node *n = new LFMB_Node;
  n->message = message;
  n->senderID = senderID;
  n->next = top.load();
  while (!top.compare_exchange_weak(n->next, n));
}

void rpp::mpscq_create(mpscq_t *self)
{
  self->head = &self->stub;
  self->tail = &self->stub;
  self->stub.next = nullptr;
}

void rpp::mpscq_push(mpscq_t *self, mpscq_node_t *n)
{
  n->next = nullptr;
  mpscq_node_t* prev = self->head.exchange(n, memory_order_acq_rel);
  prev->next = n;
}

rpp::mpscq_node_t *rpp::mpscq_pop(mpscq_t *self)
{
  mpscq_node_t* tail = self->tail;
  mpscq_node_t* next = tail->next;

  if (tail == &self->stub)
  {
    if (!next) return nullptr;
    self->tail = next;
    tail = next;
    next = next->next;
  }

  if (next)
  {
    self->tail = next;
    return tail;
  }

  mpscq_node_t* head = self->head;
  if (tail != head) return nullptr;

  mpscq_push(self, &self->stub);
  next = tail->next;
  if (next)
  {
    self->tail = next;
    return tail;
  }

  return nullptr;
}

rpp::DV_MPSCQ::DV_MPSCQ()
{
  mpscq_create(&q);
}

rpp::DV_MPSCQ::~DV_MPSCQ()
{
  clear();
}

void rpp::DV_MPSCQ::clear()
{
  message_t *message;
  actor_id_t lastSenderID;
  while (pop(&message, lastSenderID))
    if (!message->isSingleInstance) delete message;
}

bool rpp::DV_MPSCQ::pop(message_t **message, actor_id_t& senderID)
{
  mpscq_node_t* n = mpscq_pop(&q);
  if (n)
  {
    *message = n->message;
    senderID = n->senderID;
    delete n;
    return true;
  }
  return false;
}

void rpp::DV_MPSCQ::push(message_t *message, actor_id_t const& senderID)
{
  mpscq_node_t *n = new mpscq_node_t;
  n->message = message;
  n->senderID = senderID;
  mpscq_push(&q, n);
}

rpp::ZMQ_MPSCQ::ZMQ_MPSCQ()
{
}

rpp::ZMQ_MPSCQ::~ZMQ_MPSCQ()
{
  clear();
}

void rpp::ZMQ_MPSCQ::clear()
{
  message_t *message;
  actor_id_t lastSenderID;
  while (pop(&message, lastSenderID))
    if (!message->isSingleInstance) delete message;
}

bool rpp::ZMQ_MPSCQ::pop(message_t **message, actor_id_t& senderID)
{
  pair<message_t*, actor_id_t> entry;
  bool success = queue.read(&entry);
  if (!success) return false;
  *message = entry.first;
  senderID = entry.second;
  return success;
}

void rpp::ZMQ_MPSCQ::push(message_t *message, actor_id_t const& senderID)
{
  pair<message_t*, actor_id_t> entry = make_pair(message, senderID);
  mx_writer.lock();
  queue.write(entry);
  mx_writer.unlock();
}
