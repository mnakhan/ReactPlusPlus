/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "timer.hpp"

rpp::timer_t::timer_t()
: isRunning(false), hasExited(false)
{
}

rpp::timer_t::~timer_t()
{
  clear();
}

bool rpp::timer_t::cancel(uint64_t token)
{
  lock_guard<mutex> bolt(mx_dispatchTable);
  auto it1 = periodicDispatchTable.begin();
  while (it1 != periodicDispatchTable.end())
  {
    if (it1->second->token == token)
    {
      it1->second->isCanceled = true;
      return true;
    }
    it1++;
  }
  auto it2 = oneShotDispatchTable.begin();
  while (it2 != oneShotDispatchTable.end())
  {
    if (it2->second->token == token)
    {
      oneShotDispatchTable.erase(it2);
      return true;
    }
    it2++;
  }

  return false;
}

void rpp::timer_t::clear()
{
  lock_guard<mutex> bolt(mx_dispatchTable);
  auto it1 = periodicDispatchTable.cbegin();
  while (true)
  {
    if (it1 == periodicDispatchTable.cend()) break;
    delete it1->second;
    it1++;
  }

  auto it2 = oneShotDispatchTable.cbegin();
  while (true)
  {
    if (it2 == oneShotDispatchTable.cend()) break;
    delete it2->second;
    it2++;
  }

  periodicDispatchTable.clear();
  oneShotDispatchTable.clear();
}

void rpp::timer_t::initialize()
{
  isRunning = true;
  hasExited = false;
}

void rpp::timer_t::run()
{
  while (isRunning)
  {
    time_point<system_clock> currentTime = system_clock::now();
    int32_t nextAlarm1 = INT32_MAX, nextAlarm2 = INT32_MAX;
    bool ret1 = execute<true>(currentTime, nextAlarm1);
    bool ret2 = execute<false>(currentTime, nextAlarm2);
    if (!ret1 && !ret2)
    {
      unique_lock<mutex> bolt(mx_dispatchTable);
      cv_dispatchTable.wait(bolt, [this]
      {
        return !isRunning || !(periodicDispatchTable.empty() && oneShotDispatchTable.empty());
      });
    }
    else
    {
      int sleepDuration = min(nextAlarm1, nextAlarm2);
      unique_lock<mutex> bolt(mx_dispatchTable);
      cv_dispatchTable.wait_for(bolt, milliseconds(sleepDuration), [this]
      {
        return !isRunning;
      });
    }
  }
  hasExited = true;
}

void rpp::timer_t::shutdown()
{
  if (!isRunning) return;
  isRunning = false;
  while (!hasExited)
  {
    {
      lock_guard<mutex> bolt(mx_dispatchTable);
      cv_dispatchTable.notify_one();
    }
    sleep_for(nanoseconds(1));
  }
  clear();
}
