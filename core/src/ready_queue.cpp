/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "io_policy.hpp"
#include "ready_queue.hpp"

rpp::ready_queue_t::ready_queue_t()
  :isSleeping(false)
{
  localQueue.allocator = orchestrationQueue.allocator = &allocator;
}

bool rpp::ready_queue_t::balance(uint64_t LQLength, uint64_t OQLength)
{
  uint64_t delta = 0, nodesClaimed = 0;
  if (LQLength > OQLength) delta = (LQLength - OQLength) / 2;
  else if (LQLength < OQLength) delta = (OQLength - LQLength) / 2;

  if (!delta) return false;

  pair<NodeType*, NodeType*> nodes;
  if (LQLength > OQLength)
  {
    nodes = localQueue.popRange(delta, &nodesClaimed);
    assert(delta == nodesClaimed);
    orchestrationQueue.pushRange(*nodes.first, *nodes.second, nodesClaimed);
  }
  else
  {
    assert(LQLength < OQLength);
    nodes = orchestrationQueue.popRange(delta, &nodesClaimed);
    assert(delta == nodesClaimed);
    localQueue.pushRange(*nodes.first, *nodes.second, nodesClaimed);
  }
  return true;
}

uint64_t rpp::ready_queue_t::getSize() const
{
  return localQueue.getSize();
}

uint64_t rpp::ready_queue_t::getNodeSize()
{
  return sizeof(NodeType);
}

bool rpp::ready_queue_t::isEmpty() const
{
  return localQueue.isEmpty();
}

void rpp::ready_queue_t::signal() const
{
  lock_guard<mutex> bolt(mx_oq);
  cv_mayExecute.notify_one();
}

void rpp::ready_queue_t::stage(shared_ptr<runnable_t> const& job)
{
  lock_guard<mutex> bolt(mx_oq);
  NodeType *node = allocator.malloc();  // lock mx_oq or else ABA problem (multiple-consumer pattern)
  node->element = job;
  orchestrationQueue.push(*node);
  if (isSleeping) cv_mayExecute.notify_one();
}

bool rpp::ready_queue_t::swap()
{
  uint64_t LQLength, OQLength;
  LQLength = localQueue.getSize();
  lock_guard<mutex> bolt(mx_oq);
  OQLength = orchestrationQueue.getSize();

  /***********
    BEFORE:
    LQ --> nil
    OQ --> nil
    AFTER:
    LQ --> nil
    OQ --> nil
    ************/
  if (!LQLength && !OQLength) // no work
    return false;

  if (!LQLength && OQLength)
  {
    /***********
      BEFORE:
      LQ --> nil
      OQ --> a, b
      AFTER:
      LQ --> a
      OQ --> b
      ************/
    if(OQLength - LQLength > 2) // pull in half of the work
      return balance(LQLength, OQLength);

    /***********
      BEFORE:
      LQ --> nil
      OQ --> a
      AFTER:
      LQ --> a
      OQ --> nil
      ************/
    localQueue.swap(orchestrationQueue);  // claim single item
    return false;
  }

  if (LQLength && !OQLength)
  {
    /***********
      BEFORE:
      LQ --> a, b
      OQ --> nil
      AFTER:
      LQ --> b
      OQ --> a
      ************/
    if (LQLength - OQLength > 2) return balance(LQLength, OQLength); // push half of the work to sleeping workers


    /***********
      BEFORE:
      LQ --> a
      OQ --> nil
      AFTER:
      LQ --> a
      OQ --> nil
      ************/
    return false; // keep single item
  }

  /***********
    BEFORE:
    LQ --> a
    OQ --> b
    AFTER:
    LQ --> b
    OQ --> a
    ************/
  localQueue.swap(orchestrationQueue);  // prevent starvation
  return true;
}

rpp::ready_queue_t::AllocatorType *rpp::ready_queue_t::steal(ready_queue_t& other)
{
  lock_guard<mutex> bolt(other.mx_oq);
  if (other.orchestrationQueue.isEmpty()) return nullptr;
  localQueue.swap(other.orchestrationQueue);
  return &other.allocator;
}

pair<uint64_t, uint64_t> rpp::ready_queue_t::queryAllocations() const
{
  return allocator.report();
}

void rpp::ready_queue_t::wait(cluster_id_t clusterID, worker_id_t workerID, io_policy_t *policy) const
{
  unique_lock<mutex> bolt(mx_oq);
  if (!orchestrationQueue.isEmpty()) return;
  isSleeping = true;
  if (policy) policy->onBlock(clusterID, workerID);
  cv_mayExecute.wait(bolt);
  isSleeping = false;
}
