/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "default_io_policy.hpp"

#ifdef LINUX

io::default_io_policy_t::default_io_policy_t()
: nClusters(0), level0(nullptr), level1(nullptr), L1Size(0)
{
  eventCounter = 0;
}

io::default_io_policy_t::~default_io_policy_t()
{
  delete [] level0;
  level0 = nullptr;
  for (worker_id_t w = 0; w < nClusters; w++) delete [] level1[w];
  delete [] level1;
  level1 = nullptr;
}

io::_L0_ENGINE_ *io::default_io_policy_t::getL0Generator(cluster_id_t clusterID)
{
  rpp_assert(clusterID < nClusters, "Attempted to access a non-existent L0 generator.", GLOBAL(printer_t));
  return &level0[clusterID];
}

io::_L1_ENGINE_ *io::default_io_policy_t::getL1Generator(cluster_id_t clusterID, worker_id_t workerID)
{
  rpp_assert(clusterID < nClusters && workerID < L1Size, "Attempted to access a non-existent L1 generator.", GLOBAL(printer_t));
  return &level1[clusterID][workerID];
}

descriptor_t io::default_io_policy_t::getL0Handle(cluster_id_t clusterID)
{
  return level0[clusterID].getL0Handle();
}

vector<descriptor_t> io::default_io_policy_t::getL1Handles(cluster_id_t clusterID)
{
  vector<descriptor_t> noBlockHandles;
  for (worker_id_t w = 0; w < L1Size; w++)
  {
    auto& noBlock = level1[clusterID][w];
    noBlockHandles.push_back(noBlock.getL1Handle());
  }
  return noBlockHandles;
}

worker_id_t io::default_io_policy_t::getL1Size() const
{
  return L1Size;
}

void io::default_io_policy_t::initialize()
{
  rpp_assert(nClusters > 0 && L1Size > 0, "Attempted to initialize default I/O policy with incorrect parameters.", GLOBAL(printer_t));
  level0 = new _L0_ENGINE_[nClusters];
  level1 = new _L1_ENGINE_*[nClusters];
}

bool io::default_io_policy_t::onBlock(cluster_id_t clusterID, worker_id_t workerID)
{
  level0[clusterID].rearm(workerID);
  return true;
}

bool io::default_io_policy_t::onIdle(cluster_id_t clusterID, worker_id_t workerID)
{
  poll_result_t result;
  return level1[clusterID][workerID].spin<false>(result);
}

bool io::default_io_policy_t::postLaunch(cluster_id_t clusterID)
{
  level0[clusterID].launch();
  return true;
}

bool io::default_io_policy_t::preLaunch(cluster_id_t clusterID)
{
  bool partitionFDTable = environment::enablePrivateDescriptorTable;

  if (partitionFDTable)
  {
#ifdef LINUX
    unshare(CLONE_FILES);
#endif // LINUX
  }
  level1[clusterID] = new _L1_ENGINE_[L1Size];

  vector<descriptor_t> noBlockHandles;

  for (worker_id_t w = 0; w < L1Size; w++)
  {
    auto& noBlock = level1[clusterID][w];
    noBlock.initialize(clusterID, w, L1Size, this);
    noBlockHandles.push_back(noBlock.getL1Handle());
  }
  level0[clusterID].initialize(clusterID, noBlockHandles, this);

  return true;
}
#endif // LINUX
