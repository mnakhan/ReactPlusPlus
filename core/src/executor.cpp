/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "context.hpp"
#include "executor.hpp"
#include "reaper.hpp"
#include "system_statistics.hpp"

printer_t& rpp::executor_t::tscout = GLOBAL(printer_t);

pair<thread*, rpp::runnable_t*> rpp::executor_t::getReaperHandle()
{
  return pair<thread*, runnable_t*>(worker, instance);
}

rpp::executor_t::executor_t(runnable_t *instance_, reaper_t *reaper_, bool useInternalThread_)
  : instance(instance_), reaper(reaper_), useInternalThread(useInternalThread_), worker(nullptr)
{
  sm_mayExecute.reset(0);
}

rpp::executor_t::~executor_t()
{
  if (instance->isAlive) shutdown();
#ifdef TRACE
  actor_id_t actorID = instance->selfID;
#endif // DEBUG
  auto cleanup = [](pair<thread*, runnable_t*> const& handle)
  {
    thread *worker = get<0>(handle);
    if (worker && worker->joinable()) worker->join();
    delete worker;

    runnable_t *instance = get<1>(handle);
    delete instance;
  };

  if (useInternalThread && this_thread::get_id() == worker->get_id())
  {
    reaper->enqueue(cleanup, getReaperHandle());
#ifdef TRACE
    tscout.printlnnws("Running ~executor_t for actor ", actorID, ", cleanup occurs in deferred context.");
#endif // TRACE
  }
  else
  {
    cleanup(getReaperHandle());
#ifdef TRACE
    tscout.printlnnws("Running ~executor_t for actor ", actorID, ", cleanup occurs in local context.");
#endif // TRACE
  }
}

rpp::context_t *rpp::executor_t::getContext() const
{
  if (!instance) return nullptr;
  context_t *ctx = dynamic_cast<context_t*>(instance);
  return ctx;
}

bool rpp::executor_t::isEnabled() const
{
  return instance && instance->isEnabled();
}

void rpp::executor_t::send(message_t *message, actor_id_t const& senderID)
{
  if (instance->maySchedule(message, senderID)) sm_mayExecute.V();
}

void rpp::executor_t::shutdown()
{
  instance->isAlive = false;
  sm_mayExecute.V();
}
