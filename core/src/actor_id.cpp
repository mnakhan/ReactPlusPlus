/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "actor_id.hpp"
#include "generic.hpp"
#include "system.hpp"

bool rpp::actor_id_t::isChannel_s() const
{
  return GLOBAL(system_t).whichDomain(ref(*this)) == scheduler_type_t::INERT;
}

bool rpp::actor_id_t::isDetached_s() const
{
  return GLOBAL(system_t).whichDomain(ref(*this)) == scheduler_type_t::KERNEL;
}

bool rpp::actor_id_t::isExternal_s() const
{
  return *this == actor_id_t();
}

bool rpp::actor_id_t::isNative_s() const
{
  return GLOBAL(system_t).whichDomain(ref(*this)) == scheduler_type_t::USERSPACE;
}

rpp::actor_id_t::actor_id_t(cluster_id_t clusterID_, local_id_t localID_)
  :clusterID(clusterID_), localID(localID_)
{
}

rpp::actor_id_t::actor_id_t(const char *input)
  : clusterID(DEFAULT_CLUSTER_ID), localID(DEFAULT_LOCAL_ID)
{
  parseID(input, ref(*this));
}

rpp::actor_id_t::actor_id_t(string const& input)
  : clusterID(DEFAULT_CLUSTER_ID), localID(DEFAULT_LOCAL_ID)
{
  parseID(input, ref(*this));
}

bool rpp::actor_id_t::operator==(actor_id_t const& other) const
{
  return clusterID == other.clusterID && localID == other.localID;
}

bool rpp::actor_id_t::operator!=(actor_id_t const& other) const
{
  return !(*this == other);
}

bool rpp::actor_id_t::operator<(actor_id_t const& other) const
{
  return (uint64_t)*this < (uint64_t)other;
}

bool rpp::actor_id_t::operator>(actor_id_t const& other) const
{
  return (uint64_t)*this > (uint64_t)other;
}

bool rpp::actor_id_t::isValid() const
{
  return GLOBAL(system_t).exists(ref(*this));
}

rpp::actor_id_t::operator uint64_t() const
{
  return static_cast<uint64_t>(clusterID) << 32 | (localID & 0xFFFFFFFF);
}

ostream& rpp::operator<<(ostream& out, steal_frequency_t frequency)
{
  switch (frequency)
  {
  case rpp::steal_frequency_t::DISABLED:
    return out << "DISABLED";
  case rpp::steal_frequency_t::LOW:
    return out << "LOW";
  case rpp::steal_frequency_t::NORMAL:
    return out << "NORMAL";
  case rpp::steal_frequency_t::HIGH:
    return out << "HIGH";
  case rpp::steal_frequency_t::RESTLESS:
    return out << "RESTLESS";
  default:
    return out << "";
  }
}

ostream& rpp::operator<<(ostream& out, actor_id_t const& actorID)
{
  if (actorID.clusterID == DEFAULT_CLUSTER_ID) out << '_';
  else if (actorID.clusterID == DETACHED_CLUSTER_ID) out << 'X';
  else out << actorID.clusterID;
  out << '.';
  if (actorID.clusterID == DEFAULT_LOCAL_ID) out << '_';
  else out << actorID.localID;
  return out;
}

bool rpp::actor_id_t::parseID(const char *input, actor_id_t& result)
{
  vector<string> tokens = split(input, ".");
  if (tokens.size() != 2) return false;

  if (!compareIgnoreCase(tokens[0], "_") && !compareIgnoreCase(tokens[1], "_"))
  {
    result = actor_id_t();
    return true;
  }

  if (!compareIgnoreCase(tokens[0], "X") && parse<uint32_t>(tokens[1], result.localID))
  {
    result.clusterID = DETACHED_CLUSTER_ID;
    return true;
  }

  if (!parse<uint32_t>(tokens[0], result.clusterID))
  {
    result = actor_id_t();
    return false;
  }

  if (!parse<uint32_t>(tokens[1], result.localID))
  {
    result = actor_id_t();
    return false;
  }

  return true;
}

bool rpp::actor_id_t::parseID(string const& input, actor_id_t& result)
{
  return parseID(input.c_str(), result);
}
