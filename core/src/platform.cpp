/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "platform.hpp"
#include "generic.hpp"
using namespace generic_ext;
using namespace io;

void rpp::platform_t::appendCPUSet(set<processor_id_t> const& cpuSet, vector<processor_id_t>& victims)
{
  for_each(cpuSet.cbegin(), cpuSet.cend(), [&victims](processor_id_t cpuID)
  {
    victims.push_back(cpuID);
  });
}

double rpp::platform_t::findNeighborsOf(numa_node_id_t nodeID, double currentRadius, set<numa_node_id_t>& neighborNodes)
{
  assert(nodeID < nodeCount);
  double newRadius = INFINITY;
  bool found = false;
  for (numa_node_id_t i = 0; i < nodeCount; i++)
  {
    double distance = distanceMatrix[nodeID][i];
    if (distance <= currentRadius) continue;
    if (distance < newRadius)
    {
      newRadius = distance;
      found = true;
    }
  }

  if (!found) return currentRadius;
  for (numa_node_id_t i = 0; i < nodeCount; i++)
  {
    double distance = distanceMatrix[nodeID][i];
    if (distance == newRadius) neighborNodes.insert(i);
  }
  return newRadius;
}

set<processor_id_t> rpp::platform_t::intersect(set<processor_id_t> const& set1, set<processor_id_t> const& set2)
{
  set<processor_id_t> result;
  size_t n1 = set1.size(), n2 = set2.size();
  if (!n1 || !n2) return result;
  size_t min = n1 > n2 ? n2 : n1;
  set<processor_id_t>::const_iterator it_min, e_min, it_other, e_other;
  if (n1 == min)
  {
    it_min = set1.cbegin();
    e_min = set1.cend();
    it_other = set2.cbegin();
    e_other = set2.cend();
  }
  else
  {
    it_min = set2.cbegin();
    e_min = set2.cend();
    it_other = set1.cbegin();
    e_other = set1.cend();
  }

  while (it_min != e_min)
  {
    processor_id_t candidate = *it_min;
    if (find(it_other, e_other, candidate) != e_other) result.insert(candidate);
    it_min++;
  }

  return result;
}

bool rpp::platform_t::parseCoreConfig(string const& input, set<processor_id_t>& cpuSet)
{
  vector<string> tokens = generic_ext::split(input, ",");
  for (size_t t = 0; t < tokens.size(); t++)
  {
    string token = tokens[t];

    vector<string> subtokens = generic_ext::split(token, "-");
    size_t stSize = subtokens.size();
    if (stSize != 1 && stSize != 2)
    {
      scout << "Error parsing this CPU set --> " << input << endl;
      return false;
    }

    processor_id_t min = 0, max = 0;
    if (!generic_ext::parse<processor_id_t>(subtokens[0], min))
    {
      scout << "Error parsing this CPU set --> " << input << endl;
      return false;
    }

    if (stSize == 2)
    {
      if (!generic_ext::parse<processor_id_t>(subtokens[1], max) || min > max)
      {
        scout << "Error parsing this CPU set --> " << input << endl;
        return false;
      }
    }
    else max = min;
    for (processor_id_t i = min; i <= max; i++) cpuSet.insert(i);
  }
  return true;
}

void rpp::platform_t::validateCPUAllocation()
{
  if (isNUMAAware)
  {
    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      if (numaCPUSets[i].empty())
      {
        scout << "Error: Node " << i << " does not have any CPUs." << endl;
        isValid = false;
      }
    }
  }

  for (cluster_id_t i = 0; i < clusterCount; i++)
  {
    if (clusterCPUSets[i].empty())
    {
      scout << "Error: Cluster " << i << " does not have any CPUs." << endl;
      isValid = false;
    }
  }

  if (isNUMAAware)
  {
    for (numa_node_id_t i = 0; i < nodeCount - 1; i++)
    {
      for (numa_node_id_t j = i + 1; j < nodeCount; j++)
      {
        set<processor_id_t> const& set1 = numaCPUSets[i];
        set<processor_id_t> const& set2 = numaCPUSets[j];
        set<processor_id_t> common = intersect(set1, set2);
        if (!common.empty())
        {
          scout << "Error: The following CPU(s) claim to be located on both NUMA nodes " << i << " and " << j << " --> " << common << endl;
          isValid = false;
        }
      }
    }
  }

  for (cluster_id_t i = 0; i < clusterCount - 1; i++)
  {
    for (cluster_id_t j = i + 1; j < clusterCount; j++)
    {
      set<processor_id_t> const& set1 = clusterCPUSets[i];
      set<processor_id_t> const& set2 = clusterCPUSets[j];
      set<processor_id_t> common = intersect(set1, set2);
      if (!common.empty())
      {
        scout << "Error: The following CPU(s) are allocated to both clusters " << i << " and " << j << " --> " << common << endl;
        isValid = false;
      }
    }
  }
}

rpp::platform_t::platform_t()
  : distanceMatrix(nullptr), cpuAssignments(nullptr),
  nAllocatedProcessors(0), nAvailableProcessors(0),
  nodeCount(0), clusterCount(0),
  isValid(false), isNUMAAware(false)
{
}

rpp::platform_t::~platform_t()
{
  clear();
}

void rpp::platform_t::clear()
{
  if (distanceMatrix)
  {
    for (numa_node_id_t i = 0; i < nodeCount; i++) delete[] distanceMatrix[i];
    delete[] distanceMatrix;
    distanceMatrix = nullptr;
  }

  delete[] cpuAssignments;
  cpuAssignments = nullptr;

  nAllocatedProcessors = 0;
  nAvailableProcessors = 0;
  nodeCount = 0;
  clusterCount = 0;
  isValid = false;
  isNUMAAware = false;

  topology.clear();
  numaCPUSets.clear();
  clusterCPUSets.clear();
}

bool rpp::platform_t::hasValidConfiguration() const
{
  return isValid;
}

bool rpp::platform_t::hasNUMAAwareness() const
{
  return isNUMAAware;
}

bool rpp::platform_t::increaseSearchRadius(numa_node_id_t nodeID, uint32_t level, vector<processor_id_t>& neighborCPUs) const
{
  assert(nodeID < nodeCount);

#ifndef ENABLE_STRICT_CONFIG
  if (topology.empty()) return false;
#endif // !ENABLE_STRICT_CONFIG

  vector<set<numa_node_id_t>> const& neigborNodes = topology.at(nodeID);
  if (level >= neigborNodes.size()) return false;
  set<numa_node_id_t> const& increment = neigborNodes[level];
  for_each(increment.cbegin(), increment.cend(), [&](numa_node_id_t nodeID_)
  {
    set<processor_id_t> const& cpuSet = numaCPUSets.at(nodeID_);
    appendCPUSet(cpuSet, neighborCPUs);
  });

  return true;
}

bool rpp::platform_t::increaseSearchRadius(numa_node_id_t nodeID, cluster_id_t clusterID, uint32_t& level, vector<processor_id_t>& neighborCPUs) const
{
  assert(nodeID < nodeCount && clusterID < clusterCount);

#ifndef ENABLE_STRICT_CONFIG
  if (topology.empty()) return false;
#else
  rpp_assert(!topology.empty(), "Need to specify NUMA distances in the configuration file.", GLOBAL(printer_t));
#endif // !ENABLE_STRICT_CONFIG

  vector<set<numa_node_id_t>> const& neighborhood = topology.at(nodeID);
  size_t neighborhoodSize = neighborhood.size();

  if (neighborhoodSize == 0 || level >= neighborhoodSize) return false;

  bool done = false;
  size_t previousSize = neighborCPUs.size();
  set<processor_id_t> const& clusterCPUSet = clusterCPUSets.at(clusterID);
  do
  {
    set<numa_node_id_t> const& increment = neighborhood[level];

    auto it = increment.cbegin(), e = increment.cend();
    while (it != e)
    {
      set<processor_id_t> const& numaCPUSet = numaCPUSets.at(*it);
      set<processor_id_t> common = intersect(numaCPUSet, clusterCPUSet);
      if (!common.empty())
      {
        done = true;
        appendCPUSet(common, neighborCPUs);
      }
      else if (level == 0) done = true;
      it++;
    }

    level++;
    if (level >= neighborhoodSize) done = true;
  } while (!done);

  size_t currentSize = neighborCPUs.size();
  return currentSize > previousSize;
}

bool rpp::platform_t::pinSelfToCPU(processor_id_t cpuID)
{
#ifdef WINDOWS
  HANDLE handle = GetCurrentThread();
  DWORD_PTR mask = (1 << cpuID) & 0xFFFFFFFF;
  return SetThreadAffinityMask(handle, mask);
#elif defined(LINUX)
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(cpuID, &cpuset);
  return pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset) == 0;
#else
#error Platform not supported for this operation.
#endif // WINDOWS
}

bool rpp::platform_t::pinThreadToCPU(thread& t, processor_id_t cpuID)
{
#ifdef WINDOWS
  HANDLE handle = t.native_handle();
  DWORD_PTR mask = (1 << cpuID) & 0xFFFFFFFF;
  return SetThreadAffinityMask(handle, mask);
#elif defined(LINUX)
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(cpuID, &cpuset);
  return pthread_setaffinity_np(t.native_handle(), sizeof(cpu_set_t), &cpuset) == 0;
#else
#error platform_t not supported for this operation.
#endif // WINDOWS
}

void rpp::platform_t::reload(configuration_t & config)
{
  clear();
  parse<bool>(config["general"]["numaaware"], isNUMAAware);

  if (isNUMAAware)
  {
    parse<numa_node_id_t>(config["numa"]["nodecount"], nodeCount);
    if (!nodeCount || nodeCount > MAX_NUMA_NODES)
    {
      scout << "Error: NUMA awareness is enabled but node count is invalid." << endl;
      clear();
      return;
    }
  }

  parse<numa_node_id_t>(config["cluster"]["clustercount"], clusterCount);
  if (!clusterCount || clusterCount > MAX_CLUSTERS)
  {
    scout << "Error: Cluster count is invalid." << endl;
    clear();
    return;
  }

  if (isNUMAAware)
  {
    distanceMatrix = new double*[nodeCount];
    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      distanceMatrix[i] = new double[nodeCount];
      for (numa_node_id_t j = 0; j < nodeCount; j++) distanceMatrix[i][j] = INFINITY;
    }

    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      string key = string("node") + ::toString(i);
      if (!parseCoreConfig(config["numa"][key], numaCPUSets[i]))
      {
        clear();
        return;
      }
    }
  }

  for (cluster_id_t i = 0; i < clusterCount; i++)
  {
    string key = string("cluster") + ::toString(i);
    if (!parseCoreConfig(config["cluster"][key], clusterCPUSets[i]))
    {
      clear();
      return;
    }
  }

  isValid = true;
  validateCPUAllocation();
  if (!isValid)
  {
    clear();
    return;
  }

  if (isNUMAAware)
  {
    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      for (numa_node_id_t j = 0; j < nodeCount; j++)
      {
        double distance = INFINITY;
        string key = ::toString(i) + "," + ::toString(j);
        if (parse<double>(config["numa distances"][key], distance)) distanceMatrix[i][j] = distanceMatrix[j][i] = distance;
      }
    }

    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      double currentRadius = 0.0;
      bool done = false;
      numa_node_id_t nodeID = i;
      while (!done)
      {
        set<numa_node_id_t> neighbors;
        currentRadius = findNeighborsOf(nodeID, currentRadius, neighbors);
        if (!neighbors.empty()) topology[nodeID].push_back(neighbors);
        else done = true;
      }
    }
  }


  for (cluster_id_t i = 0; i < clusterCount; i++)
  {
    string key = string("cluster") + ::toString(i);
    if (!parseCoreConfig(config["cluster"][key], clusterCPUSets[i]))
    {
      clear();
      return;
    }
  }

  for (cluster_id_t i = 0; i < clusterCount; i++)
  {
    set<processor_id_t> const& cpuSet = clusterCPUSets[i];
    nAllocatedProcessors += (processor_id_t)cpuSet.size();
  }

  uint32_t crt_reported_cores = thread::hardware_concurrency();
  if (isNUMAAware)
  {
    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      set<processor_id_t> const& cpuSet = numaCPUSets[i];
      nAvailableProcessors += (processor_id_t)cpuSet.size();
    }

    if (nAvailableProcessors != crt_reported_cores)
    {
      scout << "Configuration file reports a different core count than available on the system. Total cores = " << crt_reported_cores << ", reported = " << nAvailableProcessors << endl;
      clear();
      return;
    }

    if (nAllocatedProcessors > crt_reported_cores)
    {
      scout << "No enough cores available on the system. Total cores = " << crt_reported_cores << ", allocated = " << nAllocatedProcessors << endl;
      clear();
      return;
    }
  }

  nAvailableProcessors = crt_reported_cores;
  cpuAssignments = new worker_id_t[nAvailableProcessors];
  for (processor_id_t i = 0; i < nAvailableProcessors; i++) cpuAssignments[i] = DEFAULT_WORKER_ID;
  for (cluster_id_t i = 0; i < clusterCount; i++)
  {
    set<processor_id_t> const& cpuSet = clusterCPUSets[i];
    auto it = cpuSet.cbegin(), e = cpuSet.cend();
    worker_id_t workerIndex = 0;
    while (it != e)
    {
      processor_id_t cpuID = *it;
      assert(cpuID < nAvailableProcessors);
      cpuAssignments[cpuID] = workerIndex++;
      it++;
    }
  }
}

string rpp::platform_t::toString() const
{
  if (!isValid) return "";

  ostringstream out;
  out << "Processors allocated to all clusters = " << nAllocatedProcessors << endl;
  out << "Processor count = " << nAvailableProcessors << endl << endl;
  if (isNUMAAware)
  {
    out << "CPU sets (NUMA)" << endl;
    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      out << "Node " << i << ": ";
      auto const& numaCPUSet = numaCPUSets.at(i);
      size_t idx = 0, coreCount = numaCPUSet.size();
      for_each(numaCPUSet.cbegin(), numaCPUSet.cend(), [&out, &idx, coreCount](processor_id_t cpuID)
      {
        out << cpuID;
        if (idx < coreCount - 1) out << ", ";
        idx++;
      });
      out << endl;
    }
    out << endl;
  }

  for (cluster_id_t i = 0; i < clusterCount; i++)
  {
    auto const& clusterCPUSet = clusterCPUSets.at(i);
    out << "CPU assignments for cluster " << i << endl;;
    for_each(clusterCPUSet.cbegin(), clusterCPUSet.cend(), [this, &out, i](processor_id_t cpuID)
    {
      assert(cpuAssignments[cpuID] != DEFAULT_WORKER_ID);
      out << "CPU " << cpuID << " --> cluster " << i << "/worker " << cpuAssignments[cpuID] << endl;
    });
    out << endl;
  }

  if (isNUMAAware)
  {
    out << "Distance matrix\n\t";
    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      out << "N" << i;
      if (i < nodeCount - 1) out << "\t";
    }
    out << endl;

    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      out << "N" << i << "\t";
      for (numa_node_id_t j = 0; j < nodeCount; j++)
      {
        out << distanceMatrix[i][j];
        if (j < nodeCount - 1) out << "\t";
      }
      out << endl;
    }
    out << endl;

    out << "Topology" << endl;
    size_t mapSize = topology.size();
    assert(mapSize == nodeCount);

    for_each(topology.cbegin(), topology.cend(), [&out, &mapSize](auto& kvp)
    {
      out << "Node " << kvp.first << ": ";
      auto it = kvp.second.cbegin();
      size_t lstSize = kvp.second.size(), lstIndex = 0;
      while (it != kvp.second.cend())
      {
        auto nodeSet = *it;
        out << nodeSet;
        if (lstIndex < lstSize - 1) out << " --> ";
        lstIndex++;
        it++;
      }

      if (mapSize > 1) out << endl;
      mapSize--;
    });

#ifdef TRACE_PLATFORM
    out << endl << endl;
    for (numa_node_id_t i = 0; i < nodeCount; i++)
    {
      uint32_t level = 0;
      numa_node_id_t nodeID = i;
      bool done = false;

      out << "Victim Selection/Node " << nodeID << endl;
      vector<processor_id_t> victims;
      while (!done)
      {
        done = !increaseSearchRadius(nodeID, level, victims);
        if (!done)
        {
          out << "Level " << level << ": " << victims << endl;
          level++;
        }
      }

      if (i < nodeCount - 1) out << endl;
    }
#endif // 0
  }

  return out.str();
}

numa_node_id_t rpp::platform_t::getNUMANodeCount() const
{
  return nodeCount;
}

processor_id_t rpp::platform_t::getProcessorCount(numa_node_id_t nodeID) const
{
  assert(nodeID < nodeCount);
  return (worker_id_t)numaCPUSets.at(nodeID).size();
}

cluster_id_t rpp::platform_t::getClusterCount() const
{
  return clusterCount;
}

worker_id_t rpp::platform_t::getWorkerCount(cluster_id_t clusterID) const
{
  assert(clusterID < clusterCount);
  return (worker_id_t)clusterCPUSets.at(clusterID).size();
}

processor_id_t rpp::platform_t::mapWorkerToProcessor(cluster_id_t clusterID, worker_id_t workerID) const
{
  assert(clusterID < clusterCount);
  auto const& cpuSet = clusterCPUSets.at(clusterID);
  assert(workerID < cpuSet.size());
  set<processor_id_t>::const_iterator it = cpuSet.cbegin(), e = cpuSet.cend();
  worker_id_t idx = 0;
  while (it != e)
  {
    if (idx == workerID) return *it;
    idx++;
    it++;
  }

  // should not be here
  assert(false);
  return DEFAULT_CPU_ID;
}

numa_node_id_t rpp::platform_t::mapProcessorToNode(processor_id_t cpuID) const
{
  for (numa_node_id_t i = 0; i < nodeCount; i++)
  {
    set<processor_id_t> const& cpuSet = numaCPUSets.at(i);
    if (find(cpuSet.cbegin(), cpuSet.cend(), cpuID) != cpuSet.cend()) return i;
  }
  return DEFAULT_NODE_ID;
}

worker_id_t rpp::platform_t::mapProcessorToWorker(processor_id_t cpuID) const
{
  if (cpuAssignments && cpuID < nAvailableProcessors) return cpuAssignments[cpuID];
  return DEFAULT_WORKER_ID;
}

map<thread::id, tuple<numa_node_id_t, processor_id_t, cluster_id_t, worker_id_t>> rpp::ThisThread::threadMap;
mutex rpp::ThisThread::mx_threadMap;
rpp::platform_t *rpp::ThisThread::currentPlatform = nullptr;

bool rpp::ThisThread::enableBlocking = false;

void rpp::ThisThread::registerThread(numa_node_id_t numaNodeID, processor_id_t cpuID, cluster_id_t clusterID, worker_id_t workerID)
{
  lock_guard<mutex> bolt(mx_threadMap);
  thread::id selfID = this_thread::get_id();
  threadMap[selfID] = make_tuple(numaNodeID, cpuID, clusterID, workerID);
}

void rpp::ThisThread::blockAllExcept(cluster_id_t clusterID, worker_id_t workerID)
{
  if (!enableBlocking ||
    (clusterID == ThisThread::getClusterID() && workerID == ThisThread::getWorkerID())) return;
  sleep_for(hours(24));
}

void rpp::ThisThread::clearMap()
{
  lock_guard<mutex> bolt(mx_threadMap);
  threadMap.clear();
}

numa_node_id_t rpp::ThisThread::getNUMANodeID()
{
  lock_guard<mutex> bolt(mx_threadMap);
  thread::id selfID = this_thread::get_id();
  auto it = threadMap.find(selfID);
  if (it != threadMap.cend()) return get<0>(it->second);
  return DEFAULT_NODE_ID;
}

processor_id_t rpp::ThisThread::getProcessorID()
{
  lock_guard<mutex> bolt(mx_threadMap);
  thread::id selfID = this_thread::get_id();
  auto it = threadMap.find(selfID);
  if (it != threadMap.cend()) return get<1>(it->second);
  return DEFAULT_CPU_ID;
}

cluster_id_t rpp::ThisThread::getClusterID()
{
  lock_guard<mutex> bolt(mx_threadMap);
  thread::id selfID = this_thread::get_id();
  auto it = threadMap.find(selfID);
  if (it != threadMap.cend()) return get<2>(it->second);
  return DEFAULT_CLUSTER_ID;
}

worker_id_t rpp::ThisThread::getWorkerID()
{
  lock_guard<mutex> bolt(mx_threadMap);
  thread::id selfID = this_thread::get_id();
  auto it = threadMap.find(selfID);
  if (it != threadMap.cend()) return get<3>(it->second);
  return DEFAULT_WORKER_ID;
}

string rpp::ThisThread::getPlatformString(bool globalReport)
{
  ostringstream result;

  thread::id threadID = this_thread::get_id();
  numa_node_id_t numaNodeID = getNUMANodeID();
  processor_id_t cpuID = getProcessorID();
  cluster_id_t clusterID = getClusterID();
  worker_id_t workerID = getWorkerID();

  if (globalReport)
  {
    if (currentPlatform) result << currentPlatform->toString();
  }
  else
  {
    result << "Native ID of this thread: " << threadID
      << endl
      << "Pinned to CPU " << (cpuID == DEFAULT_CPU_ID ? "<unknown>" : toString(cpuID))
      << " from NUMA node " << (numaNodeID == DEFAULT_NODE_ID ? "<unknown>" : toString(numaNodeID)) << '.'
      << endl
      << "Lives in thread pool owned by cluster " << (clusterID == DEFAULT_CLUSTER_ID ? "<unknown>" : toString(clusterID))
      << ". Index in thread pool = " << (workerID == DEFAULT_WORKER_ID ? "<unknown>" : toString(workerID)) << '.';
  }

  return result.str();
}
