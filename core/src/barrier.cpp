/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "message.hpp"
#include "barrier.hpp"
#include "system.hpp"

printer_t& rpp::blocking_handle_t::tscout = GLOBAL(printer_t);
rpp::blocking_handle_t::receiver_status_t rpp::blocking_handle_t::block(int32_t token, int32_t timeoutInMillis)
{
  bool isBlocking = timeoutInMillis < 0;
  system_clock::time_point expireAt;
  if (!isBlocking) expireAt = startFrom + milliseconds(timeoutInMillis);
  while (true)
  {
    unique_lock<mutex> bolt(mx_notification);
    auto it = notifications.find(token);
    rpp_assert(it != notifications.end(), "The blocking token is not registered.", tscout);
    if (it->second == flag_t::ACK)
    {
      notifications.erase(it);
      return receiver_status_t::RS_SUCCESS;
    }
    else
    {
      if (isBlocking) cv_blockTillNotified.wait(bolt);
      else if (expireAt > system_clock::now()) cv_blockTillNotified.wait_until(bolt, expireAt);
      else
      {
        it->second = flag_t::NACK;
        return receiver_status_t::RS_TIMED_OUT;
      }
    }
  }
}

bool rpp::blocking_handle_t::registerToken(int32_t token)
{
  lock_guard<mutex> bolt(mx_notification);
  auto it = notifications.find(token);
  if (it != notifications.end()) return false;
  notifications[token] = flag_t::INIT;
  startFrom = system_clock::now();
  return true;
}

rpp::blocking_handle_t::sender_status_t rpp::blocking_handle_t::unblock(int32_t token)
{
  lock_guard<mutex> bolt(mx_notification);
  auto it = notifications.find(token);
  if (it == notifications.end()) return sender_status_t::SS_NO_SUCH_TOKEN;

  if (it->second == flag_t::INIT)
  {
    it->second = flag_t::ACK;
    cv_blockTillNotified.notify_all();
    return sender_status_t::SS_SUCCESS;
  }
  else
  {
    rpp_assert(it->second == flag_t::NACK, "NACK expected", tscout);
    notifications.erase(it);
    return sender_status_t::SS_TIMED_OUT;
  }
}

ostream& rpp::operator<<(ostream& out, rpp::synchronize_t const& message)
{
  return out << "Synchronization token = " << message.token;
}

random_t& rpp::barrier_t::barrier_details_t::rnd = GLOBAL(generic_ext::random_t);

rpp::barrier_t::barrier_details_t::barrier_details_t(actor_id_t const& actorID_, int32_t timeoutInMillis_)
  : actorID(actorID_), token(-1), timeoutInMillis(timeoutInMillis_)
{
  blocking_handle_t& handle = GLOBAL(blocking_handle_t);
  while (true)
  {
    int32_t candidate = rnd.nextInt<int32_t>(0, INT32_MAX);
    if (handle.registerToken(candidate))
    {
      token = candidate;
      break;
    }
  }
}

rpp::barrier_t::barrier_details_t::~barrier_details_t()
{
  blocking_handle_t& handle = GLOBAL(blocking_handle_t);
  if (handle.block(token, timeoutInMillis) == blocking_handle_t::receiver_status_t::RS_TIMED_OUT)
    GLOBAL(printer_t).println("barrier timeout: waited", timeoutInMillis, "milliseconds for actor", actorID);
}

rpp::synchronize_t rpp::barrier_t::barrier_details_t::synchronize() const
{
  return synchronize_t(token);
}

void rpp::synchronize(message_t *message)
{
  synchronize_t syncMessage;
  if (!message->extract(syncMessage)) return;
  blocking_handle_t& handle = GLOBAL(blocking_handle_t);
  handle.unblock(syncMessage.token);
}

rpp::synchronize_t::synchronize_t(int32_t token_)
  : token(token_)
{
}

void rpp::synchronize_t::operator()() const
{
  blocking_handle_t& handle = GLOBAL(blocking_handle_t);
  handle.unblock(token);
}

void rpp::barrier_t::scope_s(int32_t timeoutInMillis)
{
}

rpp::barrier_t::~barrier_t()
{
  auto& sys = GLOBAL(system_t);
  for_each(details.cbegin(), details.cend(), [&](const barrier_details_t *bar_details)
  {
    sys.send(bar_details->synchronize(), bar_details->actorID);
    delete bar_details;
  });
  details.clear();
}
