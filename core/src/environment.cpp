/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "precompiled.hpp"
#include "environment.hpp"

using namespace generic_ext;
using namespace io;

numa_node_id_t environment::numaNodeCount = 0;
map<numa_node_id_t, processor_id_t> environment::cpuCountPerNode;

cluster_id_t environment::clusterCount = 0;
map<cluster_id_t, worker_id_t> environment::workerCountPerCluster;
string environment::configPath = DEFAULT_CONFIG_FILE;

string environment::ipAddress = LOCALHOST;
ushort environment::portNumber = LOCALPORT;

bool environment::pinSignaler = false;
bool environment::pinSession = false;
local_id_t environment::maxStandbyActorsPerCluster = (local_id_t)25_K;
uint32_t environment::L0MaxEvents = 16;
uint32_t environment::L1MaxEvents = 32;
uint32_t environment::backlog = 512;

bool environment::enableDirectSignaling = true;
bool environment::enablePrivateDescriptorTable = true;
bool environment::enableRestlessSpin = true;
uint32_t environment::restlessSpinCount = (uint32_t)10_K;
bool environment::enableImmediateContext = true;

bool environment::enableDistributedPolling = true;
bool environment::enableStatsReporter = true;

void environment::reload(bool printConfig)
{
  rpp::platform_t platform;
  rpp::configuration_t config;
  if (!config.load(configPath))
  {
    reset<true>(printConfig);
    return;
  }

  platform.reload(config);
  if (!platform.hasValidConfiguration())
  {
    reset<true>(printConfig);
    return;
  }

  numaNodeCount = platform.getNUMANodeCount();
  for (numa_node_id_t nodeID = 0; nodeID < numaNodeCount; nodeID++) cpuCountPerNode[nodeID] = platform.getProcessorCount(nodeID);

  clusterCount = platform.getClusterCount();
  for(cluster_id_t clusterID = 0; clusterID < clusterCount; clusterID++) workerCountPerCluster[clusterID] = platform.getWorkerCount(clusterID);

  ipAddress = config["server"]["ipaddress"];
  if (!validate<false>("IP address", ipAddress.length(), LR((size_t)7, (size_t)15))
    || !parse<ushort>(config["server"]["portnumber"], portNumber)
    || !validate<false>("port number", portNumber, LR(1000, 65535))
    || !parse<bool>(config["server"]["pinsignaler"], pinSignaler)
    || !parse<bool>(config["server"]["pinsession"], pinSession)
    || !parse<local_id_t>(config["server"]["maxstandbyactorspercluster"], maxStandbyActorsPerCluster)
    || !validate<false>("maximum standby actors per cluster", maxStandbyActorsPerCluster, LR(1U, (uint32_t)100_K))
    || !parse<uint32_t>(config["server"]["l0maxevents"], L0MaxEvents)
    || !validate<false>("maximum events on level 0", L0MaxEvents, LR(1U, 8192U))
    || !parse<uint32_t>(config["server"]["l1maxevents"], L1MaxEvents)
    || !validate<false>("maximum events on level 1", L1MaxEvents, LR(1U, 8192U))
    || !parse<uint32_t>(config["server"]["backlog"], backlog)
    || !validate<false>("TCP backlog", backlog, LR(1U, 500000U))
    || !parse<bool>(config["server"]["enabledirectsignaling"], enableDirectSignaling)
    || !parse<bool>(config["server"]["enableprivatedescriptortable"], enablePrivateDescriptorTable)
    || !parse<bool>(config["server"]["enablerestlessspin"], enableRestlessSpin)
    || !parse<uint32_t>(config["server"]["restlessspincount"], restlessSpinCount)
    || !validate<false>("restless spin count", restlessSpinCount, LR(0U, (uint32_t)10_K))
    || !parse<bool>(config["server"]["enableimmediatecontext"], enableImmediateContext)
    || !parse<bool>(config["server"]["enabledistributedpolling"], enableDistributedPolling)
    || !parse<bool>(config["server"]["enablestatsreporter"], enableStatsReporter))
    reset<false>(printConfig);

  else if(printConfig) scout << "loaded configurations from '" << configPath << "'\n" << platform.toString() << endl;
}
