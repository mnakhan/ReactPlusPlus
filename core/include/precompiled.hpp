/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef PRECOMPILED_H
#define PRECOMPILED_H

#define TRUE 1
#define FALSE 0
#define _USE_MATH_DEFINES

#include <cstdlib>
#include <stdarg.h>
#include <cassert>
#include <type_traits>
#include <typeinfo>
#include <typeindex>
#include <cstring>
#include <regex>
#include <array>
#include <vector>
#include <list>
#include <forward_list>
#include <stack>
#include <queue>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <tuple>
#include <deque>
#include <iterator>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <cfloat>
#include <numeric>
#include <climits>
#include <random>
#include <algorithm>
#include <functional>
#include <ctime>
#include <chrono>
#include <thread>
#include <mutex>
#include <future>
#include <atomic>
#include <condition_variable>

#ifdef WINDOWS
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif // !WIN32_LEAN_AND_MEAN

#include <windows.h>

#define _TIMESPEC_DEFINED
#include <pthread.h>
#include <sched.h>
#include <semaphore.h>

#elif defined (LINUX)
#include <semaphore.h>
#endif // WINDOWS

using lint = long int;
using llong = long long;
using ldouble = long double;
using uchar = unsigned char;
using ushort = unsigned short;
using uint = unsigned int;
using ulint = unsigned long int;
using ulong = unsigned long;
using ullong = unsigned long long;

using namespace std;
using namespace chrono;
using namespace this_thread;

using local_id_t = uint32_t;
using worker_id_t = uint32_t;
using cluster_id_t = uint32_t;
using numa_node_id_t = uint32_t;
using processor_id_t = uint32_t;
using descriptor_t = int;

template<typename Trait>
using Constraint = typename enable_if<Trait::value>::type;

template<typename ReturnType, typename... ArgTypes>
using signature = ReturnType(ArgTypes...);
template<typename ReturnType, typename... ArgTypes>
using const_signature = ReturnType(ArgTypes...) const;

#define DEFAULT_WORKER_ID UINT32_MAX
#define DEFAULT_CLUSTER_ID UINT32_MAX
#define DEFAULT_LOCAL_ID UINT32_MAX
#define DEFAULT_CPU_ID UINT32_MAX
#define DEFAULT_NODE_ID UINT32_MAX
#define DETACHED_CLUSTER_ID static_cast<uint32_t>(UINT32_MAX - 1)
#define DEFAULT_REPLY_TOKEN UINT64_MAX
#define DEFAULT_DESCRIPTOR -1

#define MAX_CLUSTERS 1024
#define MAX_THREADS_PER_CLUSTER 1024
#define MAX_NUMA_NODES 1024

#define DEFAULT_TYPE_ID UINT_MAX
#define MAX_BUFFER_LENGTH 4096
#define MAX_ARG_LENGTH 64

#define ZMQ_MAILBOX_ALLOC_UNIT 1024
#define RQ_CACHE_SIZE 8192
#define USE_NATIVE_ACTORS

#if !defined(USE_NATIVE_ACTORS)
#define LOCATION true
#else
#define LOCATION (location_t::map())
#endif

//#define ENABLE_HTABLE_RESIZING
#define INIT_HTABLE_BUCKET_COUNT 1024
#define MAX_HTABLE_LOAD_FACTOR 32.0f
#define HTABLE_RESIZE_FACTOR 4.0f

//#define BYPASS_HTABLE_FOR_BIG

#ifdef LINUX
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <sys/stat.h>
#include <fcntl.h>

using SOCKET = descriptor_t;
#define INVALID_SOCKET DEFAULT_DESCRIPTOR
#endif // LINUX

#define USE_EDGE_TRIGGER

#define DEFAULT_CONFIG_FILE "config.ini"
#define DEFAULT_CONFIG_FILE_ALT_1 "conf/config.ini"
#define DEFAULT_CONFIG_FILE_ALT_2 "../conf/config.ini"

//#define TEST_SHUTDOWN_SEQ
//#define TEST_MIGRATION

// if enabled, don't ignore incorrect configuration parameters
#define ENABLE_STRICT_CONFIG

#if _WIN32 || _WIN64
#if _WIN64
#define IA64
#else
#define IA32
#endif
#endif

#if __GNUC__
#if __x86_64__ || __ppc64__
#define IA64
#else
#define IA32
#endif
#endif

#define LR(X, Y) generic_ext::range_t<decltype(X), true, true>(X, Y)
#define L_EXCL(X, Y) generic_ext::range_t<decltype(X), false, true>(X, Y)
#define R_EXCL(X, Y) generic_ext::range_t<decltype(X), true, false>(X, Y)
#define LR_EXCL(X, Y) generic_ext::range_t<decltype(X), false, false>(X, Y)

//#define TRACE_GENERIC
//#define TRACE_DTOR
//#define TRACE_PLATFORM
//#define TRACE_STARTUP
//#define TRACE_SHUTDOWN
//#define TRACE_THEFT
//#define TRACE_IO_EVENT


// modify definition of INIT_SYSTEM
#define USE_CONFIG_FILE

#define LOCALHOST "127.0.0.1"
#define LOCALPORT 8080

#ifdef USE_CONFIG_FILE
#define INIT_SYSTEM do { \
  sys.restartWithConfiguration(nullptr); \
} while (false);
#else
#define INIT_SYSTEM do { \
  sys.restart<uint32_t>(nullptr, nThreads); \
} while (false);
#endif // USE_CONFIG_FILE

typedef string tag_t;
typedef atomic<uint64_t> counter_t;

#if defined __GNUC__
#define rpp_likely(x) __builtin_expect ((x), 1)
#define rpp_unlikely(x) __builtin_expect ((x), 0)
#else
#define rpp_likely(x) (x)
#define rpp_unlikely(x) (x)
#endif

template<unsigned Major>
struct __MAJOR__
{
  template<unsigned Minor>
  struct __MINOR__
  {
    template<unsigned Patch>
    struct __PATCH__
    {
      constexpr static unsigned major = Major, minor = Minor, patch = Patch;
      constexpr static unsigned version = major * 10000 + minor * 100 + patch;
    };
  };
};

typedef
__MAJOR__<0>
::__MINOR__<1>
::__PATCH__<0>
rpp_version_t;

#define RPP_VERSION rpp_version_t::version
#define RPP_VERSION_MAJOR rpp_version_t::major
#define RPP_VERSION_MINOR rpp_version_t::minor
#define RPP_VERSION_PATCH rpp_version_t::patch

//#define ENABLE_SYSTEM_STATS

#endif // PRECOMPILED_H
