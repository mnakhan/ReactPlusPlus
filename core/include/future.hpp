/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "actor_id.hpp"
#include "generic.hpp"
#include "message.hpp"
using namespace generic_ext;
using namespace io;

namespace rpp
{
  template<typename OutputType>
  class future_t
  {
  private:
    mutable future<OutputType> f;
  public:
    future_t() = default;
    future_t(future<OutputType> f_);
    OutputType block(OutputType defaultValue = OutputType()) const;
    friend ostream& operator<<(ostream& out, future_t<OutputType> const& message)
    {
      return out << "[future]/output type: " << type_index(typeid (OutputType)).name();
    }
  };

  template<typename OutputType>
  inline future_t<OutputType>::future_t(future<OutputType> f_)
    : f(move(f_))
  {
  }

  template<typename OutputType>
  inline OutputType future_t<OutputType>::block(OutputType defaultValue) const
  {
    try
    {
      return f.get();
    }
    catch (future_error const&)
    {
      return defaultValue;
    }
  }

  template<>
  class future_t<void>
  {
  private:
    mutable future<void> f;
  public:
    future_t() = default;
    future_t(future<void> f_);
    void block() const;
    friend ostream& operator<<(ostream& out, future_t<void> const& message)
    {
      return out << "[future]/output type: " << type_index(typeid (void)).name();
    }
  };

  inline future_t<void>::future_t(future<void> f_)
    :f(move(f_))
  {
  }

  inline void future_t<void>::block() const
  {
    try
    {
      return f.get();
    }
    catch (future_error const&)
    {
    }
  }

  template<typename OutputType, typename TagType, typename... InputType>
  class Promise
  {
  private:
    mutable promise<OutputType> p;
  public:
    tuple<InputType...> data;
    Promise() = default;
    void deliver(OutputType result);
    future_t<OutputType> make() const;
    void pack(InputType... data_);
    template<typename... ArgTypes>
    bool unpack(message_t *message, ArgTypes&... args);
    friend ostream& operator<<(ostream& out, Promise<OutputType, TagType, InputType...> const& message)
    {
      return out << "[promise]/input: " << toString(message.data) << ", future type: " << type_index(typeid (OutputType)).name();
    }
  };

  template<typename OutputType, typename TagType, typename... InputType>
  inline void Promise<OutputType, TagType, InputType...>::deliver(OutputType result)
  {
    p.set_value(forward<OutputType>(result));
  }

  template<typename OutputType, typename TagType, typename... InputType>
  inline future_t<OutputType> Promise<OutputType, TagType, InputType...>::make() const
  {
    return future_t<OutputType>(p.get_future());
  }

  template<typename OutputType, typename TagType, typename... InputType>
  inline void Promise<OutputType, TagType, InputType...>::pack(InputType... data_)
  {
    p = promise<OutputType>();
    data = make_tuple(forward<InputType>(data_)...);
  }

  template<typename OutputType, typename TagType, typename... InputType>
  template<typename... ArgTypes>
  inline bool Promise<OutputType, TagType, InputType...>::unpack(message_t *message, ArgTypes&... args)
  {
    using MessageType = Promise<OutputType, TagType, InputType...>;
    if (typed_message_t<MessageType> *typedMessage = dynamic_cast<typed_message_t<MessageType>*> (message))
    {
      p = move(typedMessage->contents.p);
      tie(args...) = typedMessage->contents.data;
      return true;
    }
    return false;
  }

  template<typename AtomType, typename... InputType>
  class Promise<void, AtomType, InputType...>
  {
  private:
    mutable promise<void> p;
  public:
    tuple<InputType...> data;
    Promise() = default;
    void deliver();
    future_t<void> make() const;
    void pack(InputType... data_);
    template<typename... ArgTypes>
    bool unpack(message_t *message, ArgTypes&... args);
    friend ostream& operator<<(ostream& out, Promise<void, AtomType, InputType...> const& message)
    {
      return out << "[promise]/input: " << toString(message.data) << ", future type: " << type_index(typeid (void)).name();
    }
  };

  template<typename AtomType, typename... InputType>
  inline void Promise<void, AtomType, InputType...>::deliver()
  {
    p.set_value();
  }

  template<typename AtomType, typename... InputType>
  inline future_t<void> Promise<void, AtomType, InputType...>::make() const
  {
    return future_t<void>(p.get_future());
  }

  template<typename AtomType, typename ... InputType>
  inline void Promise<void, AtomType, InputType...>::pack(InputType... data_)
  {
    p = promise<void>();
    data = make_tuple(forward<InputType>(data_)...);
  }

  template<typename AtomType, typename... InputType>
  template<typename... ArgTypes>
  inline bool Promise<void, AtomType, InputType...>::unpack(message_t *message, ArgTypes&... args)
  {
    using MessageType = Promise<void, AtomType, InputType...>;
    if (typed_message_t<MessageType> *typedMessage = dynamic_cast<typed_message_t<MessageType>*> (message))
    {
      p = move(typedMessage->contents.p);
      tie(args...) = typedMessage->contents.data;
      return true;
    }
    return false;
  }

  template<typename OutputType>
  ostream& operator<<(ostream& out, future_t<OutputType> const& message);
}
