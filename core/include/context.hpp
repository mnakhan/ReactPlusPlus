/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

#include "mailbox.hpp"
#include "message.hpp"
#include "runnable.hpp"
#include "semaphore.hpp"
#include "system_statistics.hpp"

namespace rpp
{
  class message_t;
  class behavior_t;

  class context_t : public runnable_t
  {
  private:
    atomic<bool> isParking;
    mutable atomic<uint64_t> mailboxSize;
    message_t *yieldedMessage;
    actor_id_t lastSenderID;
    atomic<thread::id> executorID;
    mailbox_t *mailbox;
    deque<pair<message_t*, actor_id_t>> skippedMessages;
    bool isYielding, isResumingTrail;
    mutable atomic<bool> ctxEnabled;
    template<typename mailbox_type_t>
    inline enable_if_t<is_base_of_v<mailbox_t, mailbox_type_t>> createMailbox()
    {
      mailbox = new mailbox_type_t();
    }

    uint64_t dispatch(uint64_t maxNumberOfMessages) override;
    uint64_t drain();
    template<typename BehaviorType>
    void generate_s(BehaviorType *behavior);
    template<typename First, typename... Rest>
    void generate_s(First *first, Rest*... rest);
    bool maySchedule(message_t *message, actor_id_t const& senderID) override;
    bool mayReschedule(uint64_t lastDispatch) override;
    bool migrate(cluster_t<NATIVE> *newResidence) override;
    bool returnToOrigin() override;
    void handle(message_t *message, actor_id_t const& senderID);
    void resume();
    bool returnToSender(message_t *message, actor_id_t const& senderID);
  protected:
    type_erased_receiver_t receiver;
    static printer_t& tscout;
    static random_t& rnd;
    bool isWaitingForReply;
    uint64_t replyToken;
    unordered_map<type_index, behavior_t*> behaviors;
    behavior_t *currentBehavior;
    void awaitReply(uint64_t replyToken_);
    template<typename BehaviorType>
    bool exists(behavior_t **behavior = nullptr) const;
    template<typename First, typename... Rest>
    void generate();
  public:
    context_t();
    context_t(context_t const&) = delete;
    virtual ~context_t();
    template<typename ContextType>
    ContextType& as() const;
    template<typename BehaviorType>
    void adoptBehavior();
    void adoptDefault();
    type_erased_receiver_t& defaultBehavior(bool reset = false);
    bool enable(bool assumingDefault);
    bool isEnabled() const override;
    void initialize();
    template<typename BehaviorType, typename... ArgTypes>
    void initializeBehavior(ArgTypes... args);

    // send operator
    template<typename MessageType>
    context_t& operator|(MessageType contents);

    // send operator for atoms
    template<typename MessageType>
    context_t& operator<<(typed_si_message_t<MessageType> const& contents);

    // link operator
    template<typename GeneratorType>
    enable_if_t<is_same_v<result_of_t<GeneratorType()>, trail_t*>, context_t&>
      operator||(GeneratorType&& generator)
    {
      trail_t *trail = generator();
      if (!trail) return *this;
      trail->link(selfID);
      mailboxSize.fetch_add(1, memory_order_acq_rel);
      mailbox->push(trail, selfID);
      return *this;
    }

    template<typename R>
    bool probe_s(R const&) const;

    template<typename MessageType>
    void resumeTrail(message_t *message);
    template<typename MessageType>
    void resumeTrailWith(message_t *message, MessageType contents);
    void park();
    void quit() const;
    bool scheduleAtSender() const override;
    void yield();

    friend class behavior_t;
    template<scheduler_type_t>
    friend class cluster_t;
  };

  template<typename R>
  inline bool context_t::probe_s(R const&) const
  {
    return false;
  }

  template<>
  inline bool context_t::probe_s<uint64_t>(uint64_t const& max) const
  {
    return mailboxSize.fetch_add(0, memory_order_acq_rel) <= max;
  }

  template<>
  inline bool context_t::probe_s<pair<uint64_t, uint64_t>>(pair<uint64_t, uint64_t> const& limits) const
  {
    uint64_t size = mailboxSize.fetch_add(0, memory_order_acq_rel);
    return size >= limits.first && size <= limits.second;
  }

  template<typename BehaviorType>
  inline void context_t::generate_s(BehaviorType *behavior)
  {
    rpp_assert(!exists<BehaviorType>(), "Same behavior type specified more than once.", tscout);
    behaviors[type_index(typeid (BehaviorType))] = behavior;
    unordered_map<type_index, behavior_t*>::const_iterator it = behaviors.begin();
    currentBehavior = it->second;
  }

  template<typename First, typename... Rest>
  inline void context_t::generate_s(First *first, Rest*...rest)
  {
    rpp_assert(!exists<First>(), "Same behavior type specified more than once.", tscout);
    behaviors[type_index(typeid (First))] = first;
    generate_s(rest...);
  }

  template<typename BehaviorType>
  inline bool context_t::exists(behavior_t **behavior) const
  {
    unordered_map<type_index, behavior_t*>::const_iterator it = behaviors.find(type_index(typeid (BehaviorType)));
    if (it != behaviors.end())
    {
      if (behavior) *behavior = it->second;
      return true;
    }
    return false;
  }

  template<typename First, typename... Rest>
  inline void context_t::generate()
  {
    First *first = new First(*this);
    behaviors[type_index(typeid (First))] = first;
    generate_s<Rest...>(new Rest(*this)...);
  }

  template<typename ContextType>
  inline ContextType& context_t::as() const
  {
    ContextType *typedContext = dynamic_cast<ContextType*> (const_cast<context_t*> (this));
    rpp_assert(typedContext, "Wrong context type.", tscout);
    return *typedContext;
  }

  template<typename BehaviorType>
  inline void context_t::adoptBehavior()
  {
    rpp_assert(exists<BehaviorType>(&currentBehavior), "Cannot adopt a non-existent behavior.", tscout);
  }

  template<typename BehaviorType, typename... ArgTypes>
  inline void context_t::initializeBehavior(ArgTypes... args)
  {
    behavior_t *behavior = nullptr;
    rpp_assert(exists<BehaviorType>(&behavior), "Cannot initialize a non-existent behavior.", tscout);
    applyFunction(&BehaviorType::initialize, (BehaviorType*) behavior, forward<ArgTypes>(args)...);
  }

  template<typename MessageType>
  inline context_t& context_t::operator|(MessageType contents)
  {
    mailboxSize.fetch_add(1, memory_order_acq_rel);
    mailbox->push(new typed_message_t<MessageType>(forward<MessageType>(contents)), selfID);
#ifdef ENABLE_SYSTEM_STATS
    GLOBAL(system_statistics_t).total_messages_sent++;
#endif // ENABLE_SYSTEM_STATS
    return *this;
  }

  template<typename MessageType>
  inline context_t& context_t::operator<<(typed_si_message_t<MessageType> const& contents)
  {
    typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
    mailboxSize.fetch_add(1, memory_order_acq_rel);
    mailbox->push(&message, selfID);
#ifdef ENABLE_SYSTEM_STATS
    GLOBAL(system_statistics_t).total_messages_sent++;
#endif // ENABLE_SYSTEM_STATS
    return *this;
  }

  template<typename MessageType>
  inline void context_t::resumeTrail(message_t *message)
  {
    rpp_assert(message->mayThrow(), "Operation not supported for this type.", GLOBAL(printer_t));
    typed_trail_t<MessageType> *typed_trail = dynamic_cast<typed_trail_t<MessageType>*>(message);
    rpp_assert(typed_trail, "Cannot change type of trail while resuming.", GLOBAL(printer_t));
    rpp_assert(typed_trail->trail_details->raised, "Cannot resume a trail that is not rewinding.", GLOBAL(printer_t));

    actor_id_t source = typed_trail->trail_details->source;
    typed_trail->trail_details->raised = false;
    typed_trail->trail_details->source = actor_id_t();
    typed_trail->trail_details->remaining = 0;
    typed_trail->trail_details->faultIndex = -1;

    if (!returnToSender(message, source)) delete message;
    isResumingTrail = true;
  }

  template<typename MessageType>
  inline void context_t::resumeTrailWith(message_t *message, MessageType contents)
  {
    rpp_assert(message->mayThrow(), "Operation not supported for this type.", GLOBAL(printer_t));
    typed_trail_t<MessageType> *typed_trail = dynamic_cast<typed_trail_t<MessageType>*>(message);
    rpp_assert(typed_trail, "Cannot change type of trail while resuming.", GLOBAL(printer_t));
    rpp_assert(typed_trail->trail_details->raised, "Cannot resume a trail that is not rewinding.", GLOBAL(printer_t));

    actor_id_t source = typed_trail->trail_details->source;
    typed_trail->snapshots.pop_back();
    typed_trail->snapshots.push_back(contents);
    typed_trail->trail_details->raised = false;
    typed_trail->trail_details->source = actor_id_t();
    typed_trail->trail_details->remaining = 0;
    typed_trail->trail_details->faultIndex = -1;

    if (!returnToSender(message, source)) delete message;
    isResumingTrail = true;
  }
}
