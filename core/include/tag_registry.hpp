/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
#include "actor_id.hpp"
#include "locks.hpp"

namespace rpp
{
  typedef pair<actor_id_t, tag_t> id_to_tag;
  typedef pair<tag_t, actor_id_t> tag_to_id;

#define ALL_IDS [](actor_id_t const& id) { return true; }
#define ALL_TAGS [](tag_t const& tag) { return true; }

  template<typename ElementType, typename DomainType>
  struct query_details_t;

  template<typename First, typename... Rest>
  struct from_exp;

  template<typename... DomainTypes>
  struct from_ext
  {
    template<typename ElementType, typename KeyPredicate, typename ValuePredicate>
    static void where(vector<ElementType>& result, KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
      from_exp<DomainTypes...>::template where<ElementType>(ref(result), forward<KeyPredicate>(keyPredicate), forward<ValuePredicate>(valuePredicate));
    }
  };

  template<>
  struct from_ext<>
  {
    template<typename ElementType, typename KeyPredicate, typename ValuePredicate>
    static void where(vector<ElementType>& result, KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
    }
  };

  class tag_registry_t
  {
  private:
    mutable generic_ext::rw_lock_t bolt;
    multimap<actor_id_t, tag_t> id2tag;
    multimap<tag_t, actor_id_t> tag2id;

    template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
    vector<actor_id_t> fetchIDs(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const;

    template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
    vector<tag_t> fetchTags(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const;

    template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
    vector<pair<actor_id_t, tag_t>> fetchBoth1(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const;

    template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
    vector<pair<tag_t, actor_id_t>> fetchBoth2(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const;

    static tag_registry_t& globalInstance();
  public:
    void bind(actor_id_t const& id, tag_t const& tag);
    void unbind(actor_id_t const& id, tag_t const& tag, bool acquire = true);

    template<typename ElementType, typename DomainType>
    friend struct query_details_t;

    template<typename ElementType>
    struct select
    {
    private:
      using ResultType = vector<ElementType>;

      template<typename DomainType>
      struct from_s
      {
        template<typename KeyPredicate, typename ValuePredicate>
        static ResultType where(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
        {
          return query_details_t<ElementType, DomainType>::apply(forward<KeyPredicate>(keyPredicate), forward<ValuePredicate>(valuePredicate));
        }
      };

    public:
      template<typename... DomainTypes>
      struct from
      {
        template<typename KeyPredicate, typename ValuePredicate>
        static ResultType where(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
        {
          ResultType result;
          from_ext<DomainTypes...>::template where<ElementType>(ref(result), forward<KeyPredicate>(keyPredicate), forward<ValuePredicate>(valuePredicate));
          return generic_ext::unique(result);
        }
      };

      template<typename First, typename... Rest>
      friend struct from_exp;
    };

    template<typename QueryResultType>
    static vector<QueryResultType> selectAll()
    {
      return select<QueryResultType>::template from<native, detached, channel>::template where(ALL_IDS, ALL_TAGS);
    }
  };

  template<typename First, typename... Rest>
  struct from_exp
  {
    template<typename ElementType, typename KeyPredicate, typename ValuePredicate>
    static void where(vector<ElementType>& result, KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
      vector<ElementType> domainResult = tag_registry_t::select<ElementType>::template from_s<First>::template where(forward<KeyPredicate>(keyPredicate), forward<ValuePredicate>(valuePredicate));
      append(result, domainResult);

      from_ext<Rest...>::template where<ElementType>(ref(result), forward<KeyPredicate>(keyPredicate), forward<ValuePredicate>(valuePredicate));
    }
  };

  template<typename ElementType, typename DomainType>
  struct query_details_t
  {
    using ResultType = vector<ElementType>;

    template<typename KeyPredicate, typename ValuePredicate>
    static ResultType apply(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
      return ResultType{};
    }
  };

  template<typename DomainType>
  struct query_details_t<actor_id_t, DomainType>
  {
    using ResultType = vector<actor_id_t>;

    template<typename KeyPredicate, typename ValuePredicate>
    static ResultType apply(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
      auto& reg = tag_registry_t::globalInstance();
      return reg.fetchIDs<DomainType>(forward<ValuePredicate>(valuePredicate), forward<KeyPredicate>(keyPredicate));
    }
  };

  template<typename DomainType>
  struct query_details_t<tag_t, DomainType>
  {
    using ResultType = vector<tag_t>;

    template<typename KeyPredicate, typename ValuePredicate>
    static ResultType apply(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
      auto& reg = tag_registry_t::globalInstance();
      return reg.fetchTags<DomainType>(forward<KeyPredicate>(keyPredicate), forward<ValuePredicate>(valuePredicate));
    }
  };

  template<typename DomainType>
  struct query_details_t<pair<actor_id_t, tag_t>, DomainType>
  {
    using ResultType = vector<pair<actor_id_t, tag_t>>;

    template<typename KeyPredicate, typename ValuePredicate>
    static ResultType apply(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
      auto& reg = tag_registry_t::globalInstance();
      return reg.fetchBoth1<DomainType>(forward<KeyPredicate>(keyPredicate), forward<ValuePredicate>(valuePredicate));
    }
  };

  template<typename DomainType>
  struct query_details_t<pair<tag_t, actor_id_t>, DomainType>
  {
    using ResultType = vector<pair<tag_t, actor_id_t>>;

    template<typename KeyPredicate, typename ValuePredicate>
    static ResultType apply(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate)
    {
      auto& reg = tag_registry_t::globalInstance();
      return reg.fetchBoth2<DomainType>(forward<ValuePredicate>(valuePredicate), forward<KeyPredicate>(keyPredicate));
    }
  };

  template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
  inline vector<actor_id_t> tag_registry_t::fetchIDs(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const
  {
    bolt.lock<generic_ext::rw_locking_mode_t::Read>();
    vector<actor_id_t> result;
    auto it = tag2id.cbegin();
    while (it != tag2id.cend())
    {
      if (it->second.is<DomainType>()
        && keyPredicate(it->first)
        && valuePredicate(it->second)) result.push_back(it->second);
      it++;
    }
    bolt.unlock<generic_ext::rw_locking_mode_t::Read>();
    return result;
  }

  template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
  inline vector<tag_t> tag_registry_t::fetchTags(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const
  {
    bolt.lock<generic_ext::rw_locking_mode_t::Read>();
    vector<tag_t> result;
    auto it = id2tag.cbegin();
    while (it != id2tag.cend())
    {
      if (it->first.is<DomainType>()
        && keyPredicate(it->first)
        && valuePredicate(it->second)) result.push_back(it->second);
      it++;
    }
    bolt.unlock<generic_ext::rw_locking_mode_t::Read>();
    return result;
  }

  template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
  inline vector<pair<actor_id_t, tag_t>> tag_registry_t::fetchBoth1(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const
  {
    bolt.lock<generic_ext::rw_locking_mode_t::Read>();
    vector<pair<actor_id_t, tag_t>> result;
    auto it = id2tag.cbegin();
    while (it != id2tag.cend())
    {
      if (it->first.is<DomainType>()
        && keyPredicate(it->first)
        && valuePredicate(it->second)) result.push_back(pair<actor_id_t, tag_t>{it->first, it->second});
      it++;
    }
    bolt.unlock<generic_ext::rw_locking_mode_t::Read>();
    return result;
  }

  template<typename DomainType, typename KeyPredicate, typename ValuePredicate>
  inline vector<pair<tag_t, actor_id_t>> tag_registry_t::fetchBoth2(KeyPredicate&& keyPredicate, ValuePredicate&& valuePredicate) const
  {
    bolt.lock<generic_ext::rw_locking_mode_t::Read>();
    vector<pair<tag_t, actor_id_t>> result;
    auto it = tag2id.cbegin();
    while (it != tag2id.cend())
    {
      if (it->second.is<DomainType>()
        && keyPredicate(it->first)
        && valuePredicate(it->second)) result.push_back(pair<tag_t, actor_id_t>{it->first, it->second});
      it++;
    }
    bolt.unlock<generic_ext::rw_locking_mode_t::Read>();
    return result;
  }

  ostream& operator<<(ostream&, rpp::id_to_tag const&);
  ostream& operator<<(ostream&, rpp::tag_to_id const&);
}
