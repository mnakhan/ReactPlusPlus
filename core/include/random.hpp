/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace generic_ext
{
  class random_t
  {
  private:
    mt19937 engine;
  public:
    random_t();
    template<typename T = int>
    T nextInt();
    template<typename T = int>
    T nextInt(T min, T max);
    template<typename T = float>
    T nextReal();
    template<typename T = float>
    T nextReal(T min, T max);
    string nextString(size_t length);
  };

  template<typename T>
  inline T random_t::nextInt()
  {
    static_assert(is_integral<T>::value);
    uniform_int_distribution<T> distribution;
    return distribution(engine);
  }

  template<typename T>
  inline T random_t::nextInt(T min, T max)
  {
    static_assert(is_integral<T>::value);
    uniform_int_distribution<T> distribution(min, max);
    return distribution(engine);
  }

  template<typename T>
  inline T random_t::nextReal()
  {
    static_assert(is_floating_point<T>::value);
    uniform_real_distribution<T> distribution;
    return distribution(engine);
  }

  template<typename T>
  inline T random_t::nextReal(T min, T max)
  {
    static_assert(is_floating_point<T>::value);
    uniform_real_distribution<T> distribution(min, max);
    return distribution(engine);
  }
}
