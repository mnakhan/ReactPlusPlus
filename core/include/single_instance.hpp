/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "printer.hpp"

namespace generic_ext
{
  class single_instance_t
  {
  public:
    template<typename T>
    static T& create();
    template<typename T>
    static T& createAsLocal();
  };

  template<typename T>
  inline T& single_instance_t::create()
  {
    static T instance;
    return instance;
  }

  template<typename T>
  inline T& single_instance_t::createAsLocal()
  {
    thread_local static T instance;
    return instance;
  }

  template<>
  inline printer_t& single_instance_t::create()
  {
    static printer_t tscout(cout);
    return tscout;
  }
}

#define GLOBAL(TypeName) generic_ext::single_instance_t::create<TypeName>()

#define THR_LOCAL(TypeName) generic_ext::single_instance_t::createAsLocal<TypeName>()
