/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "rws_worker.hpp"
#include "generic.hpp"
using namespace generic_ext;
using namespace io;

namespace rpp
{
  class policy_t
  {
  private:
    template<typename First, typename... Rest>
    static policy_t& disable_s();
    template<typename First, typename... Rest>
    static policy_t& enable_s();
    static policy_t& placeholder();
  public:
    template<typename... PolicyTypes>
    static policy_t& disableAll();
    template<typename PolicyType>
    static policy_t& disable();
    template<typename... PolicyTypes>
    static policy_t& enableAll();
    template<typename PolicyType>
    static policy_t& enable();
    template<typename PolicyType, typename... ArgTypes>
    static policy_t& enableWith(ArgTypes... args);
    template<typename PolicyType>
    static policy_t& reset();
  };

  template<typename First, typename... Rest>
  inline policy_t& policy_t::disable_s()
  {
    disable<First>();
    return disableAll<Rest...>();
  }

  template<typename... PolicyTypes>
  inline policy_t& policy_t::disableAll()
  {
    return disable_s<PolicyTypes...>();
  }

  template<>
  inline policy_t& policy_t::disableAll<>()
  {
    return placeholder();
  }

  template<typename PolicyType>
  inline policy_t& policy_t::disable()
  {
    rpp_assert(false, "This policy cannot be disabled. Maybe try to reset it?", GLOBAL(printer_t));
    return placeholder();
  }

  template<typename First, typename... Rest>
  inline policy_t& policy_t::enable_s()
  {
    enable<First>();
    return enableAll<Rest...>();
  }

  template<typename... PolicyTypes>
  inline policy_t& policy_t::enableAll()
  {
    return enable_s<PolicyTypes...>();
  }

  template<>
  inline policy_t& policy_t::enableAll<>()
  {
    return placeholder();
  }

  template<typename PolicyType>
  inline policy_t& policy_t::enable()
  {
    rpp_assert(false, "Attempted to enable a non-existent policy.", GLOBAL(printer_t));
    return placeholder();
  }

  template<typename PolicyType, typename... ArgTypes>
  inline policy_t& policy_t::enableWith(ArgTypes... args)
  {
    rpp_assert(false, "Attempted to enable a non-existent policy with parameters.", GLOBAL(printer_t));
    return placeholder();
  }

  template<typename PolicyType>
  inline policy_t& policy_t::reset()
  {
    rpp_assert(false, "Attempted to reset a non-existent policy.", GLOBAL(printer_t));
    return placeholder();
  }

  /*********************************************************************************************************************/

  struct schedule_at_sender
  {
    struct probability
    {
      static double& value();
    };

    static bool& isEnabled();
  };

  struct dispatch_size
  {
    struct single {};
  };

  struct work_stealing {};

  // policy #1 (enable, disable, reset)

  template<>
  inline policy_t& policy_t::enable<schedule_at_sender>()
  {
    schedule_at_sender::isEnabled() = true;
    schedule_at_sender::probability::value() = 1.0;
    return placeholder();
  }

  template<>
  inline policy_t& policy_t::disable<schedule_at_sender>()
  {
    schedule_at_sender::isEnabled() = false;
    schedule_at_sender::probability::value() = 0.0;
    return placeholder();
  }

  template<>
  inline policy_t& policy_t::reset<schedule_at_sender>()
  {
    return disable<schedule_at_sender>();
  }

  // policy #2 (enable, reset)
  template<>
  inline policy_t& policy_t::enableWith<schedule_at_sender::probability>(double value_)
  {
    rpp_assert(0.0 < value_ && value_ <= 1.0, "Probability must be in the range (0.0, 1.0] to enable this policy.", GLOBAL(printer_t));
    schedule_at_sender::isEnabled() = true;
    schedule_at_sender::probability::value() = value_;
    return placeholder();
  }

  template<>
  inline policy_t& policy_t::reset<schedule_at_sender::probability>()
  {
    return reset<schedule_at_sender>();
  }

  // policy #3 (enable, reset)
  template<>
  inline policy_t& policy_t::enableWith<dispatch_size>(uint64_t maxMessagesPerDispatch)
  {
    rpp_assert(maxMessagesPerDispatch != 0, "Dispatch size must be non-zero.", GLOBAL(printer_t));
    rws_worker_t::setDispatchPolicy(maxMessagesPerDispatch);
    return placeholder();
  }

  template<>
  inline policy_t& policy_t::enableWith<dispatch_size>(int maxMessagesPerDispatch)
  {
    rpp_assert(maxMessagesPerDispatch > 0, "Dispatch size must be positive.", GLOBAL(printer_t));
    return policy_t::enableWith<dispatch_size>((uint64_t)maxMessagesPerDispatch);
  }

  template<>
  inline policy_t& policy_t::reset<dispatch_size>()
  {
    rws_worker_t::resetDispatchPolicy();
    return placeholder();
  }

  // policy #4 (enable, disable, reset)
  template<>
  inline policy_t& policy_t::enable<dispatch_size::single>()
  {
    return enableWith<dispatch_size>(1);
  }

  template<>
  inline policy_t& policy_t::disable<dispatch_size::single>()
  {
    return reset<dispatch_size>();
  }

  template<>
  inline policy_t& policy_t::reset<dispatch_size::single>()
  {
    return reset<dispatch_size>();
  }

  // policy #5 (enable, disable, reset)
  template<>
  inline policy_t& policy_t::enable<work_stealing>()
  {
    rws_worker_t::setStealFrequency(steal_frequency_t::NORMAL);
    rws_worker_t::setFrequencyMultiplier(1.0);
    return placeholder();
  }

  template<>
  inline policy_t& policy_t::enableWith<work_stealing>(steal_frequency_t frequency, double freqMultiplier)
  {
    auto& tscout = GLOBAL(printer_t);
    rpp_assert(frequency != steal_frequency_t::DISABLED, "Cannot use this frequency value with an enabler.", tscout);
    rpp_assert(freqMultiplier >= 1.0, "Multiplier must be greater than or equal to 1.", tscout);

    rws_worker_t::setStealFrequency(frequency);
    rws_worker_t::setFrequencyMultiplier(freqMultiplier);
    return placeholder();
  }

  template<>
  inline policy_t& policy_t::enableWith<work_stealing>(steal_frequency_t frequency)
  {
    return policy_t::enableWith<work_stealing>(frequency, rws_worker_t::getFrequencyMultiplier());
  }

  template<>
  inline policy_t& policy_t::enableWith<work_stealing>(double freqMultiplier)
  {
    return policy_t::enableWith<work_stealing>(rws_worker_t::getStealFrequency(), freqMultiplier);
  }

  template<>
  inline policy_t& policy_t::disable<work_stealing>()
  {
    rws_worker_t::setStealFrequency(steal_frequency_t::DISABLED);
    rws_worker_t::setFrequencyMultiplier(1.0);
    return placeholder();
  }

  template<>
  inline policy_t& policy_t::reset<work_stealing>()
  {
    return enable<work_stealing>();
  }
}
