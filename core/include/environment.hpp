/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "platform.hpp"
#include "generic.hpp"

namespace environment
{
  extern numa_node_id_t numaNodeCount;
  extern cluster_id_t clusterCount;
  extern map<numa_node_id_t, processor_id_t> cpuCountPerNode;
  extern map<cluster_id_t, worker_id_t> workerCountPerCluster;
  extern string configPath;

  extern string ipAddress;
  extern ushort portNumber;

  extern bool pinSignaler;
  extern bool pinSession;
  extern local_id_t maxStandbyActorsPerCluster;
  extern uint32_t L0MaxEvents;
  extern uint32_t L1MaxEvents;
  extern uint32_t backlog;

  extern bool enableDirectSignaling;
  extern bool enablePrivateDescriptorTable;
  extern bool enableRestlessSpin;
  extern uint32_t restlessSpinCount;
  extern bool enableImmediateContext;

  extern bool enableDistributedPolling;
  extern bool enableStatsReporter;


  void reload(bool printConfig = false);
  template<bool ResetPlatform>
  inline void reset(bool printConfig = false)
  {
    if (ResetPlatform)
    {
      numaNodeCount = 0;
      cpuCountPerNode.clear();

      // by default, create a single cluster with all available cores from all nodes
      clusterCount = 1;
      workerCountPerCluster.clear();
      workerCountPerCluster[0] = MAX_HW_THREADS;
    }

    ipAddress = LOCALHOST;
    portNumber = LOCALPORT;

    pinSignaler = false;
    pinSession = false;
    maxStandbyActorsPerCluster = (local_id_t)25000;
    L0MaxEvents = 16;
    L1MaxEvents = 32;
    backlog = 512;

    enableDirectSignaling = true;
    enablePrivateDescriptorTable = true;
    enableRestlessSpin = true;
    restlessSpinCount = (uint32_t)16;
    enableImmediateContext = true;
    enableDistributedPolling = true;
    enableStatsReporter = true;

    if (printConfig)
    {
      rpp::platform_t& platform = GLOBAL(rpp::platform_t);
      io::scout << "loaded default configurations";

      string platformDesc = platform.toString();
      if (!platformDesc.empty()) io::scout << '\n' << platformDesc;
      io::scout << endl;
    }
  }
};
