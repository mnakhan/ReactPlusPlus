/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "single_instance.hpp"
#include "random.hpp"
#include "synchronized_stream.hpp"

#define ARRAY_SIZE(a) ((uint32_t)sizeof a / sizeof a[0])

#define BLOCK() { spin([] { return false; }); }

#define MIN(a, b) (((a) <= (b)) ? (a) : (b))

#define MAX(a, b) (((a) >= (b)) ? (a) : (b))

#define PRINT_ARRAY(a, n) { \
  cout << "["; \
  for (decltype(n) i = 0; i < n; i++) { \
    cout << a[i]; \
    if (i < n - 1) cout << ", "; } \
  cout << "]" << endl; }

#define PRINT_ARRAY2(a, n, str) { \
  str << "["; \
  for (decltype(n) i = 0; i < n; i++) { \
    str << a[i]; \
    if (i < n - 1) str << ", "; } \
  str << "]"; }

#define RM_LF() { cin.clear(); while(cin.get() != '\n'); }

#define RND_SEED() { srand(static_cast<uint32_t>(time(nullptr))); }

#ifdef WINDOWS
#define __BASENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#else
#define __BASENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif // WINDOWS

#define rpp_assert(expr, msg, tscout) \
  do { \
    if(expr) break; \
    ostringstream out; \
    out << "FATAL: " << msg << endl << "Location: (" << #expr << ") is false at " << __BASENAME__ << ':' << __LINE__ << endl; \
    tscout.println(out.str()); \
    abort(); \
  } while (false)

#define MAX_HW_THREADS thread::hardware_concurrency()

#define UNUSED_PARAM(param) ((void)param)

namespace generic_ext
{
  template<typename ElementType>
  void append(vector<ElementType>& dst, vector<ElementType> const& src);

  template<typename FunctionType, typename Tuple, size_t... Indices>
  inline constexpr decltype(auto) apply_s(FunctionType&& function, Tuple&& args, index_sequence<Indices...>)
  {
    return invoke(forward<FunctionType>(function), get<Indices>(forward<Tuple>(args))...);
  }

  template<typename FunctionType, typename Tuple>
  inline constexpr decltype(auto) apply_t(FunctionType&& function, Tuple&& args)
  {
    auto seq = make_index_sequence<tuple_size_v<remove_reference_t < Tuple>>>
    {
    };
    return apply_s(function, args, seq);
  }

  template<typename FunctionType, typename... ArgTypes>
  inline constexpr decltype(auto) applyFunction(FunctionType&& function, ArgTypes... args)
  {
    return apply(function, make_tuple(args...));
  }

  template<typename T>
  inline void arrayCopy(const T *src, T *dst, int size)
  {
    if (is_pod<T>::value) memcpy(dst, src, size * sizeof(T));
    else
      for (int i = 0; i < size; i++) dst[i] = src[i];
  }

  template<typename T>
  inline T clamp(T const& min, T const& max, T const& value)
  {
    if (value < min) return min;
    if (value > max) return max;
    return value;
  }

  int compareIgnoreCase(string const& s1, string const& s2);

  template<size_t Size, typename ElementType>
  ElementType draw_h(ElementType *elements, double *cumulative_dist, double sum)
  {
    assert(fabs(sum - 1.0) < DBL_EPSILON);
    auto& rnd = THR_LOCAL(random_t);
    double x = rnd.nextReal(0.0, 1.0);
    for (size_t i = 0; i < Size; i++)
      if (x < cumulative_dist[i]) return elements[i];

    assert(false);    // should not be here
    return ElementType();
  }

  template<size_t Index = 0, typename ElementType, typename First, typename... Rest>
  enable_if_t<
    is_floating_point_v<typename First::second_type>
    , ElementType>
    draw_h(ElementType *elements, double *cumulative_dist, double sum, First first, Rest... rest)
  {
    elements[Index] = get<0>(first);
    sum += get<1>(first);
    cumulative_dist[Index] = sum;
    return draw_h<Index + 1>(elements, cumulative_dist, sum, rest...);
  }

  template<typename ElementType, typename... ArgTypes>
  ElementType draw(ArgTypes... args)
  {
    constexpr size_t nElements = sizeof ... (args);
    ElementType elements[nElements];
    double cumulative_dist[nElements];
    return draw_h(elements, cumulative_dist, 0.0, args...);
  }

  void joinAll();

  template<typename First, typename... Rest>
  inline void joinAll(First& first, Rest&... rest)
  {
    if (first.joinable()) first.join();
    joinAll(rest...);
  }

  void lockAll();

  template<typename First, typename... Rest>
  inline void lockAll(First& first, Rest&... rest)
  {
    while (!first.try_lock());
    lockAll(rest...);
  }

  inline constexpr ullong operator "" _K(ullong units)
  {
    return units * kilo().num;
  }

  inline constexpr ullong operator "" _M(ullong units)
  {
    return units * mega().num;
  }

  inline constexpr ullong operator "" _G(ullong units)
  {
    return units * giga().num;
  }

  inline constexpr ullong operator "" _T(ullong units)
  {
    return units * tera().num;
  }

  inline constexpr ullong operator "" _P(ullong units)
  {
    return units * peta().num;
  }

  inline constexpr ullong operator "" _E(ullong units)
  {
    return units * exa().num;
  }


  template<typename, typename = void_t<>>
  struct isMapOrSet : false_type { };

  template<typename ContainerType>
  struct isMapOrSet<ContainerType,
    void_t<typename ContainerType::key_type, typename ContainerType::value_type>> : true_type { };

  // map<K, V>, unordered_map<K, V>, set<T> and unordered_set<T>
  template<typename ContainerType,
    enable_if_t<isMapOrSet<ContainerType>::value, int> = 0>
    inline ostream& outputContainer(ostream& out, ContainerType const& container)
  {
    out << '[';
    size_t n = container.size();
    for_each(container.cbegin(), container.cend(), [&out, &n](auto& kvp)
    {
      out << kvp;
      if (n > 1) out << ", ";
      n--;
    });
    return out << ']';
  }

  // forward_list<T>
  template<typename ContainerType,
    enable_if_t<is_same_v<ContainerType, forward_list<typename ContainerType::value_type>>, int> = 0>
    inline ostream& outputContainer(ostream& out, ContainerType const& container)
  {
    out << '[';
    auto it = container.cbegin();
    while (it != container.cend())
    {
      out << *it;
      it++;
      if (it != container.cend()) out << ", ";
    }
    return out << ']';
  }

  // vector<T> and list<T>
  template<typename ContainerType,
    typename = enable_if_t<!isMapOrSet<ContainerType>::value &&
    !is_same_v<ContainerType, forward_list<typename ContainerType::value_type>>>>
    inline ostream& outputContainer(ostream& out, ContainerType const& container)
  {
    out << '[';
    if (!container.empty())
    {
      auto it = end(container);
      it--;
      copy(begin(container), it, ostream_iterator<typename ContainerType::value_type>(out, ", "));
      out << *it;
    }
    return out << ']';
  }

  template<typename FirstType, typename SecondType>
  inline ostream& operator<<(ostream& out, pair<FirstType, SecondType> const& p)
  {
    out << '(' << p.first << ", " << p.second << ')';
    return out;
  }

  template<typename ContainerType, typename = typename ContainerType::value_type>
  inline ostream& operator<<(ostream& out, ContainerType const& container)
  {
    outputContainer(out, container);
    return out;
  }


  template<typename T>
  inline list<T> quickSort(list<T> lst)
  {
    if (lst.size() < 2) return lst;

    random_t& rnd = THR_LOCAL(random_t);
    size_t pivotIndex = rnd.nextInt<size_t>(0, lst.size() - 1);
    typename list<T>::iterator it = lst.begin();
    advance(it, pivotIndex);
    assert(it != lst.end());
    T pivotValue = *it;
    lst.erase(it);
    typename list<T>::iterator pivot = partition(lst.begin(), lst.end(), [pivotValue](T const& x)
    {
      return x <= pivotValue;
    });

    list<T> firstPartition, secondPartition;
    firstPartition.splice(firstPartition.end(), lst, lst.begin(), pivot);
    secondPartition.splice(secondPartition.end(), lst, lst.begin(), lst.end());

    firstPartition = quickSort(move(firstPartition));
    secondPartition = quickSort(move(secondPartition));

    firstPartition.push_back(pivotValue);
    firstPartition.splice(firstPartition.end(), secondPartition, secondPartition.begin(), secondPartition.end());
    return firstPartition;
  }

  template<typename T, typename Predicate>
  inline T& randomDraw(random_t& rnd, vector<T>& lst, Predicate&& predicate)
  {
    size_t max = lst.size();
    if (max == 1)
    {
      auto& item = lst[0];
      if (predicate(item)) return item;
      throw logic_error("Predicate is false for the only item in the list.");
    }

    while (true)
    {
      size_t index = rnd.nextInt<size_t>(0, max - 1);
      auto& item = lst[index];
      if (predicate(item)) return item;
    }
  }

  template<typename FunctionType>
  inline void runTest(FunctionType&& test, uint32_t nIterations = UINT32_MAX)
  {
    for (uint32_t i = 0; i < nIterations;)
    {
      test();
      if (nIterations < UINT32_MAX) i++;
    }
  }

  template<typename PredicateType>
  inline void spin(PredicateType&& predicate)
  {
    while (!predicate()) sleep_for(seconds(1));
  }

  ostream& operator<<(ostream& out, string const& s);

  template<size_t I = 0, typename... ArgTypes>
  inline typename enable_if<I == sizeof...(ArgTypes), ostringstream&>::type toString_s(ostringstream& ss, tuple<ArgTypes...> const& t)
  {
    return ss;
  }

  template<size_t I = 0, typename... ArgTypes>
  inline typename enable_if < I < sizeof...(ArgTypes), ostringstream&>::type toString_s(ostringstream& ss, tuple<ArgTypes...> const& t)
  {
    ss << get<I>(t);
    if (I + 1 < sizeof...(ArgTypes)) ss << ", ";
    toString_s < I + 1, ArgTypes...>(ss, t);
    return ss;
  }

  template<typename... ArgTypes>
  inline string toString(tuple<ArgTypes...> const& t)
  {
    ostringstream ss;
    ss << '{';
    toString_s(ss, t);
    ss << '}';
    return ss.str();
  }

  template<typename T>
  inline string toString(T const& x)
  {
    ostringstream ss;
    ss << x;
    return ss.str();
  }

  template<typename... ArgTypes>
  inline ostream& operator<<(ostream& out, tuple<ArgTypes...> const& t)
  {
    return out << toString(t);
  }

  template<typename T>
  inline bool parse(string const& s, T& result)
  {
    istringstream ss(s);
    ss >> result;
    return !ss.fail() && ss.eof();
  }

  string padLeft(string const& s, int width, char c = ' ');
  string padRight(string const& s, int width, char c = ' ');

  template<typename T>
  void parallel_fill_h(T *data, uint32_t low, uint32_t high, uint32_t max);

  template<typename T>
  T *parallel_fill(uint32_t size, uint32_t maxThreads);

  vector<string> split(string const& s, string const& delims = ",");

  template<typename T>
  inline string suffix(T input)
  {
    static_assert(is_arithmetic_v<T>, "Input type must be arithematic.");

    double units = static_cast<double>(input);
    pair<uint64_t, char> denominators[] =
    {
      make_pair(exa().num, 'E'),
      make_pair(peta().num, 'P'),
      make_pair(tera().num, 'T'),
      make_pair(giga().num, 'G'),
      make_pair(mega().num, 'M'),
      make_pair(kilo().num, 'K')
    };

    double quotient;
    uint64_t i;
    for (i = 0; i < ARRAY_SIZE(denominators); i++)
    {
      quotient = units / (double)denominators[i].first;
      if (quotient >= 1.0) break;
    }
    if (i < ARRAY_SIZE(denominators)) return toString(quotient) + toString(denominators[i].second);
    return toString(units);
  }

  string suffix2(uint64_t sizeInBytes);

  template<typename T>
  inline void swap(T& first, T& second)
  {
    T tmp = move(first);
    first = move(second);
    second = move(tmp);
  }

  string toLowercase(string const& s);
  string toUppercase(string const& s);
  string trim(string const& s, string const& charsToTrim = " ");
  void unlockAll();

  template<typename First, typename... Rest>
  inline void unlockAll(First& first, Rest&... rest)
  {
    first.unlock();
    unlockAll(rest...);
  }

  void wait();


  template<bool IsAscending = true, typename T>
  bool isSorted(const T *data, uint32_t size)
  {
    if (size == 0) return true;
    for (uint32_t i = 0; i < size - 1; i++)
      if ((IsAscending && data[i] > data[i + 1])
        || (!IsAscending && data[i] < data[i + 1])) return false;
    return true;
  }

  template<typename T, bool LeftInclusive = true, bool RightInclusive = true>
  struct range_t
  {
  private:
    T leftBound, rightBound;
  public:
    range_t(T const& leftBound_, T const& rightBound_);
    bool within(T const& value) const;
    string toString() const;
  };

  template<bool Exit = true, bool SuppressWarning = false, typename T, typename RangeType>
  inline bool validate(const char *paramName, T const& value, RangeType const& range)
  {
    if (range.within(value)) return true;
    if (SuppressWarning) io::scout << "Input value " << value << " for parameter \"" << paramName << "\" is out of range " << range.toString() << "." << endl;
    if (Exit) exit(1);     // don't call exit when system is being initialized --> destroys initialization lock without releasing it
    return false;
  }

  template<typename ElementType>
  vector<ElementType> unique(vector<ElementType> const& input);
}

template<typename ElementType>
inline void generic_ext::append(vector<ElementType>& dst, vector<ElementType> const& src)
{
  if (&dst == &src)
  {
    vector<ElementType> copy = dst;
    append(dst, copy);
    return;
  }
  auto it = src.cbegin();
  while (it != src.cend())
  {
    dst.push_back(*it);
    it++;
  }
}

template<typename T, bool LeftInclusive, bool RightInclusive>
inline generic_ext::range_t<T, LeftInclusive, RightInclusive>::range_t(T const& leftBound_, T const& rightBound_)
  :leftBound(leftBound_), rightBound(rightBound_)
{
  rpp_assert(leftBound <= rightBound, "Left bound must be less than or equal to right bound.", GLOBAL(printer_t));
}

template<typename T, bool LeftInclusive, bool RightInclusive>
inline bool generic_ext::range_t<T, LeftInclusive, RightInclusive>::within(T const & value) const
{
  if (LeftInclusive && RightInclusive) return leftBound <= value && value <= rightBound;
  if (!LeftInclusive && RightInclusive) return leftBound < value && value <= rightBound;
  if (LeftInclusive && !RightInclusive) return leftBound <= value && value < rightBound;
  if (!LeftInclusive && !RightInclusive) return leftBound < value && value < rightBound;

  assert(false);  // unreachable
  return false;
}

template<typename T, bool LeftInclusive, bool RightInclusive>
inline string generic_ext::range_t<T, LeftInclusive, RightInclusive>::toString() const
{
  ostringstream result;
  result << (LeftInclusive ? "[" : "(")
    << leftBound << ", " << rightBound
    << (RightInclusive ? "]" : ")");
  return result.str();
}

template<typename ElementType>
inline vector<ElementType> generic_ext::unique(vector<ElementType> const& input)
{
  vector<ElementType> input_copy = input;
  std::sort(input_copy.begin(), input_copy.end());

  vector<ElementType> result;
  auto it = input_copy.cbegin();
  bool isValid = false;
  ElementType previousValue;
  while (it != input_copy.cend())
  {
    ElementType currentValue = *it;
    if (isValid)
    {
      if (currentValue != previousValue)
      {
        result.push_back(*it);
        previousValue = currentValue;
      }
    }
    else
    {
      result.push_back(*it);
      previousValue = currentValue;
      isValid = true;
    }
    it++;
  }
  return result;
}

template<typename T>
inline void generic_ext::parallel_fill_h(T *data, uint32_t low, uint32_t high, uint32_t max)
{
  for (uint32_t i = low; i <= high; i++) data[i] = max - i;
}

template<typename T>
inline T *generic_ext::parallel_fill(uint32_t size, uint32_t maxThreads)
{
  T *data = new T[size];
  uint32_t nThreads = size >= maxThreads ? maxThreads : 1;

  uint32_t partitionSize = size / nThreads;
  uint32_t lastPartitionSize;
  if (nThreads > 1) lastPartitionSize = size - (nThreads - 1) * partitionSize;
  else lastPartitionSize = partitionSize;

  vector<thread> workers(nThreads);
  for (uint32_t i = 0; i < nThreads; i++)
  {
    uint32_t low = i * partitionSize;
    uint32_t high;
    if (i < nThreads - 1) high = (i + 1) * partitionSize - 1;
    else high = low + lastPartitionSize - 1;
    workers[i] = thread(generic_ext::parallel_fill_h<T>, data, low, high, size - 1);
  }
  for (uint32_t i = 0; i < nThreads; i++) workers[i].join();

  return data;
}
