/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "configuration.hpp"

namespace rpp
{
  class platform_t
  {
  private:
    double **distanceMatrix;
    worker_id_t *cpuAssignments;
    processor_id_t nAllocatedProcessors, nAvailableProcessors;
    numa_node_id_t nodeCount;
    cluster_id_t clusterCount;
    bool isValid, isNUMAAware;
    map<numa_node_id_t, vector<set<numa_node_id_t>>> topology;
    map<numa_node_id_t, set<processor_id_t>> numaCPUSets;
    map<cluster_id_t, set<processor_id_t>> clusterCPUSets;

    static void appendCPUSet(set<processor_id_t> const& cpuSet, vector<processor_id_t>& victims);
    double findNeighborsOf(numa_node_id_t nodeID, double currentRadius, set<numa_node_id_t>& neighborNodes);
    static set<processor_id_t> intersect(set<processor_id_t> const& set1, set<processor_id_t> const& set2);
    static bool parseCoreConfig(string const& input, set<processor_id_t>& cpuSet);
    void validateCPUAllocation();
  public:
    platform_t();
    ~platform_t();
    void clear();
    bool hasValidConfiguration() const;
    bool hasNUMAAwareness() const;
    bool increaseSearchRadius(numa_node_id_t nodeID, uint32_t level, vector<processor_id_t>& neighborCPUs) const;
    bool increaseSearchRadius(numa_node_id_t nodeID, cluster_id_t clusterID, uint32_t& level, vector<processor_id_t>& neighborCPUs) const;
    static bool pinSelfToCPU(processor_id_t cpuID);
    static bool pinThreadToCPU(thread& t, processor_id_t cpuID);
    void reload(configuration_t& config);

    string toString() const;

    numa_node_id_t getNUMANodeCount() const;
    processor_id_t getProcessorCount(numa_node_id_t nodeID) const;
    cluster_id_t getClusterCount() const;
    worker_id_t getWorkerCount(cluster_id_t clusterID) const;
    processor_id_t mapWorkerToProcessor(cluster_id_t clusterID, worker_id_t workerID) const;
    numa_node_id_t mapProcessorToNode(processor_id_t cpuID) const;
    worker_id_t mapProcessorToWorker(processor_id_t cpuID) const;
  };

  namespace ThisThread
  {
    extern map<thread::id, tuple<numa_node_id_t, processor_id_t, cluster_id_t, worker_id_t>> threadMap;
    extern mutex mx_threadMap;
    extern platform_t *currentPlatform;

    void registerThread(numa_node_id_t numaNodeID, processor_id_t cpuID, cluster_id_t clusterID, worker_id_t workerID);
    void clearMap();
    numa_node_id_t getNUMANodeID();
    processor_id_t getProcessorID();
    cluster_id_t getClusterID();
    worker_id_t getWorkerID();
    string getPlatformString(bool globalReport = false);


    // debugging utilities
    extern bool enableBlocking;
    void blockAllExcept(cluster_id_t clusterID, worker_id_t workerID);
  };
}
