/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace generic_ext
{
  class semaphore_t
  {
  private:
    int initialValue;
    sem_t s;
  public:
    semaphore_t(int initialValue_ = 1);
    semaphore_t(semaphore_t const&) = delete;
    semaphore_t& operator=(semaphore_t const&) = delete;
    ~semaphore_t();
    void P();
    void reset(int initialValue_);
    void V();
  };
}
