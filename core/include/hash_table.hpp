/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "locks.hpp"
#include "generic.hpp"

namespace generic_ext
{
#ifndef ENABLE_HTABLE_RESIZING
  template<typename KeyType, typename ValueType>
  class hash_table_t
  {
  public:
    hash_table_t(uint32_t n_buckets_ = 8_K);
    hash_table_t(hash_table_t const& table) = delete;
    hash_table_t(hash_table_t&& table) = delete;
    ~hash_table_t();
    void clear();
    bool get(KeyType const& key, ValueType& value) const;
    void insert(KeyType const& key, ValueType const& value);

    friend ostream& operator<<(ostream& out, hash_table_t<KeyType, ValueType> const& table)
    {
      for (uint32_t i = 0; i < table.n_buckets; i++)
      {
        rw_lock_t& bolt = table.rw_buckets[i];
        bolt.lock<rw_locking_mode_t::Read>();
        out << table.buckets[i] << endl;
        bolt.unlock<rw_locking_mode_t::Read>();
      }
      return out;
    }
    void remove(KeyType const& key);

  private:
    uint32_t n_buckets;
    vector<unordered_map<KeyType, ValueType>> buckets;
    mutable rw_lock_t *rw_buckets;
    mutable hash<KeyType> hashGenerator;
    friend class system_t;
  };

  template<typename KeyType, typename ValueType>
  hash_table_t<KeyType, ValueType>::hash_table_t(uint32_t n_buckets_)
    : n_buckets(n_buckets_)
  {
    buckets.resize(n_buckets);
    rw_buckets = new rw_lock_t[n_buckets];
  }

  template<typename KeyType, typename ValueType>
  hash_table_t<KeyType, ValueType>::~hash_table_t()
  {
    delete[] rw_buckets;
  }

  template<typename KeyType, typename ValueType>
  void hash_table_t<KeyType, ValueType>::clear()
  {
    for (uint32_t i = 0; i < n_buckets; i++)
    {
      rw_lock_t& bolt = rw_buckets[i];
      bolt.lock<rw_locking_mode_t::Write>();
      buckets[i].clear();
      bolt.unlock<rw_locking_mode_t::Write>();
    }
  }

  template<typename KeyType, typename ValueType>
  bool hash_table_t<KeyType, ValueType>::get(KeyType const& key, ValueType& value) const
  {
    uint32_t bucketIndex = hashGenerator(key) % n_buckets;
    rw_lock_t& bolt = rw_buckets[bucketIndex];
    bolt.lock<rw_locking_mode_t::Read>();
    typename unordered_map<KeyType, ValueType>::const_iterator it = buckets[bucketIndex].find(key);
    if (it == buckets[bucketIndex].cend())
    {
      bolt.unlock<rw_locking_mode_t::Read>();
      return false;
    }
    value = it->second;
    bolt.unlock<rw_locking_mode_t::Read>();
    return true;
  }

  template<typename KeyType, typename ValueType>
  void hash_table_t<KeyType, ValueType>::insert(KeyType const& key, ValueType const& value)
  {
    uint32_t bucketIndex = hashGenerator(key) % n_buckets;
    rw_lock_t& bolt = rw_buckets[bucketIndex];
    bolt.lock<rw_locking_mode_t::Write>();
    if (buckets[bucketIndex].find(key) == buckets[bucketIndex].cend()) buckets[bucketIndex][key] = value;
    bolt.unlock<rw_locking_mode_t::Write>();
  }

  template<typename KeyType, typename ValueType>
  void hash_table_t<KeyType, ValueType>::remove(KeyType const& key)
  {
    uint32_t bucketIndex = hashGenerator(key) % n_buckets;
    rw_lock_t& bolt = rw_buckets[bucketIndex];
    bolt.lock<rw_locking_mode_t::Write>();
    typename unordered_map<KeyType, ValueType>::iterator it = buckets[bucketIndex].find(key);
    if (it == buckets[bucketIndex].cend())
    {
      bolt.unlock<rw_locking_mode_t::Write>();
      return;
    }
    buckets[bucketIndex].erase(it);
    bolt.unlock<rw_locking_mode_t::Write>();
  }
#else
  template<typename KeyType, typename ValueType>
  class hash_table_t
  {
  private:
    uint32_t n_buckets;
    atomic<uint32_t> n_keys;
    vector<unordered_map<KeyType, ValueType>> buckets;
    mutable rw_lock_t *rw_buckets, rw_root;
    mutable hash<KeyType> hashGenerator;
    float getLoadFactor() const;
  public:
    hash_table_t(uint32_t n_buckets_ = INIT_HTABLE_BUCKET_COUNT);
    hash_table_t(hash_table_t<KeyType, ValueType> const&) = delete;
    hash_table_t<KeyType, ValueType>& operator=(hash_table_t<KeyType, ValueType> const&) = delete;
    ~hash_table_t();
    void clear();
    bool get(KeyType const& key, ValueType& value) const;
    void insert(KeyType const& key, ValueType const& value);

    friend ostream& operator<<(ostream& out, hash_table_t<KeyType, ValueType> const& table)
    {
      table.rw_root.lock<rw_locking_mode_t::Read>();
      for (uint32_t i = 0; i < table.n_buckets; i++)
      {
        rw_lock_t& bolt = table.rw_buckets[i];
        bolt.lock<rw_locking_mode_t::Read>();
        out << table.buckets[i] << endl;
        bolt.unlock<rw_locking_mode_t::Read>();
      }
      table.rw_root.unlock<rw_locking_mode_t::Read>();
      return out;
    }
    void remove(KeyType const& key);
    void resize();
    void update(KeyType const& key, ValueType const& value);
  };

  template<typename KeyType, typename ValueType>
  inline float hash_table_t<KeyType, ValueType>::getLoadFactor() const
  {
    float loadFactor = n_keys / (float)n_buckets;
    return loadFactor;
  }

  template<typename KeyType, typename ValueType>
  inline hash_table_t<KeyType, ValueType>::hash_table_t(uint32_t n_buckets_)
    : n_buckets(n_buckets_), n_keys(0)
  {
    buckets.resize(n_buckets);
    rw_buckets = new rw_lock_t[n_buckets];
  }

  template<typename KeyType, typename ValueType>
  inline hash_table_t<KeyType, ValueType>::~hash_table_t()
  {
    delete[] rw_buckets;
  }

  template<typename KeyType, typename ValueType>
  inline void hash_table_t<KeyType, ValueType>::clear()
  {
    rw_root.lock<rw_locking_mode_t::Write>();
    for (uint32_t i = 0; i < n_buckets; i++)
    {
      rw_lock_t& bolt = rw_buckets[i];
      bolt.lock<rw_locking_mode_t::Write>();
      buckets[i].clear();
      bolt.unlock<rw_locking_mode_t::Write>();
    }
    n_keys = 0;
    rw_root.unlock<rw_locking_mode_t::Write>();
  }

  template<typename KeyType, typename ValueType>
  inline bool hash_table_t<KeyType, ValueType>::get(KeyType const& key, ValueType& value) const
  {
    rw_root.lock<rw_locking_mode_t::Read>();
    uint32_t bucketIndex = hashGenerator(key) % n_buckets;
    rw_lock_t& bolt = rw_buckets[bucketIndex];
    bolt.lock<rw_locking_mode_t::Read>();
    typename unordered_map<KeyType, ValueType>::const_iterator it = buckets[bucketIndex].find(key);
    if (it == buckets[bucketIndex].cend())
    {
      bolt.unlock<rw_locking_mode_t::Read>();
      rw_root.unlock<rw_locking_mode_t::Read>();
      return false;
    }
    value = it->second;
    bolt.unlock<rw_locking_mode_t::Read>();
    rw_root.unlock<rw_locking_mode_t::Read>();
    return true;
  }

  template<typename KeyType, typename ValueType>
  inline void hash_table_t<KeyType, ValueType>::insert(KeyType const& key, ValueType const& value)
  {
    rw_root.lock<rw_locking_mode_t::Read>();
    uint32_t bucketIndex = hashGenerator(key) % n_buckets;
    rw_lock_t& bolt = rw_buckets[bucketIndex];
    bolt.lock<rw_locking_mode_t::Write>();
    if (buckets[bucketIndex].find(key) == buckets[bucketIndex].cend())
    {
      buckets[bucketIndex][key] = value;
      n_keys++;
    }
    bolt.unlock<rw_locking_mode_t::Write>();
    rw_root.unlock<rw_locking_mode_t::Read>();
    resize();
  }

  template<typename KeyType, typename ValueType>
  inline void hash_table_t<KeyType, ValueType>::remove(KeyType const& key)
  {
    rw_root.lock<rw_locking_mode_t::Read>();
    uint32_t bucketIndex = hashGenerator(key) % n_buckets;
    rw_lock_t& bolt = rw_buckets[bucketIndex];
    bolt.lock<rw_locking_mode_t::Write>();
    typename unordered_map<KeyType, ValueType>::iterator it = buckets[bucketIndex].find(key);
    if (it == buckets[bucketIndex].cend())
    {
      bolt.unlock<rw_locking_mode_t::Write>();
      rw_root.unlock<rw_locking_mode_t::Read>();
      return;
    }
    buckets[bucketIndex].erase(it);
    assert(n_keys > 0);
    n_keys--;
    bolt.unlock<rw_locking_mode_t::Write>();
    rw_root.unlock<rw_locking_mode_t::Read>();
  }

  template<typename KeyType, typename ValueType>
  inline void hash_table_t<KeyType, ValueType>::resize()
  {
    if (getLoadFactor() < MAX_HTABLE_LOAD_FACTOR) return;
    rw_root.lock<rw_locking_mode_t::Write>();
    if (getLoadFactor() < MAX_HTABLE_LOAD_FACTOR) // check again, it's possible another thread has already resized it
    {
      rw_root.unlock<rw_locking_mode_t::Write>();
      return;
    }

    uint32_t old_n_buckets = n_buckets;
    vector<unordered_map<KeyType, ValueType>> old_buckets = move(buckets);
    rw_lock_t *old_rw_buckets = rw_buckets;

    assert(HTABLE_RESIZE_FACTOR > 1.0f);
    n_buckets *= HTABLE_RESIZE_FACTOR;
    buckets.resize(n_buckets);
    rw_buckets = new rw_lock_t[n_buckets];

    for (uint32_t bucketIndex = 0; bucketIndex < old_n_buckets; bucketIndex++)
    {
      rw_lock_t& bolt = rw_buckets[bucketIndex];
      bolt.lock<rw_locking_mode_t::Read>();
      typename unordered_map<KeyType, ValueType>::iterator it = old_buckets[bucketIndex].begin();
      while (it != old_buckets[bucketIndex].end())
      {
        KeyType key = it->first;
        uint32_t bucketIndex = hashGenerator(key) % n_buckets;
        buckets[bucketIndex][key] = it->second;
        it++;
      }
      bolt.unlock<rw_locking_mode_t::Read>();
    }
    rw_root.unlock<rw_locking_mode_t::Write>();
    delete[] old_rw_buckets;
  }

  template<typename KeyType, typename ValueType>
  inline void hash_table_t<KeyType, ValueType>::update(KeyType const& key, ValueType const& value)
  {
    rw_root.lock<rw_locking_mode_t::Read>();
    uint32_t bucketIndex = hashGenerator(key) % n_buckets;
    rw_lock_t& bolt = rw_buckets[bucketIndex];
    bolt.lock<rw_locking_mode_t::Write>();
    if (buckets[bucketIndex].find(key) != buckets[bucketIndex].cend()) buckets[bucketIndex][key] = value;
    bolt.unlock<rw_locking_mode_t::Write>();
    rw_root.unlock<rw_locking_mode_t::Read>();
  }
#endif // !ENABLE_HTABLE_RESIZING 
}
