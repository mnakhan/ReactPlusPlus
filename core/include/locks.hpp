/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace generic_ext
{
  class spin_lock_t
  {
  private:
    atomic_flag isLocked = ATOMIC_FLAG_INIT;
  public:
    spin_lock_t();
    spin_lock_t(spin_lock_t const&) = delete;
    spin_lock_t& operator=(spin_lock_t const&) = delete;
    void lock();
    bool try_lock();
    void unlock();
  };

  enum class rw_locking_mode_t
  {
    Read, Write
  };

  struct rw_lock_t
  {
  private:
    mutable mutex mx;
    mutable condition_variable cv_mayNotRead, cv_mayNotWrite;
    uint32_t nReaders, nReadersInQueue, nWritersInQueue;
    bool writeInProgress;
  public:
    rw_lock_t();
    rw_lock_t(rw_lock_t const&) = delete;
    rw_lock_t& operator=(rw_lock_t const&) = delete;
    template<rw_locking_mode_t m>
    void lock();
    template<rw_locking_mode_t m>
    void unlock();
  };

  template<>
  inline void rw_lock_t::lock<rw_locking_mode_t::Read>()
  {
    unique_lock<mutex> bolt(mx);
    if (writeInProgress || nWritersInQueue > 0)
    {
      nReadersInQueue++;
      cv_mayNotRead.wait(bolt, [this]
      {
        return !writeInProgress && nWritersInQueue == 0;
      });
      nReadersInQueue--;
    }
    nReaders++;
  }

  template<>
  inline void rw_lock_t::lock<rw_locking_mode_t::Write>()
  {
    unique_lock<mutex> bolt(mx);
    if (writeInProgress || nReaders > 0)
    {
      nWritersInQueue++;
      cv_mayNotWrite.wait(bolt, [this]
      {
        return !writeInProgress && nReaders == 0;
      });
      nWritersInQueue--;
    }
    writeInProgress = true;
  }

  template<>
  inline void rw_lock_t::unlock<rw_locking_mode_t::Read>()
  {
    unique_lock<mutex> bolt(mx);
    nReaders--;
    if (nWritersInQueue > 0) cv_mayNotWrite.notify_one();
  }

  template<>
  inline void rw_lock_t::unlock<rw_locking_mode_t::Write>()
  {
    unique_lock<mutex> bolt(mx);
    writeInProgress = false;
    if (nWritersInQueue > 0) cv_mayNotWrite.notify_one();
    else if (nReadersInQueue > 0) cv_mayNotRead.notify_all();
  }
}
