/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "actor_id.hpp"
#include "generic.hpp"
#include "locks.hpp"

namespace rpp
{
  class message_t;
  struct mailbox_t
  {
    virtual ~mailbox_t();
    virtual void clear() = 0;
    virtual bool pop(message_t **message, actor_id_t& senderID) = 0;
    virtual void push(message_t *message, actor_id_t const& senderID) = 0;
  };

  class STD_DEQUE : public mailbox_t
  {
  private:
    deque<pair<message_t*, actor_id_t>> queue;
    mutable mutex mx_queue;
  public:
    STD_DEQUE();
    ~STD_DEQUE();
    void clear() override;
    bool pop(message_t **message, actor_id_t& senderID) override;
    void push(message_t *message, actor_id_t const& senderID) override;
  };

  struct LFMB_Node
  {
    message_t *message;
    actor_id_t senderID;
    LFMB_Node *next;
  };

  /** 
    * Queue based on a cached stack adapted from C++ Actor Framework.
    **/
  class CAF_MPSCQ : public mailbox_t
  {
  private:
    atomic<LFMB_Node*> top;
    LFMB_Node *cache;
    static void reverse(LFMB_Node **lst, LFMB_Node **result);
  public:
    CAF_MPSCQ();
    ~CAF_MPSCQ();
    void clear() override;
    bool pop(message_t **message, actor_id_t& senderID) override;
    void push(message_t *message, actor_id_t const& senderID) override;
  };

  struct alignas(16) mpscq_node_t
  {
    message_t *message;
    actor_id_t senderID;
    mpscq_node_t* volatile next;
  };

  struct alignas(16) mpscq_t
  {
    atomic<mpscq_node_t*> volatile head;
    mpscq_node_t *tail, stub;
  };

#define MPSCQ_STATIC_INIT(self) {&self.stub, &self.stub, {0}}

  void mpscq_create(mpscq_t *self);
  mpscq_node_t *mpscq_pop(mpscq_t *self);
  void mpscq_push(mpscq_t *self, mpscq_node_t *n);

  /**
    * Bounded MPMC queue
    * Source: http://www.1024cores.net/home/lock-free-algorithms/queues/bounded-mpmc-queue
    **/
  class DV_MPSCQ : public mailbox_t
  {
  private:
    mpscq_t q;
  public:
    DV_MPSCQ();
    ~DV_MPSCQ();
    void clear() override;
    bool pop(message_t **message, actor_id_t& senderID) override;
    void push(message_t *message, actor_id_t const& senderID) override;
  };

  template <typename T, int N>
  class ZMQ_SPSCQ
  {
  private:
    struct chunk_t
    {
      T values[N];
      chunk_t *prev, *next;
    };
    chunk_t *begin_chunk, *back_chunk, *end_chunk;
    int begin_pos, back_pos, end_pos;
    atomic<chunk_t*> spare_chunk;

  public:
    ZMQ_SPSCQ();
    ~ZMQ_SPSCQ();
    ZMQ_SPSCQ(ZMQ_SPSCQ const&) = delete;
    ZMQ_SPSCQ const& operator=(ZMQ_SPSCQ const&) = delete;
    T& back();
    void clear();
    T& front();
    void pop();
    void push();
  };

  template <typename T, int N>
  class ypipe_t
  {
  private:
    ZMQ_SPSCQ <T, N> queue;
    T *r, *w, *f;
    atomic<T*> c;
    ypipe_t(ypipe_t const&) = delete;
    ypipe_t const& operator=(ypipe_t const&) = delete;
  public:
    ypipe_t();
    ~ypipe_t();
    bool check_read();
    void clear();
    bool read(T *value_);
    void write(T const& value_);
  };

  template<typename T, int N>
  inline ZMQ_SPSCQ<T, N>::ZMQ_SPSCQ()
    : back_chunk(nullptr), begin_pos(0), back_pos(0), end_pos(0), spare_chunk(nullptr)
  {
    begin_chunk = new chunk_t;
    end_chunk = begin_chunk;
  }

  template<typename T, int N>
  inline ZMQ_SPSCQ<T, N>::~ZMQ_SPSCQ()
  {
  }

  template<typename T, int N>
  inline T& ZMQ_SPSCQ<T, N>::back()
  {
    return back_chunk->values[back_pos];
  }

  template<typename T, int N>
  inline void ZMQ_SPSCQ<T, N>::clear()
  {
    if (!back_chunk) return;
    while (true)
    {
      if (begin_chunk == end_chunk)
      {
        delete begin_chunk;
        break;
      }
      chunk_t *o = begin_chunk;
      begin_chunk = begin_chunk->next;
      delete o;
    }

    chunk_t *sc = spare_chunk.exchange(nullptr, memory_order_acq_rel);
    delete sc;
    back_chunk = nullptr;
  }

  template<typename T, int N>
  inline T& ZMQ_SPSCQ<T, N>::front()
  {
    return begin_chunk->values[begin_pos];
  }

  template<typename T, int N>
  inline void ZMQ_SPSCQ<T, N>::pop()
  {
    if (++begin_pos == N)
    {
      chunk_t *o = begin_chunk;
      begin_chunk = begin_chunk->next;
      begin_chunk->prev = nullptr;
      begin_pos = 0;
      chunk_t *cs = spare_chunk.exchange(o, memory_order_acq_rel);
      delete cs;
    }
  }

  template<typename T, int N>
  inline void ZMQ_SPSCQ<T, N>::push()
  {
    back_chunk = end_chunk;
    back_pos = end_pos;
    if (++end_pos != N) return;

    chunk_t *sc = spare_chunk.exchange(nullptr, memory_order_acq_rel);
    if (sc)
    {
      end_chunk->next = sc;
      sc->prev = end_chunk;
    }
    else
    {
      end_chunk->next = new chunk_t;
      end_chunk->next->prev = end_chunk;
    }
    end_chunk = end_chunk->next;
    end_pos = 0;
  }

  template<typename T, int N>
  inline ypipe_t<T, N>::ypipe_t()
  {
    queue.push();
    r = w = f = &queue.back();
    c.store(&queue.back(), memory_order_release);
  }

  template<typename T, int N>
  inline ypipe_t<T, N>::~ypipe_t()
  {
  }

  template<typename T, int N>
  inline bool ypipe_t<T, N>::check_read()
  {
    if (&queue.front() != r && r) return true;
    r = &queue.front();
    c.compare_exchange_strong(r, nullptr, memory_order_acquire, memory_order_relaxed);
    if (&queue.front() == r || !r) return false;
    return true;
  }

  template<typename T, int N>
  inline void ypipe_t<T, N>::clear()
  {
    queue.clear();
  }

  template<typename T, int N>
  inline bool ypipe_t<T, N>::read(T *value_)
  {
    if (!check_read()) return false;
    *value_ = queue.front();
    queue.pop();
    return true;
  }

  template<typename T, int N>
  inline void ypipe_t<T, N>::write(T const& value_)
  {
    queue.back() = value_;
    queue.push();
    f = &queue.back();
    if (w == f) return;
    T *tmp = w;
    if (!c.compare_exchange_strong(tmp, f, memory_order_acquire, memory_order_relaxed)) c.store(f, memory_order_release);
    w = f;
  }

  /**
    * Multiple-producer, single-consumer queue based on ZMQ's mailbox.
    * Source: https://github.com/zeromq/libzmq
    **/
  class ZMQ_MPSCQ : public mailbox_t
  {
  private:
    mutable generic_ext::spin_lock_t mx_writer;
    ypipe_t<pair<message_t*, actor_id_t>, ZMQ_MAILBOX_ALLOC_UNIT> queue;
  public:
    ZMQ_MPSCQ();
    ~ZMQ_MPSCQ();
    void clear() override;
    bool pop(message_t **message, actor_id_t& senderID) override;
    void push(message_t *message, actor_id_t const& senderID) override;
  };
}
