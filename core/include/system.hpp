/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "cluster.hpp"
#include "configuration.hpp"
#include "barrier.hpp"
#include "timer.hpp"
#include "future.hpp"
#include "environment.hpp"
#include "tag_registry.hpp"

namespace rpp
{
  template<typename>
  class actor_t;

  template<scheduler_type_t>
  class cluster_t;
  class system_t;

  template<worker_id_t cpuID>
  struct CPU
  {
    inline constexpr worker_id_t operator()() const
    {
      return cpuID;
    }

    inline constexpr operator worker_id_t() const
    {
      return cpuID;
    }
  };

  template<cluster_id_t clusterID>
  struct CLUSTER
  {
    inline constexpr cluster_id_t operator()() const
    {
      return clusterID;
    }

    inline constexpr operator cluster_id_t () const
    {
      return clusterID;
    }
  };

  struct location_t
  {
  private:
    static system_t& sys;
    static printer_t& tscout;
    static random_t& rnd;
    static rw_lock_t mx_generator;
    static vector<atomic<worker_id_t>> generator;
    static cluster_id_t getClusterCount();
    static worker_id_t getWorkerCount(cluster_id_t clusterID);
  public:
    static const location_t DetachedCluster;
    cluster_id_t clusterID;
    worker_id_t workerID;
    location_t(cluster_id_t clusterID_ = DEFAULT_CLUSTER_ID, worker_id_t workerID_ = DEFAULT_WORKER_ID);
    bool exists() const;
    static location_t map(bool pinToWorker = false, cluster_id_t clusterID = DEFAULT_CLUSTER_ID, worker_id_t workerID = DEFAULT_WORKER_ID);
    friend ostream& operator<<(ostream& out, location_t const& location);
    static void resetGenerator(cluster_id_t nClusters);

    template<typename T = Anywhere>
    struct generate
    {
      static location_t on(cluster_id_t clusterID);
    };
  };

  template<>
  struct location_t::generate<Fixed>
  {
    static location_t on(cluster_id_t clusterID)
    {
      worker_id_t workerID = CPU<0>{};
      mx_generator.lock<rw_locking_mode_t::Read>();
      rpp_assert(clusterID < getClusterCount(), "Attempting to spawn actor on a non-existent cluster.", tscout);
      rpp_assert(clusterID < generator.size(), "Location generator is not initialized.", tscout);
      workerID = generator[clusterID].load(memory_order_acquire);
      mx_generator.unlock<rw_locking_mode_t::Read>();
      return location_t(clusterID, workerID);
    }
  };

  template<>
  struct location_t::generate<RoundRobin>
  {
    static location_t on(cluster_id_t clusterID)
    {
      worker_id_t workerID = CPU<0>{};
      mx_generator.lock<rw_locking_mode_t::Read>();
      rpp_assert(clusterID < getClusterCount(), "Attempting to spawn actor on a non-existent cluster.", tscout);
      rpp_assert(clusterID < generator.size(), "Location generator is not initialized.", tscout);
      worker_id_t nWorkers = getWorkerCount(clusterID);
      workerID = generator[clusterID].fetch_add(1, memory_order_acq_rel) % nWorkers;
      mx_generator.unlock<rw_locking_mode_t::Read>();
      return location_t(clusterID, workerID);
    }
  };

  template<>
  struct location_t::generate<Anywhere>
  {
    static location_t on(cluster_id_t clusterID)
    {
      worker_id_t workerID = CPU<0>{};
      mx_generator.lock<rw_locking_mode_t::Read>();
      rpp_assert(clusterID < getClusterCount(), "Attempting to spawn actor on a non-existent cluster.", tscout);
      rpp_assert(clusterID < generator.size(), "Location generator is not initialized.", tscout);
      worker_id_t nWorkers = getWorkerCount(clusterID);
      workerID = rnd.nextInt<worker_id_t>(0, nWorkers - 1);
      mx_generator.unlock<rw_locking_mode_t::Read>();
      return location_t(clusterID, workerID);
    }
  };

  ostream& operator<<(ostream& out, location_t const& location);

  struct props_t
  {
    template<mailbox_type_t QueueType>
    struct with
    {
    private:
      mailbox_type_t queueType;
      location_t location;
      uint64_t dispatchSize;
      bool isInert;
      double senderAffinity;
    public:
      with()
        :queueType(QueueType), dispatchSize(UINT64_MAX), isInert(false), senderAffinity(0.0)
      {
      }

      inline with<QueueType>& at(location_t const& location_)
      {
        location = location_;
        return *this;
      }

      inline with<QueueType>& withDispatchSize(uint64_t dispatchSize_ = UINT64_MAX)
      {
        dispatchSize = dispatchSize_;
        return *this;
      }

      inline with<QueueType>& asActive()
      {
        isInert = false;
        return *this;
      }

      inline with<QueueType>& asInert()
      {
        isInert = true;
        return *this;
      }

      inline with<QueueType>& withSenderAffinity(double senderAffinity_)
      {
        senderAffinity = senderAffinity_;
        return *this;
      }

      friend class system_t;
      friend class actor_t<native>;
      friend class actor_t<detached>;
      friend class actor_t<channel>;
    };
  };

  class system_t
  {
  private:
    static printer_t& tscout;
    static random_t& rnd;
    mutable mutex mx_state;
    bool isRunning;
    vector<worker_id_t> threadsPerCluster;
    cluster_id_t nClusters;
    cluster_t<NATIVE> *clusters;
    cluster_t<DETACHED> detachedCluster;
    thread *pool;

    thread timer_thread;
    platform_t *platform;
    tag_registry_t tags;
    timer_t timer;

    void extract();
    template<typename First, typename... Rest>
    void extract(First first, Rest... rest);
    void joinAll();
    void loadPlatform();
    bool rewind(message_t *message, actor_id_t const& receiverID, actor_id_t const& senderID = actor_id_t());
  public:

    system_t();
    ~system_t();
    system_t(system_t const&) = delete;
    void bind(actor_id_t const& id, tag_t const& tag);
    template<bool isAtom = false, typename MessageType>
    bool broadcast(MessageType contents, vector<actor_id_t> const& receivers, actor_id_t const& senderID = actor_id_t());
    void clearStats() const;
    bool exists(actor_id_t const& actorID) const;
    map<cluster_id_t, uint64_t> getActorCount() const;
    cluster_id_t getClusterCount() const;

    tag_registry_t& getTagRegistry();
    timer_t& getTimer();
    map<cluster_id_t, worker_id_t> getWorkerCount() const;

    template<typename ContextType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<context_t, ContextType>> initialize(actor_id_t const& actorID, ArgTypes... args)
    {
      cluster_id_t clusterID = actorID.clusterID;
      if (clusterID < nClusters)
      {
        shared_ptr<runnable_t> instance;
        if (!clusters[clusterID].registry.get(actorID.localID, instance)) return;
        applyFunction(&ContextType::initialize, (ContextType*)instance.get(), forward<ArgTypes>(args)...);
      }
      else if (clusterID == DETACHED_CLUSTER_ID)
      {
        shared_ptr<executor_t> executor;
        if (!detachedCluster.registry.get(actorID.localID, executor)) return;
        applyFunction(&ContextType::initialize, (ContextType*)executor->instance, forward<ArgTypes>(args)...);
      }
      else rpp_assert(clusterID < nClusters, "Actor ID referenced a non-existent cluster during actor initialization.", tscout);
    }

    template<typename ContextType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<context_t, ContextType>> initializeEach(vector<actor_id_t> const& actorIDs, ArgTypes... args) const
    {
      vector<actor_id_t>::const_iterator it = actorIDs.cbegin(), e = actorIDs.cend();
      while (it != e)
      {
        initialize<ContextType, ArgTypes...>(*it, args...);
        it++;
      }
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initialize(actor_id_t const& actorID, ArgTypes... args) const
    {
      cluster_id_t clusterID = actorID.clusterID;
      if (clusterID < nClusters)
      {
        shared_ptr<runnable_t> instance;
        if (!clusters[clusterID].registry.get(actorID.localID, instance)) return;
        context_t *ctx = (context_t*)instance.get();
        ctx->initializeBehavior<BehaviorType>(forward<ArgTypes>(args)...);
      }
      else if (clusterID == DETACHED_CLUSTER_ID)
      {
        shared_ptr<executor_t> executor;
        if (!detachedCluster.registry.get(actorID.localID, executor)) return;
        context_t *ctx = (context_t*)executor->instance;
        ctx->initializeBehavior<BehaviorType>(forward<ArgTypes>(args)...);
      }
      else rpp_assert(clusterID < nClusters, "Actor ID referenced a non-existent cluster during actor initialization.", tscout);
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initializeEach(vector<actor_id_t> const& actorIDs, ArgTypes... args) const
    {
      vector<actor_id_t>::const_iterator it = actorIDs.cbegin(), e = actorIDs.cend();
      while (it != e)
      {
        initialize<BehaviorType, ArgTypes...>(*it, args...);
        it++;
      }
    }

    template<typename OutputType, typename... InputType>
    future_t<OutputType> make(Promise<OutputType, InputType...>& p, actor_id_t const& receiverID, actor_id_t const& senderID = actor_id_t());
    bool migrate(actor_id_t const& actorID, cluster_id_t targetClusterID);
    system_t& operator=(system_t const&) = delete;
    void raise(cluster_id_t clusterID, worker_id_t workerID);
    string report() const;
    template<typename... ArgTypes>
    void restart(io_policy_t *policy, ArgTypes... args);
    void restartWithConfiguration(io_policy_t *policy, cluster_id_t nClusters_, worker_id_t nWorkersPerCluster);
    void restartWithConfiguration(io_policy_t *policy);
    bool returnToOrigin(actor_id_t const& actorID);
    void shutdown();
    template<bool isAtom = false, typename MessageType>
    bool send(MessageType contents, actor_id_t const& receiverID, actor_id_t const& senderID = actor_id_t());

    template<typename MessageType>
    bool syncSend(MessageType contents, actor_id_t const& receiverID, uint64_t& replyToken, actor_id_t const& senderID = actor_id_t());

    template<typename MessageType>
    bool reply(MessageType contents, message_t *original, actor_id_t const& receiverID, actor_id_t const& senderID = actor_id_t());

    template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
    actor_id_t spawn(props_t::with<MType> const& props, ArgTypes... args);

    template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
    vector<actor_id_t> spawnGroup(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args);

    void unbind(actor_id_t const& id, tag_t const& tag);
    void waitForAll() const;

    scheduler_type_t whichDomain(actor_id_t const& actorID) const;

    template<typename>
    friend class actor_t;
    friend struct location_t;
    friend class context_t;
  };



  template<typename First, typename... Rest>
  inline void system_t::extract(First first, Rest... rest)
  {
    rpp_assert(0 < first && first < MAX_THREADS_PER_CLUSTER, "Exceeded limit for max number of kernel threads assignable per cluster. Try raising MAX_THREADS_PER_CLUSTER.", tscout);
    threadsPerCluster.push_back(first);
    extract(rest...);
  }

  template<bool isAtom, typename MessageType>
  inline bool system_t::broadcast(MessageType contents, vector<actor_id_t> const& receivers, actor_id_t const& senderID)
  {
    size_t nDeliveries = 0;
    vector<actor_id_t>::const_iterator it = receivers.cbegin(), e = receivers.cend();
    while (it != e)
    {
      if (send<isAtom, MessageType>(contents, *it, senderID)) nDeliveries++;
      it++;
    }
    return nDeliveries;
  }

  template<typename OutputType, typename... InputType>
  inline future_t<OutputType> system_t::make(Promise<OutputType, InputType...>& p, actor_id_t const& receiverID, actor_id_t const& senderID)
  {
    cluster_id_t clusterID = receiverID.clusterID;
    rpp_assert(clusterID < nClusters, "Actor ID referenced a non-existent cluster while sending a message.", tscout);
    shared_ptr<runnable_t> instance;
    clusters[clusterID].registry.get(receiverID.localID, instance);
    rpp_assert(instance && instance->isEnabled(), "A non-existent/disabled actor cannot make a promise.", tscout);
    future_t<OutputType> f = p.make();
    message_t *message = new typed_message_t<Promise<OutputType, InputType...>>(move(p));
    if (instance->maySchedule(message, senderID)) instance->residence->scheduler.enqueue(instance);
    return f;
  }

  template<typename... ArgTypes>
  inline void system_t::restart(io_policy_t *policy, ArgTypes... args)
  {
    shutdown();

    lock_guard<mutex> bolt(mx_state);
    if (isRunning) return;

#ifdef TRACE_STARTUP
    tscout.println("TRACE: initializing actor system");
#endif // TRACE_STARTUP

    isRunning = true;
    loadPlatform();

    detachedCluster.run();
    extract(args...);
    nClusters = (cluster_id_t)threadsPerCluster.size();
    location_t::resetGenerator(nClusters);
    clusters = new cluster_t<NATIVE>[nClusters];
    pool = new thread[nClusters];

    if (policy)
    {
      rpp_assert(nClusters == policy->getClusterCount(), "Runtime and I/O subsystem do not agree on cluster count.", tscout);
      for (cluster_id_t i = 0; i < nClusters; i++)
        rpp_assert(threadsPerCluster[i] == policy->getWorkerCount(i), "Runtime and I/O subsystem do not agree on worker count per cluster.", tscout);
    }

    for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].initialize(i, threadsPerCluster[i], policy);
    for (cluster_id_t i = 0; i < nClusters; i++) pool[i] = thread(&cluster_t<NATIVE>::run, ref(clusters[i]));
    for (cluster_id_t i = 0; i < nClusters; i++) clusters[i].pollSchedulerStatus();

    timer.initialize();
    timer_thread = thread(&timer_t::run, ref(timer));
  }

  template<bool isAtom, typename MessageType>
  inline bool system_t::send(MessageType contents, actor_id_t const& receiverID, actor_id_t const& senderID)
  {
    cluster_id_t clusterID = receiverID.clusterID;
    if (clusterID < nClusters)
    {
      shared_ptr<runnable_t> instance;
      if (clusters[clusterID].registry.get(receiverID.localID, instance))
      {
        if (!instance->isEnabled()) return false;
        if (isAtom)
        {
          typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
          if (instance->maySchedule(&message, senderID)) instance->residence->scheduler.enqueue(instance);
        }
        else
        {
          message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
          if (instance->maySchedule(message, senderID)) instance->residence->scheduler.enqueue(instance);
        }
        return true;
      }
      return false;
    }
    else if (clusterID == DETACHED_CLUSTER_ID)
    {
      shared_ptr<executor_t> executor;
      if (detachedCluster.registry.get(receiverID.localID, executor))
      {
        if (!executor->isEnabled()) return false;
        if (isAtom)
        {
          typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
          executor->send(&message, senderID);
        }
        else
        {
          message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
          executor->send(message, senderID);
        }
        return true;
      }
      return false;
    }
    else rpp_assert(false, "Actor ID referenced a non-existent cluster while sending a message.", tscout);

    return false;
  }

  template<typename MessageType>
  inline bool system_t::syncSend(MessageType contents, actor_id_t const& receiverID, uint64_t& replyToken,  actor_id_t const& senderID)
  {
    cluster_id_t clusterID = receiverID.clusterID;
    if (clusterID < nClusters)
    {
      shared_ptr<runnable_t> instance;
      if (clusters[clusterID].registry.get(receiverID.localID, instance))
      {
        if (!instance->isEnabled()) return false;
        message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
        message->generateToken();
        replyToken = message->replyToken;
        if (instance->maySchedule(message, senderID)) instance->residence->scheduler.enqueue(instance);
        return true;
      }
      return false;
    }
    else if (clusterID == DETACHED_CLUSTER_ID)
    {
      shared_ptr<executor_t> executor;
      if (detachedCluster.registry.get(receiverID.localID, executor))
      {
        if (!executor->isEnabled()) return false;
        message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
        message->generateToken();
        replyToken = message->replyToken;
        executor->send(message, senderID);
        return true;
      }
      return false;
    }
    else rpp_assert(false, "Actor ID referenced a non-existent cluster while sending a message.", tscout);

    return false;
  }

  template<typename MessageType>
  inline bool system_t::reply(MessageType contents, message_t *original, actor_id_t const& receiverID, actor_id_t const& senderID)
  {
    cluster_id_t clusterID = receiverID.clusterID;
    if (clusterID < nClusters)
    {
      shared_ptr<runnable_t> instance;
      if (clusters[clusterID].registry.get(receiverID.localID, instance))
      {
        if (!instance->isEnabled()) return false;
        message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
        message->replyToken = original->replyToken;
        if (instance->maySchedule(message, senderID)) instance->residence->scheduler.enqueue(instance);
        return true;
      }
      return false;
    }
    else if (clusterID == DETACHED_CLUSTER_ID)
    {
      shared_ptr<executor_t> executor;
      if (detachedCluster.registry.get(receiverID.localID, executor))
      {
        if (!executor->isEnabled()) return false;
        message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
        message->replyToken = original->replyToken;
        executor->send(message, senderID);
        return true;
      }
      return false;
    }
    else rpp_assert(false, "Actor ID referenced a non-existent cluster while sending a message.", tscout);

    return false;
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline actor_id_t system_t::spawn(props_t::with<MType> const& props, ArgTypes... args)
  {
    actor_id_t actorID;
    if (props.location.clusterID != DETACHED_CLUSTER_ID)
    {
      rpp_assert(props.location.clusterID < nClusters, "Attempting to spawn actor on a non-existent cluster.", tscout);
      clusters[props.location.clusterID].template spawn<ContextType, MType, false, ArgTypes...>(props.location.workerID, props.dispatchSize, props.senderAffinity, &actorID, nullptr, forward<ArgTypes>(args)...);
    }
    else detachedCluster.template spawn<ContextType, MType, false, ArgTypes...>(!props.isInert, props.dispatchSize, props.senderAffinity, &actorID, nullptr, forward<ArgTypes>(args)...);

    return actorID;
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline vector<actor_id_t> system_t::spawnGroup(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args)
  {
    vector<actor_id_t> actorIDs(nActors);
    for (local_id_t i = 0; i < nActors; i++) actorIDs[i] = spawn<ContextType, MType, ArgTypes...>(props, args...);
    return actorIDs;
  }

  class actor_system_t
  {
  private:
    static system_t& sys;
  public:
    actor_system_t();
    ~actor_system_t();
    system_t& operator()() const;
  };
}
