/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "behavior.hpp"

namespace rpp
{

  template<typename... BehaviorTypes>
  struct from_t
  {

    template<typename BehaviorType>
    inline constexpr static bool exists()
    {
      return exists_s<BehaviorType, BehaviorTypes...>();
    }

    template<typename BehaviorType, typename FirstCandidate, typename... Rest>
    inline constexpr static bool exists_s()
    {
      if (is_same_v<BehaviorType, FirstCandidate>) return true;
      return exists_s<BehaviorType, Rest...>();
    }

    template<typename BehaviorType>
    inline constexpr static bool exists_s()
    {
      return false;
    }
  };

  template<typename... BehaviorTypes>
  class typed_context_t : public context_t
  {
  public:
    typed_context_t();

    template<typename BehaviorType>
    inline static void become(behavior_t *behavior)
    {
      static_assert(from_t<BehaviorTypes...>::template exists<BehaviorType>(), "Cannot adopt a non-existent behavior.");
      behavior->ctx.adoptBehavior<BehaviorType>();
    }

    template<typename BehaviorType>
    inline static void become(context_t *ctx)
    {
      static_assert(from_t<BehaviorTypes...>::template exists<BehaviorType>(), "Cannot adopt a non-existent behavior.");
      ctx->adoptBehavior<BehaviorType>();
    }

    static void becomeDefault(behavior_t *behavior)
    {
      behavior->ctx.adoptDefault();
    }

    static void becomeDefault(context_t *ctx)
    {
      ctx->adoptDefault();
    }
  };

  template<typename... BehaviorTypes>
  inline typed_context_t<BehaviorTypes...>::typed_context_t()
  {
    generate<BehaviorTypes...>();
  }

  template<typename BehaviorType>
  class typed_context_t<BehaviorType> : public context_t
  {
  public:
    typed_context_t();

    template<typename TargetBehaviorType>
    inline static void become(behavior_t *behavior)
    {
      static_assert(from_t<BehaviorType>::template exists<TargetBehaviorType>(), "Cannot adopt a non-existent behavior.");
      behavior->ctx.adoptBehavior<TargetBehaviorType>();
    }

    template<typename TargetBehaviorType>
    inline static void become(context_t *ctx)
    {
      static_assert(from_t<BehaviorType>::template exists<TargetBehaviorType>(), "Cannot adopt a non-existent behavior.");
      ctx->adoptBehavior<TargetBehaviorType>();
    }

    inline static void becomeDefault(behavior_t *behavior)
    {
      behavior->ctx.adoptDefault();
    }

    inline static void becomeDefault(context_t *ctx)
    {
      ctx->adoptDefault();
    }
  };

  template<typename BehaviorType>
  inline typed_context_t<BehaviorType>::typed_context_t()
  {
    BehaviorType *behavior = new BehaviorType(*this);
    behaviors[type_index(typeid (BehaviorType))] = behavior;
    currentBehavior = behavior;
  }

  template<>
  class typed_context_t<> : public context_t
  {
  public:

    typed_context_t()
    {
    }

    template<typename BehaviorType>
    inline static void become(behavior_t *behavior) // behavior list is empty, must not compile
    {
      static_assert(from_t<>::template exists<BehaviorType>(), "Cannot adopt a non-existent behavior.");
      behavior->ctx.adoptBehavior<BehaviorType>();
    }

    template<typename BehaviorType>
    inline static void become(context_t *ctx) // behavior list is empty, must not compile
    {
      static_assert(from_t<>::template exists<BehaviorType>(), "Cannot adopt a non-existent behavior.");
      ctx->adoptBehavior<BehaviorType>();
    }

    inline static void becomeDefault(behavior_t *behavior)
    {
      behavior->ctx.adoptDefault();
    }

    inline static void becomeDefault(context_t *ctx)
    {
      ctx->adoptDefault();
    }
  };
}
