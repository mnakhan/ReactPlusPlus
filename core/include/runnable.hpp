/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "actor_id.hpp"
#include "receiver.hpp"

namespace rpp
{
  template<typename>
  class actor_t;

  template<scheduler_type_t>
  class cluster_t;

  class runnable_t
  {
  private:
    bool isAlive;
  protected:
    actor_id_t selfID;
    worker_id_t workerID;
    uint64_t privateDispatchSize;
    double senderAffinity;
    cluster_t<NATIVE> *origin, *residence;
    cluster_t<DETACHED> *detachedOrigin;
  public:
    runnable_t();
    virtual ~runnable_t();
    virtual uint64_t dispatch(uint64_t maxNumberOfMessages) = 0;
    actor_id_t getID() const;
    virtual bool isEnabled() const = 0;
    virtual bool maySchedule(message_t *message, actor_id_t const& senderID) = 0;
    virtual bool mayReschedule(uint64_t lastDispatch) = 0;
    virtual bool scheduleAtSender() const = 0;
    virtual bool migrate(cluster_t<NATIVE> *newResidence) = 0;
    template<typename ContextType, typename... Restrictions>
    bool probe(Restrictions... args)
    {
      ContextType* ptr = static_cast<ContextType*>(this);
      return ptr->template probe_s<Restrictions...>(forward<Restrictions>(args)...);
    }
    virtual bool returnToOrigin() = 0;

    friend class rws_scheduler_t;
    friend class system_t;

    template<typename>
    friend class actor_t;

    template<scheduler_type_t>
    friend class cluster_t;
    friend class executor_t;
  };
}
