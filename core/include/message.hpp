/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "actor_id.hpp"
#include "generic.hpp"
using namespace generic_ext;
using namespace io;

namespace rpp
{
  class trail_t;
  class message_t
  {
  private:
    static random_t& rnd;
    void generateToken();
    bool hasMatched;
    uint64_t replyToken;
    static message_t& emptyHandle();
  protected:
    bool isSingleInstance, isTrail;
    type_index tiContents;
  public:
    message_t(bool hasMatched_ = false);
    virtual ~message_t();
    template<typename FunctionType>
    message_t& any(FunctionType&& function);
    template<typename MessageType>
    static trail_t *beginTrail(MessageType contents);
    virtual void discard();
    template<typename MessageType>
    bool extract(MessageType& result);
    template<typename MessageType>
    trail_t *fork();
    template<typename MessageType>
    trail_t *fork(MessageType contents);
    virtual actor_id_t getFaultLocation() const;
    template<typename MessageType, typename ObjectType, typename FunctionType>
    bool forwardAtomIf(ObjectType * const obj, FunctionType&& function);
    template<typename MessageType, typename ObjectType, typename FunctionType>
    bool forwardIf(ObjectType * const obj, FunctionType&& function);
    template<typename MessageType>
    bool isAtom() const;
    virtual bool isRaised() const;
    bool isSynchronous() const;
    template<typename MessageType>
    trail_t *link();
    template<typename MessageType>
    trail_t *link(MessageType contents);
    virtual bool mayThrow() const;
    template<typename MessageType>
    bool ofType() const;
    template<typename MessageType, typename FunctionType>
    message_t& on(FunctionType&& function);
    friend ostream& operator<<(ostream& out, message_t const& message);
    template<typename MessageType>
    bool peek(MessageType& result);
    virtual message_t& raise(actor_id_t const& source);
    virtual bool rewind();
    virtual string toString() const;

    friend class type_erased_receiver_t;
    friend class context_t;
    friend class system_t;
    template<typename>
    friend class actor_t;
    friend class STD_DEQUE;
    friend class CAF_MPSCQ;
    friend class DV_MPSCQ;
    friend class ZMQ_MPSCQ;
  };

  struct si_message_t : message_t
  {
    si_message_t();
  };

  template<typename MessageType>
  struct typed_si_message_t : si_message_t
  {
    typed_si_message_t();
  };

  template<typename MessageType>
  inline typed_si_message_t<MessageType>::typed_si_message_t()
  {
    isSingleInstance = true;
    tiContents = type_index(typeid (MessageType));
  }

#define DECLARE_ATOM_TYPE(MessageType) typed_si_message_t<MessageType>

  template<typename MessageType>
  struct typed_message_t : message_t
  {
    MessageType contents;
    typed_message_t() = default;
    typed_message_t(MessageType contents_);
    string toString() const override;
  };

  class trail_t : public message_t
  {
  public:
    struct trail_details_t
    {
      vector<actor_id_t> path;
      bool raised;
      actor_id_t source;
      vector<actor_id_t>::const_reverse_iterator fault;
      uint32_t remaining;
      int faultIndex;
      trail_details_t();
    };

  protected:
    bool empty;
    trail_details_t *trail_details;
  public:
    trail_t();
    virtual ~trail_t();
    void discard() override;
    actor_id_t getFaultLocation() const override;
    template<typename MessageType>
    static trail_t *fork(message_t *src, message_t *dst);
    bool isRaised() const override;
    void link(actor_id_t const& senderID);
    template<typename MessageType>
    static trail_t *link(message_t *src, message_t *dst);
    bool mayThrow() const override;
    message_t& raise(actor_id_t const& source) override;
    bool rewind() override;

    friend class message_t;
    friend class context_t;
  };

  template<typename MessageType>
  class typed_trail_t : public trail_t
  {
  private:
    list<MessageType> snapshots;
  public:
    typed_trail_t();
    ~typed_trail_t();
    string toString() const override;

    friend class message_t;
    friend class context_t;
    friend class trail_t;
  };

  template<typename FunctionType>
  inline message_t& message_t::any(FunctionType&& function)
  {
    if(hasMatched) return emptyHandle();
    function();
    return emptyHandle();
  }

  template<typename MessageType>
  inline trail_t *message_t::beginTrail(MessageType contents)
  {
    typed_trail_t<MessageType> *typedTrail = new typed_trail_t<MessageType>;
    typedTrail->trail_details = new trail_t::trail_details_t;
    typedTrail->snapshots.push_back(forward<MessageType>(contents));
    return typedTrail;
  }

  template<typename MessageType>
  inline bool message_t::extract(MessageType& result)
  {
    typed_message_t<MessageType> *typedMessage = dynamic_cast<typed_message_t<MessageType>*> (this);
    if (typedMessage)
    {
      result = move(typedMessage->contents);
      return true;
    }
    return false;
  }

  template<typename MessageType>
  inline trail_t *message_t::fork()
  {
    rpp_assert(mayThrow(), "Operation not supported for this type.", GLOBAL(printer_t));
    typed_trail_t<MessageType> *src = dynamic_cast<typed_trail_t<MessageType>*> (this);
    rpp_assert(src, "Cannot change type of trail.", GLOBAL(printer_t));
    rpp_assert(!src->trail_details->raised, "Trail could be forked because it is rewinding.", GLOBAL(printer_t));

    MessageType contents;
    rpp_assert(message_t::peek(contents), "Cannot change type of trail.", GLOBAL(printer_t));
    typed_trail_t<MessageType> *dst = new typed_trail_t<MessageType>;
    // copy snapshots for forks
    dst->snapshots = src->snapshots;
    dst->snapshots.push_back(forward<MessageType>(contents));
    return trail_t::fork<MessageType>(this, dst);
  }

  template<typename MessageType>
  inline trail_t *message_t::fork(MessageType contents)
  {
    rpp_assert(mayThrow(), "Operation not supported for this type.", GLOBAL(printer_t));
    typed_trail_t<MessageType> *src = dynamic_cast<typed_trail_t<MessageType>*> (this);
    rpp_assert(src, "Cannot change type of trail.", GLOBAL(printer_t));
    rpp_assert(!src->trail_details->raised, "Trail could be forked because it is rewinding.", GLOBAL(printer_t));

    typed_trail_t<MessageType> *dst = new typed_trail_t<MessageType>;
    // copy snapshots for forks
    dst->snapshots = src->snapshots;
    dst->snapshots.push_back(forward<MessageType>(contents));
    return trail_t::fork<MessageType>(this, dst);
  }

  template<typename MessageType, typename ObjectType, typename FunctionType>
  inline bool message_t::forwardAtomIf(ObjectType * const obj, FunctionType&& function)
  {
    if (isAtom<MessageType>())
    {
      applyFunction(function, obj);
      return true;
    }

    return false;
  }

  template<typename MessageType>
  inline bool message_t::isAtom() const
  {
    return dynamic_cast<typed_si_message_t<MessageType>*> (const_cast<message_t*> (this));
  }

  template<typename MessageType>
  inline trail_t *message_t::link()
  {
    rpp_assert(mayThrow(), "Operation not supported for this type.", GLOBAL(printer_t));
    typed_trail_t<MessageType> *src = dynamic_cast<typed_trail_t<MessageType>*> (this);
    rpp_assert(src, "Cannot change type of trail.", GLOBAL(printer_t));
    rpp_assert(!src->trail_details->raised, "Trail could be forwarded because it is rewinding.", GLOBAL(printer_t));

    MessageType contents;
    rpp_assert(message_t::peek(contents), "Cannot change type of trail.", GLOBAL(printer_t));
    src->snapshots.push_back(forward<MessageType>(contents));
    typed_trail_t<MessageType> *dst = new typed_trail_t<MessageType>;
    return trail_t::link<MessageType>(this, dst);
  }

  template<typename MessageType>
  inline trail_t *message_t::link(MessageType contents)
  {
    rpp_assert(mayThrow(), "Operation not supported for this type.", GLOBAL(printer_t));
    typed_trail_t<MessageType> *src = dynamic_cast<typed_trail_t<MessageType>*> (this);
    rpp_assert(src, "Cannot change type of trail.", GLOBAL(printer_t));
    rpp_assert(!src->trail_details->raised, "Trail could be forwarded because it is rewinding.", GLOBAL(printer_t));
    src->snapshots.push_back(forward<MessageType>(contents));
    typed_trail_t<MessageType> *dst = new typed_trail_t<MessageType>;
    return trail_t::link<MessageType>(this, dst);
  }

  template<typename MessageType>
  inline bool message_t::ofType() const
  {
    return dynamic_cast<typed_message_t<MessageType>*> (const_cast<message_t*> (this));
  }

  template<typename MessageType, typename FunctionType>
  inline message_t& message_t::on(FunctionType&& function)
  {
    if (typed_si_message_t<MessageType> *typedSIMessage = dynamic_cast<typed_si_message_t<MessageType>*> (this))
    {
      function(MessageType());
      return emptyHandle();
    }

    typed_message_t<MessageType> *typedMessage = dynamic_cast<typed_message_t<MessageType>*>(this);
    if (typedMessage)
    {
      function(move(typedMessage->contents));
      return emptyHandle();
    }

    return *this;
  }

  template<typename MessageType>
  inline bool message_t::peek(MessageType& result)
  {
    typed_trail_t<MessageType> *typedTrail = dynamic_cast<typed_trail_t<MessageType>*> (this);
    if (typedTrail && !typedTrail->empty)
    {
      auto it = typedTrail->snapshots.crbegin();
      if (it == typedTrail->snapshots.crend()) return false;

      result = *it;
      return true;
    }
    return false;
  }

  template<typename MessageType, typename ObjectType, typename FunctionType>
  inline bool message_t::forwardIf(ObjectType * const obj, FunctionType&& function)
  {
    MessageType typedMessage;
    if (extract(typedMessage))
    {
      applyFunction(function, obj, typedMessage);
      return true;
    }
    return false;
  }

  struct Delete
  {
  };

  struct Park
  {
  };

  struct Barrier
  {
  };

  struct Resume
  {
  };


  template<typename MessageType>
  inline typed_message_t<MessageType>::typed_message_t(MessageType contents_)
    : contents(move(contents_))
  {
    tiContents = type_index(typeid (MessageType));
  }

  template<typename MessageType>
  inline string typed_message_t<MessageType>::toString() const
  {
    return "";
  }

  ostream& operator<<(ostream& out, message_t const& message);
  ostream& operator<<(ostream& out, Delete const& message);
  ostream& operator<<(ostream& out, Park const& message);

  template<typename MessageType>
  inline trail_t *trail_t::fork(message_t *src, message_t* dst)
  {
    typed_trail_t<MessageType> *typed_src = static_cast<typed_trail_t<MessageType>*>(src);
    typed_trail_t<MessageType> *typed_dst = static_cast<typed_trail_t<MessageType>*>(dst);
    typed_dst->trail_details = new trail_t::trail_details_t;
    // copy path for forks
    typed_dst->trail_details->path = typed_src->trail_details->path;
    return typed_dst;
  }

  template<typename MessageType>
  inline trail_t *trail_t::link(message_t *src, message_t *dst)
  {
    typed_trail_t<MessageType> *typed_src = static_cast<typed_trail_t<MessageType>*>(src);
    typed_trail_t<MessageType> *typed_dst = static_cast<typed_trail_t<MessageType>*>(dst);

    typed_dst->trail_details = typed_src->trail_details;
    typed_dst->snapshots = move(typed_src->snapshots);
    typed_src->trail_details = nullptr;
    typed_src->empty = true;

    return typed_dst;
  }

  template<typename MessageType>
  inline typed_trail_t<MessageType>::typed_trail_t()
  {
    tiContents = type_index(typeid (MessageType));
    empty = false;
  }

  template<typename MessageType>
  inline typed_trail_t<MessageType>::~typed_trail_t()
  {
  }

  template<typename MessageType>
  inline string typed_trail_t<MessageType>::toString() const
  {
    if (!trail_details) return string();

    assert(trail_details->path.size() == snapshots.size());

    ostringstream result;
    auto pathItr = trail_details->path.cbegin();
    auto contentsItr = snapshots.cbegin();
    int32_t index = 0, depth = 0;

    if (trail_details->raised) result << "type: trail (rewinding)\nexception source: " << trail_details->source;
    else result << "type: trail";
    result << endl;

    while (pathItr != trail_details->path.cend())
    {
      
      result << "sender = " << *pathItr << ", message = " << *contentsItr;
      if (index == trail_details->faultIndex) result << " <-- raised";
      result << endl;
      pathItr++;
      contentsItr++;
      index++;
      depth++;
      if (pathItr != trail_details->path.cend())
      {
        assert(contentsItr != snapshots.cend());
        for (int32_t i = 0; i < depth - 1; i++) result << "  ";
        result << '|' << endl;
        for (int32_t i = 0; i < depth - 1; i++) result << "  ";
        result << "|-->";
      }
    }
    return result.str();
  }

  template<typename = Either>
  struct atom_t
  {
    template<typename MessageType>
    static bool type(const message_t *message)
    {
      return false;
    }
  };

  template<>
  struct atom_t<True>
  {
    template<typename MessageType>
    static bool type(const message_t *message)
    {
      return dynamic_cast<typed_si_message_t<MessageType>*> (const_cast<message_t*> (message));
    }
  };

  template<>
  struct atom_t<False>
  {
    template<typename MessageType>
    static bool type(const message_t *message)
    {
      return dynamic_cast<typed_message_t<MessageType>*> (const_cast<message_t*> (message));
    }
  };

  template<>
  struct atom_t<Either>
  {
    template<typename MessageType>
    static bool type(const message_t *message)
    {
      return atom_t<True>::template type<MessageType>(message) || atom_t<False>::template type<MessageType>(message);
    }
  };
}
