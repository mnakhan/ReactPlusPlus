/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "allocator.hpp"
#include "runnable.hpp"

namespace io
{
  struct io_policy_t;
}

namespace rpp
{
  class ready_queue_t
  {
  public:
    using QueueType = generic_ext::intrusive_list_t<shared_ptr<runnable_t>, RQ_CACHE_SIZE>;
    using AllocatorType = generic_ext::allocator_t<shared_ptr<runnable_t>, RQ_CACHE_SIZE>;
    using NodeType = generic_ext::intrusive_list_node_t<shared_ptr<runnable_t>>;

    QueueType localQueue, orchestrationQueue;
    AllocatorType allocator;
    mutable mutex mx_oq;
    mutable condition_variable cv_mayExecute;
    mutable bool isSleeping;
  public:
    ready_queue_t();
    bool balance(uint64_t LQLength, uint64_t OQLength);
    uint64_t getSize() const;
    static uint64_t getNodeSize();
    bool isEmpty() const;
    pair<uint64_t, uint64_t> queryAllocations() const;
    void signal() const;
    void stage(shared_ptr<runnable_t> const& job);
    AllocatorType *steal(ready_queue_t& other);
    bool swap();
    template<typename FunctionType>
    void traverse(FunctionType&& executor, AllocatorType *victimsAllocator = nullptr);
    void wait(cluster_id_t clusterID, worker_id_t workerID, io::io_policy_t *policy = nullptr) const;
  };

  template<typename FunctionType>
  inline void ready_queue_t::traverse(FunctionType&& executor, AllocatorType *victimsAllocator)
  {
    NodeType *p = localQueue.header->right, *q;
    while (p != localQueue.header)
    {
      if (executor(p->element))  // reschedule
      {
        p = p->right;
        continue;
      }

      // unlink blocked runnable object
      q = p;
      p = p->right;
      q->left->right = q->right;
      q->right->left = q->left;
      q->left = q->right = nullptr;
      q->isAttached = false;
      q->element.reset();
      if (!victimsAllocator) allocator.free(q); // reclaim locally
      else victimsAllocator->free(q); // hand over to the allocator that allocated this object
      localQueue.size--;
    }
  }
}
