/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
#include "actor_id.hpp"

namespace rpp
{
  class ready_queue_t;
}

namespace generic_ext
{
  template<typename T>
  struct intrusive_list_node_t
  {
    T element;
    intrusive_list_node_t<T> *left, *right, *next;
    bool isAttached, isManaged;
    intrusive_list_node_t(T const& element_ = T());
    ~intrusive_list_node_t();
  };

  template<typename T, uint64_t CacheSize>
  class allocator_t;

  template<typename T, uint64_t CacheSize>
  class intrusive_list_t
  {
  private:
    intrusive_list_node_t<T> *header;
    allocator_t<T, CacheSize> *allocator;
    uint64_t size;
  public:
    intrusive_list_t();
    ~intrusive_list_t();
    T& back();
    const T& back() const;
    T& front();
    const T& front() const;
    uint64_t getSize() const;
    bool isEmpty() const;
    intrusive_list_node_t<T> *pop();
    pair<intrusive_list_node_t<T>*, intrusive_list_node_t<T>*> popRange(uint64_t maxNodesInRange, uint64_t *nRemoved = nullptr);
    void push(intrusive_list_node_t<T>& node);
    void pushRange(intrusive_list_node_t<T>& first, intrusive_list_node_t<T>& last, uint64_t length);
    void setAllocator(allocator_t<T, CacheSize> *allocator_);
    intrusive_list_node_t<T> *shift();
    void swap(intrusive_list_t<T, CacheSize>& other);
    string toString() const;
    void unshift(intrusive_list_node_t<T>& node);

    friend ostream& operator<<(ostream& out, intrusive_list_t<T, CacheSize> const& lst)
    {
      return out << lst.toString();
    }

    friend class rpp::ready_queue_t;
  };

  template<typename T>
  inline intrusive_list_node_t<T>::intrusive_list_node_t(T const& element_)
    : element(element_), left(nullptr), right(nullptr), next(nullptr), isAttached(false), isManaged(false)
  {
  }

  template<typename T>
  inline intrusive_list_node_t<T>::~intrusive_list_node_t()
  {
  }

  template<typename T, uint64_t CacheSize>
  inline intrusive_list_t<T, CacheSize>::intrusive_list_t()
    : allocator(nullptr), size(0)
  {
    header = new intrusive_list_node_t<T>;
    header->left = header->right = header;
    header->isManaged = header->isAttached = true;
  }

  template<typename T, uint64_t CacheSize>
  inline intrusive_list_t<T, CacheSize>::~intrusive_list_t()
  {
    assert(allocator);
    while (intrusive_list_node_t<T> *node = pop()) allocator->free(node);
    delete header;
  }

  template<typename T, uint64_t CacheSize>
  inline T& intrusive_list_t<T, CacheSize>::back()
  {
    assert(!isEmpty());
    return header->left->element;
  }

  template<typename T, uint64_t CacheSize>
  inline const T& intrusive_list_t<T, CacheSize>::back() const
  {
    assert(!isEmpty());
    return header->left->element;
  }

  template<typename T, uint64_t CacheSize>
  inline T& intrusive_list_t<T, CacheSize>::front()
  {
    assert(!isEmpty());
    return header->right->element;
  }

  template<typename T, uint64_t CacheSize>
  inline const T& intrusive_list_t<T, CacheSize>::front() const
  {
    assert(!isEmpty());
    return header->right->element;
  }

  template<typename T, uint64_t CacheSize>
  inline uint64_t intrusive_list_t<T, CacheSize>::getSize() const
  {
    return size;
  }

  template<typename T, uint64_t CacheSize>
  inline bool intrusive_list_t<T, CacheSize>::isEmpty() const
  {
    if (size == 0)
    {
      assert(header == header->left && header == header->right);
      return true;
    }
    return false;
  }

  template<typename T, uint64_t CacheSize>
  inline intrusive_list_node_t<T> *intrusive_list_t<T, CacheSize>::pop()
  {
    if (isEmpty()) return nullptr;
    intrusive_list_node_t<T> *p = header->left;
    p->isAttached = false;
    p->left->right = p->right;
    p->right->left = p->left;
    p->left = p->right = nullptr;
    size--;
    return p;
  }

  template<typename T, uint64_t CacheSize>
  inline pair<intrusive_list_node_t<T>*, intrusive_list_node_t<T>*> intrusive_list_t<T, CacheSize>::popRange(uint64_t maxNodesInRange, uint64_t * nRemoved)
  {
    if (isEmpty() || !maxNodesInRange)
    {
      if (nRemoved) *nRemoved = 0;
      return make_pair(nullptr, nullptr);
    }

    if (maxNodesInRange >= size)
    {
      intrusive_list_node_t<T> *first = header->right, *last = header->left;
      header->left = header->right = header;
      first->left = last->right = nullptr;
      if (nRemoved) *nRemoved = size;
      size = 0;
      return make_pair(first, last);
    }

    intrusive_list_node_t<T> *first = header->right, *last = header;
    for (uint64_t i = 0; i < maxNodesInRange; i++) last = last->right;
    header->right = last->right;
    last->right->left = header;
    first->left = last->right = nullptr;
    if (nRemoved) *nRemoved = maxNodesInRange;
    size -= maxNodesInRange;

    return make_pair(first, last);
  }

  template<typename T, uint64_t CacheSize>
  inline void intrusive_list_t<T, CacheSize>::push(intrusive_list_node_t<T>& node)
  {
    assert(!node.isAttached);
    node.isAttached = true;
    node.left = header->left;
    node.right = header;
    header->left->right = &node;
    header->left = &node;
    size++;
  }

  template<typename T, uint64_t CacheSize>
  inline void intrusive_list_t<T, CacheSize>::pushRange(intrusive_list_node_t<T>& first, intrusive_list_node_t<T>& last, uint64_t length)
  {
    assert(length > 0);
    first.left = header->left;
    last.right = header;
    header->left->right = &first;
    header->left = &last;
    size += length;
  }

  template<typename T, uint64_t CacheSize>
  inline void intrusive_list_t<T, CacheSize>::setAllocator(allocator_t<T, CacheSize> *allocator_)
  {
    allocator = allocator_;
  }

  template<typename T, uint64_t CacheSize>
  inline intrusive_list_node_t<T> *intrusive_list_t<T, CacheSize>::shift()
  {
    if (isEmpty()) return nullptr;
    intrusive_list_node_t<T> *p = header->right;
    p->isAttached = false;
    p->left->right = p->right;
    p->right->left = p->left;
    p->left = p->right = nullptr;
    size--;
    return p;
  }

  template<typename T, uint64_t CacheSize>
  inline void intrusive_list_t<T, CacheSize>::swap(intrusive_list_t<T, CacheSize>& other)
  {
    if (this == &other) return;
    generic_ext::swap(header, other.header);
    generic_ext::swap(size, other.size);
  }

  template<typename T, uint64_t CacheSize>
  inline string intrusive_list_t<T, CacheSize>::toString() const
  {
    ostringstream ss;
    ss << "[";
    intrusive_list_node_t<T> *p = header->right;
    while (p != header)
    {
      ss << p->element;
      p = p->right;
      if (p != header) ss << ", ";
    }
    ss << "]";
    return ss.str();
  }

  template<typename T, uint64_t CacheSize>
  inline void intrusive_list_t<T, CacheSize>::unshift(intrusive_list_node_t<T>& node)
  {
    assert(!node.isAttached);
    node.isAttached = true;
    intrusive_list_node_t<T> *p = header->right;
    node.left = p->left;
    node.right = p;
    p->left->right = &node;
    p->left = &node;
    size++;
  }
}
