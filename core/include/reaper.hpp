/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace rpp
{
  class reaper_t
  {
  private:
    deque<packaged_task<void()>> tasks;
    mutable mutex mx_tasks;
    mutable condition_variable cv_noTasks;
    atomic<bool> isRunning, hasExited, reaperActive;
  public:
    reaper_t();
    ~reaper_t();
    template<typename Function, typename... ArgTypes>
    void enqueue(Function function, ArgTypes... args);
    void initialize();
    void pollReaperStatus();
    void run();
    void shutdown();
  };

  template<typename Function, typename... ArgTypes>
  inline void reaper_t::enqueue(Function function, ArgTypes... args)
  {
    packaged_task<void()> task(bind(function, args...));
    lock_guard<mutex> bolt(mx_tasks);
    bool signal = tasks.empty();
    tasks.push_back(move(task));
    if (signal) cv_noTasks.notify_one();
  }
}
