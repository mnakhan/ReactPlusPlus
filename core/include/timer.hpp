/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
using namespace generic_ext;
using namespace io;

namespace rpp
{
  template<bool IsPeriodic = false>
  struct event_t
  {
    int32_t period;
    bool isCanceled;
    uint64_t token;
    static random_t& rnd;
    static uint64_t generateToken();

    event_t() : period(0), isCanceled(false), token(generateToken()) {}
    virtual ~event_t() {}
    virtual void raise() = 0;
  };

  template<bool IsPeriodic>
  random_t& event_t<IsPeriodic>::rnd = THR_LOCAL(random_t);

  class one_shot_task_t : public event_t<false>
  {
  private:
    packaged_task<void()> task;
  public:
    template<typename FunctionType, typename... ArgTypes>
    one_shot_task_t(FunctionType&& function, ArgTypes... args);
    void raise() override;
    one_shot_task_t(one_shot_task_t const&) = delete;
    one_shot_task_t& operator=(one_shot_task_t const&) = delete;
  };

  template<typename... ArgTypes>
  class periodic_task_t : public event_t<true>
  {
  private:
    function<void(ArgTypes...)> fn_ptr;
    tuple<ArgTypes...> args;
  public:
    periodic_task_t(function<void(ArgTypes...)>&& fn_ptr_, ArgTypes... args_);
    void raise() override;
    periodic_task_t(periodic_task_t const&) = delete;
    periodic_task_t& operator=(periodic_task_t const&) = delete;
  };

  class timer_t
  {
  private:
    mutable mutex mx_dispatchTable;
    mutable condition_variable cv_dispatchTable;
    atomic<bool> isRunning, hasExited;
    multimap<time_point<system_clock>, event_t<true>*> periodicDispatchTable;
    multimap<time_point<system_clock>, event_t<false>*> oneShotDispatchTable;
    template<bool IsPeriodic = false>
    bool execute(time_point<system_clock> const& currentTime, int32_t& nextAlarm);
  public:
    timer_t();
    ~timer_t();
    bool cancel(uint64_t token);
    void clear();
    void initialize();
    template<bool IsPeriodic>
    uint64_t registerHandler(int32_t timeout, event_t<IsPeriodic> *event);
    void run();
    void shutdown();
  };

  template<> bool timer_t::execute<true>(time_point<system_clock> const& currentTime, int32_t& nextAlarm);
  template<> bool timer_t::execute<false>(time_point<system_clock> const& currentTime, int32_t& nextAlarm);
  template<> uint64_t timer_t::registerHandler<true>(int32_t period, event_t<true> *event);
  template<> uint64_t timer_t::registerHandler<false>(int32_t timeout, event_t<false> *event);

  template<typename FunctionType, typename... ArgTypes>
  inline one_shot_task_t::one_shot_task_t(FunctionType&& function, ArgTypes... args)
  {
    packaged_task<void()> task_(bind(function, args...));
    task = move(task_);
  }

  inline void one_shot_task_t::raise()
  {
    task();
  }

  template<typename... ArgTypes>
  inline periodic_task_t<ArgTypes...>::periodic_task_t(function<void(ArgTypes...)>&& fn_ptr_, ArgTypes... args_)
    :fn_ptr(move(fn_ptr_)), args(make_tuple(forward<ArgTypes>(args_)...))
  {
  }

  template<typename... ArgTypes>
  inline void periodic_task_t<ArgTypes...>::raise()
  {
    apply_t(fn_ptr, args);
  }

  template<>
  inline bool timer_t::execute<true>(time_point<system_clock> const& currentTime, int32_t& nextAlarm)
  {
    bool mayExecute = false;
    while (true)
    {
      multimap<time_point<system_clock>, event_t<true>*>::iterator it;
      event_t<true> *e = nullptr;
      {
        lock_guard<mutex> bolt(mx_dispatchTable);
        it = periodicDispatchTable.begin();
        if (it == periodicDispatchTable.end()) break;
        if (currentTime < it->first)
        {
          duration<double, milli> delta = it->first - currentTime;
          assert(delta.count() > 0);
          nextAlarm = static_cast<int32_t>(delta.count());
          mayExecute = true;
          break;
        }
        e = it->second;
      }

      assert(e);
      e->raise();
      time_point<system_clock> nextRaiseAt = system_clock::now() + milliseconds(e->period);
      {
        lock_guard<mutex> bolt(mx_dispatchTable);
        periodicDispatchTable.erase(it);
        if (!e->isCanceled) periodicDispatchTable.emplace(make_pair(nextRaiseAt, e));
        else delete e;
      }
    }
    return mayExecute;
  }

  template<>
  inline bool timer_t::execute<false>(time_point<system_clock> const& currentTime, int32_t& nextAlarm)
  {
    bool mayExecute = false;
    while (true)
    {
      multimap<time_point<system_clock>, event_t<false>*>::iterator it;
      event_t<false> *e = nullptr;
      {
        lock_guard<mutex> bolt(mx_dispatchTable);
        it = oneShotDispatchTable.begin();
        if (it == oneShotDispatchTable.end()) break;
        if (currentTime < it->first)
        {
          duration<double, milli> delta = it->first - currentTime;
          assert(delta.count() > 0);
          nextAlarm = static_cast<int32_t>(delta.count());
          mayExecute = true;
          break;
        }
        e = it->second;
        oneShotDispatchTable.erase(it);
      }
      assert(e);
      e->raise();
      delete e;
    }
    return mayExecute;
  }

  template<>
  inline uint64_t timer_t::registerHandler<true>(int32_t period, event_t<true> *event)
  {
    event->period = period;
    time_point<system_clock> expireAt = system_clock::now() + milliseconds(period);
    lock_guard<mutex> bolt(mx_dispatchTable);
    bool signal = periodicDispatchTable.empty();
    periodicDispatchTable.emplace(make_pair(expireAt, event));
    if (signal) cv_dispatchTable.notify_one();
    return event->token;
  }

  template<>
  inline uint64_t timer_t::registerHandler<false>(int32_t timeout, event_t<false> *event)
  {
    time_point<system_clock> expireAt = system_clock::now() + milliseconds(timeout);
    lock_guard<mutex> bolt(mx_dispatchTable);
    bool signal = oneShotDispatchTable.empty();
    oneShotDispatchTable.emplace(make_pair(expireAt, event));
    if (signal) cv_dispatchTable.notify_one();
    return event->token;
  }

  template<bool IsPeriodic>
  inline uint64_t event_t<IsPeriodic>::generateToken()
  {
    return rnd.nextInt<uint64_t>();
  }
}
