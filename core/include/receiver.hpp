/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "message.hpp"

namespace rpp
{
  class message_t;
  template<typename MessageType>
  struct typed_message_t;
  class context_t;

  struct lambda_t
  {
  };

  template<typename MessageType>
  struct typed_lambda_t : lambda_t
  {
    function<void(context_t*, MessageType, actor_id_t) > handlerPtr;
    function<bool(MessageType&&) > predicatePtr;
  };

  struct TypedErasedLambda : lambda_t
  {
    bool isSet = false;
    function<void(context_t*, message_t*, actor_id_t) > handlerPtr;
    function<bool(const message_t*) > predicatePtr;
  };

  template<typename MessageType>
  struct DefaultPredicate
  {

    bool operator()(MessageType const&) const
    {
      return true;
    }
  };

  struct DefaultTypeErasedPredicate
  {

    bool operator()(const message_t*) const
    {
      return true;
    }
  };

  struct receiver_t
  {
    virtual ~receiver_t() = default;
    virtual bool match(context_t *ctx, message_t *message, actor_id_t const& senderID) const = 0;
  };

  class context_t;
  class type_erased_receiver_t : public receiver_t
  {
  private:
    unordered_map<type_index, receiver_t*> dispatchTable;
    TypedErasedLambda anyHandler;
    bool isDisabled;
  public:
    type_erased_receiver_t(bool isDisabled_ = false);
    ~type_erased_receiver_t();
    type_erased_receiver_t(type_erased_receiver_t&& other);
    type_erased_receiver_t& operator=(type_erased_receiver_t&& receiver);
    void clear();
    static type_erased_receiver_t& placeholder();
    bool match(context_t *ctx, message_t *message, actor_id_t const& senderID) const override;
    template<typename MessageType>
    type_erased_receiver_t& off();
    template<typename MessageType, typename LambdaType, typename PredicateType = DefaultPredicate<MessageType>>
    type_erased_receiver_t& on(LambdaType&& lambda, PredicateType&& predicate = DefaultPredicate<MessageType>{});

    template<typename LambdaType, typename PredicateType = DefaultTypeErasedPredicate>
    type_erased_receiver_t& any(LambdaType&& lambda, PredicateType&& predicate = DefaultTypeErasedPredicate{});

    type_erased_receiver_t& reset();

    type_erased_receiver_t(type_erased_receiver_t const&) = delete;
    type_erased_receiver_t& operator=(type_erased_receiver_t const&) = delete;
  };

  template<typename MessageType, typename LambdaType, typename PredicateType>
  class typed_receiver_t : public receiver_t
  {
  private:
    typed_lambda_t<MessageType> handler;
  public:
    typed_receiver_t(LambdaType&& lambda, PredicateType&& predicate);
    typed_receiver_t(typed_receiver_t&& receiver);
    typed_receiver_t& operator=(typed_receiver_t&& receiver);
    bool match(context_t *ctx, message_t *message, actor_id_t const& senderID) const override;

    typed_receiver_t(typed_receiver_t const&) = delete;
    typed_receiver_t& operator=(typed_receiver_t const&) = delete;
  };

  template<typename MessageType>
  inline type_erased_receiver_t& type_erased_receiver_t::off()
  {
    if (isDisabled) return *this;
    type_index tIndex = type_index(typeid (MessageType));
    unordered_map<type_index, receiver_t*>::iterator it = dispatchTable.find(tIndex);
    if (it == dispatchTable.end()) return *this;
    delete it->second;
    dispatchTable.erase(tIndex);
    return *this;
  }

  template<typename MessageType, typename LambdaType, typename PredicateType>
  inline type_erased_receiver_t& type_erased_receiver_t::on(LambdaType&& lambda, PredicateType&& predicate)
  {
    if (isDisabled) return *this;
    typed_receiver_t<MessageType, LambdaType, PredicateType> *result = new typed_receiver_t<MessageType, LambdaType, PredicateType>(forward<LambdaType>(lambda), forward<PredicateType>(predicate));
    type_index tIndex = type_index(typeid (MessageType));
    unordered_map<type_index, receiver_t*>::iterator it = dispatchTable.find(tIndex);
    if (it != dispatchTable.end())
    {
      delete it->second;
      it->second = result;
    }
    else dispatchTable[tIndex] = result;
    return *this;
  }

  template<typename LambdaType, typename PredicateType>
  inline type_erased_receiver_t& type_erased_receiver_t::any(LambdaType&& lambda, PredicateType&& predicate)
  {
    if (isDisabled) return *this;
    anyHandler.isSet = true;
    anyHandler.handlerPtr = move(lambda());
    anyHandler.predicatePtr = move(predicate);
    return *this;
  }

  template<typename MessageType, typename LambdaType, typename PredicateType>
  inline typed_receiver_t<MessageType, LambdaType, PredicateType>::typed_receiver_t(LambdaType&& lambda, PredicateType&& predicate)
  {
    handler.handlerPtr = move(lambda());
    handler.predicatePtr = move(predicate);
  }

  template<typename MessageType, typename LambdaType, typename PredicateType>
  inline typed_receiver_t<MessageType, LambdaType, PredicateType>::typed_receiver_t(typed_receiver_t<MessageType, LambdaType, PredicateType>&& receiver)
  {
    handler.handlerPtr = move(receiver.handler.handlerPtr);
    handler.predicatePtr = move(receiver.handler.predicatePtr);
  }

  template<typename MessageType, typename LambdaType, typename PredicateType>
  inline bool typed_receiver_t<MessageType, LambdaType, PredicateType>::match(context_t *ctx, message_t *message, actor_id_t const& senderID) const
  {
    if (typed_si_message_t<MessageType> *typedSIMessage = dynamic_cast<typed_si_message_t<MessageType>*> (message))
    {
      handler.handlerPtr(ctx, MessageType(), senderID);
      return true;
    }

    typed_message_t<MessageType> *typedMessage = dynamic_cast<typed_message_t<MessageType>*> (message);
    if (!typedMessage || !handler.predicatePtr(forward<MessageType>(typedMessage->contents))) return false;
    handler.handlerPtr(ctx, forward<MessageType>(typedMessage->contents), senderID);
    return true;
  }

  template<typename MessageType, typename LambdaType, typename PredicateType>
  inline typed_receiver_t<MessageType, LambdaType, PredicateType>& typed_receiver_t<MessageType, LambdaType, PredicateType>::operator=(typed_receiver_t<MessageType, LambdaType, PredicateType>&& receiver)
  {
    if (this == &receiver) return *this;
    handler.handlerPtr = move(receiver.handler.handlerPtr);
    handler.predicatePtr = move(receiver.handler.predicatePtr);
    return *this;
  }
}
