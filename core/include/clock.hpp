/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace generic_ext
{
  template<typename ClockType>
  class clock_base_t
  {
  private:
    time_point<ClockType> from, to;
    bool isRunning;
    vector<double> records;
  public:
    clock_base_t();
    ~clock_base_t();
    double getElapsed();
    int getPrecision() const;
    string getRecords() const;
    double getResolution() const;
    void lap();
    friend ostream& operator<<(ostream& out, clock_base_t<ClockType>& w)
    {
      out << std::fixed << setprecision(w.getPrecision());
      out << "Elapsed: " << w.getElapsed() << " s";
      if (w.records.size() > 0)
      {
        out << endl;
        out << "Records:" << endl;
        out << w.getRecords();
      }

      return out;
    }
    void pause();
    void reset();
    void restart();
    void start();
  };

  using clock_t = clock_base_t<system_clock>;
  using clock_hr_t = clock_base_t<high_resolution_clock>;

  template<typename ClockType>
  clock_base_t<ClockType>::clock_base_t() : isRunning(false) {}

  template<typename ClockType>
  clock_base_t<ClockType>::~clock_base_t() {}

  template<typename ClockType>
  double clock_base_t<ClockType>::getElapsed()
  {
    if (isRunning) to = ClockType::now();
    auto diff = to - from;
    llong ticks = diff.count();
    double durationInSeconds = ticks * getResolution();
    return durationInSeconds;
  }

  template<typename ClockType>
  int clock_base_t<ClockType>::getPrecision() const
  {
    // 1 ns (10^-9) --> precision = 9
    return log10(1.0 / getResolution());
  }

  template<typename ClockType>
  string clock_base_t<ClockType>::getRecords() const
  {
    if (records.size() == 0) return "";
    ostringstream buffer;
    buffer << std::fixed << setprecision(getPrecision());
    for (int i = 0; i < records.size(); i++)
    {
      buffer << records[i] << " s";
      if (i > 0) buffer << ", delta = " << records[i] - records[i - 1];
      if (i < records.size() - 1) buffer << endl;
    }
    return buffer.str();
  }

  template<typename ClockType>
  double clock_base_t<ClockType>::getResolution() const
  {
    return ClockType::period::num / (double)ClockType::period::den;
  }

  template<typename ClockType>
  void clock_base_t<ClockType>::lap()
  {
    if (!isRunning) return;
    records.push_back(getElapsed());
  }

  template<typename ClockType>
  void clock_base_t<ClockType>::pause()
  {
    if (!isRunning) return;
    to = ClockType::now();
    isRunning = false;
  }

  template<typename ClockType>
  void clock_base_t<ClockType>::reset()
  {
    if (isRunning) return;
    isRunning = false;
    from = to;
    records.clear();
  }

  template<typename ClockType>
  inline void clock_base_t<ClockType>::restart()
  {
    pause();
    reset();
    start();
  }

  template<typename ClockType>
  void clock_base_t<ClockType>::start()
  {
    if (isRunning) return;
    isRunning = true;
    from = ClockType::now();
  }
}
