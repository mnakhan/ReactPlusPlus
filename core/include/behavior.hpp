/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "context.hpp"
#include "system.hpp"

namespace rpp
{
  class behavior_t
  {
  protected:
    context_t& ctx;
    system_t& sys = GLOBAL(system_t);
    void yield_();
  public:
    behavior_t(context_t& ctx_);
    void initialize();
    virtual ~behavior_t();
    virtual void receive(message_t *message, actor_id_t const& senderID) {}

    template<typename T>
    void waitForReply(T contents, actor_id_t const& receiverID);

    template<typename ContextType>
    ContextType& as() const;

    template<typename ContextType>
    ContextType *typedContext() const;

    template<typename... BehaviorTypes>
    friend class typed_context_t;

#define YIELD() \
    { \
      yield_(); \
      return; \
    }
  };

  template<typename T>
  inline void behavior_t::waitForReply(T contents, actor_id_t const& receiverID)
  {
    uint64_t replyToken = DEFAULT_REPLY_TOKEN;
    sys.syncSend(forward<T>(contents), receiverID, replyToken, ctx.getID());
    ctx.awaitReply(replyToken);
  }

  template<typename ContextType>
  inline ContextType& behavior_t::as() const
  {
    return ctx.as<ContextType>();
  }

  template<typename ContextType>
  inline ContextType *behavior_t::typedContext() const
  {
    return &ctx.as<ContextType>();
  }
}
