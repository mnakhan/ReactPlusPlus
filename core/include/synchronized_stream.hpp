/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "printer.hpp"

namespace io
{

  template<typename ElementType, typename Traits>
  class synchronized_stream_t
  {
  private:
    static thread_local uint64_t nEntries;
    basic_ostream<ElementType, Traits>& out;
  public:
    synchronized_stream_t(basic_ostream<ElementType, Traits>& out_);
    void flush();
    template<typename T>
    synchronized_stream_t& operator<<(T const& value);
    synchronized_stream_t& operator<<(basic_ostream<ElementType, Traits>& (*manipulator)(basic_ostream<ElementType, Traits>&));

    synchronized_stream_t(synchronized_stream_t const&) = delete;
    synchronized_stream_t& operator=(synchronized_stream_t const&) = delete;
  };

  template<typename ElementType, typename Traits>
  thread_local uint64_t synchronized_stream_t<ElementType, Traits>::nEntries = 0ULL;

  template<typename ElementType, typename Traits>
  inline synchronized_stream_t<ElementType, Traits>::synchronized_stream_t(basic_ostream<ElementType, Traits>& out_)
    : out(out_)
  {
  }

  template<typename ElementType, typename Traits>
  template<typename T>
  inline synchronized_stream_t<ElementType, Traits>& synchronized_stream_t<ElementType, Traits>::operator<<(T const& value)
  {
    nEntries++;
    generic_ext::g_streamLock.mx_out.lock();
    out << value;
    return *this;
  }

  template<typename ElementType, typename Traits>
  inline synchronized_stream_t<ElementType, Traits>& synchronized_stream_t<ElementType, Traits>::operator<<(basic_ostream<ElementType, Traits>& (*manipulator)(basic_ostream<ElementType, Traits>&))
  {
    manipulator(out);
    if (manipulator == &endl<ElementType, Traits>) flush();
    return *this;
  }

  template<typename ElementType, typename Traits>
  inline void synchronized_stream_t<ElementType, Traits>::flush()
  {
    while (nEntries > 0)
    {
      generic_ext::g_streamLock.mx_out.unlock();
      nEntries--;
    }
  }

  using synchronized_char_ostream = synchronized_stream_t<char, char_traits<char>>;
  using synchronized_wchar_ostream = synchronized_stream_t<wchar_t, char_traits<wchar_t>>;
  extern synchronized_char_ostream scout;
  extern synchronized_wchar_ostream swcout;
}
