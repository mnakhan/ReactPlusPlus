/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
using namespace generic_ext;
using namespace io;

namespace rpp
{

  class blocking_handle_t
  {
  public:
    enum class flag_t : uint32_t
    {
      INIT, ACK, NACK
    };

    enum class receiver_status_t : uint32_t
    {
      RS_TIMED_OUT, RS_SUCCESS
    };

    enum class sender_status_t : uint32_t
    {
      SS_TIMED_OUT, SS_NO_SUCH_TOKEN, SS_SUCCESS
    };

  private:
    mutex mx_notification;
    condition_variable cv_blockTillNotified;
    unordered_map<int32_t, flag_t> notifications;
    static printer_t& tscout;
    system_clock::time_point startFrom;
  public:
    receiver_status_t block(int32_t token, int32_t timeoutInMillis);
    bool registerToken(int32_t token);
    sender_status_t unblock(int32_t token);
  };

  class message_t;
  void synchronize(message_t *message);
  struct synchronize_t
  {
  private:
    int32_t token;
  public:
    synchronize_t(int32_t token_ = -1);
    friend ostream& operator<<(ostream& out, synchronize_t const& message);
    void operator()() const;
    friend void synchronize(message_t *message);
  };

  ostream& operator<<(ostream& out, synchronize_t const& message);

  class barrier_t
  {
  private:
    struct barrier_details_t
    {
      actor_id_t actorID;
      int32_t token, timeoutInMillis;
      static random_t& rnd;
      barrier_details_t(actor_id_t const& actorID_, int32_t timeoutInMillis_);
      ~barrier_details_t();
      synchronize_t synchronize() const;
    };

    vector<barrier_details_t*> details;

    void scope_s(int32_t timeoutInMillis);
    template<typename First, typename... Rest>
    void scope_s(int32_t timeoutInMillis, First first, Rest... rest);
  public:
    template<typename... ArgTypes>
    barrier_t(int32_t timeoutInMillis, ArgTypes... args);
    template<typename T, uint32_t Size>
    barrier_t(int32_t timeoutInMillis, array<T, Size> const& args);
    ~barrier_t();
  };

  template<typename... ArgTypes>
  inline barrier_t::barrier_t(int32_t timeoutInMillis, ArgTypes... args)
  {
    static_assert(sizeof ... (args) > 0, "Cannot create barrier with no actors to wait for.");
    scope_s(timeoutInMillis, forward<ArgTypes>(args)...);
  }

  template<typename T, uint32_t Size>
  inline barrier_t::barrier_t(int32_t timeoutInMillis, array<T, Size> const& args)
  {
    static_assert(Size > 0, "Cannot create barrier with no actors to wait for.");
    for (uint32_t i = 0; i < Size; i++)
    {
      details.push_back(new barrier_details_t(args[i].getID(), timeoutInMillis));
    }
  }

  template<typename First, typename... Rest>
  inline void barrier_t::scope_s(int32_t timeoutInMillis, First first, Rest... rest)
  {
    details.push_back(new barrier_details_t(first.getID(), timeoutInMillis));
    scope_s(timeoutInMillis, forward<Rest>(rest)...);
  }
}
