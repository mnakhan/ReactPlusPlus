/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "intrusive_list.hpp"

namespace generic_ext
{

  template<typename T, uint64_t CacheSize>
  class allocator_t
  {
  private:
    atomic<intrusive_list_node_t<T>*> top;
    atomic<uint64_t> newCounter, deleteCounter;
    void push(intrusive_list_node_t<T> *node);
    intrusive_list_node_t<T> *pop();
  public:
    allocator_t();
    ~allocator_t();
    void clearStats();
    intrusive_list_node_t<T> *malloc();
    void free(intrusive_list_node_t<T> *node);
    pair<uint64_t, uint64_t> report() const;
    string toString() const;

    friend ostream& operator<<(ostream& out, allocator_t<T, CacheSize> const& ac)
    {
      return out << ac.toString();
    }
  };

  template<typename T, uint64_t CacheSize>
  inline void allocator_t<T, CacheSize>::push(intrusive_list_node_t<T> *node)
  {
    node->next = top.load(memory_order_relaxed);
    while (!top.compare_exchange_weak(node->next, node, memory_order_acquire, memory_order_relaxed));
  }

  template<typename T, uint64_t CacheSize>
  inline intrusive_list_node_t<T> *allocator_t<T, CacheSize>::pop()
  {
    intrusive_list_node_t<T> *node = top.load(memory_order_relaxed);
    while (node && !top.compare_exchange_weak(node, node->next, memory_order_acquire, memory_order_relaxed));
    return node;
  }

  template<typename T, uint64_t CacheSize>
  inline allocator_t<T, CacheSize>::allocator_t()
  : top(nullptr), newCounter(0), deleteCounter(0)
  {
    for (uint64_t i = 0; i < CacheSize; i++)
    {
      //intrusive_list_node_t<T> *node = (intrusive_list_node_t<T>*)tc_malloc(sizeof(intrusive_list_node_t<T>));
      //new (node) intrusive_list_node_t<T>();
      intrusive_list_node_t<T> *node = new intrusive_list_node_t<T>;
      node->isManaged = true;
      push(node);
    }
  }

  template<typename T, uint64_t CacheSize>
  inline allocator_t<T, CacheSize>::~allocator_t()
  {
    while (intrusive_list_node_t<T> *node = pop())
    {
      //node->~intrusive_list_node_t();
      //tc_free(node);
      delete node;
    }
  }

  template<typename T, uint64_t CacheSize>
  inline void allocator_t<T, CacheSize>::clearStats()
  {
    newCounter = deleteCounter = 0;
  }

  template<typename T, uint64_t CacheSize>
  inline intrusive_list_node_t<T> *allocator_t<T, CacheSize>::malloc()
  {
    if (intrusive_list_node_t<T> *node = pop()) return node;
    newCounter++;
    //intrusive_list_node_t<T> *node = (intrusive_list_node_t<T>*)tc_malloc(sizeof(intrusive_list_node_t<T>));
    //new (node) intrusive_list_node_t<T>();
    intrusive_list_node_t<T> *node = new intrusive_list_node_t<T>;
    return node;
  }

  template<typename T, uint64_t CacheSize>
  inline void allocator_t<T, CacheSize>::free(intrusive_list_node_t<T> *node)
  {
    if (!node->isManaged)
    {
      deleteCounter++;
      //node->~intrusive_list_node_t();
      //tc_free(node);
      delete node;
    }
    else push(node);
  }

  template<typename T, uint64_t CacheSize>
  inline pair<uint64_t, uint64_t> allocator_t<T, CacheSize>::report() const
  {
    return pair<uint64_t, uint64_t>(newCounter.load(), deleteCounter.load());
  }

  template<typename T, uint64_t CacheSize>
  inline string allocator_t<T, CacheSize>::toString() const
  {
    ostringstream out;
    out << "reserved = " << CacheSize << ", #new = " << newCounter.load() << " #delete = " << deleteCounter.load();
    return out.str();
  }
}
