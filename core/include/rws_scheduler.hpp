/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "actor_id.hpp"
#include "io_policy.hpp"
#include "generic.hpp"
using namespace generic_ext;
using namespace io;

namespace rpp
{
  class runnable_t;
  class rws_worker_t;
  template<scheduler_type_t>
  class cluster_t;
  class platform_t;
  class rws_scheduler_t
  {
  private:
    static random_t& rnd;
    static printer_t& tscout;

    cluster_id_t clusterID;
    worker_id_t nWorkers;
    rws_worker_t *workers;
    thread *pool;
    unordered_map<thread::id, worker_id_t> workerLocations;

    atomic<uint64_t> nActivations;
    atomic<bool> isRunning, schActive;
    io_policy_t *policy;
    const platform_t *platform;

    void joinAll() const;
    void pollWorkerStatus() const;
  public:
    rws_scheduler_t();
    ~rws_scheduler_t();

    void clearStats();
    void enqueue(shared_ptr<runnable_t> const& job);
    void raise(worker_id_t workerID) const;
    string readStats() const;
    void run(cluster_id_t clusterID_, worker_id_t nWorkers_, io_policy_t *policy_, const platform_t* platform_);
    void shutdown();

    rws_scheduler_t(rws_scheduler_t const&) = delete;
    rws_scheduler_t& operator=(rws_scheduler_t const&) = delete;
    friend class rws_worker_t;
    friend class cluster_t<NATIVE>;
  };
}
