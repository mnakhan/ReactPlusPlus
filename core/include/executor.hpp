/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "generic.hpp"
#include "runnable.hpp"
#include "semaphore.hpp"
using namespace generic_ext;
using namespace io;

namespace rpp
{
  template<typename>
  class actor_t;

  template<scheduler_type_t>
  class cluster_t;
  class context_t;
  class reaper_t;
  class executor_t
  {
  private:
    static printer_t& tscout;
    runnable_t *instance;
    reaper_t *reaper;
    bool useInternalThread;
    thread *worker;
    semaphore_t sm_mayExecute;

    pair<thread*, runnable_t*> getReaperHandle();
  public:
    executor_t(runnable_t *instance_, reaper_t *reaper_, bool useInternalThread_);
    ~executor_t();
    context_t *getContext() const;
    bool isEnabled() const;
    template<bool InternalWorkerActive>
    uint64_t run(bool block, uint64_t maxNumberOfMessages);
    void send(message_t *message, actor_id_t const& senderID);
    void shutdown();

    friend class system_t;
    friend class cluster_t<DETACHED>;
    friend class actor_t<detached>;
    friend class actor_t<channel>;
  };

  template<>
  inline uint64_t executor_t::run<true>([[maybe_unused]] bool block, // unused when internal thread is active
    uint64_t maxNumberOfMessages)
  {
    runnable_t *local_instance = instance; // Cache this pointer because ~executor_t may be invoked by the dispatch below when actor is self-terminating and implicitly drops the last reference to itself. The runnable object is still allocated (its lifetime is extended by reaper thread until the termination of this thread) but the address stored in "instance" is lost as soon as executor_t object is destroyed. "local_instance->isAlive" is a reliable flag to check even after this object is gone.
    uint64_t totalProcessed = 0;
    while (local_instance->isAlive)
    {
      uint64_t ret = local_instance->dispatch(maxNumberOfMessages);
      totalProcessed += ret;
      if (!local_instance->isAlive) break;
      if (!local_instance->mayReschedule(ret)) sm_mayExecute.P();
    }
    return totalProcessed;
  }

  template<>
  inline uint64_t executor_t::run<false>(bool block, uint64_t maxNumberOfMessages)
  {
    if (!instance->isAlive) return 0;
    uint64_t ret = instance->dispatch(maxNumberOfMessages);
    if (!instance->mayReschedule(ret) && block) sm_mayExecute.P();
    return ret;
  }
}
