/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace generic_ext
{
  struct stream_lock_t
  {
    recursive_mutex mx_out;
  };

  extern stream_lock_t g_streamLock;

  class printer_t
  {
  private:
    ostream& out;
  public:
    printer_t(ostream& out_);
    printer_t(printer_t const&) = delete;

    template<size_t insertSeparator = 1, typename First, typename... Rest>
    printer_t const& print(First first, Rest... rest) const;

    template<size_t insertSeparator = 1, typename Last>
    printer_t const& print(Last last) const;

    printer_t const& print() const;

    template<size_t insertSeparator = 1, typename... ArgTypes>
    printer_t const& println(ArgTypes... args) const;

    printer_t const& println() const;

    template<typename First, typename... Rest>
    printer_t const& printnws(First first, Rest... rest) const;

    printer_t const& printnws() const;

    template<typename... ArgTypes>
    printer_t const& printlnnws(ArgTypes... args) const;

    printer_t const& printlnnws() const;
  };

  template<size_t insertSeparator, typename First, typename... Rest>
  inline printer_t const& printer_t::print(First first, Rest... rest) const
  {
    lock_guard<recursive_mutex> bolt(g_streamLock.mx_out);
    out << first;
    if (insertSeparator) out << ' ';
    return print<insertSeparator>(rest...);
  }

  template<size_t insertSeparator, typename Last>
  inline printer_t const& printer_t::print(Last last) const
  {
    lock_guard<recursive_mutex> bolt(g_streamLock.mx_out);
    out << last;
    return *this;
  }

  template<size_t insertSeparator, typename... ArgTypes>
  inline printer_t const& printer_t::println(ArgTypes... args) const
  {
    lock_guard<recursive_mutex> bolt(g_streamLock.mx_out);
    print<insertSeparator>(args...);
    out << endl;
    return *this;
  }

  template<typename First, typename... Rest>
  inline printer_t const& printer_t::printnws(First first, Rest... rest) const
  {
    return print<0, First, Rest...>(first, rest...);
  }

  template<typename... ArgTypes>
  inline printer_t const& printer_t::printlnnws(ArgTypes... args) const
  {
    return println<0, ArgTypes...>(args...);
  }
}
