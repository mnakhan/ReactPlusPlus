/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace io
{

  enum class poller_type_t : uint32_t
  {
    MASTER = 0, SLAVE = 1
  };

  enum class poll_result_t : uint32_t
  {
    PR_SUCCESS = 0, PR_FAILURE = 1, PR_TRY_AGAIN = 2
  };

  struct generic_event_t
  {
  };

  struct io_event_t
  {
    poller_type_t poller_type;
#ifdef LINUX
    epoll_event *details;
#else
    generic_event_t *details;
#endif // LINUX
  private:
    io_event_t(poller_type_t poller_type_, void *details_);
  public:
    template<poller_type_t OriginType>
    static io_event_t raise(void *details_);
  };

  template<>
  inline io_event_t io_event_t::raise<poller_type_t::MASTER>(void *details_)
  {
    return io_event_t(poller_type_t::MASTER, details_);
  }

  template<>
  inline io_event_t io_event_t::raise<poller_type_t::SLAVE>(void *details_)
  {
    return io_event_t(poller_type_t::SLAVE, details_);
  }

  struct io_policy_t
  {
    uint32_t spin = 1;
    atomic<uint64_t> eventCounter;
    virtual ~io_policy_t();
    virtual bool onBlock(cluster_id_t clusterID, worker_id_t workerID) = 0;
    virtual bool onEvent(cluster_id_t clusterID, worker_id_t workerID, io_event_t const& event) = 0;
    virtual bool onIdle(cluster_id_t clusterID, worker_id_t workerID) = 0;
    virtual bool postLaunch(cluster_id_t clusterID) = 0;
    virtual bool preLaunch(cluster_id_t clusterID) = 0;
    virtual cluster_id_t getClusterCount() const;
    virtual worker_id_t getWorkerCount(cluster_id_t clusterID) const;
  };
}
