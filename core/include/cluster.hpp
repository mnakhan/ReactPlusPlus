/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "context.hpp"
#include "executor.hpp"
#include "hash_table.hpp"
#include "reaper.hpp"
#include "rws_scheduler.hpp"
#include "system_statistics.hpp"

namespace io
{
  struct io_policy_t;
}

namespace rpp
{
  class platform_t;

  template<typename>
  class actor_t;

  template<scheduler_type_t SchedType = NATIVE>
  class cluster_t
  {
  };

  struct cluster_context_t
  {
  };

  template<>
  class cluster_t<NATIVE>
  {
  private:
    static printer_t& tscout;

    cluster_id_t clusterID;
    worker_id_t nWorkers;
    atomic<local_id_t> nextLocalID, actorCount;
    atomic<bool> isRunning, hasShutdown;
    io::io_policy_t *policy;
    const platform_t *platform;
    cluster_context_t *clusterContext;

    hash_table_t<local_id_t, shared_ptr<runnable_t>> registry;
    rws_scheduler_t scheduler;
    mutable mutex mx_actorCount, mx_isShuttingdown;
    mutable condition_variable cv_noActors, cv_isShuttingdown;
    void decrementActorCount();
  public:
    cluster_t();
    ~cluster_t();
    cluster_t(cluster_t<NATIVE> const&) = delete;
    void clearStats();
    uint64_t getActorCount() const;
    worker_id_t getWorkerCount() const;
    void initialize(cluster_id_t clusterID_, worker_id_t nWorkers_, io::io_policy_t *policy_, const platform_t *platform_ = nullptr);
    void kill(actor_id_t const& actorID);
    cluster_t<NATIVE>& operator=(cluster_t<NATIVE> const&) = delete;
    void pollSchedulerStatus() const;
    string report() const;
    void run();
    void shutdown();
    template<typename ContextType, mailbox_type_t Mtype, bool ReturnHandle, typename... ArgTypes>
    void spawn(worker_id_t workerID, uint64_t privateDispatchSize, double senderAffinity, actor_id_t *actorIDPtr, shared_ptr<runnable_t> *handle, ArgTypes... args);

    void waitForAll() const;

    friend class context_t;
    friend class system_t;
    friend class actor_t<native>;
  };

  inline void cluster_t<NATIVE>::decrementActorCount()
  {
    uint64_t current = actorCount.fetch_sub(1);
    if (current > 1) return;
    lock_guard<mutex> bolt(mx_actorCount);
    cv_noActors.notify_one();
  }

  inline cluster_t<NATIVE>::cluster_t()
    :clusterID(DEFAULT_CLUSTER_ID), nWorkers(0), nextLocalID(0), actorCount(0), isRunning(false), hasShutdown(false), policy(nullptr), clusterContext(nullptr)
  {
  }

  inline cluster_t<NATIVE>::~cluster_t()
  {
    shutdown();
  }

  inline void cluster_t<NATIVE>::clearStats()
  {
    scheduler.clearStats();
  }

  inline uint64_t cluster_t<NATIVE>::getActorCount() const
  {
    return actorCount;
  }

  inline worker_id_t cluster_t<NATIVE>::getWorkerCount() const
  {
    return nWorkers;
  }

  inline void cluster_t<NATIVE>::initialize(cluster_id_t clusterID_, worker_id_t nWorkers_, io::io_policy_t *policy_, const platform_t *platform_)
  {
    clusterID = clusterID_;
    nWorkers = nWorkers_;
    policy = policy_;
    platform = platform_;
    nextLocalID = 0;
    actorCount = 0;
    isRunning = true;
    hasShutdown = false;
    scheduler.schActive = false;
  }

  inline void cluster_t<NATIVE>::kill(actor_id_t const& actorID)
  {
    shared_ptr<runnable_t> instance;
    if (registry.get(actorID.localID, instance))
    {
      registry.remove(actorID.localID);
      decrementActorCount();
#ifdef ENABLE_SYSTEM_STATS
      GLOBAL(system_statistics_t).total_native_actors_killed++;
#endif // ENABLE_SYSTEM_STATS
      return;
    }
  }

  inline void cluster_t<NATIVE>::pollSchedulerStatus() const
  {
    while (!scheduler.schActive);
  }

  inline string cluster_t<NATIVE>::report() const
  {
    return scheduler.readStats();
  }

  inline void cluster_t<NATIVE>::run()
  {
    if (policy) policy->preLaunch(clusterID);
    scheduler.run(clusterID, nWorkers, policy, platform);
    if (policy) policy->postLaunch(clusterID);
    unique_lock<mutex> bolt(mx_isShuttingdown, defer_lock);
    bolt.lock();
    cv_isShuttingdown.wait(bolt, [this]
    {
      return !isRunning;
    });
    bolt.unlock();
    scheduler.shutdown();
    registry.clear();
    hasShutdown = true;
  }

  inline void cluster_t<NATIVE>::shutdown()
  {
    if (!isRunning) return;
    isRunning = false;
    while (!hasShutdown)
    {
      lock_guard<mutex> bolt(mx_isShuttingdown);
      cv_isShuttingdown.notify_one();
    }
#ifdef TRACE_SHUTDOWN
    tscout.println("TRACE: cluster", clusterID, "has shut down");
#endif // TRACE_SHUTDOWN
  }

  template<typename ContextType, mailbox_type_t Mtype, bool ReturnHandle, typename... ArgTypes>
  inline void cluster_t<NATIVE>::spawn(worker_id_t workerID, uint64_t privateDispatchSize, double senderAffinity, actor_id_t *actorIDPtr, shared_ptr<runnable_t> *handle, ArgTypes... args)
  {
    runnable_t *instance = new ContextType(forward<ArgTypes>(args)...);
    local_id_t localID = nextLocalID.fetch_add(1);
    actor_id_t actorID(clusterID, localID);

    instance->selfID = actorID;
    instance->origin = instance->residence = this;
    if (workerID < nWorkers) instance->workerID = workerID;
    if(privateDispatchSize) instance->privateDispatchSize = privateDispatchSize;
    instance->senderAffinity = senderAffinity;
    context_t& context = *((context_t*)instance);
    switch (Mtype)
    { 
    case mailbox_type_t::STD:
      context.createMailbox<STD_DEQUE>();
      break;
    case mailbox_type_t::CAF:
      context.createMailbox<CAF_MPSCQ>();
      break;
    case mailbox_type_t::ZMQ:
      context.createMailbox<ZMQ_MPSCQ>();
      break;
    case mailbox_type_t::DV:
    default:
      context.createMailbox<DV_MPSCQ>();
      break;
    }

    actorCount++;
    shared_ptr<runnable_t> sp_instance(instance);
    registry.insert(localID, sp_instance);

#ifdef ENABLE_SYSTEM_STATS
    GLOBAL(system_statistics_t).total_native_actors_spawned++;
#endif // ENABLE_SYSTEM_STATS

    if (ReturnHandle)
    {
      rpp_assert(handle, "FATAL: actor handle was NULL", tscout);
      *handle = move(sp_instance);
      return;
    }
    rpp_assert(actorIDPtr, "FATAL: actorID ptr was NULL", tscout);
    *actorIDPtr = actorID;
  }

  inline void cluster_t<NATIVE>::waitForAll() const
  {
    unique_lock<mutex> bolt(mx_actorCount);
    cv_noActors.wait(bolt, [this]
    {
      return actorCount == 0;
    });
  }

  class reaper_t;
  template<>
  struct cluster_t<DETACHED>
  {
  private:
    static printer_t& tscout;
    cluster_id_t clusterID;
    atomic<local_id_t> nextLocalID, actorCount;
    atomic<bool> isRunning;
    reaper_t reaper;
    thread reaper_thread;

    hash_table_t<local_id_t, shared_ptr<executor_t>> registry;
    mutable mutex mx_actorCount;
    mutable condition_variable cv_noActors;
    void decrementActorCount();

  public:
    cluster_t();
    ~cluster_t();
    cluster_t(cluster_t<DETACHED> const&) = delete;
    uint64_t getActorCount() const;
    void kill(actor_id_t const& actorID);
    cluster_t<DETACHED>& operator=(cluster_t<DETACHED> const&) = delete;
    void run();
    void shutdown();
    template<typename ContextType, mailbox_type_t Mtype, bool ReturnHandle, typename... ArgTypes>
    void spawn(bool useInternalThread, uint64_t privateDispatchSize, double senderAffinity, actor_id_t *actorIDPtr, shared_ptr<executor_t> *handle, ArgTypes... args);

    void waitForAll() const;

    friend class system_t;
    friend class actor_t<detached>;
    friend class actor_t<channel>;
  };

  inline void cluster_t<DETACHED>::decrementActorCount()
  {
    uint64_t current = actorCount.fetch_sub(1);
    if (current > 1) return;
    lock_guard<mutex> bolt(mx_actorCount);
    cv_noActors.notify_one();
  }

  inline cluster_t<DETACHED>::cluster_t()
    :clusterID(DETACHED_CLUSTER_ID), nextLocalID(0), actorCount(0), isRunning(false)
  {
  }

  inline cluster_t<DETACHED>::~cluster_t()
  {
    shutdown();
  }

  inline uint64_t cluster_t<DETACHED>::getActorCount() const
  {
    return actorCount;
  }

  inline void cluster_t<DETACHED>::kill(actor_id_t const& actorID)
  {
    shared_ptr<executor_t> executor;
    if (registry.get(actorID.localID, executor))
    {
      executor->shutdown();
      registry.remove(actorID.localID);
      decrementActorCount();
#ifdef ENABLE_SYSTEM_STATS
      if (executor->useInternalThread) GLOBAL(system_statistics_t).total_detached_actors_killed++;
      else GLOBAL(system_statistics_t).total_channels_closed++;
#endif // ENABLE_SYSTEM_STATS
      return;
    }
  }

  inline void cluster_t<DETACHED>::run()
  {
    rpp_assert(!isRunning, "Illegal reentry into cluster_t<DETACHED>::run while it is still active.", tscout);
    nextLocalID = 0;
    actorCount = 0;
    reaper.initialize();
    reaper_thread = thread(&reaper_t::run, &reaper);
    reaper.pollReaperStatus();
    isRunning = true;
  }

  inline void cluster_t<DETACHED>::shutdown()
  {
    isRunning = false;
    reaper.shutdown();
    joinAll(reaper_thread);
    registry.clear();
#ifdef TRACE_SHUTDOWN
    tscout.println("TRACE: detached cluster has shut down");
#endif // TRACE_SHUTDOWN
  }

  inline void cluster_t<DETACHED>::waitForAll() const
  {
    unique_lock<mutex> bolt(mx_actorCount);
    cv_noActors.wait(bolt, [this]
    {
      return actorCount == 0;
    });
  }

  template<typename ContextType, mailbox_type_t Mtype, bool ReturnHandle, typename ...ArgTypes>
  inline void cluster_t<DETACHED>::spawn(bool useInternalThread, uint64_t privateDispatchSize, double senderAffinity, actor_id_t *actorIDPtr, shared_ptr<executor_t> *handle, ArgTypes... args)
  {
    runnable_t *instance = new ContextType(forward<ArgTypes>(args)...);
    local_id_t localID = nextLocalID.fetch_add(1);
    actor_id_t actorID(clusterID, localID);
    instance->selfID = actorID;
    instance->detachedOrigin = this;
    if (privateDispatchSize) instance->privateDispatchSize = privateDispatchSize;
    instance->senderAffinity = senderAffinity;
    context_t& context = *((context_t*)instance);
    switch (Mtype)
    {
    case mailbox_type_t::STD:
      context.createMailbox<STD_DEQUE>();
      break;
    case mailbox_type_t::CAF:
      context.createMailbox<CAF_MPSCQ>();
      break;
    case mailbox_type_t::ZMQ:
      context.createMailbox<ZMQ_MPSCQ>();
      break;
    case mailbox_type_t::DV:
    default:
      context.createMailbox<DV_MPSCQ>();
      break;
    }

    shared_ptr<executor_t> executor = make_shared<executor_t>(instance, &reaper, useInternalThread);
    // A memory leak would occur here if the worker thread was given a shared_ptr to executor, i.e. given "executor" instread of "ref(*executor)", possibly due to cyclic self-reference.
    if (useInternalThread) executor->worker = new thread(&executor_t::run<true>, ref(*executor), true, UINT64_MAX);
    actorCount++;
    registry.insert(localID, executor);

#ifdef ENABLE_SYSTEM_STATS
    if (useInternalThread) GLOBAL(system_statistics_t).total_detached_actors_spawned++;
    else GLOBAL(system_statistics_t).total_channels_opened++;
#endif // ENABLE_SYSTEM_STATS

    if (ReturnHandle)
    {
      rpp_assert(handle, "FATAL: actor handle was NULL", tscout);
      *handle = move(executor);
      return;
    }
    rpp_assert(actorIDPtr, "FATAL: actorID ptr was NULL", tscout);
    *actorIDPtr = actorID;
  }
}
