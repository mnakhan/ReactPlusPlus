/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once

namespace rpp
{
  enum class scheduler_type_t : uint32_t
  {
    USERSPACE = 0, KERNEL = 1, INERT = 2, NONE = 3
  };

#define NATIVE scheduler_type_t::USERSPACE
#define DETACHED scheduler_type_t::KERNEL
#define CHANNEL scheduler_type_t::INERT
#define EXTERNAL scheduler_type_t::NONE

  typedef integral_constant<scheduler_type_t, NATIVE> native;
  typedef integral_constant<scheduler_type_t, DETACHED> detached;
  typedef integral_constant<scheduler_type_t, CHANNEL> channel;
  typedef integral_constant<scheduler_type_t, EXTERNAL> external;

  enum class tristate_t : uint32_t
  {
    FALSE_VALUE = 0, TRUE_VALUE = 1, EITHER = 2
  };

  typedef integral_constant<tristate_t, tristate_t::FALSE_VALUE> True;
  typedef integral_constant<tristate_t, tristate_t::TRUE_VALUE> False;
  typedef integral_constant<tristate_t, tristate_t::EITHER> Either;


  enum class location_selector_t : uint32_t
  {
    FIXED_LOC = 0, ROTATE_LOC = 1, RANDOM_LOC = 2
  };

  typedef integral_constant<location_selector_t, location_selector_t::FIXED_LOC> Fixed;
  typedef integral_constant<location_selector_t, location_selector_t::ROTATE_LOC> RoundRobin;
  typedef integral_constant<location_selector_t, location_selector_t::RANDOM_LOC> Anywhere;

  // steal frequency of randomized work-stealing scheduler in userspace
  // effective frequency can be controlled by a multiplier
  // see policy_t::enableWith<work_stealing>(steal_frequency_t, double) in policy.hpp
  enum class steal_frequency_t : uint32_t
  {
    DISABLED = 0,
    LOW = 1,
    NORMAL = 3,
    HIGH = 10,
    RESTLESS = 11
  };

  enum class mailbox_type_t
  {
    STD = 0, CAF = 1, DV = 2, ZMQ = 3
  };

  struct actor_id_t
  {
  private:
    bool isChannel_s() const;
    bool isDetached_s() const;
    bool isExternal_s() const;
    bool isNative_s() const;

  public:
    cluster_id_t clusterID;
    local_id_t localID;

    actor_id_t(cluster_id_t clusterID_ = DEFAULT_CLUSTER_ID, local_id_t localID_ = DEFAULT_LOCAL_ID);
    actor_id_t(const char *input);
    actor_id_t(string const& input);
    friend ostream& operator<<(ostream& out, actor_id_t const& actorID);
    bool operator==(actor_id_t const& other) const;
    bool operator!=(actor_id_t const& other) const;
    bool operator<(actor_id_t const& other) const;
    bool operator>(actor_id_t const& other) const;
    bool isValid() const;
    operator uint64_t() const;
    bool parseID(const char *input, actor_id_t& result);
    bool parseID(string const& input, actor_id_t& result);

    template<typename>
    bool is() const;
  };

  typedef actor_id_t channel_id_t;

  ostream& operator<<(ostream& out, steal_frequency_t frequency);
  ostream& operator<<(ostream& out, actor_id_t const& actorID);

  template<>
  inline bool actor_id_t::is<native>() const
  {
    return isNative_s();
  }

  template<>
  inline bool actor_id_t::is<detached>() const
  {
    return isDetached_s();
  }

  template<>
  inline bool actor_id_t::is<channel>() const
  {
    return isChannel_s();
  }

  template<>
  inline bool actor_id_t::is<external>() const
  {
    return isExternal_s();
  }
}

namespace rpp
{
#if !defined(USE_NATIVE_ACTORS)
  typedef detached domain_t;
#else
  typedef native domain_t;
#endif
}
