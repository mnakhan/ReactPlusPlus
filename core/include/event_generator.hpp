/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "io_policy.hpp"
#include "generic.hpp"
#include "environment.hpp"
#include "system_statistics.hpp"

using namespace generic_ext;

#ifdef LINUX
namespace io
{

  template<bool Incoming>
  inline bool enablePolling(descriptor_t pollerHandle, descriptor_t descriptor)
  {
    epoll_event e;
    e.events = 0;
#ifdef USE_EDGE_TRIGGER
    e.events |= EPOLLET;
#endif // USE_EDGE_TRIGGER
    if (Incoming) e.events |= (EPOLLIN | EPOLLPRI | EPOLLRDHUP | EPOLLERR | EPOLLHUP);
    else e.events |= EPOLLOUT;
    e.data.fd = descriptor;
    return epoll_ctl(pollerHandle, EPOLL_CTL_ADD, descriptor, &e) != -1;
  }

  template<poller_type_t PollerType>
  class event_generator_t
  {
  };

  template<>
  class event_generator_t<poller_type_t::MASTER>
  {
  private:
    cluster_id_t clusterID;
    descriptor_t L0Handle;
    vector<descriptor_t> L1Handles;

    io_policy_t *policy;
    thread worker;

    epoll_event *events;
    uint32_t maxEvents;

    static printer_t& tscout;
    static rpp::system_statistics_t& stats;

    void run();
  public:
    event_generator_t();
    ~event_generator_t();
    descriptor_t getL0Handle() const;
    void initialize(cluster_id_t clusterID_, vector<descriptor_t> const& L1Handles_, io_policy_t *policy_);
    void launch();
    void rearm(worker_id_t L1Index);

    event_generator_t(event_generator_t const&) = delete;
    event_generator_t& operator=(event_generator_t const&) = delete;
  };

  using _L0_ENGINE_ = event_generator_t<io::poller_type_t::MASTER>;

  inline void _L0_ENGINE_::run()
  {
    while (true)
    {
      int nEvents = epoll_wait(L0Handle, events, maxEvents, -1);
      if (nEvents <= 0) return;

      for (int i = 0; i < nEvents; i++)
      {
        epoll_event& e = events[i];
        worker_id_t workerID = (worker_id_t) e.data.u64;
        if (policy) policy->onEvent(clusterID, workerID, io_event_t::raise<poller_type_t::MASTER>(&e));
      }

      if (stats.isShuttingDown) break;
    }
  }

  inline _L0_ENGINE_::event_generator_t()
  : clusterID(DEFAULT_CLUSTER_ID), L0Handle(-1), policy(nullptr)
  {
    maxEvents = environment::L0MaxEvents;
    events = new epoll_event[maxEvents];
  };

  inline _L0_ENGINE_::~event_generator_t()
  {
#ifdef TRACE_DTOR
    scout << "~_L0_ENGINE_: cluster ID = " << clusterID << endl;
#endif // TRACE_DTOR
    joinAll(worker);
    close(L0Handle);
    delete[] events;
    events = nullptr;
  }

  inline descriptor_t _L0_ENGINE_::getL0Handle() const
  {
    return L0Handle;
  }

  inline void _L0_ENGINE_::initialize(cluster_id_t clusterID_, vector<descriptor_t> const& L1Handles_, io_policy_t *policy_)
  {
    clusterID = clusterID_;
    L0Handle = epoll_create(1);
    rpp_assert(L0Handle != -1, "FATAL: could not initialize _L0_ENGINE_", tscout);

#ifdef TRACE_IO_EVENT
    tscout.println("TRACE: created _L0_ENGINE_ handle", L0Handle, "on cluster", clusterID);
#endif // TRACE_IO_EVENT

    L1Handles = L1Handles_;
    worker_id_t L1Size = L1Handles.size();
    rpp_assert(L1Size > 0, "L1 size cannot be 0.", tscout);

    policy = policy_;

    for (worker_id_t w = 0; w < L1Size; w++)
    {
      epoll_event e;
      e.events = EPOLLIN | EPOLLET | EPOLLONESHOT;
      e.data.u64 = (uint64_t) w;
      rpp_assert(epoll_ctl(L0Handle, EPOLL_CTL_ADD, L1Handles[w], &e) != -1, "FATAL: _L0_ENGINE_ could not register _L1_ENGINE_ handle", tscout);

#ifdef TRACE_IO_EVENT
      tscout.println("TRACE: registered _L1_ENGINE_", L1Handles[w], "from cluster", clusterID, "and assigned to worker", w);
#endif // DEBUG
    }
  }

  inline void _L0_ENGINE_::launch()
  {
    worker = thread(&event_generator_t<poller_type_t::MASTER>::run, this);
  }

  inline void _L0_ENGINE_::rearm(worker_id_t L1Index)
  {
    assert(L1Index < L1Handles.size());
    descriptor_t L1Handle = L1Handles[L1Index];
    epoll_event e;
    e.events = EPOLLIN | EPOLLET | EPOLLONESHOT;
    e.data.u64 = (uint64_t) L1Index;
    if (epoll_ctl(L0Handle, EPOLL_CTL_MOD, L1Handle, &e) == -1)
    {
      tscout.println("WARNING: _L0_ENGINE_ cannot be rearmed @", __FILE__, __LINE__);
      return;
    }

#ifdef TRACE_IO_EVENT
    tscout.println("TRACE: rearmed _L0_ENGINE_", L1Handle, "from cluster", clusterID, "with packed data", e.data.u64);
#endif // TRACE_IO_EVENT
  }

  template<>
  class event_generator_t<poller_type_t::SLAVE>
  {
  private:
    cluster_id_t clusterID;
    worker_id_t workerID;
    descriptor_t L1Handle;
    worker_id_t L1Size;

    io_policy_t *policy;

    epoll_event *events;
    uint32_t maxEvents;
    mutable mutex mx_epoll;

    static printer_t& tscout;
  public:
    event_generator_t();
    ~event_generator_t();

    template<bool Incoming>
    bool add(descriptor_t descriptor);
    descriptor_t getL1Handle() const;
    void initialize(cluster_id_t clusterID_, worker_id_t workerID_, worker_id_t L1Size_, io_policy_t *policy_);
    bool remove(descriptor_t descriptor);

    template<bool Serialize>
    bool spin(poll_result_t& result, uint32_t burstLength = 1);


    event_generator_t(event_generator_t const&) = delete;
    event_generator_t& operator=(event_generator_t const&) = delete;
  };

  using _L1_ENGINE_ = event_generator_t<io::poller_type_t::SLAVE>;

  inline _L1_ENGINE_::event_generator_t()
  : clusterID(DEFAULT_CLUSTER_ID), L1Handle(-1)
  {
    maxEvents = environment::L1MaxEvents;
    events = new epoll_event[maxEvents];
  };

  inline _L1_ENGINE_::~event_generator_t()
  {
#ifdef TRACE_DTOR
    scout << "~_L1_ENGINE_: cluster ID = " << clusterID << ", worker ID = " << workerID << endl;
#endif // TRACE_DTOR
    close(L1Handle);
    delete[] events;
    events = nullptr;
  }

  template<bool Incoming>
  inline bool _L1_ENGINE_::add(descriptor_t descriptor)
  {
    return enablePolling<Incoming>(L1Handle, descriptor);
  }

  inline descriptor_t _L1_ENGINE_::getL1Handle() const
  {
    return L1Handle;
  }

  inline void _L1_ENGINE_::initialize(cluster_id_t clusterID_, worker_id_t workerID_, worker_id_t L1Size_, io_policy_t *policy_)
  {
    clusterID = clusterID_;
    workerID = workerID_;
    L1Size = L1Size_;
    policy = policy_;
    L1Handle = epoll_create(1);
    rpp_assert(L1Handle != -1, "cannot initialize _L1_ENGINE_", tscout);

#ifdef TRACE_IO_EVENT
    tscout.println("TRACE: created L1 handle", L1Handle, "on cluster", clusterID);
#endif // TRACE_IO_EVENT
  }

  inline bool _L1_ENGINE_::remove(descriptor_t descriptor)
  {
    return epoll_ctl(L1Handle, EPOLL_CTL_DEL, descriptor, nullptr) != -1;
  }

  template<>
  inline bool _L1_ENGINE_::spin<true>(poll_result_t& result, uint32_t burstLength)
  {
    rpp_assert(clusterID != DEFAULT_CLUSTER_ID, "I/O subsystem is not initialized.", tscout);
    for (uint32_t x = 0; x < burstLength; x++)
    {
      if (!mx_epoll.try_lock())
      {
        result = poll_result_t::PR_TRY_AGAIN;
        return false;
      }

      int nEvents = epoll_wait(L1Handle, events, maxEvents, 0);
      if (nEvents <= 0)
      {
        mx_epoll.unlock();
        result = poll_result_t::PR_FAILURE;
        return false;
      }
      mx_epoll.unlock();

      if (policy) policy->eventCounter += nEvents;
      for (int i = 0; i < nEvents; i++)
      {
        epoll_event& e = events[i];
        if (policy) policy->onEvent(clusterID, workerID, io_event_t::raise<poller_type_t::SLAVE>(&e));
      }
    }
    result = poll_result_t::PR_SUCCESS;
    return true;
  }

  template<>
  inline bool _L1_ENGINE_::spin<false>(poll_result_t& result, uint32_t burstLength)
  {
    rpp_assert(clusterID != DEFAULT_CLUSTER_ID, "I/O subsystem is not initialized.", tscout);
    for (uint32_t x = 0; x < burstLength; x++)
    {
      int nEvents = epoll_wait(L1Handle, events, maxEvents, 0);
      if (nEvents <= 0)
      {
        result = poll_result_t::PR_FAILURE;
        return false;
      }

      if (policy) policy->eventCounter += nEvents;
      for (int i = 0; i < nEvents; i++)
      {
        epoll_event& e = events[i];
        if (policy) policy->onEvent(clusterID, workerID, io_event_t::raise<poller_type_t::SLAVE>(&e));
      }
    }
    result = poll_result_t::PR_SUCCESS;
    return true;
  }
}
#endif // LINUX
