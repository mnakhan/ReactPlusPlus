/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "ready_queue.hpp"

namespace rpp
{
  class runnable_t;
  class rws_scheduler_t;

  class rws_worker_t
  {
  private:
    static random_t& rnd;
    static printer_t& tscout;
    cluster_id_t clusterID;
    worker_id_t workerID;
    numa_node_id_t nodeID;
    processor_id_t cpuID;
    uint64_t nInvocations, nThefts, nIdleSpins;
    rws_scheduler_t *scheduler;
    atomic<bool> isRunning, hasShutdown, workerActive;
    ready_queue_t readyQueue;
    vector<processor_id_t> neighborCPUs;
    uint32_t searchLevel;
    bool isNUMAAware;
    static uint64_t maxMessagesPerDispatch;
    static steal_frequency_t frequency;
    static double freqMultiplier;
  public:
    rws_worker_t();
    ~rws_worker_t();
    worker_id_t chooseVictim() const;
    void clearStats();
    static double getFrequencyMultiplier();
    uint64_t getIdleSpinCount() const;
    uint64_t getInvocationCount() const;
    uint64_t getReadyQueueLength() const;
    static steal_frequency_t getStealFrequency();
    uint64_t getTheftCount() const;
    static void setFrequencyMultiplier(double freqMultiplier_);
    static void setStealFrequency(steal_frequency_t frequency_);
    void initialize(cluster_id_t clusterID_, worker_id_t workerID_, rws_scheduler_t *scheduler_);
    void makeReady(shared_ptr<runnable_t> const& job);
    static void resetDispatchPolicy();
    void run();
    static void setDispatchPolicy(uint64_t maxMessagesPerDispatch_);
    void shutdown();

    rws_worker_t(rws_worker_t const&) = delete;
    rws_worker_t& operator=(rws_worker_t const&) = delete;
    friend class rws_scheduler_t;
    friend class cluster_t<NATIVE>;
  };
}
