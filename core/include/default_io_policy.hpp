/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "event_generator.hpp"

#ifdef LINUX

namespace io
{

  class default_io_policy_t : public io_policy_t
  {
  protected:
    cluster_id_t nClusters;
    _L0_ENGINE_ *level0; // top-level epoll (blocks on empty)
    _L1_ENGINE_ **level1; // second-level epoll (never blocks)
    worker_id_t L1Size; // how many second-level pollers per cluster?

  public:
    default_io_policy_t();
    ~default_io_policy_t();

    _L0_ENGINE_ *getL0Generator(cluster_id_t clusterID);
    _L1_ENGINE_ *getL1Generator(cluster_id_t clusterID, worker_id_t workerID);
    descriptor_t getL0Handle(cluster_id_t clusterID);
    vector<descriptor_t> getL1Handles(cluster_id_t clusterID);
    worker_id_t getL1Size() const;

    void initialize();
    bool onBlock(cluster_id_t clusterID, worker_id_t workerID) override;
    bool onIdle(cluster_id_t clusterID, worker_id_t workerID) override;
    bool postLaunch(cluster_id_t clusterID) override;
    bool preLaunch(cluster_id_t clusterID) override;

    template<uint32_t Level, bool Incoming>
    inline enable_if_t<Level == 0
    || Level == 1, void> watchOnLevel(cluster_id_t clusterID, descriptor_t descriptor)
    {
      switch (Level)
      {
        case 0:
          rpp_assert(false, "Default policy does not allow direct interaction with level 0 pollers.", GLOBAL(printer_t));
          break;
        case 1:
          rpp_assert(clusterID < nClusters, "Invalid cluster ID was specified.", GLOBAL(printer_t));
          // publish to all pollers on level 1 for this cluster
          for (worker_id_t i = 0; i < L1Size; i++) level1[clusterID][i].add<Incoming>(descriptor);
          break;
        default:
          break;
      }
    }

    template<bool Incoming>
    void watchWithNoBlock(cluster_id_t clusterID, worker_id_t workerID, descriptor_t descriptor);
  };

  template<bool Incoming>
  void io::default_io_policy_t::watchWithNoBlock(cluster_id_t clusterID, worker_id_t workerID, descriptor_t descriptor)
  {
    level1[clusterID][workerID].add<Incoming>(descriptor);
  }
}
#endif // LINUX
