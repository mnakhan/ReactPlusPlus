/******************************************************************************
    Copyright (C) Navid Khan 2020

    React++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#pragma once
#include "actor_id.hpp"
#include "system.hpp"
#include "typed_context.hpp"

namespace rpp
{
  template<typename = native>
  class actor_t
  {
  };

  template<typename ContextType>
  class context_wrapper_t
  {
  private:
    context_t *ctx;
  public:
    context_wrapper_t(context_t *ctx_);
    template<typename BehaviorType>
    void withBehavior(BehaviorType* = nullptr);
    void withDefault();
  };

  template<typename ContextType>
  inline context_wrapper_t<ContextType>::context_wrapper_t(context_t *ctx_)
    :ctx(ctx_)
  {
  }

  template<typename ContextType>
  inline void context_wrapper_t<ContextType>::withDefault()
  {
    if (ctx) ctx->enable(true);
  }

  template<typename ContextType>
  template<typename BehaviorType>
  inline void context_wrapper_t<ContextType>::withBehavior(BehaviorType*)
  {
    if (ctx && ctx->enable(false)) ContextType::template become<BehaviorType>(ctx);
  }

  template<>
  class actor_t<native>
  {
  private:
    static system_t &sys;
    static printer_t& tscout;
    shared_ptr<runnable_t> instance;
    actor_id_t streamSignature;
    actor_t(shared_ptr<runnable_t>&& instance_);
  public:

    class throughput_t
    {
    private:
      actor_t<native> const& handle;
      pair<uint64_t, uint64_t> watermarks;
      throughput_t(actor_t<native> const& handle_) : handle(handle_) {}
      throughput_t(actor_t<native> const& handle_, pair<uint64_t, uint64_t> const& watermarks_)
        : handle(handle_), watermarks(watermarks_) {}
    public:
      template<typename MessageType>
      const throughput_t& operator<<(typed_si_message_t<MessageType> const& contents) const
      {
        if (!handle.instance || !handle.instance->isEnabled() || !handle.instance->template probe<context_t>(watermarks)) return *this;
        typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
        if (handle.instance->maySchedule(&message, handle.streamSignature)) handle.instance->residence->scheduler.enqueue(handle.instance);
        return *this;
      }

      template<typename MessageType>
      const throughput_t& operator|(MessageType contents) const
      {
        if (!handle.instance || !handle.instance->isEnabled() || !handle.instance->template probe<context_t>(watermarks)) return *this;
        message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
        if (handle.instance->maySchedule(message, handle.streamSignature)) handle.instance->residence->scheduler.enqueue(handle.instance);
        return *this;
      }

      friend class actor_t<native>;
    };

    actor_t();
    actor_t(actor_t<native> const& actor);
    actor_t(actor_t<native>&& actor);
    type_erased_receiver_t& defaultBehavior(bool reset = false);
    template<bool IsAtom = false, typename MessageType>
    static size_t broadcast(MessageType contents, vector<actor_t<native>> const& receivers, actor_id_t const& senderID = actor_id_t());
    template<typename ContextType = typed_context_t<>>
    context_wrapper_t<ContextType> enableContext() const;

    template<typename ContextType, typename BehaviorType>
    static void enableContexts(vector<actor_id_t> const& actorIDs);

    template<typename ContextType, typename BehaviorType>
    static void enableContexts(vector<actor_t<native>> const& actors);

    template<typename ContextType = typed_context_t<>>
    static void enableContextsWithDefault(vector<actor_id_t> const& actorIDs);

    template<typename ContextType = typed_context_t<>>
    static void enableContextsWithDefault(vector<actor_t<native>> const& actors);

    context_t *getContext() const;
    actor_id_t getID() const;
    worker_id_t getWorkerID() const;

    template<typename ContextType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<context_t, ContextType>> initialize(ArgTypes... args) const
    {
      if (!instance || instance->isEnabled()) return;
      applyFunction(&ContextType::initialize, (ContextType*)instance.get(), forward<ArgTypes>(args)...);
    }

    template<typename ContextType, typename... ArgTypes>
    inline static enable_if_t<is_base_of_v<context_t, ContextType>> initializeEach(vector<actor_t<native>> const& actors, ArgTypes... args)
    {
      vector<actor_t<native>>::const_iterator it = actors.cbegin(), e = actors.cend();
      while (it != e)
      {
        it->initialize<ContextType, ArgTypes...>(args...);
        it++;
      }
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initialize(ArgTypes... args) const
    {
      if (!instance || instance->isEnabled()) return;
      context_t *ctx = (context_t*)instance.get();
      ctx->initializeBehavior<BehaviorType>(forward<ArgTypes>(args)...);
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline static enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initializeEach(vector<actor_t<native>> const& actors, ArgTypes... args)
    {
      vector<actor_t<native>>::const_iterator it = actors.cbegin(), e = actors.cend();
      while (it != e)
      {
        it->initialize<BehaviorType, ArgTypes...>(args...);
        it++;
      }
    }

    template<typename ContextType>
    static ContextType *map(actor_t<native> const& actor);
    static actor_t<native> map(actor_id_t const& actorID);
    bool migrate(cluster_id_t clusterID);
    operator bool() const;
    actor_t<native>& operator=(actor_t<native> const& actor);
    actor_t<native>& operator=(actor_t<native>&& actor);
    template<typename MessageType>
    const actor_t<native>& operator|(MessageType contents) const;
    template<typename MessageType>
    const actor_t<native>& operator<<(typed_si_message_t<MessageType> const& contents) const;
    template<typename OutputType, typename... InputType>
    future_t<OutputType> operator+(Promise<OutputType, InputType...>& p);

    template<typename GeneratorType>
    enable_if_t<is_same_v<result_of_t<GeneratorType()>, trail_t*>, actor_t<native> const&>
      operator||(GeneratorType&& generator) const
    {
      trail_t *trail = generator();
      trail->link(streamSignature);
      if (!trail) return *this;
      if (!instance || !instance->isEnabled())
      {
        delete trail;
        return *this;
      }
      if (instance->maySchedule(trail, streamSignature)) instance->residence->scheduler.enqueue(instance);
      return *this;
    }

    runnable_t& operator*() const;
    runnable_t *operator->() const;
    template<typename MessageType>
    bool reply(MessageType contents, const message_t *original, actor_id_t const& senderID = actor_id_t()) const;
    void reset();
    bool returnToOrigin();
    template<typename MessageType>
    bool send(MessageType contents, actor_id_t const& senderID = actor_id_t()) const;
    void sign(actor_id_t const& streamSignature_);

    template<typename ContextType = typed_context_t<>, mailbox_type_t MType, typename... ArgTypes>
    static actor_t<native> spawn(props_t::with<MType> const& props, ArgTypes... args);

    template<typename ContextType = typed_context_t<>, mailbox_type_t MType, typename... ArgTypes>
    static vector<actor_t<native>> spawnGroup(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args);

    static throughput_t throttle(uint64_t low, uint64_t high, actor_t<native> const& handle);
  };

  inline actor_t<native>::actor_t(shared_ptr<runnable_t>&& instance_)
    : instance(move(instance_))
  {
  }

  inline actor_t<native>::actor_t()
    : instance()
  {
  }

  inline actor_t<native>::actor_t(actor_t<native> const& actor)
    : instance(actor.instance)
  {
  }

  inline actor_t<native>::actor_t(actor_t<native>&& actor)
    : instance(move(actor.instance))
  {
  }

  inline type_erased_receiver_t& actor_t<native>::defaultBehavior(bool reset)
  {
    rpp_assert(instance, "Cannot modify behavior of a non-existent actor.", tscout);

    // if context is enabled, directly changing an actor's behavior by an external entity (i.e. its parent) is forbidden
    // therefore, intercept this call and return an empty receiver (translates to a NOP)
    if (instance->isEnabled()) return type_erased_receiver_t::placeholder();

    context_t *ctx = (context_t*)(instance.get());
    return ctx->defaultBehavior(reset);
  }

  template<bool IsAtom, typename MessageType>
  inline size_t actor_t<native>::broadcast(MessageType contents, vector<actor_t<native>> const& receivers, actor_id_t const& senderID)
  {
    size_t nDeliveries = 0;
    vector<actor_t<native>>::const_iterator it = receivers.cbegin(), e = receivers.cend();
    while (it != e)
    {
      if (!IsAtom)
      {
        if (it->send(contents, senderID)) nDeliveries++;
      }
      else
      {
        auto& instance = it->instance;
        if (instance && instance->isEnabled())
        {
          typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
          if (instance->maySchedule(&message, it->streamSignature)) instance->residence->scheduler.enqueue(instance);
          nDeliveries++;
        }
      }
      it++;
    }
    return nDeliveries;
  }

  template<typename ContextType>
  inline context_wrapper_t<ContextType> actor_t<native>::enableContext() const
  {
    return context_wrapper_t<ContextType>{ getContext() };
  }

  template<typename ContextType, typename BehaviorType>
  inline void actor_t<native>::enableContexts(vector<actor_id_t> const& actorIDs)
  {
    for (size_t i = 0; i < actorIDs.size(); i++) map(actorIDs[i]).template enableContext<ContextType>().template withBehavior<BehaviorType>();
  }

  template<typename ContextType, typename BehaviorType>
  inline void actor_t<native>::enableContexts(vector<actor_t<native>> const& actors)
  {
    for (size_t i = 0; i < actors.size(); i++) actors[i].template enableContext<ContextType>().template withBehavior<BehaviorType>();
  }

  template<typename ContextType>
  inline void actor_t<native>::enableContextsWithDefault(vector<actor_id_t> const& actorIDs)
  {
    for (size_t i = 0; i < actorIDs.size(); i++) map(actorIDs[i]).template enableContext<ContextType>().withDefault();
  }

  template<typename ContextType>
  inline void actor_t<native>::enableContextsWithDefault(vector<actor_t<native>> const& actors)
  {
    for (size_t i = 0; i < actors.size(); i++) actors[i].template enableContext<ContextType>().withDefault();
  }

  inline context_t *actor_t<native>::getContext() const
  {
    if (!instance) return nullptr;
    context_t *ctx = dynamic_cast<context_t*>(instance.get());
    return ctx;
  }

  inline actor_id_t actor_t<native>::getID() const
  {
    if (instance) return instance->selfID;
    return actor_id_t();
  }

  inline worker_id_t actor_t<native>::getWorkerID() const
  {
    if (instance) return instance->workerID;
    return DEFAULT_WORKER_ID;
  }

  inline actor_t<native> actor_t<native>::map(actor_id_t const& actorID)
  {
    actor_t<native> actor;
    cluster_id_t clusterID = actorID.clusterID;
    if (clusterID == DEFAULT_CLUSTER_ID) return actor;
    rpp_assert(clusterID < sys.nClusters, "Actor ID referenced a non-existent cluster.", tscout);
    shared_ptr<runnable_t> instance;
    if (sys.clusters[clusterID].registry.get(actorID.localID, instance)) actor.instance = instance;
    return actor;
  }

  inline bool actor_t<native>::migrate(cluster_id_t clusterID)
  {
    rpp_assert(clusterID != DETACHED_CLUSTER_ID, "Cannot migrate an actor to the detached cluster.", tscout);
    rpp_assert(clusterID < sys.nClusters, "Cluster ID referenced a non-existent cluster.", tscout);
    cluster_t<NATIVE> *newResidence = &sys.clusters[clusterID];
    if (instance) return instance->migrate(newResidence);
    return false;
  }

  inline actor_t<native>::operator bool() const
  {
    return (bool)instance;
  }

  template<typename ContextType>
  inline ContextType *actor_t<native>::map(actor_t<native> const& actor)
  {
    runnable_t& instance = *actor;
    if (ContextType *typedInstance = dynamic_cast<ContextType*> (&instance)) return typedInstance;
    return nullptr;
  }

  inline actor_t<native>& actor_t<native>::operator=(actor_t<native> const& actor)
  {
    if (this == &actor) return *this;
    instance = actor.instance;
    return *this;
  }

  inline actor_t<native>& actor_t<native>::operator=(actor_t<native>&& actor)
  {
    if (this == &actor) return *this;
    instance = move(actor.instance);
    return *this;
  }

  template<typename MessageType>
  inline const actor_t<native>& actor_t<native>::operator|(MessageType contents) const
  {
    send(forward<MessageType>(contents), streamSignature);
    return *this;
  }

  template<typename MessageType>
  inline const actor_t<native>& actor_t<native>::operator<<(typed_si_message_t<MessageType> const& contents) const
  {
    if (!instance || !instance->isEnabled()) return *this;
    typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
    if (instance->maySchedule(&message, streamSignature)) instance->residence->scheduler.enqueue(instance);
    return *this;
  }

  template<typename OutputType, typename... InputType>
  inline future_t<OutputType> actor_t<native>::operator+(Promise<OutputType, InputType...>& p)
  {
    rpp_assert(instance && instance->isEnabled(), "A non-existent/disabled actor cannot make a promise.", tscout);
    future_t<OutputType> f = p.make();
    message_t *message = new typed_message_t<Promise<OutputType, InputType...>>(move(p));
    if (instance->maySchedule(message, streamSignature)) instance->residence->scheduler.enqueue(instance);
    return f;
  }

  inline runnable_t& actor_t<native>::operator*() const
  {
    return *instance;
  }

  inline runnable_t *actor_t<native>::operator->() const
  {
    return instance.get();
  }

  inline void actor_t<native>::reset()
  {
    instance.reset();
  }

  inline bool actor_t<native>::returnToOrigin()
  {
    if (instance) return instance->returnToOrigin();
    return false;
  }

  template<typename MessageType>
  inline bool actor_t<native>::reply(MessageType contents, const message_t *original, actor_id_t const& senderID) const
  {
    if (!instance || !instance->isEnabled()) return false;
    message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
    message->replyToken = original->replyToken;
    if (instance->maySchedule(message, senderID)) instance->residence->scheduler.enqueue(instance);
    return true;
  }

  template<typename MessageType>
  inline bool actor_t<native>::send(MessageType contents, actor_id_t const& senderID) const
  {
    if (!instance || !instance->isEnabled()) return false;
    message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
    if (instance->maySchedule(message, senderID)) instance->residence->scheduler.enqueue(instance);
    return true;
  }

  inline void actor_t<native>::sign(actor_id_t const& streamSignature_)
  {
    streamSignature = streamSignature_;
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline actor_t<native> actor_t<native>::spawn(props_t::with<MType> const& props, ArgTypes... args)
  {
    rpp_assert(props.location.clusterID < sys.nClusters, "Attempting to spawn actor on a non-existent cluster.", tscout);
    shared_ptr<runnable_t> handle;
    sys.clusters[props.location.clusterID].template spawn<ContextType, MType, true, ArgTypes...>(props.location.workerID, props.dispatchSize, props.senderAffinity, nullptr, &handle, forward<ArgTypes>(args)...);
    return actor_t<native>(move(handle));
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline vector<actor_t<native>> actor_t<native>::spawnGroup(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args)
  {
    vector<actor_t < native >> actors(nActors);
    for (local_id_t i = 0; i < nActors; i++) actors[i] = spawn<ContextType, MType, ArgTypes...>(props, forward<ArgTypes>(args)...);
    return actors;
  }

  inline actor_t<native>::throughput_t
    actor_t<native>::throttle(uint64_t low, uint64_t high, actor_t<native> const& handle)
  {
    return throughput_t(handle, make_pair(low, high));
  }

  template<>
  class actor_t<detached>
  {
  private:
    static system_t &sys;
    static printer_t& tscout;
    shared_ptr<executor_t> executor;
    actor_id_t streamSignature;
    actor_t(shared_ptr<executor_t>&& executor_);
  public:

    class throughput_t
    {
    private:
      actor_t<detached> const& handle;
      pair<uint64_t, uint64_t> watermarks;
      throughput_t(actor_t<detached> const& handle_) : handle(handle_) {}
      throughput_t(actor_t<detached> const& handle_, pair<uint64_t, uint64_t> const& watermarks_)
        : handle(handle_), watermarks(watermarks_) {}
    public:
      template<typename MessageType>
      const throughput_t& operator<<(typed_si_message_t<MessageType> const& contents) const
      {
        if (!handle.executor || !handle.executor->isEnabled() || !handle.executor->instance->template probe<context_t>(watermarks)) return *this;
        typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
        handle.executor->send(&message, handle.streamSignature);
        return *this;
      }

      template<typename MessageType>
      const throughput_t& operator|(MessageType contents) const
      {
        if (!handle.executor || !handle.executor->isEnabled() || !handle.executor->instance->template probe<context_t>(watermarks)) return *this;
        message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
        handle.executor->send(message, handle.streamSignature);
        return *this;
      }

      friend class actor_t<detached>;
    };

    actor_t();
    actor_t(actor_t<detached> const& actor);
    actor_t(actor_t<detached>&& actor);
    type_erased_receiver_t& defaultBehavior(bool reset = false);
    template<bool IsAtom = false, typename MessageType>
    static size_t broadcast(MessageType contents, vector<actor_t<detached>> const& receivers, actor_id_t const& senderID = actor_id_t());
    template<typename ContextType = typed_context_t<>>
    context_wrapper_t<ContextType> enableContext() const;

    template<typename ContextType, typename BehaviorType>
    static void enableContexts(vector<actor_id_t> const& actorIDs);

    template<typename ContextType, typename BehaviorType>
    static void enableContexts(vector<actor_t<detached>> const& actors);

    template<typename ContextType = typed_context_t<>>
    static void enableContextsWithDefault(vector<actor_id_t> const& actorIDs);

    template<typename ContextType = typed_context_t<>>
    static void enableContextsWithDefault(vector<actor_t<detached>> const& actors);

    context_t *getContext() const;
    actor_id_t getID() const;

    template<typename ContextType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<context_t, ContextType>> initialize(ArgTypes... args) const
    {
      if (!executor || executor->isEnabled()) return;
      applyFunction(&ContextType::initialize, (ContextType*)executor->instance, forward<ArgTypes>(args)...);
    }

    template<typename ContextType, typename... ArgTypes>
    inline static enable_if_t<is_base_of_v<context_t, ContextType>> initializeEach(vector<actor_t<detached>> const& actors, ArgTypes... args)
    {
      vector<actor_t<detached>>::const_iterator it = actors.cbegin(), e = actors.cend();
      while (it != e)
      {
        it->initialize<ContextType, ArgTypes...>(args...);
        it++;
      }
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initialize(ArgTypes... args) const
    {
      if (!executor || executor->isEnabled()) return;
      context_t *ctx = (context_t*)executor->instance;
      ctx->initializeBehavior<BehaviorType>(forward<ArgTypes>(args)...);
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline static enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initializeEach(vector<actor_t<detached>> const& actors, ArgTypes... args)
    {
      vector<actor_t<detached>>::const_iterator it = actors.cbegin(), e = actors.cend();
      while (it != e)
      {
        it->initialize<BehaviorType, ArgTypes...>(args...);
        it++;
      }
    }

    template<typename ContextType>
    static ContextType *map(actor_t<detached> const& actor);
    static actor_t<detached> map(actor_id_t const& actorID);
    operator bool() const;
    actor_t<detached>& operator=(actor_t<detached> const& actor);
    actor_t<detached>& operator=(actor_t<detached>&& actor);
    template<typename MessageType>
    const actor_t<detached>& operator|(MessageType contents) const;
    template<typename MessageType>
    const actor_t<detached>& operator<<(typed_si_message_t<MessageType> const& contents) const;
    template<typename OutputType, typename... InputType>
    future_t<OutputType> operator+(Promise<OutputType, InputType...>& p);

    template<typename GeneratorType>
    enable_if_t<is_same_v<result_of_t<GeneratorType()>, trail_t*>, actor_t<detached> const&>
      operator||(GeneratorType&& generator) const
    {
      trail_t *trail = generator();
      trail->link(streamSignature);
      if (!trail) return *this;
      if (!executor || !executor->isEnabled())
      {
        delete trail;
        return *this;
      }

      executor->send(trail, streamSignature);
      return *this;
    }

    runnable_t& operator*() const;
    runnable_t *operator->() const;
    bool poll() const;
    bool process(bool block = false, uint64_t maxNumberOfMessages = 1, uint64_t *nProcessed = nullptr) const;
    void reset();
    template<typename MessageType>
    bool reply(MessageType contents, const message_t *original, actor_id_t const& senderID = actor_id_t()) const;
    template<typename MessageType>
    bool send(MessageType contents, actor_id_t const& senderID = actor_id_t()) const;
    void sign(actor_id_t const& streamSignature_);

    template<typename ContextType = typed_context_t<>, mailbox_type_t MType, typename... ArgTypes>
    static actor_t<detached> spawn(props_t::with<MType> const& props, ArgTypes... args);

    template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
    static vector<actor_t<detached>> spawnGroup(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args);

    static throughput_t throttle(uint64_t low, uint64_t high, actor_t<detached> const& handle);
  };

  inline actor_t<detached>::actor_t(shared_ptr<executor_t>&& executor_)
    : executor(move(executor_))
  {
  }

  inline actor_t<detached>::actor_t()
    : executor()
  {
  }

  inline actor_t<detached>::actor_t(actor_t<detached> const& actor)
    : executor(actor.executor)
  {
  }

  inline actor_t<detached>::actor_t(actor_t<detached>&& actor)
    : executor(move(actor.executor))
  {
  }

  inline type_erased_receiver_t& actor_t<detached>::defaultBehavior(bool reset)
  {
    rpp_assert(executor, "Cannot modify behavior of a non-existent actor.", tscout);
    if (executor->isEnabled()) return type_erased_receiver_t::placeholder();

    context_t *ctx = (context_t*)(executor->instance);
    return ctx->defaultBehavior(reset);
  }

  template<bool IsAtom, typename MessageType>
  inline size_t actor_t<detached>::broadcast(MessageType contents, vector<actor_t<detached>> const& receivers, actor_id_t const& senderID)
  {
    size_t nDeliveries = 0;
    vector<actor_t<detached>>::const_iterator it = receivers.cbegin(), e = receivers.cend();
    while (it != e)
    {
      if (!IsAtom)
      {
        if (it->send(contents, senderID)) nDeliveries++;
      }
      else
      {
        auto& executor = it->executor;
        if (executor && executor->isEnabled())
        {
          typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
          executor->send(&message, it->streamSignature);
          nDeliveries++;
        }
      }
      it++;
    }
    return nDeliveries;
  }

  template<typename ContextType>
  inline context_wrapper_t<ContextType> actor_t<detached>::enableContext() const
  {
    if (executor) return context_wrapper_t<ContextType>{ executor->getContext() };
    return context_wrapper_t<ContextType>{ nullptr };
  }

  template<typename ContextType, typename BehaviorType>
  inline void actor_t<detached>::enableContexts(vector<actor_id_t> const& actorIDs)
  {
    for (size_t i = 0; i < actorIDs.size(); i++) map(actorIDs[i]).template enableContext<ContextType>().template withBehavior<BehaviorType>();
  }

  template<typename ContextType, typename BehaviorType>
  inline void actor_t<detached>::enableContexts(vector<actor_t<detached>> const& actors)
  {
    for (size_t i = 0; i < actors.size(); i++) actors[i].template enableContext<ContextType>().template withBehavior<BehaviorType>();
  }

  template<typename ContextType>
  inline void actor_t<detached>::enableContextsWithDefault(vector<actor_id_t> const& actorIDs)
  {
    for (size_t i = 0; i < actorIDs.size(); i++) map(actorIDs[i]).template enableContext<ContextType>().withDefault();
  }

  template<typename ContextType>
  inline void actor_t<detached>::enableContextsWithDefault(vector<actor_t<detached>> const& actors)
  {
    for (size_t i = 0; i < actors.size(); i++) actors[i].template enableContext<ContextType>().withDefault();
  }

  inline context_t *actor_t<detached>::getContext() const
  {
    if (!executor) return nullptr;
    return executor->getContext();
  }

  inline actor_id_t actor_t<detached>::getID() const
  {
    if (executor) return executor->instance->selfID;
    return actor_id_t();
  }

  inline actor_t<detached> actor_t<detached>::map(actor_id_t const& actorID)
  {
    cluster_id_t clusterID = actorID.clusterID;
    rpp_assert(clusterID == DETACHED_CLUSTER_ID, "Actor ID referenced a non-existent cluster.", tscout);
    actor_t<detached> actor;
    shared_ptr<executor_t> executor;
    if (sys.detachedCluster.registry.get(actorID.localID, executor)) actor.executor = executor;
    return actor;
  }

  template<typename ContextType>
  inline ContextType *actor_t<detached>::map(actor_t<detached> const& actor)
  {
    runnable_t& instance = *actor;
    if (ContextType * typedInstance = dynamic_cast<ContextType*> (&instance)) return typedInstance;
    return nullptr;
  }

  inline actor_t<detached>::operator bool() const
  {
    return (bool)executor;
  }

  inline actor_t<detached>& actor_t<detached>::operator=(actor_t<detached> const& actor)
  {
    if (this == &actor) return *this;
    executor = actor.executor;
    return *this;
  }

  inline actor_t<detached>& actor_t<detached>::operator=(actor_t<detached>&& actor)
  {
    if (this == &actor) return *this;
    executor = move(actor.executor);
    return *this;
  }

  template<typename MessageType>
  inline const actor_t<detached>& actor_t<detached>::operator|(MessageType contents) const
  {
    send(forward<MessageType>(contents), streamSignature);
    return *this;
  }

  template<typename MessageType>
  inline const actor_t<detached>& actor_t<detached>::operator<<(typed_si_message_t<MessageType> const& contents) const
  {
    if (!executor || !executor->isEnabled()) return *this;
    typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
    executor->send(&message, streamSignature);
    return *this;
  }

  template<typename OutputType, typename... InputType>
  inline future_t<OutputType> actor_t<detached>::operator+(Promise<OutputType, InputType...>& p)
  {
    rpp_assert(executor && executor->isEnabled(), "A non-existent/disabled actor cannot make a promise.", tscout);
    future_t<OutputType> f = p.make();
    message_t *message = new typed_message_t<Promise<OutputType, InputType...>>(move(p));
    executor->send(message, streamSignature);
    return f;
  }

  inline runnable_t& actor_t<detached>::operator*() const
  {
    return *(executor->instance);
  }

  inline runnable_t *actor_t<detached>::operator->() const
  {
    return executor->instance;
  }

  inline void actor_t<detached>::reset()
  {
    executor.reset();
  }

  template<typename MessageType>
  inline bool actor_t<detached>::reply(MessageType contents, const message_t *original, actor_id_t const& senderID) const
  {
    if (!executor || !executor->isEnabled()) return false;
    message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
    message->replyToken = original->replyToken;
    executor->send(message, senderID);
    return true;
  }

  template<typename MessageType>
  inline bool actor_t<detached>::send(MessageType contents, actor_id_t const& senderID) const
  {
    if (!executor || !executor->isEnabled()) return false;
    message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
    executor->send(message, senderID);
    return true;
  }

  inline void actor_t<detached>::sign(actor_id_t const& streamSignature_)
  {
    streamSignature = streamSignature_;
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline actor_t<detached> actor_t<detached>::spawn(props_t::with<MType> const& props, ArgTypes... args)
  {
    shared_ptr<executor_t> handle;
    sys.detachedCluster.template spawn<ContextType, MType, true, ArgTypes...>(!props.isInert, props.dispatchSize, props.senderAffinity, nullptr, &handle, forward<ArgTypes>(args)...);
    return actor_t<detached>(move(handle));
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline vector<actor_t<detached>> actor_t<detached>::spawnGroup(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args)
  {
    vector<actor_t < detached >> actors(nActors);
    for (local_id_t i = 0; i < nActors; i++) actors[i] = spawn<ContextType, MType, ArgTypes...>(props, forward<ArgTypes>(args)...);
    return actors;
  }

  inline actor_t<detached>::throughput_t
    actor_t<detached>::throttle(uint64_t low, uint64_t high, actor_t<detached> const& handle)
  {
    return throughput_t(handle, make_pair(low, high));
  }

  template<>
  class actor_t<channel>
  {
  private:
    static system_t &sys;
    static printer_t& tscout;
    shared_ptr<executor_t> executor;
    channel_id_t streamSignature;
    actor_t(shared_ptr<executor_t>&& executor_);
    static void drainEach() {}
    static void processEach(uint64_t maxNumberOfMessages) {}
  public:

    class throughput_t
    {
    private:
      actor_t<channel> const& handle;
      pair<uint64_t, uint64_t> watermarks;
      throughput_t(actor_t<channel> const& handle_) : handle(handle_) {}
      throughput_t(actor_t<channel> const& handle_, pair<uint64_t, uint64_t> const& watermarks_)
        : handle(handle_), watermarks(watermarks_) {}
    public:
      template<typename MessageType>
      const throughput_t& operator<<(typed_si_message_t<MessageType> const& contents) const
      {
        if (!handle.executor || !handle.executor->isEnabled() || !handle.executor->instance->template probe<context_t>(watermarks)) return *this;
        typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
        handle.executor->send(&message, handle.streamSignature);
        return *this;
      }

      template<typename MessageType>
      const throughput_t& operator|(MessageType contents) const
      {
        if (!handle.executor || !handle.executor->isEnabled() || !handle.executor->instance->template probe<context_t>(watermarks)) return *this;
        message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
        handle.executor->send(message, handle.streamSignature);
        return *this;
      }

      friend class actor_t<channel>;
    };

    actor_t();
    actor_t(actor_t<channel> const& channel);
    actor_t(actor_t<channel>&& channel);
    type_erased_receiver_t& defaultBehavior(bool reset = false);
    template<bool IsAtom = false, typename MessageType>
    static size_t broadcast(MessageType contents, vector<actor_t<channel>> const& receivers, actor_id_t const& senderID = actor_id_t());
    void drain() const;

    template<typename First, typename... Rest>
    static enable_if_t<is_same_v<First, actor_t<channel>>> drainEach(First first, Rest... rest)
    {
      first.drain();
      drainEach(forward<Rest>(rest)...);
    }

    template<typename ContextType = typed_context_t<>>
    context_wrapper_t<ContextType> enableContext() const;

    template<typename ContextType, typename BehaviorType>
    static void enableContexts(vector<channel_id_t> const& channelIDs);

    template<typename ContextType, typename BehaviorType>
    static void enableContexts(vector<actor_t<channel>> const& channels);

    template<typename ContextType = typed_context_t<>>
    static void enableContextsWithDefault(vector<channel_id_t> const& channelIDs);

    template<typename ContextType = typed_context_t<>>
    static void enableContextsWithDefault(vector<actor_t<channel>> const& channels);

    context_t *getContext() const;
    channel_id_t getID() const;

    template<typename ContextType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<context_t, ContextType>> initialize(ArgTypes... args) const
    {
      if (!executor || executor->isEnabled()) return;
      applyFunction(&ContextType::initialize, (ContextType*)executor->instance, forward<ArgTypes>(args)...);
    }

    template<typename ContextType, typename... ArgTypes>
    inline static enable_if_t<is_base_of_v<context_t, ContextType>> initializeEach(vector<actor_t<channel>> const& channels, ArgTypes... args)
    {
      vector<actor_t<channel>>::const_iterator it = channels.cbegin(), e = channels.cend();
      while (it != e)
      {
        it->initialize<ContextType, ArgTypes...>(args...);
        it++;
      }
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initialize(ArgTypes... args) const
    {
      if (!executor || executor->isEnabled()) return;
      context_t *ctx = (context_t*)executor->instance;
      ctx->initializeBehavior<BehaviorType>(forward<ArgTypes>(args)...);
    }

    template<typename BehaviorType, typename... ArgTypes>
    inline static enable_if_t<is_base_of_v<behavior_t, BehaviorType>> initializeEach(vector<actor_t<channel>> const& channels, ArgTypes... args)
    {
      vector<actor_t<channel>>::const_iterator it = channels.cbegin(), e = channels.cend();
      while (it != e)
      {
        it->initialize<BehaviorType, ArgTypes...>(args...);
        it++;
      }
    }

    template<typename ContextType>
    static ContextType *map(actor_t<channel> const& channel);
    static actor_t<channel> map(channel_id_t const& channelID);

    template<typename ContextType = typed_context_t<>, mailbox_type_t MType, typename... ArgTypes>
    static actor_t<channel> open(props_t::with<MType> const& props, ArgTypes... args);

    template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
    static vector<actor_t<channel>> openAll(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args);

    operator bool() const;
    actor_t<channel>& operator=(actor_t<channel> const& channel);
    actor_t<channel>& operator=(actor_t<channel>&& channel);
    template<typename MessageType>
    const actor_t<channel>& operator|(MessageType contents) const;
    template<typename MessageType>
    const actor_t<channel>& operator<<(typed_si_message_t<MessageType> const& contents) const;
    template<typename OutputType, typename... InputType>
    future_t<OutputType> operator+(Promise<OutputType, InputType...>& p);

    template<typename GeneratorType>
    enable_if_t<is_same_v<result_of_t<GeneratorType()>, trail_t*>, actor_t<channel> const&>
      operator||(GeneratorType&& generator) const
    {
      trail_t *trail = generator();
      trail->link(streamSignature);
      if (!trail) return *this;
      if (!executor || !executor->isEnabled())
      {
        delete trail;
        return *this;
      }

      executor->send(trail, streamSignature);
      return *this;
    }

    runnable_t& operator*() const;
    runnable_t *operator->() const;
    bool poll() const;
    bool process(bool block = false, uint64_t maxNumberOfMessages = 1, uint64_t *nProcessed = nullptr) const;

    template<typename First, typename... Rest>
    static void processEach(uint64_t maxNumberOfMessages, First first, Rest... rest)
    {
      if (first.poll()) first.process(false, maxNumberOfMessages);
      processEach(maxNumberOfMessages, forward<Rest>(rest)...);
    }

    void reset();
    template<typename MessageType>
    bool reply(MessageType contents, const message_t *original, actor_id_t const& senderID = actor_id_t()) const;
    template<typename MessageType>
    bool send(MessageType contents, actor_id_t const& senderID = actor_id_t()) const;
    void sign(actor_id_t const& streamSignature_);

    static throughput_t throttle(uint64_t low, uint64_t high, actor_t<channel> const& handle);
  };

  inline actor_t<channel>::actor_t(shared_ptr<executor_t>&& executor_)
    : executor(move(executor_))
  {
  }

  inline actor_t<channel>::actor_t()
    : executor()
  {
  }

  inline actor_t<channel>::actor_t(actor_t<channel> const& channel)
    : executor(channel.executor)
  {
  }

  inline actor_t<channel>::actor_t(actor_t<channel>&& channel)
    : executor(move(channel.executor))
  {
  }

  template<bool IsAtom, typename MessageType>
  inline size_t actor_t<channel>::broadcast(MessageType contents, vector<actor_t<channel>> const& receivers, actor_id_t const& senderID)
  {
    size_t nDeliveries = 0;
    vector<actor_t<channel>>::const_iterator it = receivers.cbegin(), e = receivers.cend();
    while (it != e)
    {
      if (!IsAtom)
      {
        if (it->send(contents, senderID)) nDeliveries++;
      }
      else
      {
        auto& executor = it->executor;
        if (executor && executor->isEnabled())
        {
          typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
          executor->send(&message, it->streamSignature);
          nDeliveries++;
        }
      }
      it++;
    }
    return nDeliveries;
  }

  inline type_erased_receiver_t& actor_t<channel>::defaultBehavior(bool reset)
  {
    rpp_assert(executor, "Cannot modify behavior of a non-existent channel.", tscout);
    if (executor->isEnabled()) return type_erased_receiver_t::placeholder();

    context_t *ctx = (context_t*)(executor->instance);
    return ctx->defaultBehavior(reset);
  }

  inline void actor_t<channel>::drain() const
  {
    if (!executor) return;
    context_t *ctx = getContext();
    ctx->park();

    DECLARE_ATOM_TYPE(Delete) term;
    *this << term;
    while (poll()) process(false, UINT64_MAX);
  }

  template<typename ContextType>
  inline context_wrapper_t<ContextType> actor_t<channel>::enableContext() const
  {
    if (executor) return context_wrapper_t<ContextType>{ executor->getContext() };
    return context_wrapper_t<ContextType>{ nullptr };
  }

  template<typename ContextType, typename BehaviorType>
  inline void actor_t<channel>::enableContexts(vector<channel_id_t> const& actorIDs)
  {
    for (size_t i = 0; i < actorIDs.size(); i++) map(actorIDs[i]).template enableContext<ContextType>().template withBehavior<BehaviorType>();
  }

  template<typename ContextType, typename BehaviorType>
  inline void actor_t<channel>::enableContexts(vector<actor_t<channel>> const& actors)
  {
    for (size_t i = 0; i < actors.size(); i++) actors[i].template enableContext<ContextType>().template withBehavior<BehaviorType>();
  }

  template<typename ContextType>
  inline void actor_t<channel>::enableContextsWithDefault(vector<channel_id_t> const& actorIDs)
  {
    for (size_t i = 0; i < actorIDs.size(); i++) map(actorIDs[i]).template enableContext<ContextType>().withDefault();
  }

  template<typename ContextType>
  inline void actor_t<channel>::enableContextsWithDefault(vector<actor_t<channel>> const& actors)
  {
    for (size_t i = 0; i < actors.size(); i++) actors[i].template enableContext<ContextType>().withDefault();
  }

  inline context_t *actor_t<channel>::getContext() const
  {
    if (!executor) return nullptr;
    return executor->getContext();
  }

  inline channel_id_t actor_t<channel>::getID() const
  {
    if (executor) return executor->instance->selfID;
    return channel_id_t();
  }

  inline actor_t<channel> actor_t<channel>::map(channel_id_t const& actorID)
  {
    cluster_id_t clusterID = actorID.clusterID;
    rpp_assert(clusterID == DETACHED_CLUSTER_ID, "Actor ID referenced a non-existent cluster.", tscout);
    actor_t<channel> channel;
    shared_ptr<executor_t> executor;
    if (sys.detachedCluster.registry.get(actorID.localID, executor)) channel.executor = executor;
    return channel;
  }

  template<typename ContextType>
  inline ContextType *actor_t<channel>::map(actor_t<channel> const& channel)
  {
    runnable_t& instance = *channel;
    if (ContextType * typedInstance = dynamic_cast<ContextType*> (&instance)) return typedInstance;
    return nullptr;
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline actor_t<channel> actor_t<channel>::open(props_t::with<MType> const& props, ArgTypes... args)
  {
    shared_ptr<executor_t> handle;
    sys.detachedCluster.template spawn<ContextType, MType, true, ArgTypes...>(false, props.dispatchSize, props.senderAffinity, nullptr, &handle, forward<ArgTypes>(args)...);
    return actor_t<channel>(move(handle));
  }

  template<typename ContextType, mailbox_type_t MType, typename... ArgTypes>
  inline vector<actor_t<channel>> actor_t<channel>::openAll(props_t::with<MType> const& props, local_id_t nActors, ArgTypes... args)
  {
    vector<actor_t<channel>> actors(nActors);
    for (local_id_t i = 0; i < nActors; i++) actors[i] = actor_t<channel>::open<ContextType, MType, ArgTypes...>(props, forward<ArgTypes>(args)...);
    return actors;
  }

  inline actor_t<channel>::operator bool() const
  {
    return (bool)executor;
  }

  inline actor_t<channel>& actor_t<channel>::operator=(actor_t<channel> const& channel)
  {
    if (this == &channel) return *this;
    executor = channel.executor;
    return *this;
  }

  inline actor_t<channel>& actor_t<channel>::operator=(actor_t<channel>&& channel)
  {
    if (this == &channel) return *this;
    executor = move(channel.executor);
    return *this;
  }

  template<typename MessageType>
  inline const actor_t<channel>& actor_t<channel>::operator|(MessageType contents) const
  {
    send(forward<MessageType>(contents), streamSignature);
    return *this;
  }

  template<typename MessageType>
  inline const actor_t<channel>& actor_t<channel>::operator<<(typed_si_message_t<MessageType> const& contents) const
  {
    if (!executor || !executor->isEnabled()) return *this;
    typed_si_message_t<MessageType>& message = GLOBAL(typed_si_message_t<MessageType>);
    executor->send(&message, streamSignature);
    return *this;
  }

  template<typename OutputType, typename... InputType>
  inline future_t<OutputType> actor_t<channel>::operator+(Promise<OutputType, InputType...>& p)
  {
    rpp_assert(executor && executor->isEnabled(), "A non-existent/disabled channel cannot make a promise.", tscout);
    future_t<OutputType> f = p.make();
    message_t *message = new typed_message_t<Promise<OutputType, InputType...>>(move(p));
    executor->send(message, streamSignature);
    return f;
  }

  inline runnable_t& actor_t<channel>::operator*() const
  {
    return *(executor->instance);
  }

  inline runnable_t *actor_t<channel>::operator->() const
  {
    return executor->instance;
  }

  inline bool actor_t<channel>::poll() const
  {
    if (!executor) return false;
    rpp_assert(!executor->useInternalThread, "Channels must not have their own thread of execution.", tscout);
    if (!executor->instance->isAlive) return false;
    return executor->instance->mayReschedule(0) != 0ULL;
  }

  inline bool actor_t<channel>::process(bool block, uint64_t maxNumberOfMessages, uint64_t *nProcessed) const
  {
    if (!executor) return false;
    rpp_assert(!executor->useInternalThread, "Channels must not have their own thread of execution.", tscout);
    if (!executor->instance->isAlive) return false;
    uint64_t ret = executor->run<false>(block, maxNumberOfMessages);
    if (nProcessed) *nProcessed = ret;
    return true;
  }

  inline void actor_t<channel>::reset()
  {
    executor.reset();
  }

  template<typename MessageType>
  inline bool actor_t<channel>::reply(MessageType contents, const message_t *original, actor_id_t const& senderID) const
  {
    if (!executor || !executor->isEnabled()) return false;
    message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
    message->replyToken = original->replyToken;
    executor->send(message, senderID);
    return true;
  }

  template<typename MessageType>
  inline bool actor_t<channel>::send(MessageType contents, actor_id_t const& senderID) const
  {
    if (!executor || !executor->isEnabled()) return false;
    message_t *message = new typed_message_t<MessageType>(forward<MessageType>(contents));
    executor->send(message, senderID);
    return true;
  }

  inline void actor_t<channel>::sign(actor_id_t const& streamSignature_)
  {
    streamSignature = streamSignature_;
  }

  inline actor_t<channel>::throughput_t
    actor_t<channel>::throttle(uint64_t low, uint64_t high, actor_t<channel> const& handle)
  {
    return throughput_t(handle, make_pair(low, high));
  }

  typedef actor_t<channel> channel_t;
}
