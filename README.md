React++
-------

A lightweight C++ actor library. To build, run make in the base directory (requires C++17).

> cd ReactPlusPlus   
make all  

Benchmarks are placed in tests/src directory. To run the benchmarks, adjust the following parameters in scripts/collector.sh.

> N\_SAMPLES=5  
MIN\_CORE=2  
MAX\_CORE=32  

And then invoke the launcher with sudo, which is required by perf.

> sudo ./launch.sh  

The above configuration runs the collector script for each benchmark 5 times with core count set to 1, 2, 4, 6, ..., 32. All data is accumulated in the collector's output directory with the following naming convention.

>collector\_base/benchmark\_name/framework\_name/output.txt  

The python script *plot.py* plots running time and CPU utilization. Requires the following packages: *numpy* and *matplotlib*. Most of the benchmarks are taken from the Savina benchmark suite. The sample data were collected on Intel Xeon E5-4610 v2 @2.30GHz 32-core processor without using hyperthreading. A summary is included here.

Producer-Consumer (N, M, X)
---------------------------
N producers each send X messages to each of M consumers. Total number of messages = N \* M * X.

Latency (N) / Ping-Pong (N)
---------------------------
An actor decrements a token from initial value N and sends it to its partner. The partner does the same and sends it back. The benchmark terminates when the token reaches 0. Total number of messages = N.

Counting-Actor (N)
------------------
An actor sends N messages to a receiver, which increments a private counter upon receiving each message. Once the sender is done, it asks the receiver the value of the counter. Total number of messages = N + 1.

Fork-Join Throughput (N, M)
---------------------------
N actors are spawned and each actor is sent M messages. Total number of messages = NM. Messages are sent in round-robin fashion, i.e. for N = 2 and M = 2, the sequence is

actor 1 &#8592; message 1  
actor 2 &#8592; message 1  
actor 1 &#8592; message 2  
actor 2 &#8592; message 2  
  

Fork-Join Spawn (N)
-------------------
N actors are spawned and each is sent a message. As soon as an actor receives a message, it terminates. Therefore it is not a requirement to have N actors resident in memory at the same time. Total number of messages = N.

Big (N, M)
----------
N actors are spawned and each knows the addresses of all the other actors. Each actor randomly samples an address from the pool (which may be its own address) and sends a ping. The receiver of the ping responds with a pong. After each actor sends N pings, it notifies a sink actor which waits until it receives such notifications from all N actors. Then the sink terminates N actors by sending a delete message to each.

Total message count = N \* M pings + N * M pongs + N deletes = 2 \* N \* (M + 1).

ThreadRing (N, M)
-----------------
This is a general version of the latency benchmark above. A ring of N actors are created. An initial token M is passed around this ring, which involves each actor decrementing the token and passing it to the next actor in the ring. When the value reaches 0, the receiving actor generates a delete message which is forwarded around the ring in a similar manner, terminating each actor. Total number of messages = M + N.

Quicksort (N, M)
----------------
This benchmark is a parallel randomized quicksort on a linked list of integers of size N using a tree of actors. M is the concurrent threshold. Each actor partitions the list based on a randomized pivot, spawns two children and sends the partitions to the new actors as messages. After both children have replied, the sorted partitions are merged along with the pivot by the parent.

The threshold indicates the maximum number of integers that should be sorted without increasing the depth of the tree. For example, if the threshold is set to 16, any partition that has less than or equal to 16 items is sorted immediately by the receiver without spawning further actors.

Lists are *moved* through messages to avoid unnecessary copying. Total number of messages = 2 \* N - 2. Number of a ctors spawned = 1 + 2 + 4 + 8 + ... + N when M is set to 1.

Quicksort2 (N, M)
-----------------
This is an array-based parallel quicksort which each actor takes an array of size N, creates two partitions and leaves the second partition to a newly spawned actor, which does the same recursively. The actors thus form a tree with each node representing an actor sorting a partition, not necessarily of the same size. The parameter M controls the maximum height of the tree.

Laplace Expansion (N, M)
------------------------
Actors compute the determinant of an N x N matrix by LaPlace expansion, where the determinant can be obtained by summing up the determinants of (N - 1) x (N - 1) submatrices. There are N such submatrices for an N x N matrix. Note that each (N - 1) x (N - 1) matrix can be broken into N - 1 smaller submatrices of size (N - 2) x (N - 2) and so on. Each actor receiving an N x N matrix will spawn N children that compute the determinants of smaller (N - 1) x (N - 1) submatrices and the results are sent back to their respective parent actors. Once all the replies arrive, a parent actor applies signs to the returned determinants and produces cofactors before adding them up. M controls the maximum dimension of the matrix that an actor is allowed to delegate to its child. For example, if N = 12 and M = 8, then the original 12 x 12 is broken into 11 x 11, 10 x 10 ... until 8 x 8 submatrices are produced. An actor receiving an 8 x 8 submatrix does not spawn new actors but rather computes the determinant itself synchronously.
